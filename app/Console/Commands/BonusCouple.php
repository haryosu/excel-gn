<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\BonusCouple as ModelsBonusCouple;
use App\Models\BonusCoupleRank;
use App\Models\BonusExceed;
use App\Models\BonusLog;
use App\Models\BvUserLog;
use App\Models\Cron;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BonusCouple extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'command:name';
    protected $signature = 'excel:couple-bonus';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'bonus couple & exceed here';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    { 
   
            // Get the users who registered today and sum their BV
            $getCronData = Cron::where('type','1')
            ->where('status', 0) 
            // ->where('created_at', '>=', Carbon::today()->toDateString())
            ->groupBy(['user_id', 'leg'])
            ->orderBy('id','asc')
            ->get(); 
            // $getCronData = User::orderBy('id','asc')->get();
            echo "run";
            echo $getCronData;
            // echo $startOfDay;
            // echo $endOfDay;
            
        foreach ($getCronData as $gc) {
                Log::info($gc);
                Log::info('---');

                $user1 = BvUserLog::where('user_id',$gc->user_id)
                ->where('leg',$gc->leg)
                ->where('position',$gc->position)
                ->first();
                // echo $user1;
                $user2 = BvUserLog::where('user_id',$gc->user_id)->where('leg',$gc->leg)->where('position', '!=', $gc->position)->first();
                
                $refs = User::where('id',$gc->user_id)->first();
                // echo $refs;
                Log::info($user2);
                Log::info($user1);
                
                if($user2){
                    $updateCron = Cron::where('type','1')->where('status', 0)
                    ->where('leg', $gc->leg) 
                    ->where('user_id',$gc->user_id)
                    ->where('position',$gc->position)
                    ->update([
                        'status' => '1', 
                    ]);
                    $updateCron2 = Cron::where('type','1')->where('status', 0)
                    ->where('leg', $gc->leg) 
                    ->where('user_id',$gc->user_id)
                    ->where('position',$user2->position)
                    ->update([
                        'status' => '1', 
                    ]);
                    // Log::info($gc.'<br>');
                    if($refs->pin_id != 1){
                        if ($user2){
                            echo('do');
                            if($user1->bv_log > $user2->bv_log){
                                $val= $user2->bv_log;
                            }elseif($user1->bv_log < $user2->bv_log){
                                $val= $user1->bv_log;
                            }else{
                                $val= $user1->bv_log;
                            }
                            // 5% x ( harga bv (1jt) x value pairing)
                            $totalPairing = 1000000*$val*(5/100); 
                            
                            if($val !=0){
                                Log::info('--'.$user1->user_id.'__'.$user2->user_id);
                                Log::info('--'.$user1->leg.'__'.$user2->leg);
                                Log::info('--'.$user1->position.'__'.$user2->position);
                                Log::info('--'.$user1->bv_log.'__'.$user2->bv_log);
                                Log::info('--'.$totalPairing);
                                Log::info('--'.$val);
                                Log::info('--');
                                Log::info('paired');
                                $i1 = $user1->position;
                                $i2 = $user2->position;
                                $bonusCouple = new ModelsBonusCouple(); 
                                $bonusCouple->user_id =  $user1->user_id;
                                $bonusCouple->bonus_val = $totalPairing;
                                $bonusCouple->bonus_net = $totalPairing*(80/100);
                                $bonusCouple->bonus_autosave = $totalPairing*(20/100); 
                                $bonusCouple->desc = 'Bonus Couple Leg'.$gc->leg .'-Pos'.$i1 .'-Pos'.$i2;
                                $bonusCouple->is_wd = 0; 
                                $bonusCouple->created_at = $gc->created_at; 
                                $bonusCouple->updated_at = $gc->created_at; 
                                $bonusCouple->save(); 
 
                                // echo $bonusCouple;
                                $bonusLog = new BonusLog();
                                $bonusLog->user_id =  $gc->user_id; 
                                $bonusLog->bonus_type =  2;
                                $bonusLog->bonus_plan =  1;
                                $bonusLog->bonus_val = $totalPairing;
                                $bonusLog->bonus_net = $totalPairing*(80/100);
                                $bonusLog->bonus_autosave = $totalPairing*(20/100);
                                $bonusLog->desc = 'Bonus Couple Leg'.$gc->leg .'-Pos'.$i1 .'-Pos'.$i2;
                                $bonusLog->is_wd = 0;
                                
                                $bonusLog->created_at = $gc->created_at; 
                                $bonusLog->updated_at = $gc->created_at; 

                                $bonusLog->save();  

                                $autosavelog = AutosaveLog::where('user_id',$gc->user_id)->first();
                                if($autosavelog){
                                $autosavelog->value += $bonusLog->bonus_autosave;
                                $autosavelog->save();
                                }else
                                { 
                                    $save = new AutosaveLog();
                                    $save->user_id = $gc->user_id;
                                    $save->value =  $bonusLog->bonus_autosave;
                                    $save->save();
                                } 
                                $refBV = User::where('id',$gc->user_id)->with('pin')->first();
                                    if ($val > $refBV->pin->price){ 
                                        $bonusexceed = (10/100)*$refBV->pin->price;
                                        $bonusCoupleexceed = new BonusExceed(); 
                                        $bonusCoupleexceed->user_id =  $gc->user_id;
                                        $bonusCoupleexceed->bonus_val = $bonusexceed;
                                        $bonusCoupleexceed->bonus_net = $bonusexceed*(80/100);
                                        $bonusCoupleexceed->bonus_autosave = $bonusexceed*(20/100); 
                                        $bonusCoupleexceed->desc = 'Bonus Exceed dari Leg'. $gc->leg .' - '.$gc->leg .'-Pos'.$i1 .'-Pos'.$i2;
                                        $bonusCoupleexceed->is_wd = 0; 
                                        $bonusCoupleexceed->created_at = $updateCron->created_at; 

                                        $bonusCoupleexceed->save();  

                                        $bonusLogexceed = new BonusLog();
                                        $bonusLogexceed->user_id =  $gc->user_id;
                                                                            
                                        $bonusLog->bonus_type =  3;
                                        $bonusLog->bonus_plan =  1;
                                        $bonusLogexceed->bonus_val = $totalPairing;
                                        $bonusLogexceed->bonus_net = $totalPairing*(80/100);
                                        $bonusLogexceed->bonus_autosave = $totalPairing*(20/100);
                                        $bonusLogexceed->desc = 'Bonus Exceed dari Leg'.$gc->leg .' - '.$gc->leg .'-Pos'.$i1 .'-Pos'.$i2;
                                        $bonusLogexceed->is_wd = 0;
                                        $bonusLogexceed->created_at = $updateCron->created_at;
                                        $bonusLogexceed->save(); 

                                        $autosavelog = AutosaveLog::where('user_id',$gc->user_id)->first();
                                        if($autosavelog){
                                        $autosavelog->value += $bonusCoupleexceed->bonus_autosave;
                                        $autosavelog->save();
                                        }else
                                        { 
                                            $save = new AutosaveLog();
                                            $save->user_id = $gc->user_id;
                                            $save->value =  $bonusLogexceed->bonus_autosave;
                                            $save->save();
                                        }
                                    }
            
            
                               
                                $ids= $user1->update([
                                    'bv_log' => $user1->bv_log-$val, 
                                ]);
                                $ids2= $user2->update([
                                    'bv_log' => $user2->bv_log-$val, 
                                ]);  
                            }
                        } 
                    }
                    
                }
 
                $updateCron4 = Cron::where('type','1')->where('status', 0)
                ->where('leg', $gc->leg) 
                ->where('user_id',$gc->user_id)
                // ->where('position',$user2->position)
                ->update([
                    'status' => '1', 
                ]);
                  // exit;
        }
    } 
}
