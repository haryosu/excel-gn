<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\BonusGeneration as ModelsBonusGeneration;
use App\Models\BonusLog;
use App\Models\BvUserLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BonusGeneration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:generation-bonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'bonus generation here';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    { 
        $user = User::
            where('created_at', Carbon::today()->toDateString())->orderBy('id','desc')
            ->get(); 
            
        foreach($user as $u){ 
            if ($u->pin_id != 1){
                Log::info('id ='.$u->id); 
                Log::info('ref id ='.$u->ref_id); 
                $arrs = getUplineData($u->id);
                Log::info(implode(',',$arrs)); 
                $bv = $u->bv;
                $total = (1000000*$bv)*(3/100); 
                $data = []; 
                $manager = null; 
                $dm = []; 
                // ambil data bv
                foreach( $arrs as $r){
                    $ud = User::where('id',$r)->first(); 
                    if($ud->manager_id !=null || $ud->manager_id > $manager){ 
                        if(!in_array($ud->manager_id, $dm)) {
                            $data[] = $ud->id;
                            $dm[] = $ud->manager_id;
                            // Log::info($ud->managerial->name??'no-label manager'); 
                            Log::info($ud->id.'-'.$ud->managerial->name??'no-label manager'); 
                            $BonusGeneration = new ModelsBonusGeneration(); 
                            $BonusGeneration->user_id =  $ud->id;
                            $BonusGeneration->bonus_val = $total;
                            $BonusGeneration->bonus_net = $total*(80/100);
                            $BonusGeneration->bonus_autosave = $total*(20/100); 
                            $BonusGeneration->desc = 'Bonus Genereasi dari '.$u->username;
                            $BonusGeneration->is_wd = 0; 
                            $BonusGeneration->save(); 
                
                            // echo $bonusCouple;
                            $bonusLog = new BonusLog();
                            $bonusLog->user_id = $ud->id;
                            $bonusLog->bonus_type =  4;
                            $bonusLog->bonus_plan =  1;
                            $bonusLog->bonus_val = $total;
                            $bonusLog->bonus_net = $total*(80/100);
                            $bonusLog->bonus_autosave = $total*(20/100);
                            $bonusLog->desc = 'Bonus Genereasi dari Leg'.$u->username;
                            $bonusLog->is_wd = 0;
                            $bonusLog->save();  
                
                            $autosavelog = AutosaveLog::where('user_id',$ud->id)->first();
                            if($autosavelog){
                            $autosavelog->value += $bonusLog->bonus_autosave;
                            $autosavelog->save();
                            }else
                            { 
                                $save = new AutosaveLog();
                                $save->user_id = $ud->id;
                                $save->value =  $bonusLog->bonus_autosave;
                                $save->save();
                            } 
    
                        } 
                    } 
                }
                Log::info('*****************************************************************************************'); 
            }

        }

        
    }
}
