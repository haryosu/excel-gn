<?php

namespace App\Console\Commands;

use App\Models\BonusLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BonusLeadership extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:bonus-leadership';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        // - untuk mendapatkan bonus ini, user harus memiliki peringkat managerial
        // - mendapat bonus sebesar 2% dari masing2 leg, dengan keterangan mendapatkan bonus $$$ dari leg ***
        // - pendapatan bonus harus secara recursive terurut
        // - apabila diatas sama level managerial, maka akan berhenti, wajib level diatas user terdampak


        $user = User::
            // where('managerial_id' != null)
            where(function($query) {
                $query->whereNotNull('pr')
                      ->where('pr', '>=', 1);
            })
            ->orWhere('pin_id','=', 5)
            ->get();

        foreach($user as $u){
            
            if($u->manager_id){
                // Log::info($u->id);
                // Log::info($u->manager_id);
                // Log::info('=============');
                $ref = User::where('ref_id',$u->id)->get();
                // $ref = getDownlineData($u->id);
                $datadownline = [];
                foreach($ref as $r){
                    $datadownline[] = $r->id;
                }
                // Log::info(implode(',',$datadownline));
                if($ref){
                    $bonuslog = BonusLog::whereIn('id', $datadownline)
                    ->where('created_at', '>=', Carbon::today()->toDateString())
                    ->sum('bonus_autosave');
                    if($bonuslog){
                        
                        // Log::info($u->id);
                        Log::info($u->managerial->name);
                        Log::info($u->id);
                        Log::info($bonuslog);
                        Log::info('=============');
                    }
                }
            }
        }
    }
}
