<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\BonusLog;
use App\Models\BonusProfitSharing as ModelsBonusProfitSharing;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BonusProfitSharing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:profit-sharing-bonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $user = User::where('manager_id',4)->get();
        $userCount = $user->count();
        // Get the current date and time in the server's timezone
        // $now = Carbon::now(); 
        // Get the start and end of today as Carbon objects in the server's timezone
        // $startOfDay = $now->startOfDay();
        // $endOfDay = $now->endOfDay(); 
        // Get the users who registered today and sum their BV
        $totalBv = User::where('created_at', '>=', Carbon::today()->toDateString())
            ->sum('bv');

        $getsales = ((1000000*$totalBv)*(2/100))/$userCount;
        foreach ($user as $u) {
            // generate bonus
            $bonusSharing = BonusProfitSharing::where('user_id',$u->id)->get()->count();
            if($bonusSharing < 168){ 
                $saveBonus = new ModelsBonusProfitSharing();
                $saveBonus->user_id =  $u->id;
                $saveBonus->bonus_val = $getsales;
                $saveBonus->bonus_net = $getsales*(80/100);
                $saveBonus->bonus_autosave = $getsales*(20/100); 
                $saveBonus->desc = 'Bonus Profit Sharing Diamond Member';
                $saveBonus->is_wd = 0; 
                $saveBonus->save(); 


                $bonusLog = new BonusLog();
                $bonusLog->user_id =  $u->id;
                $bonusLog->bonus_type =  6;
                $bonusLog->bonus_plan =  1;
                $bonusLog->bonus_val = $getsales;
                $bonusLog->bonus_net = $getsales*(80/100);
                $bonusLog->bonus_autosave = $getsales*(20/100);
                $bonusLog->desc = 'Bonus Profit Sharing Diamond Member';
                $bonusLog->is_wd = 0;
                $bonusLog->save(); 

                
                $autosavelog = AutosaveLog::where('user_id',$u->id)->first();
                if($autosavelog){
                $autosavelog->value += $bonusLog->bonus_autosave;
                $autosavelog->save();
                }else
                { 
                    $save = new AutosaveLog();
                    $save->user_id = $u->id;
                    $save->value =  $bonusLog->bonus_autosave;
                    $save->save();
                } 

            } 
        }
        Log::info('Cron Profit Sharing sudah jalan!');
    }
}
