<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\BonusLog;
use App\Models\BonusReshopping as ModelsBonusReshopping;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BonusReshopping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:reshopping-bonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;

        $user = User::where(function($query) {
            $query->whereNotNull('pr')
                  ->where('pr', '>=', 1);
        })
        ->orWhere('pin_id','=', 5) 
        ->get();
        foreach($user as $u){
            $bonusLog = BonusLog::where('user_id',$u->id)->where('bonus_plan',1)->sum('bonus_autosave');
            if( $bonusLog && $bonusLog != 0 ){
                Log::info('id = '.$u->id);
                Log::info('username = '.$u->username);
                Log::info('pr = '.$u->pr);
                Log::info('pin = '.$u->pin_id);
                Log::info('bonus = '.$bonusLog);
                $getbonus = 3/100 * $bonusLog;
                
                $saveBonus = new ModelsBonusReshopping();
                $saveBonus->user_id =  $u->id;
                $saveBonus->bonus_val = $getbonus;
                $saveBonus->bonus_net = $getbonus*(80/100);
                $saveBonus->bonus_autosave = $getbonus*(20/100); 
                $saveBonus->desc = 'Bonus Reshopping '. $u->username;
                $saveBonus->is_wd = 0; 
                $saveBonus->save(); 


                $bonusLog = new BonusLog();
                $bonusLog->user_id =  $u->id;
                $bonusLog->bonus_type =  6;
                $bonusLog->bonus_plan =  1;
                $bonusLog->bonus_val = $getbonus;
                $bonusLog->bonus_net = $getbonus*(80/100);
                $bonusLog->bonus_autosave = $getbonus*(20/100);
                $bonusLog->desc = 'Bonus Reshopping '. $u->username;
                $bonusLog->is_wd = 0;
                $bonusLog->save(); 

                
                $autosavelog = AutosaveLog::where('user_id',$u->id)->first();
                if($autosavelog){
                $autosavelog->value += $bonusLog->bonus_autosave;
                $autosavelog->save();
                }else
                { 
                    $save = new AutosaveLog();
                    $save->user_id = $u->id;
                    $save->value =  $bonusLog->bonus_autosave;
                    $save->save();
                } 
                // $upliner = User::
                // // where('managerial_id' != null)
                // where(function($query) {
                    //     $query->whereNotNull('pr')
                    //     ->where('pr', '>=', 1);
                    // })
                    // ->orWhere('pin_id','=', 5)
                    // ->get();
                $upline = getUplineData($u->id);
                Log::info($upline);
                // $merge = \array_merge($u->id,$upline);
                $userRef = User::
                where(function($query) {
                    $query->whereNotNull('pr')
                          ->where('pr', '>=', 1);
                        })
                // ->where('manager_id', '>=', 1)
                // ->orWhere('pin_id','=', 5) 
                ->whereIn('id',$upline)
                ->get();
                $count = 0;
                foreach($userRef as $ur){
                    if($count == 8){
                        break;
                    }
                    Log::info('---------');
                    Log::info('ref_id ='.$ur->id);
                    Log::info('ref_pr ='.$ur->pr);
                    Log::info($ur->managerial->name);
                    Log::info($ur->pr);
                    Log::info('---------');
                        
                    $saveBonus = new ModelsBonusReshopping();
                    $saveBonus->user_id =  $ur->id;
                    $saveBonus->bonus_val = $getbonus;
                    $saveBonus->bonus_net = $getbonus*(80/100);
                    $saveBonus->bonus_autosave = $getbonus*(20/100); 
                    $saveBonus->desc = 'Bonus Reshopping '. $u->username;
                    $saveBonus->is_wd = 0; 
                    $saveBonus->save(); 

                    $bonusLog = new BonusLog();
                    $bonusLog->user_id =  $ur->id;
                    $bonusLog->bonus_type =  6;
                    $bonusLog->bonus_plan =  1;
                    $bonusLog->bonus_val = $getbonus;
                    $bonusLog->bonus_net = $getbonus*(80/100);
                    $bonusLog->bonus_autosave = $getbonus*(20/100);
                    $bonusLog->desc = 'Bonus Reshopping '. $u->username;
                    $bonusLog->is_wd = 0;
                    $bonusLog->save(); 

                    
                    $autosavelog = AutosaveLog::where('user_id',$ur->id)->first();
                    if($autosavelog){
                    $autosavelog->value += $bonusLog->bonus_autosave;
                    $autosavelog->save();
                    }else
                    { 
                        $save = new AutosaveLog();
                        $save->user_id = $ur->id;
                        $save->value =  $bonusLog->bonus_autosave;
                        $save->save();
                    } 
                } 
                // $getTotalBonus = 
                Log::info('~~~~~~~~~~~~~~~~~~~~~'); 
                

            }
        }
    }
}
