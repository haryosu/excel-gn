<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\BonusCouple;
use App\Models\BonusCoupleRank;
use App\Models\BonusExceed;
use App\Models\BonusGeneration;
use App\Models\BonusLog;
use App\Models\BonusProfitSharing;
use App\Models\BonusReferral;
use App\Models\Pin;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class GenUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'genuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate(); 
        DB::table('autosave_log')->truncate();
        DB::table('bonus_basic_reshoping')->truncate();
        DB::table('bonus_couple')->truncate();
        DB::table('bonus_couple_rank')->truncate();
        DB::table('bonus_exceed')->truncate();
        DB::table('bonus_generation')->truncate();
        DB::table('bonus_leadership')->truncate();
        DB::table('bonus_log')->truncate();
        DB::table('bonus_profit_sharing')->truncate();
        DB::table('bonus_referral')->truncate();
        DB::table('bonus_reshopping')->truncate();
        DB::table('bonus_royalty_rank')->truncate();
        DB::table('bv_user_log')->truncate();
        DB::table('bv_user_log')->truncate();
        DB::table('crons')->truncate(); 
        // DB::statement('SET FOREIGN_KEY_CHECKS=1');

        // $datauser =
       $json =  file_get_contents('http://excel-gn.com/csvjson.json'); 
    $gravatar = json_decode($json);
    // dd($gravatar);
      // Log::info($gravatar);  
        foreach($gravatar as $u){
            // Log::info($u->id); 
            //   Log::info($u->id);
            //   Log::info($u->ref_id); 
            //   Log::info($u->position);
            //   Log::info($u->firstname);
            Log::info($u->username);
            //   Log::info($u->email);
            //   Log::info($u->mobile);
            //   Log::info($u->pin_id);
            //   Log::info($u->password);
          if($u->position == 1 || $u->position == 2){
              $leg = 1; 
          } 
          if($u->position == 3 || $u->position == 4){
              $leg = 2; 
          }
          if($u->position == 5 || $u->position == 6){
              $leg = 3; 
          }
          $userReff =  [
              'id' => $u->id,
              'ref_id' => $u->ref_id,
              'position' => $u->position,
              'leg' => $leg,
              'firstname' => $u->firstname,
              'lastname' => "",
              'username' => $u->username,
              'email' => $u->email,
              'mobile' => $u->mobile,
              'pin_id' => $u->pin_id,
              'bv' => $u->bv,
              'pr' => $u->pr,
              'country_code' => "ID",

              'password' => Hash::make($u->newpass),
              'country_code' => "ID",
              'identity'=>json_encode([
                              'nik' => "",
                              'gender' => "",
                              'dob' => "",
                              'pob' => "",
                          ], true),
              'bank_account' => $u->bank_account,
              'bank_number' => $u->bank_number,
              'bank_provider' => $u->bank_provider,
              
              'nik'=>"",
              'gender'=>"",
              'dob'=>"",
              'pob'=>"",
              
              'status' => '1',
              'ev' => '1',
              'sv' => '1',
              'ts' => 0,
              'tv' => 1,
            ] 
          ;
          DB::table('users')->insert($userReff);
          $getPin = Pin::where('id',$userReff['pin_id'])->first();

          updateBvRecursive($u->id,$u->id, $getPin->bv, $u->position,$leg); 
          \generateReferralUser($u->id); 
          echo 'cron berjalan'.$u->username;
          Log::info('cron berjalan'.$u->username);
                          
        }
        // bonus generate
        $getbon = file_get_contents('http://excel-gn.com/bonuslog.json'); 
        $bonuslog = json_decode($getbon);
        foreach($bonuslog as $bon)
        {
          echo '</br> cron  bonus berjalan'.$u->username;

            // "id": 2097,
            // "user_id": 185,
            // "bonus_name": 6,
            // "bonus_net": 7200,
            // "description": "Profit Sharing tanggal 2023-03-31",
            // "bonus_val": 7200,
            // "bonus_autosave": 0,
            // "bonus_datetime": "4/1/2023 0:02",
            // "bonus_date": "3/31/2023",
            // "is_wd": 0

            // echo $bonusCouple;
            $bonusLog = new BonusLog();
            $bonusLog->user_id =  $bon->user_id; 
            $bonusLog->bonus_type =   $bon->bonus_name;
            $bonusLog->bonus_plan =  1;
            $bonusLog->bonus_val = $bon->bonus_val;
            $bonusLog->bonus_net = $bon->bonus_net;
            $bonusLog->bonus_autosave = $bon->bonus_autosave;
            $bonusLog->desc = $bon->description;
            $bonusLog->is_wd = $bon->is_wd;
            $bonusLog->save();  

            $autosavelog = AutosaveLog::where('user_id',$bon->user_id)->first();
            if($autosavelog){
            $autosavelog->value += $bon->bonus_autosave;
            $autosavelog->save();
            }else
            { 
                $save = new AutosaveLog();
                $save->user_id = $bon->user_id;
                $save->value =  $bon->bonus_autosave;
                $save->save();
            } 
            if($bon->bonus_name === 1){
                $bonusCouple = new BonusReferral(); 
                $bonusCouple->user_id =  $bon->user_id; 
                $bonusCouple->sender_id =  $bon->user_id; 
                $bonusCouple->bonus_val = $bon->bonus_val;
                $bonusCouple->bonus_net = $bon->bonus_net;
                $bonusCouple->bonus_autosave = $bon->bonus_autosave;
                $bonusCouple->desc = $bon->description;
                $bonusCouple->is_wd = $bon->is_wd;
                $bonusCouple->save(); 
            }
            if($bon->bonus_name === 2){
                $bonusCouple = new BonusCouple(); 
                $bonusCouple->user_id =  $bon->user_id; 
                $bonusCouple->bonus_val = $bon->bonus_val;
                $bonusCouple->bonus_net = $bon->bonus_net;
                $bonusCouple->bonus_autosave = $bon->bonus_autosave;
                $bonusCouple->desc = $bon->description;
                $bonusCouple->is_wd = $bon->is_wd;
                $bonusCouple->save(); 
            }
            if($bon->bonus_name === 3){
                $bonusCouple = new BonusExceed(); 
                $bonusCouple->user_id =  $bon->user_id; 
                $bonusCouple->bonus_val = $bon->bonus_val;
                $bonusCouple->bonus_net = $bon->bonus_net;
                $bonusCouple->bonus_autosave = $bon->bonus_autosave;
                $bonusCouple->desc = $bon->description;
                $bonusCouple->is_wd = $bon->is_wd;
                $bonusCouple->save(); 
            }
            if($bon->bonus_name === 4){
                $bonusCouple = new BonusGeneration(); 
                $bonusCouple->user_id =  $bon->user_id; 
                $bonusCouple->bonus_val = $bon->bonus_val;
                $bonusCouple->bonus_net = $bon->bonus_net;
                $bonusCouple->bonus_autosave = $bon->bonus_autosave;
                $bonusCouple->desc = $bon->description;
                $bonusCouple->is_wd = $bon->is_wd;
                $bonusCouple->save(); 
            }
            if($bon->bonus_name === 5){
                $bonusCouple = new BonusCoupleRank(); 
                $bonusCouple->user_id =  $bon->user_id; 
                $bonusCouple->bonus_val = $bon->bonus_val;
                $bonusCouple->bonus_net = $bon->bonus_net;
                $bonusCouple->bonus_autosave = $bon->bonus_autosave;
                $bonusCouple->desc = $bon->description;
                $bonusCouple->is_wd = $bon->is_wd;
                $bonusCouple->save(); 
            }
            if($bon->bonus_name === 6){
                $bonusCouple = new BonusProfitSharing(); 
                $bonusCouple->user_id =  $bon->user_id; 
                $bonusCouple->bonus_val = $bon->bonus_val;
                $bonusCouple->bonus_net = $bon->bonus_net;
                $bonusCouple->bonus_autosave = $bon->bonus_autosave;
                $bonusCouple->desc = $bon->description;
                $bonusCouple->is_wd = $bon->is_wd;
                $bonusCouple->save(); 
            }

        }

        $manageril = User::orderBy('id','desc')->get();
        foreach ($manageril as $m) {
            if($m->ref_id != 0){
                \generateReferralUser($m->ref_id); 
                echo ('run managerial');
            }
            # code...
        }


    }
}
