<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\BonusCouple;
use App\Models\BonusCoupleRank;
use App\Models\BonusLog;
use App\Models\BvUserLog;
use App\Models\CrbLog;
use App\Models\Cron;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InjectCoupleRankingBonus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel-inject:couple-ranking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'bonus couple ranking here';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $getBonusCouple = Cron::where('status', 1) 
        // ->groupBy(['user_id' ])
        // ->orderBy('id','desc')
        // ->where('updated_at', '>=', Carbon::today()->toDateString()) 
        // ->get();  
        // foreach($getBonusCouple as $gbc){
        //     $user1 = User::where('id',$gbc->user_id)->where('leg',$gbc->leg)->where('position',$gbc->position)->first();
        //     if($user1){
        //         $user2 = User::where('ref_id',$user1->ref_id)
        //                 ->where('leg',$user1->leg)->where('position', 'not like', $user1->position)->first();
        //         if($user2){ 
        //             $bc1 = BonusCouple::where('user_id',$user1->id) 
        //             ->first();  
        //             $bc2 = BonusCouple::where('user_id',$user2->id) 
        //             ->first(); 
        //             if($bc1 && $bc2){  
        //                 $user1l = BvUserLog::
        //                 where('user_id',$user1->ref_id)
        //                 ->where('leg',$gbc->leg)
        //                 ->where('updated_at', Carbon::today()->toDateString())
        //                 ->where('position',$gbc->position)
        //                 ->first(); 
        //                 $user2l = BvUserLog::
        //                 where('user_id',$user1->ref_id)
        //                 ->where('leg',$gbc->leg)
        //                 ->where('updated_at', Carbon::today()->toDateString())
        //                 ->where('position', '!=', $gbc->position)->first();
        //                 if($user1l && $user2l){ 
        //                     Log::info('ambil ini '.$bc1->user_id.'--'. $bc2->user_id);
        //                     Log::info($user2l->user_id.'--'. $user1l->user_id);
        //                     Log::info('buat si ini '.$user1->ref_id);
        //                     // Log::info('sekarang '.Carbon::today()->toDateString());
        //                     Log::info('bv '.$user2l->bv_log.'--'. $user1l->bv_log);
        //                     Log::info('leg '.$user2l->leg.'--'. $user1l->leg);
        //                     // Log::info($user2l->position.'--'. $user1l->position);
        //                     Log::info('---');
     
        //                 } 
        //                 $ref = User::where('id',$user1->ref_id)->first();
    
        //                 if ($ref->manager_id != null  && $user1l && $user2l) {  
        //                     if($user1l->bv_log > $user2l->bv_log){
        //                         $valBig= $user1l->bv_log;
        //                         $valLow= $user2l->bv_log;
        //                     }elseif($user1l->bv_log < $user2l->bv_log){
        //                         $valBig= $user2l->bv_log;
        //                         $valLow= $user1l->bv_log;
        //                     }else{
        //                         $valBig= $user1l->bv_log;
        //                         $valLow= $user2l->bv_log;
        //                     }
    
        //                     $calc1 = $valBig*20000;
        //                     $calc2 = $valLow*30000;
        //                     $val = $calc1+$calc2;
        //                     /* total bonus  */    
        //                     if ($val !=0){
        //                         $totalbonus = $val;  
        //                             // Log::info('--exist'); 
        //                             Log::info('ref = '.$ref->manager_id); 
        //                             $bonusCoupleRank2 = new BonusCoupleRank(); 
        //                             $bonusCoupleRank2->user_id =$user1->ref_id;
        //                             $bonusCoupleRank2->bonus_val = $totalbonus;
        //                             $bonusCoupleRank2->bonus_net = $totalbonus*(80/100);
        //                             $bonusCoupleRank2->bonus_autosave = $totalbonus*(20/100); 
        //                             $bonusCoupleRank2->desc = 'Bonus Couple Rank dari downline '.$user1->username.' -- '.$user2->username.' Leg '.$user1->leg;
        //                             $bonusCoupleRank2->is_wd = 0; 
        //                             $bonusCoupleRank2->is_wd = 0; 
        //                             $bonusCoupleRank2->save();  

        //                             $bonusLog3 = new BonusLog();      
        //                             $bonusLog3->bonus_type =  5;
        //                             $bonusLog3->bonus_plan =  1;
        //                             $bonusLog3->user_id =   $user1->ref_id;
        //                             $bonusLog3->bonus_val = $totalbonus;
        //                             $bonusLog3->bonus_net = $totalbonus*(80/100);
        //                             $bonusLog3->bonus_autosave = $totalbonus*(20/100); 
        //                             $bonusLog3->desc = 'Bonus Couple Rank dari downline '.$user1->ref_id.' -- '.$user2->id.' Leg '.$user1->leg;
        //                             $bonusLog3->is_wd = 0;
        //                             $bonusLog3->save();


        //                             $autosavelog = AutosaveLog::where('user_id',$user1->ref_id)->first();
        //                             if($autosavelog){
        //                             $autosavelog->value += $bonusLog3->bonus_autosave;
        //                             $autosavelog->save();
        //                             }else
        //                             { 
        //                                 $save = new AutosaveLog();
        //                                 $save->user_id = $user1->ref_id;
        //                                 $save->value =  $bonusLog3->bonus_autosave;
        //                                 $save->save();
        //                             } 

        //                             $data = []; 
        //                             $manager = $ref->manager_id; 
        //                             $dm = []; 
        //                             foreach( getUplineData($user1->id) as $r){
        //                                 $ud = User::where('id',$r)->first(); 
        //                                 if($ud->manager_id !=null){ 
        //                                     if($ud->manager_id > $manager){
        //                                         if(!in_array($ud->manager_id, $dm)) {
        //                                             $data[] = $ud->id;
        //                                             $dm[] = $ud->manager_id; 
        //                                             $bonusCoupleRank2 = new BonusCoupleRank(); 
        //                                             $bonusCoupleRank2->user_id = $ud->id;
        //                                             $bonusCoupleRank2->bonus_val = $totalbonus;
        //                                             $bonusCoupleRank2->bonus_net = $totalbonus*(80/100);
        //                                             $bonusCoupleRank2->bonus_autosave = $totalbonus*(20/100); 
        //                                             $bonusCoupleRank2->desc = 'Bonus Couple Rank dari downline '.$user1->username.' -- '.$user2->username.' Leg '.$user1->leg;
        //                                             $bonusCoupleRank2->is_wd = 0; 
        //                                             $bonusCoupleRank2->is_wd = 0; 
        //                                             $bonusCoupleRank2->save();  
                    
        //                                             $bonusLog3 = new BonusLog();      
        //                                             $bonusLog3->bonus_type =  5;
        //                                             $bonusLog3->bonus_plan =  1;
        //                                             $bonusLog3->user_id =   $ud->id;
        //                                             $bonusLog3->bonus_val = $totalbonus;
        //                                             $bonusLog3->bonus_net = $totalbonus*(80/100);
        //                                             $bonusLog3->bonus_autosave = $totalbonus*(20/100); 
        //                                             $bonusLog3->desc = 'Bonus Couple Rank dari downline '.$user1->ref_id.' -- '.$user2->id.' Leg '.$user1->leg;
        //                                             $bonusLog3->is_wd = 0;
        //                                             $bonusLog3->save();
                    
                    
        //                                             $autosavelog = AutosaveLog::where('user_id',$ud->id)->first();
        //                                             if($autosavelog){
        //                                             $autosavelog->value += $bonusLog3->bonus_autosave;
        //                                             $autosavelog->save();
        //                                             }else
        //                                             { 
        //                                                 $save = new AutosaveLog();
        //                                                 $save->user_id = $ud->id;
        //                                                 $save->value =  $bonusLog3->bonus_autosave;
        //                                                 $save->save();
        //                                             } 
    
        //                                         }
        //                                     }
        //                                 }
        //                             } 
    
        //                     }
    
    
        //                 }
        //             } 
                        
        
        //             $updateCron = Cron::where('type','1')
        //             ->where('leg', $gbc->leg) 
        //             ->where('user_id',$user1->user_id)
        //             // ->where('position',$gbc->position)
        //             ->update([
        //                 'status' => '2', 
        //             ]);
        //             $updateCron2 = Cron::where('type','1')
        //             ->where('leg', $gbc->leg) 
        //             ->where('user_id',$user2->user_id) 
        //             ->update([
        //                 'status' => '2', 
        //             ]); 
        //         }
        //     }
        // }
        // $updateCron2 = Cron::where('type','1') 
        // ->update([
        //     'status' => '2', 
        // ]);  
        // DB::table('crb_log')->truncate(); 
        // DB::table('bonus_couple_rank')->truncate(); 

        $getBonusCoupleLog = BonusCouple::orderBy('id','asc')
        // ->where('is_wd',0)
        ->whereDate('created_at',Carbon::today()->toDateString())
        ->get();
        foreach($getBonusCoupleLog as $gbl){
            $u = User::where('id',$gbl->user_id)->first();
            $r = User::where('id',$u->ref_id)->first();
            if($r){
                if($r->manager_id != null || $r->manager_id != 0 ){
                    if($u->manager_id == null){
                        $u_mn = 0;
                        $um = 'nol';
                    }else{
                        $u_mn = $u->manager_id;
                        $um = $u->managerial->name;
                    }
                    if($r->manager_id > $u->manager_id ){

                        Log::info('---' );
                        Log::info( $gbl->bonus_val .'---' );
                        Log::info( $gbl->created_at .'---' );
                        Log::info('ambil ini '.$r->username.'  --crb'.$r->id );
                        Log::info('ambil ini '.$r->managerial->name );
                        Log::info('ref = '.$r->manager_id); 
                        Log::info('dari downline '.$u->username);
                        Log::info('dari leg '.$u->leg);
                        Log::info('dari position '.$u->position );
                        Log::info('dari position '.$um );
                        Log::info('---' );

                        $getlog = CrbLog::where('user_id',$r->id)->where('leg',$u->leg)->where('status',0)->first();
                        if(!$getlog){
                            if($u->position === 1 || $u->position === 3 || $u->position === 5){
                                $cLog = CrbLog::create([
                                    'user_id'=> $r->id,
                                    'leg'=> $u->leg,
                                    'pos_1'=> $u->position, 
                                    'pos_1_val'=> $gbl->bonus_val,  
                                    'created_at'=> $gbl->created_at,  

                                ]);
                            }else{
                                $cLog = CrbLog::create([
                                    'user_id'=> $r->id,
                                    'leg'=> $u->leg,
                                    'pos_2'=> $u->position, 
                                    'pos_2_val'=> $gbl->bonus_val,  
                                    'created_at'=> $gbl->created_at,  
                                ]); 
                            }
                        }else{ 
                            if($u->position === 1 || $u->position === 3 || $u->position === 5){ 
                            $cLog =$getlog->update([ 
                                'pos_1'=> $u->position, 
                                'pos_1_val'=> $getlog->pos_1_val + $gbl->bonus_val,  
                                'updated_at'=> $gbl->created_at,  

                            ]);
                            }else{ 
                                $cLog =$getlog->update([ 
                                    'pos_2'=> $u->position, 
                                    'pos_2_val'=> $getlog->pos_2_val + $gbl->bonus_val,   
                                    'updated_at'=> $gbl->created_at,  
                                ]);
                            } 
                        }


                        $CrbLog = CrbLog::where('user_id',$r->id)->where('leg',$u->leg)->whereNotNull(['pos_1_val','pos_2_val'])->orderBy('id','desc')->first();
                        
                        // echo($CrbLog);
                        if($CrbLog){
                            $CrbLog= $CrbLog->update([ 
                                'total'=> $CrbLog->pos_2_val + $CrbLog->pos_1_val,   
                            ]);
                        } 
                        
                        $Crb= CrbLog::where('user_id',$r->id)->where('leg',$u->leg)->whereNotNull(['pos_1_val','pos_2_val'])->orderBy('id','desc')->first();
                        if($Crb){
                            $Crb= $Crb->update([ 
                                'status'=> '1',  
                            ]);
                        } 

                        // if($CrbLog2->pos_1_val != null && $CrbLog2->pos_1_val!= null){
                        // }

                        
                    }

                }
            }
             

        }



        // $CrbLogs = CrbLog::whereNotNull('pos_1_val')
        // ->whereNotNull('pos_2_val')
        // ->where('status',1)
        // ->orderBy('id','asc')
        // ->get(); 
        // foreach( $CrbLogs as $CrbLog2){
        //     // log::info($CrbLog2);

        //     $r = User::where('id',$CrbLog2->id)->first();
        //     if($r){  
        //         if($CrbLog2->pos_1_val > $CrbLog2->pos_2_val){
        //                 $valBig= $CrbLog2->pos_1_val;
        //                 $valLow= $CrbLog2->pos_2_val;
        //         }elseif($CrbLog2->pos_1_val < $CrbLog2->pos_2_val){
        //                 $valBig= $CrbLog2->pos_2_val;
        //                 $valLow= $CrbLog2->pos_1_val;
        //         }else{
        //             $valBig= $CrbLog2->pos_1_val;
        //             $valLow= $CrbLog2->pos_2_val;
        //         }
    
        //         $calc1 = (($valBig/50000)-($valLow/50000))*20000;
        //         $calc2 = ($valLow/50000)*40000;
        //         $val = $calc1+$calc2; 
        //         Log::info($valBig.' -- '.$valBig/50000);
        //         Log::info($valLow.' -- '.$valLow/50000);
        //         Log::info('ttl --'. $val);
        //         /* total bonus  */    
        //         if ($val !=0){
        //             $user1 = User::where('ref_id',$r->id)->where('position',$CrbLog2->pos_1)->first();
        //             Log::info('ttl --'. $user1);
        //             $user2 = User::where('ref_id',$r->id)->where('position',$CrbLog2->pos_2)->first();
        //             Log::info('ttl --'. $user2);
        //             $totalbonus = $val;  
        //             $bonusCoupleRank2 = new BonusCoupleRank(); 
        //             $bonusCoupleRank2->user_id =$r->id;
        //             $bonusCoupleRank2->bonus_val = $totalbonus;
        //             $bonusCoupleRank2->bonus_net = $totalbonus*(80/100);
        //             $bonusCoupleRank2->bonus_autosave = $totalbonus*(20/100); 
        //             $bonusCoupleRank2->desc = 'Bonus Couple Rank Leg '.$CrbLog2->leg;
        //             $bonusCoupleRank2->is_wd = 0; 
        //             $bonusCoupleRank2->created_at = $CrbLog2->created_at; 
        //             $bonusCoupleRank2->updated_at = $CrbLog2->updated_at; 
        //             $bonusCoupleRank2->save();  
    
        //             $bonusLog3 = new BonusLog();      
        //             $bonusLog3->bonus_type =  5;
        //             $bonusLog3->bonus_plan =  1;
        //             $bonusLog3->user_id = $r->id;
        //             $bonusLog3->bonus_val = $totalbonus;
        //             $bonusLog3->bonus_net = $totalbonus*(80/100);
        //             $bonusLog3->bonus_autosave = $totalbonus*(20/100); 
        //             $bonusLog3->desc = 'Bonus Couple Rank Leg '.$CrbLog2->leg;
        //             $bonusLog3->is_wd = 0;
        //             $bonusLog3->updated_at = $CrbLog2->updated_at;
        //             $bonusLog3->updated_at = $CrbLog2->updated_at;
        //             $bonusLog3->save();
    
    
        //             $autosavelog = AutosaveLog::where('user_id',$r->id)->first();
        //             if($autosavelog){
        //             $autosavelog->value += $bonusLog3->bonus_autosave;
        //             $autosavelog->save();
        //             }else
        //             { 
        //                 $save = new AutosaveLog();
        //                 $save->user_id = $r->id;
        //                 $save->value =  $bonusLog3->bonus_autosave;
        //                 $save->save();
        //             } 
    
        //             $data = []; 
        //             $manager = $r->manager_id; 
        //             $dm = [];  
        //             foreach( getUplineData($r->id) as $rx){ 
        //                 $ud = User::where('id',$rx)->first();  
        //                 if($ud){ 
        //                     log::info($ud->id);
        //                     if($ud->manager_id !=null){ 
        //                         if($ud->manager_id > $manager){
        //                             if(!in_array($ud->manager_id, $dm)) {
    
        //                                 $data[] = $ud->id;
        //                                 $dm[] = $ud->manager_id; 
        //                                 $manager = $ud->manager_id; 
        //                                 $bonusCoupleRank2 = new BonusCoupleRank(); 
        //                                 $bonusCoupleRank2->user_id = $ud->id;
        //                                 $bonusCoupleRank2->bonus_val = $totalbonus;
        //                                 $bonusCoupleRank2->bonus_net = $totalbonus*(80/100);
        //                                 $bonusCoupleRank2->bonus_autosave = $totalbonus*(20/100); 
        //                                 $bonusCoupleRank2->desc = 'Bonus Couple Rank dari downline '.$r->username.' Leg '.$r->leg;
        //                                 $bonusCoupleRank2->is_wd = 0; 
        //                                 $bonusCoupleRank2->created_at = $CrbLog2->created_at;
        //                                 $bonusCoupleRank2->updated_at = $CrbLog2->updated_at;
        //                                 $bonusCoupleRank2->save();  
        
        //                                 $bonusLog3 = new BonusLog();      
        //                                 $bonusLog3->bonus_type =  5;
        //                                 $bonusLog3->bonus_plan =  1;
        //                                 $bonusLog3->user_id =   $ud->id;
        //                                 $bonusLog3->bonus_val = $totalbonus;
        //                                 $bonusLog3->bonus_net = $totalbonus*(80/100);
        //                                 $bonusLog3->bonus_autosave = $totalbonus*(20/100); 
        //                                 $bonusLog3->desc = 'Bonus Couple Rank dari downline '.$r->username.' Leg '.$r->leg;
        //                                 $bonusLog3->is_wd = 0;
        //                                 $bonusLog3->created_at = $CrbLog2->created_at;
        //                                 $bonusLog3->updated_at = $CrbLog2->updated_at;
        //                                 $bonusLog3->save();
        
        
        //                                 $autosavelog = AutosaveLog::where('user_id',$ud->id)->first();
        //                                 if($autosavelog){
        //                                 $autosavelog->value += $bonusLog3->bonus_autosave;
        //                                 $autosavelog->save();
        //                                 }else
        //                                 { 
        //                                     $save = new AutosaveLog();
        //                                     $save->user_id = $ud->id;
        //                                     $save->value =  $bonusLog3->bonus_autosave;
        //                                     $save->save();
        //                                 } 
    
        //                             }
        //                         }
        //                     }
        //                 }
        //             }  
        //         }
        //         $CrbL = CrbLog::where('id',$CrbLog2->id)->first();
        //         $CrbL = $CrbL->update([ 
        //             'status'=> '2',  
        //         ]);
    
                

        //     }
        // }
        // $sum = CrbLog::where('status',1)->sum('total');
        // echo( 'total = '.$sum);

         
    }

     
}
