<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\Bonus\ReferalBonus;
use App\Models\BonusLog;
use App\Models\Pin;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class TestGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testGenerator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        // DB::table('autosave_log')->truncate();
        // DB::table('bonus_basic_reshoping')->truncate();
        // DB::table('bonus_couple')->truncate();
        // DB::table('bonus_couple_rank')->truncate();
        // DB::table('bonus_exceed')->truncate();
        // DB::table('bonus_generation')->truncate();
        // DB::table('bonus_leadership')->truncate();
        // DB::table('bonus_log')->truncate();
        // DB::table('bonus_profit_sharing')->truncate();
        // DB::table('bonus_referral')->truncate();
        // DB::table('bonus_reshopping')->truncate();
        // DB::table('bonus_royalty_rank')->truncate();
        // DB::table('bv_user_log')->truncate();
        // DB::table('bv_user_log')->truncate();
        // DB::table('crons')->truncate(); 
        // $user = User::where('id','!=',1)->orderBy('id','desc')->get();
        // foreach($user as $u){
        //     // Log::info('---');
        //     // Log::info('upline = '.implode(',',getUplineData($u->id)));
        //     // Log::info('managerial = '.implode(',',jumpToUplineOfUpline($u->id))); 
        //     // Log::info(jumpToUplineOfUpline($u->id)); 
        //     // foreach(jumpToUplineOfUpline($u->id) as $man){
        //     //     $u = User::find($man);
        //     //     Log::info($u->id .'--'.$u->managerial->name); 
        //     // }
        //     // Log::info('---');

        //     // Log::info(jumpToUplineOfUpline(135));
        //     // $u->user_gen =implode(',',getUplineData($u->id));
        //     // $u->save();
            
        //     // $getPin = Pin::where('id',$u->pin_id)->first();
        //     // $getIDUser = User::where('id',$u->ref_id)->first();
        //     // if($u->ref_id !=0 && $u->pin_id !=1){
        //     //     $refBonus = new ReferalBonus();
        //     //     $refBonus->user_id = $u->ref_id;
        //     //     $refBonus->sender_id = $u->id;
        //     //     $refBonus->bonus_val = $getPin->ref_bonus;
        //     //     $refBonus->bonus_net =(80/100) * $getPin->ref_bonus;
        //     //     $refBonus->bonus_autosave =(20/100) * $getPin->ref_bonus;
        //     //     $refBonus->desc = 'Referal Bonus From '.$u->username.' to '.$getIDUser->username.' on Leg '.$u->leg;
        //     //     $refBonus->save(); 
        //     //     $bonusLog = new BonusLog(); 
        //     //     $bonusLog->bonus_type =  1;
        //     //     $bonusLog->bonus_plan =  1;
        //     //     $bonusLog->user_id = $getIDUser->id;
        //     //     // $refBonus->sender_id = $u->id;
        //     //     $bonusLog->bonus_val = $getPin->ref_bonus;
        //     //     $bonusLog->bonus_net =(80/100) * $getPin->ref_bonus;
        //     //     $bonusLog->bonus_autosave =(20/100) * $getPin->ref_bonus;
        //     //     $bonusLog->desc =  'Referal Bonus From '.$u->username.' to '.$getIDUser->username.' on Leg '.$u->leg;
        //     //     $bonusLog->is_wd = 0;
        //     //     $bonusLog->save(); 
                            
        //     //     $autosavelog = AutosaveLog::where('user_id',$getIDUser->id)->first();
        //     //     if($autosavelog){
        //     //     $autosavelog->value += $bonusLog->bonus_autosave;
        //     //     $autosavelog->save();
        //     //     }else
        //     //     { 
        //     //         $save = new AutosaveLog();
        //     //         $save->user_id = $getIDUser->id;
        //     //         $save->value =  $bonusLog->bonus_autosave;
        //     //         $save->save();
        //     //     } 
        //     //     updateBvRecursive($u->ref_id,$u->id, $u->pin->bv, $u->position,$u->leg); 
        //     //     \BonusCron($u->id,1);  
        //     //     Log::info($u->ref_id.'---');
        //     // }
        // }
        $now = Carbon::now();
        File::append(Storage::disk('local')->path('test.csv'), PHP_EOL."text to append ".$now);
        echo "work";

    }
}
