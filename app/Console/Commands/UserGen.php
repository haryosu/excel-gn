<?php

namespace App\Console\Commands;

use App\Models\BvUserLog;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class UserGen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        // $user = User::where('id',227)->first();
        // $get = getUplineData($user->id);
        // \print_r($get) ;
        // $user = BvUserLog::orderBy('user_id','asc')->whereIn('position',[1,3,5])->get();
        // foreach($user as $bv){
        //     $match1 = $bv->bv_log;
        //     $match2 = BvUserLog::where('user_id',$bv->user_id)->where('leg',$bv->leg)->where('position',$bv->position+1)->first();
        //     if($match1!=0 && $match2 && $match2->bv_log !=0){
        //         $myfile=fopen("data-bvlog-".date('m-d-Y').".txt", "a")or die("Unable to open file!");
        //         // $i[] = $us->id;
        //         $message = $match2->user_id.'-- '.$match2->leg.'-- '.$match1.' - '.$match2->bv_log;
        //         fwrite($myfile, "\n". $message);
        //         fclose($myfile);
        //         $m1 = $match1;
        //         $m2 = $match2->bv_log;
        //         if($m1 > $m2){
        //             $val = $m2;
        //         }
        //         else {
        //             $val = $m1;
        //         }
        //         $up = BvUserLog::where('user_id',$bv->user_id)
        //         ->where('leg',$bv->leg)->update(['bv_log'=> DB::raw('bv_log - ' . $val)]);
        //         // $up2 = BvUserLog::where('user_id',$match2->user_id)
        //         // ->where('leg',$bv->leg)->where('position',$match2->position) ->update(['bv_log'=>$match2->bvlog - $val]);
                
        //         // if($m1==$m2){
        //         //     $val = $m1;
        //         //     // $bv->update(['bv_log'=>0]);
        //         //     // $match2->update(['bv_log'=>0]);
        //         //     $bvupdate1 = BvUserLog::where('user_id',$bv->user_id)->where('leg',$bv->leg)->where('position',$bv->position)->update(['bv_log'=>0]);
        //         //     $bvupdate1 = BvUserLog::where('user_id',$bv->user_id)->where('leg',$bv->leg)->where('position',$match2->position)->update(['bv_log'=>0]);
        //         // }
        //     }
        // }
         
        // $crons = \BonusCron($user->id,1);

        $gu = User::whereIn('id',[826])->get();
        foreach ($gu as $getuser) {
            # code...
            updateBvRecursive($getuser->ref_id,$getuser->id, $getuser->bv, $getuser->position,$getuser->leg); 
            \BonusCron($getuser->id,1,$getuser->created_at); 
        }
        
    }
}
