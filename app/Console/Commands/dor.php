<?php

namespace App\Console\Commands;

use App\Models\AutosaveLog;
use App\Models\BonusCouple;
use App\Models\BonusExceed;
use App\Models\BonusLog;
use App\Models\BvUserLog;
use App\Models\Cron;
use App\Models\Pin;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class dor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:dor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $users = User::whereBetween('created_at', ['2023-04-02 00:00:00', now()->addSecond()])
        ->where('pin_id','>',1)
        // ->groupBy(DB::raw('DATE(created_at)'))
        // ->groupBy(function($date) {
        //     return Carbon::parse($date->created_at)->format('Y'); // grouping by years
        //     return Carbon::parse($date->created_at)->format('m'); // grouping by months
        // })
        ->get();
        $x = array(
            // 1141,
            // 1144,
            1153 	
            ) ;
        foreach ($x as $w) {
            $u =User::where('id',$w)->first();
            $myfile=fopen("data-dor-".date('m-d-Y').".txt", "a")or die("Unable to open file!");
            // file_put_contents($myfile, $message, FILE_APPEND | LOCK_EX);
            
            // $myfile = fopen("logs.txt", "a") or die("Unable to open file!");
            // $txt = "user id date";
            // echo($u);
            // echo('<br/>');
            // Log::info($u);
            $userid = $u->id;
            $countBv = User::whereDate('created_at', '=', $u->created_at)->whereMonth('created_at', '=', $u->created_at)->sum('bv');
            $message = $u->id."--". $u->created_at."--".$countBv;
            fwrite($myfile, "\n". $message);
            fclose($myfile);

            \BonusCron($u->id,1,$u->created_at); 

            $getCronData = Cron::where('type','1')
            ->where('status', 1) 
            // ->where('created_at', '>=', Carbon::today()->toDateString())
            ->groupBy(['user_id', 'leg'])
            ->orderBy('id','asc')
            ->get(); 
            // $getCronData = User::orderBy('id','asc')->get();


            echo "run";
            echo $getCronData;

            // echo $startOfDay;
            // echo $endOfDay;
            
            foreach ($getCronData as $gc) {
                    Log::info($gc);
                    Log::info('---');

                    $user1 = BvUserLog::where('user_id',$gc->user_id)
                    ->where('leg',$gc->leg)
                    ->where('position',$gc->position)
                    ->first();
                    if(!$user1){
                        $user1 = new BvUserLog();
                        $user1->user_id = $gc->user_id;
                        $user1->leg = $gc->leg;
                        $user1->position = $gc->position;
                        $user1->bv_log = 0;
                        $user1->save();
                    }
                    // echo $user1;
                    if($gc->position == 1){
                        $pos2 = 2;
                    }elseif($gc->position == 2){
                        $pos2 = 1;
                    }elseif ($gc->postition == 3) {
                        $pos2 = 4;
                    }elseif ($gc->postition == 4) {
                        $pos2 = 3;
                    }elseif ($gc->postition == 5) {
                        $pos2 = 6;
                    }else{
                        $pos2 = 5;

                    }
                    $user2 = BvUserLog::where('user_id',$gc->user_id)->where('leg',$gc->leg)->where('position', $pos2)->first();
                    if(!$user2){
                        $user2 = new BvUserLog();
                        $user2->user_id = $gc->user_id;
                        $user2->leg = $gc->leg;
                        $user2->position = $pos2;
                        $user2->bv_log = 0;
                        $user2->save();

                    }
                    $refs = User::where('id',$gc->user_id)->first();
                    $pinPrice = Pin::where('id',$refs->pin_id)->first();
                    // echo $refs;
                    Log::info($user2);
                    Log::info($user1);
                    
                    if($user2){
                        $updateCron = Cron::where('type','1')->where('status', 0)
                        ->where('leg', $gc->leg) 
                        ->where('user_id',$gc->user_id)
                        ->where('position',$gc->position)
                        ->update([
                            'status' => '1', 
                        ]);
                        $updateCron2 = Cron::where('type','1')->where('status', 0)
                        ->where('leg', $gc->leg) 
                        ->where('user_id',$gc->user_id)
                        ->where('position',$user2->position)
                        ->update([
                            'status' => '1', 
                        ]);
                        // Log::info($gc.'<br>');
                        if($refs->pin_id != 1){
                            if ($user2){
                                echo('do');
                                if($user1->bv_log > $user2->bv_log){
                                    $val= $user2->bv_log;
                                }elseif($user1->bv_log < $user2->bv_log){
                                    $val= $user1->bv_log;
                                }else{
                                    $val= $user1->bv_log;
                                }
                                // 5% x ( harga bv (1jt) x value pairing)
                                $totalPairing = 1000000*$val*(5/100);
                                if($totalPairing > $pinPrice->price){
                                    $totalPairing = $pinPrice->price;
                                } 
                                if($val !=0){
                                    Log::info('--'.$user1->user_id.'__'.$user2->user_id);
                                    Log::info('--'.$user1->leg.'__'.$user2->leg);
                                    Log::info('--'.$user1->position.'__'.$user2->position);
                                    Log::info('--'.$user1->bv_log.'__'.$user2->bv_log);
                                    Log::info('--'.$totalPairing);
                                    Log::info('--'.$val);
                                    Log::info('--');
                                    Log::info('paired');

                                    $bonusCouple = new BonusCouple(); 
                                    $bonusCouple->user_id =  $user1->user_id;
                                    $bonusCouple->bonus_val = $totalPairing;
                                    $bonusCouple->bonus_net = $totalPairing*(80/100);
                                    $bonusCouple->bonus_autosave = $totalPairing*(20/100); 
                                    $bonusCouple->desc = 'Bonus Couple Leg'.$gc->leg .' .Couple '.$user1->username .''.$user2->username;
                                    $bonusCouple->is_wd = 0; 
                                    $bonusCouple->save(); 
    
                                    // echo $bonusCouple;
                                    $bonusLog = new BonusLog();
                                    $bonusLog->user_id =  $user1->user_id; 
                                    $bonusLog->bonus_type =  2;
                                    $bonusLog->bonus_plan =  1;
                                    $bonusLog->bonus_val = $totalPairing;
                                    $bonusLog->bonus_net = $totalPairing*(80/100);
                                    $bonusLog->bonus_autosave = $totalPairing*(20/100);
                                    $bonusLog->desc = 'Bonus Couple Leg'.$gc->leg .' .Couple '.$user1->username .''.$user2->username;
                                    $bonusLog->is_wd = 0;
                                    $bonusLog->save();  

                                    $autosavelog = AutosaveLog::where('user_id',$gc->user_id)->first();
                                    if($autosavelog){
                                    $autosavelog->value += $bonusLog->bonus_autosave;
                                    $autosavelog->save();
                                    }else
                                    { 
                                        $save = new AutosaveLog();
                                        $save->user_id = $gc->user_id;
                                        $save->value =  $bonusLog->bonus_autosave;
                                        $save->save();
                                    } 
                                    $refBV = User::where('id',$gc->user_id)->with('pin')->first();
                                        if ($val > $refBV->pin->price){ 
                                            $bonusexceed = (10/100)*$refBV->pin->price;
                                            $bonusCoupleexceed = new BonusExceed(); 
                                            $bonusCoupleexceed->user_id =  $user1->id;
                                            $bonusCoupleexceed->bonus_val = $bonusexceed;
                                            $bonusCoupleexceed->bonus_net = $bonusexceed*(80/100);
                                            $bonusCoupleexceed->bonus_autosave = $bonusexceed*(20/100); 
                                            $bonusCoupleexceed->desc = 'Bonus Exceed Leg'. $gc->leg .' .Couple '.$user1->username .''.$user2->username;
                                            $bonusCoupleexceed->is_wd = 0; 
                                            $bonusCoupleexceed->created_at = $updateCron->created_at; 

                                            $bonusCoupleexceed->save();  

                                            $bonusLogexceed = new BonusLog();
                                            $bonusLogexceed->user_id =  $gc->user_id;
                                                                                
                                            $bonusLog->bonus_type =  3;
                                            $bonusLog->bonus_plan =  1;
                                            $bonusLogexceed->bonus_val = $totalPairing;
                                            $bonusLogexceed->bonus_net = $totalPairing*(80/100);
                                            $bonusLogexceed->bonus_autosave = $totalPairing*(20/100);
                                            $bonusLogexceed->desc = 'Bonus Exceed Leg'.$gc->leg;
                                            $bonusLogexceed->is_wd = 0;
                                            $bonusLogexceed->created_at = $updateCron->created_at;
                                            $bonusLogexceed->save(); 
                                            $autosavelog = AutosaveLog::where('user_id',$gc->user_id)->first();
                                            if($autosavelog){
                                            $autosavelog->value += $bonusCoupleexceed->bonus_autosave;
                                            $autosavelog->save();
                                            }else
                                            { 
                                                $save = new AutosaveLog();
                                                $save->user_id = $gc->user_id;
                                                $save->value =  $bonusLogexceed->bonus_autosave;
                                                $save->save();
                                            }
                                        }
                
                
                                
                                    $ids= $user1->update([
                                        'bv_log' => $user1->bv_log-$val, 
                                    ]);
                                    $ids2= $user2->update([
                                        'bv_log' => $user2->bv_log-$val, 
                                    ]);  
                                }
                            } 
                        }
                        
                    }
    
                    $updateCron4 = Cron::where('type','1')->where('status', 0)
                    ->where('leg', $gc->leg) 
                    ->where('user_id',$gc->user_id)
                    // ->where('position',$user2->position)
                    ->update([
                        'status' => '1', 
                    ]);
                    // exit;
            }
            # code...
        }

    }
}
