<?php

namespace App\Console\Commands;

use App\Models\BvUserLog;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class dorBv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:dorbv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $usercountBv
        $users = User::whereBetween('created_at', ['2023-04-02 00:00:00', now()->addSecond()])
        ->where('pin_id','>',1)->select('id')->get();
        $i=[];
        
        foreach($users as $us){
            $myfile=fopen("data-n-".date('m-d-Y').".txt", "a")or die("Unable to open file!");

            // $myfile="data-n-".date('m-d-Y').'.txt';
            $i[] = $us->id;
            $message = $us->id;
            fwrite($myfile, "\n". $message);
            fclose($myfile);
            // file_put_contents($myfile, $message, FILE_APPEND | LOCK_EX);
        }
        // print_r($i);
        echo(implode(',',$i));
        $useru = User::orderBy('id', 'asc')->whereNotIn('id',$i)->select('id')->get();
        foreach($useru as $uu){
            $myfile=fopen("data-o-".date('m-d-Y').".txt", "a")or die("Unable to open file!");
            $message = $uu->id;
            // file_put_contents($myfile, $message, FILE_APPEND | LOCK_EX);

            // $myfile = fopen("logs.txt", "a") or die("Unable to open file!");
            // $txt = "user id date";
            fwrite($myfile, "\n". $message);
            fclose($myfile);
        }
        // $x[] = $useru;
        // print_r(implode(',',$x));
        // echo($useru);
        foreach ($useru as $userAuth) {
            echo($userAuth);
            
            $ref = User::where('ref_id',$userAuth->id);
            $ref1 = $ref->where('position',1)->first(); 
            if($ref1){
                $refdata1 = getDownlineData($ref1->id)->sum(function ($product) {
                    return intval($product['bv']);
                })+ $ref1->bv; 
            }else{
                $refdata1 = 0; 
            }
            $updatebv1 = BvUserLog::where('position',1)->where('user_id',$userAuth->id)->update(['bv_log'=>$refdata1]); 
            $ref2 = User::with('children')->where('ref_id',$userAuth->id)->where('position',2)->first();
            if($ref2){
                $refdata2 = getDownlineData($ref2->id)->sum(function ($product) {
                    return intval($product['bv']);
                })+$ref2->bv;
            }else{
                $refdata2 =0;
            } 
            $updatebv2 = BvUserLog::where('position',2)->where('user_id',$userAuth->id)->update(['bv_log'=>$refdata2]);
 
            $ref3 = User::with('children')->where('ref_id',$userAuth->id)->where('position',3)->first();
            if($ref3!=null){
                $refdata3 = getDownlineData($ref3->id)->sum(function ($product) {
                    return intval($product['bv']);
                })+$ref3->bv;
            }else{
                $refdata3=0;
            }
            $updatebv3 = BvUserLog::where('position',3)->where('user_id',$userAuth->id)->update(['bv_log'=>$refdata3]);

            $ref4 = User::with('children')->where('ref_id',$userAuth->id)->where('position',4)->first();
            if($ref4!=null){
                $refdata4 = getDownlineData($ref4->id)->sum(function ($product) {
                    return intval($product['bv']);
                })+$ref4->bv;
            }else{
                $refdata4=0;
            }
            $updatebv4 = BvUserLog::where('position',4)->where('user_id',$userAuth->id)->update(['bv_log'=>$refdata4]);

            $ref5 = User::with('children')->where('ref_id',$userAuth->id)->where('position',5)->first();
            if($ref5){
                $refdata5 = getDownlineData($ref5->id)->sum(function ($product) {
                    return intval($product['bv']);
                })+$ref5->bv;
            }else{
                $refdata5=0;
            }
            $updatebv5 = BvUserLog::where('position',5)->where('user_id',$userAuth->id)->update(['bv_log'=>$refdata5]);

            $ref6 = User::with('children')->where('ref_id',$userAuth->id)->where('position',6)->first();
            if($ref6){
                $refdata6 = getDownlineData($ref6->id)->sum(function ($product) {
                    return intval($product['bv']);
                })+$ref6->bv;
            }else{
                $refdata6=0;
            }
            $updatebv6 = BvUserLog::where('position',6)->where('user_id',$userAuth->id)->update(['bv_log'=>$refdata6]);

        }

        $userZ = User::orderBy('id','desc')->whereNotIn('id',$i)->get();
        foreach ($userZ as $gc) {
            Log::info($gc);
            Log::info('---');

            $user1 = BvUserLog::where('user_id',$gc->id)
            ->where('leg',$gc->leg)
            ->where('position',$gc->position)
            ->first();
            if(!$user1){
                $user1 = new BvUserLog();
                $user1->user_id = $gc->id;
                $user1->leg = $gc->leg;
                $user1->position = $gc->position;
                $user1->bv_log = 0;
                $user1->save();
            } 
            if($gc->position == 1){
                $pos2 = 2;
            }elseif($gc->position == 2){
                $pos2 = 1;
            }elseif ($gc->postition == 3) {
                $pos2 = 4;
            }elseif ($gc->postition == 4) {
                $pos2 = 3;
            }elseif ($gc->postition == 5) {
                $pos2 = 6;
            }else{
                $pos2 = 5;

            }
            $user2 = BvUserLog::where('user_id',$gc->id)->where('leg',$gc->leg)->where('position', $pos2)->first();
            if(!$user2){
                $user2 = new BvUserLog();
                $user2->user_id = $gc->id;
                $user2->leg = $gc->leg;
                $user2->position = $pos2;
                $user2->bv_log = 0;
                $user2->save();

            }
            $refs = User::where('id',$gc->id)->first(); 
            Log::info($user2);
            Log::info($user1);
            
            if($user2){
                    
                // Log::info($gc.'<br>');
                if($refs->pin_id != 1){
                    if ($user2){
                        echo('do');
                        if($user1->bv_log > $user2->bv_log){
                            $val= $user2->bv_log;
                        }elseif($user1->bv_log < $user2->bv_log){
                            $val= $user1->bv_log;
                        }else{
                            $val= $user1->bv_log;
                        } 
                        $totalPairing = 1000000*$val*(5/100); 
                        if($val !=0){
                                            
                            $ids= $user1->update([
                                'bv_log' => $user1->bv_log-$val, 
                            ]);
                            $ids2= $user2->update([
                                'bv_log' => $user2->bv_log-$val, 
                            ]); 
        
            
                        }
                    } 
                }
                
            }
 
        }
            
    }
}
