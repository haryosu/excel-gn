<?php

namespace App\Constants;

abstract class BaseConstant
{
    abstract public static function labels(): array;

    public static function label($id = false): string
    {
        if ($id === false) {
            return '';
        }

        return static::labels()[$id] ?? '';
    }
}
