<?php

namespace App\Constants;

class BonusType extends BaseConstant
{
    const BONUS_REFERRAL = 1;
    const BONUS_COUPLE = 2;
    const BONUS_EXCEED = 3;
    const BONUS_GENERATION = 4;
    const BONUS_COUPLE_RANK = 5;
    const BONUS_PROFIT_SHARING = 6;
    
    const BONUS_RESHOPPING = 7;
    const BONUS_LEADERSHIP = 8;
    const BONUS_ROYALTY_RANK = 9;
    const BASIC_RESHOPPING = 10;

    public static function labels(): array
    {
        return [
            self::BONUS_REFERRAL => "Bonus Referral",
            self::BONUS_COUPLE => "Bonus Couple",
            self::BONUS_EXCEED => "Bonus Exceed",
            self::BONUS_GENERATION => "Bonus Generasi",
            self::BONUS_COUPLE_RANK => "Bonus Couple Ranking",
            self::BONUS_PROFIT_SHARING => "Bonus Profit Sharing",
            self::BONUS_RESHOPPING => "Bonus Reshopping",
            self::BONUS_LEADERSHIP => "Bonus Leadership",
            self::BONUS_ROYALTY_RANK => "Bonus Royalty Ranking",
            self::BASIC_RESHOPPING => "Bonus Basic Reshopping"
        ];
    }

}
