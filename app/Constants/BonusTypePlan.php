<?php

namespace App\Constants;

class BonusTypePlan extends BaseConstant
{
    const BONUS_PLAN_A = 1;
    const BONUS_PLAN_B = 2;

    public static function labels(): array
    {
        return [
            self::BONUS_PLAN_A => "Plan A",
            self::BONUS_PLAN_B => "Plan B",
        ];
    }

}
