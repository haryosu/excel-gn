<?php

namespace App\Constants;

class Is_PR extends BaseConstant
{
    const NO = 0;
    const YES = 1;

    public static function labels(): array
    {
        return [
            self::NO => "NO",
            self::YES => "YES",
        ];
    }

}
