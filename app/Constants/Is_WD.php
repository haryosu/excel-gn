<?php

namespace App\Constants;

class Is_WD extends BaseConstant
{
    const PENDING = 0;
    const SUCCESS = 1;

    public static function labels(): array
    {
        return [
            self::PENDING => "Pending",
            self::SUCCESS => "Success",
        ];
    }

    public static function labelHTML($status)
    {
    	switch ($status) {
    		case self::PENDING:
    			$html = '<span class="badge bg-outline-warning">Pending</span>';
    			break;
    		case self::SUCCESS:
    			$html = '<span class="badge bg-outline-success">Success</span>';
                break;
            default:
                $html = '-';
    	}

    	return $html;
    }

}
