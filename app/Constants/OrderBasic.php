<?php

namespace App\Constants;

class OrderBasic extends BaseConstant
{
    const NO = 0;
    const YES = 1; 

    public static function labels(): array
    {
        return [
            self::NO => "GENERAL",
            self::YES => "RESHOPPING BASIC", 
        ];
    }

    public static function labelHTML($status)
    {
    	switch ($status) {
    		case self::PENDING:
    			$html = '<span class="badge bg-outline-warning">GENERAL ORDER</span>';
    			break;
    		case self::SUCCESS:
    			$html = '<span class="badge bg-outline-success">RE-SHOPPING BASIC</span>';
                break; 
            default:
                $html = '-';
    	}

    	return $html;
    }

}
