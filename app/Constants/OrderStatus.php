<?php

namespace App\Constants;

class OrderStatus extends BaseConstant
{
    const PENDING = 0;
    const SUCCESS = 1;
    const FAILED = 2;
    const REJECTED = 3;

    public static function labels(): array
    {
        return [
            self::PENDING => "Pending",
            self::SUCCESS => "Success",
            self::FAILED => "Failed",
            self::REJECTED => "Rejected",
        ];
    }

    public static function labelHTML($status)
    {
    	switch ($status) {
    		case self::PENDING:
    			$html = '<span class="badge bg-outline-warning">Pending</span>';
    			break;
    		case self::SUCCESS:
    			$html = '<span class="badge bg-outline-success">Success</span>';
                break;
    		case self::FAILED:
    			$html = '<span class="badge bg-outline-danger">Failed</span>';
                break;
    		case self::REJECTED:
    			$html = '<span class="badge bg-outline-danger">Rejected</span>';
                break;
            default:
                $html = '-';
    	}

    	return $html;
    }

}
