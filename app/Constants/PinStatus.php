<?php

namespace App\Constants;

class PinStatus extends BaseConstant
{
    const ACTIVE = 1;
    const INACTIVE = 2;

    public static function labels(): array
    {
        return [
            self::ACTIVE => "Aktif",
            self::INACTIVE => "Tidak Aktif",
        ];
    }

    public static function html($status)
    {
    	switch ($status) {
    		case self::ACTIVE:
    			$html = '<span class="label label-finish">Aktif</span>';
    			break;
    		case self::INACTIVE:
    			$html = '<span class="label label-high">Tidak Aktif</span>';
                break;
            default:
                $html = '-';
    	}

    	return $html;
    }

}
