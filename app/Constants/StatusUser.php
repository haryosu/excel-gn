<?php

namespace App\Constants;

class StatusUser extends BaseConstant
{
    const BANNED = 0;
    const ACTIVE = 1;

    public static function labels(): array
    {
        return [
            self::BANNED => "BANNED",
            self::ACTIVE => "ACTIVE",
        ];
    }

    public static function labelHTML($status)
    {
    	switch ($status) {
    		case self::ACTIVE:
    			$html = '<span class="badge rounded-pill bg-success">Active</span>';
    			break;
    		case self::BANNED:
    			$html = '<span class="badge rounded-pill bg-danger">Banned</span>';
                break;
            default:
                $html = '-';
    	}

    	return $html;
    }

}
