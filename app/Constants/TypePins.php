<?php

namespace App\Constants;

class TypePins extends BaseConstant
{
    const BV = 1;
    const PR = 2;

    public static function labels(): array
    {
        return [
            self::BV => "BV",
            self::PR => "PR",
        ];
    }

}
