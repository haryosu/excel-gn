<?php

namespace App\Exports;

use App\Constants\Is_WD;
use App\Models\BonusLog;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings; 
class BonusExports implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'user_id',
            'total_bonus_val',
            'total_bonus_net',
            'total_bonus',
            'detail',
            'admin_charge',
            'total_transfer',
            'username',
            'email',
            'mobile',
            'bank_account',
            'bank_number',
            'bank_provider', 
        ];
    }
    public function collection()
    {
        //
        $getBonusLog = BonusLog::with('user')
        ->where('is_wd', Is_WD::PENDING) 
        ->groupBy('user_id')
        ->get(); 
        $getBonusLog->transform(function($i){
            $admincharge = 10000;
            $total_bonus_val = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_val');
            $total_bonus_net = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_net');
            $totalbonus = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_net');
            $totaltrnsfer = $totalbonus - $admincharge;
            return [
                'user_id' => $i->user_id,
                'total_bonus_val' => $total_bonus_val,
                'total_bonus_net' => $total_bonus_net,
                'total_bonus' => $totalbonus,
                'detail' => $i->where('user_id',$i->user_id )->get(),
                'admin_charge' => $admincharge,
                'total_transfer' => $totaltrnsfer,
                'username' => $i->user->username,
                'email' => $i->user->email,
                'mobile' => $i->user->mobile,
                'bank_account' => $i->user->bank_account,
                'bank_number' => $i->user->bank_number,
                'bank_provider' => $i->user->bank_provider,
                // 'bank_account' => $i->where('user_id',$i->user_id )->get()
            ];
        });
        
        $getBonusLogAll = $getBonusLog->where('total_bonus_val', '>=',50000);
        return $getBonusLogAll;
    }
}
