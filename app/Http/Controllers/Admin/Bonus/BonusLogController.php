<?php

namespace App\Http\Controllers\Admin\Bonus;

use App\Constants\BonusType;
use App\Constants\BonusTypePlan;
use App\Constants\Is_WD;
use App\Http\Controllers\Controller;
use App\Models\BonusLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BonusLogController extends Controller
{
    public function index(Request $request){
        $getBonusLog = BonusLog::with('user')
        // Filter
        ->when(isset($request->search), function($query) use($request){
            $query->whereHas('user', function($j) use($request){
                $j->where('username', 'like', '%'.$request->search.'%')->orWhere('firstname', 'like', '%'.$request->search.'%');
            });
        })
        // Filter
        ->select(DB::raw('DATE(updated_at) as updated_date'),'bonus_log.*')
        ->where('is_wd', Is_WD::SUCCESS)
        ->groupBy('user_id','updated_date')
        ->orderBy('updated_at','desc')
        ->paginate(10);
        $getBonusLog->getCollection()->transform(function($i){
            return [
                'updated_at' => $i->updated_at,
                'created_at' => $i->created_at,
                'user_id' => $i->user_id,
                'user_name' => $i->user->username,

                'bank_number' => $i->user->bank_number,
                'bank_name' => $i->user->bank_provider,
                'bank_provider' => $i->user->bank_provider,
                'bank_account' => $i->user->bank_account, 
                // 'total_bonus_val' => $totalbonus,

                // Nonus Net
                'total_bonus_net' => $i->where('user_id',$i->user_id )
                                        ->where('is_wd', Is_WD::SUCCESS)
                                        ->whereDate('updated_at', '=', $i->updated_at)
                                    ->sum('bonus_net'),
                'total_bonus_net_plan_a' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_A )
                                            ->where('is_wd', Is_WD::SUCCESS)
                                            ->whereDate('updated_at', '=', $i->updated_at)


                                            ->sum('bonus_net'),
                'total_bonus_net_plan_b' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_B )
                                            ->where('is_wd', Is_WD::SUCCESS)
                                            ->whereDate('updated_at', '=', $i->updated_at)


                                            ->sum('bonus_net'),
                // Nonus Net

                // Bonus Value
                'total_bonus_val' => $i->where('user_id',$i->user_id )
                                    ->where('is_wd', Is_WD::SUCCESS)
                                    ->whereDate('updated_at', '=', $i->updated_at)

                                    ->sum('bonus_val'),
                'total_bonus_val_plan_a' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_A )
                                            ->whereDate('updated_at', '=', $i->updated_at)

                                            ->where('is_wd', Is_WD::SUCCESS)
                                            ->sum('bonus_val'),
                'total_bonus_val_plan_b' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_B )
                                            ->whereDate('updated_at', '=', $i->updated_at)

                                            ->where('is_wd', Is_WD::SUCCESS)
                                            ->sum('bonus_val'),
                // Bonus Value

                'is_wd' => $i->is_wd,

                'detail' => BonusLog::groupBy('bonus_type')
                                ->where('is_wd', Is_WD::SUCCESS)
                                ->whereDate('updated_at', '=', $i->updated_at)
                                ->get()->map(function($b) use($i){
                
                    return[
                        'created_at' => $b->created_at,
                        'bonus_type_name' => BonusType::label($b->bonus_type),
                        'bonus_type' => $b->where('user_id',$i->user_id )
                                                ->where('is_wd', Is_WD::SUCCESS) 
                                                ->where('bonus_type', $b->bonus_type )->get()
                                                ->map(function($b){
                                                    return $b ;
                                                }),

                                                                                       
                        'total_val_bonus_type' => $b->where('user_id',$i->user_id )
                                                ->where('bonus_type',$b->bonus_type )
                                                // ->where('is_wd', Is_WD::PENDING)
                                                ->where('is_wd', Is_WD::SUCCESS) 
                                                ->whereDate('updated_at', '=', $i->updated_at)

                                                ->sum('bonus_val'),

                        'total_net_bonus_type' => $b->where('user_id',$i->user_id )
                                                    ->where('bonus_type',$b->bonus_type )
                                                    // ->where('is_wd', Is_WD::PENDING)
                                                ->where('is_wd', Is_WD::SUCCESS) 
                                                ->whereDate('updated_at', '=', $i->updated_at)

                                                ->sum('bonus_net'),
                        // 'total_val_bonus_type' => $b->where('user_id',$i->user_id )
                        //                         ->where('bonus_type',$b->bonus_plan )
                        //                         ->sum('bonus_val'),

                        // 'total_net_bonus_type' => $b->where('user_id',$i->user_id )
                        //                         ->where('bonus_type',$b->bonus_plan )
                        //                         ->sum('bonus_net')
                    ];
                })
            ];
        });
        $getBonusLogAll = $getBonusLog;
        // dd($getBonusLogAll);
        return view('adminv2.bonus.log.index',compact('getBonusLogAll'));
    }

    // public function index(){
    //     $bonusTypeA = BonusTypePlan::BONUS_PLAN_A;
    //     $bonusTypeB = BonusTypePlan::BONUS_PLAN_B;
        
    //     $getBonusLog = BonusLog::with('user')
    //     ->select('user_id', 
    //     DB::raw('count(*) as total'), 
    //     DB::raw('SUM(bonus_net) as bonus_net'),  
    //     DB::raw('SUM(bonus_val) as bonus_val'),
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeA' THEN bonus_net END) as bonus_net_plan_a"),
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeB' THEN bonus_net END) as bonus_net_plan_b"), 
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeA' THEN bonus_val END) as bonus_val_plan_a"),
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeB' THEN bonus_val END) as bonus_val_plan_b"), 
    //     'is_wd'
    //     )
    //     ->groupBy('user_id')->get();
    //     $getBonusLogAll = $getBonusLog->where('is_wd', Is_WD::SUCCESS);
    //     return view('adminv2.bonus.log.index', compact('getBonusLogAll'));
    // }
}
