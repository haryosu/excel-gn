<?php

namespace App\Http\Controllers\Admin\Bonus;

use App\Constants\BonusType;
use App\Constants\BonusTypePlan;
use App\Constants\Is_WD;
use App\Http\Controllers\Controller;
use App\Models\BonusLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BonusSummaryController extends Controller
{
    public function index(Request $request){
        $getBonusLog = BonusLog::with('user')
        // Filter
        ->when(isset($request->search), function($query) use($request){
            $query->whereHas('user', function($j) use($request){
                $j->where('username', 'like', '%'.$request->search.'%')->orWhere('firstname', 'like', '%'.$request->search.'%');
            });
            })
        // Filter
        ->groupBy('user_id')
        ->where('is_wd', Is_WD::PENDING)
        // ->whereBetween('created_at',[Carbon::yesterday(),Carbon::now()->subDays(8)])
        // ->whereDate('created_at', Carbon::now()->subDays(7))
        // ->orderBy('created_at', 'desc')
        ->paginate(10);
        $getBonusLog->getCollection()->transform(function($i){
            return [
                'user_id' => $i->user_id,
                'created_at' => $i->created_at,
                'user_name' => $i->user->username,
                'bank_account' => $i->user->bank_account,
                'bank_number' => $i->user->bank_number,
                'bank_provider' => $i->user->bank_provider,


                'total_bonus_net' => $i->where('user_id',$i->user_id )
                                ->where('is_wd', Is_WD::PENDING)
                                ->sum('bonus_net'),
                'total_bonus_net_plan_a' => $i->where('user_id',$i->user_id )
                                        ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_A )
                                        ->where('is_wd', Is_WD::PENDING)
                                        ->sum('bonus_net'),
                'total_bonus_net_plan_b' => $i->where('user_id',$i->user_id )
                                        ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_B )
                                        ->where('is_wd', Is_WD::PENDING)
                                        ->sum('bonus_net'),

                // // Nonus Net

                // 'total_bonus_net' => $i->where('user_id',$i->user_id )->sum('bonus_net'),
                // 'total_bonus_net_plan_a' => $i->where('user_id',$i->user_id )
                //                             ->where('bonus_type',BonusTypePlan::BONUS_PLAN_A )
                //                             ->where('is_wd', Is_WD::PENDING)

                //                             ->sum('bonus_net'),
                // 'total_bonus_net_plan_b' => $i->where('user_id',$i->user_id )
                //                             ->where('bonus_type',BonusTypePlan::BONUS_PLAN_B )
                //                             ->where('is_wd', Is_WD::PENDING)
                //                             ->sum('bonus_net'),
                // Nonus Net

                // Bonus Value
                'total_bonus_val' => $i->where('user_id',$i->user_id )
                ->where('is_wd', Is_WD::PENDING)
                ->sum('bonus_val'),
                'total_bonus_val_plan_a' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_A )
                                            ->where('is_wd', Is_WD::PENDING)
                                            ->sum('bonus_val'),
                'total_bonus_val_plan_b' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_B )
                                            ->where('is_wd', Is_WD::PENDING)
                                            ->sum('bonus_val'),
                // // Bonus Value

                'is_wd' => $i->is_wd,

                'detail' => BonusLog::groupBy('bonus_type')
                            ->where('is_wd', Is_WD::PENDING)
                            ->get()->map(function($b) use($i){
                    return[
                        'bonus_type_name' => BonusType::label($b->bonus_type ),
                        'bonus_type' => $b->where('user_id',$i->user_id )
                                                ->where('bonus_type', $b->bonus_type )
                                                ->where('is_wd', Is_WD::PENDING)
                                                ->get()
                                                ->map(function($b){
                                                    return $b ;
                                                }), 
                        'total_val_bonus_type' => $b->where('user_id',$i->user_id )
                                                ->where('bonus_type',$b->bonus_type )
                                                ->where('is_wd', Is_WD::PENDING) 

                                                ->sum('bonus_val'),

                        'total_net_bonus_type' => $b->where('user_id',$i->user_id )
                                                ->where('bonus_type',$b->bonus_type )
                                                ->where('is_wd', Is_WD::PENDING) 

                                                ->sum('bonus_net')
                    ];
                })
            ];
        });
        $getBonusLogAll = $getBonusLog;
        // dd($getBonusLogAll);
        return view('adminv2.bonus.summary.index',compact('getBonusLogAll'));
    }

    // public function index(Request $request){


    //     $bonusTypeA = BonusTypePlan::BONUS_PLAN_A;
    //     $bonusTypeB = BonusTypePlan::BONUS_PLAN_B;
        
    //     $getBonusLogAll = BonusLog::with('user')
    //     ->select('id','user_id', 
    //     DB::raw('count(*) as total'), 
    //     DB::raw('SUM(bonus_net) as bonus_net'),  
    //     DB::raw('SUM(bonus_val) as bonus_val'),
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeA' THEN bonus_net END) as bonus_net_plan_a"),
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeB' THEN bonus_net END) as bonus_net_plan_b"), 
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeA' THEN bonus_val END) as bonus_val_plan_a"),
    //     DB::raw("SUM(CASE WHEN bonus_type = '$bonusTypeB' THEN bonus_val END) as bonus_val_plan_b"), 
    //     'is_wd'
    //     )
    //     ->groupBy('user_id')
    //     // Filter
    //     ->when(isset($request->search), function($query) use($request){
    //             $query->where('user_id', $request->search);
    //         })
    //     // Filter
    //     ->paginate(10);
    //     return view('adminv2.bonus.summary.index' , compact('getBonusLogAll'));
    // }
}
