<?php

namespace App\Http\Controllers\Admin\Bonus;

use App\Constants\BonusType;
use App\Constants\BonusTypePlan;
use App\Constants\Is_WD;
use App\Exports\BonusExports;
use App\Http\Controllers\Controller;
use App\Models\BonusBasicReshopping;
use App\Models\BonusCouple;
use App\Models\BonusCoupleRank;
use App\Models\BonusExceed;
use App\Models\BonusGeneration;
use App\Models\BonusLeadership;
use App\Models\BonusLog;
use App\Models\BonusProfitSharing;
use App\Models\BonusReferral;
use App\Models\BonusRoyaltyRank;
use App\Models\BonusTransferLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use League\Csv\CharsetConverter;
use Maatwebsite\Excel\Facades\Excel;
class BonusTransferController extends Controller
{
    public function index(Request $request){
        $getBonusLog = BonusLog::with('user')
        // Filter
        ->when(isset($request->search), function($query) use($request){
                $query->whereHas('user', function($j) use($request){
                    $j->where('username', 'like', '%'.$request->search.'%')->orWhere('firstname', 'like', '%'.$request->search.'%');
                });
            })
        // Filter
        ->where('is_wd', Is_WD::PENDING)
        // ->whereBetween('created_at',[Carbon::yesterday(),Carbon::now()->subDays(8)])
        // ->whereDate('created_at', Carbon::now()->subDays(7)) 
        ->groupBy('user_id')
        ->get();
        // \dd($getBonusLog);
        $getBonusLog->transform(function($i){
            $admincharge = 10000; 
            $totalbonus = $i->where('is_wd', Is_WD::PENDING)
            // ->whereDate('created_at', Carbon::now()->subDays(7))
            ->where('user_id',$i->user_id )->sum('bonus_net');
            $totaltrnsfer = $totalbonus - $admincharge;
            return [
                // 'id' => $i->id,
                'created_at' => $i->created_at,
                'user_id' => $i->user_id,
                'user_name' => $i->user->username,

                'bank_number' => $i->user->bank_number,
                'bank_name' => $i->user->bank_provider,
                'bank_provider' => $i->user->bank_provider,
                'bank_account' => $i->user->bank_account,

                
                'admin_charge' => $admincharge,
                'total_transfer' => $totaltrnsfer,
                // 'total_bonus_val' => $totalbonus,

                // Nonus Net
                'total_bonus_net' => $i->where('user_id',$i->user_id )
                                    ->where('is_wd', Is_WD::PENDING)
                                    ->sum('bonus_net'),
                'total_bonus_net_plan_a' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_A )
                                            ->where('is_wd', Is_WD::PENDING)
                                            ->sum('bonus_net'),
                'total_bonus_net_plan_b' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_B )
                                            ->where('is_wd', Is_WD::PENDING)
                                            ->sum('bonus_net'),
                // Nonus Net

                // Bonus Value
                'total_bonus_val' => $i->where('user_id',$i->user_id )
                                    ->where('is_wd', Is_WD::PENDING)
                                    ->sum('bonus_val'),
                'total_bonus_val_plan_a' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_A )
                                            ->where('is_wd', Is_WD::PENDING)
                                            ->sum('bonus_val'),
                'total_bonus_val_plan_b' => $i->where('user_id',$i->user_id )
                                            ->where('bonus_plan',BonusTypePlan::BONUS_PLAN_B )
                                            ->where('is_wd', Is_WD::PENDING)
                                            ->sum('bonus_val'),
                // Bonus Value

                'is_wd' => $i->is_wd,

                // 'detail' => BonusLog::groupBy('bonus_type')
                //                 ->where('is_wd', Is_WD::PENDING)
                //                 ->get()->map(function($b) use($i){
                
                //     return[
                //         // 'created_at' => BonusType::label($b->bonus_type),
                //         'bonus_type_name' => BonusType::label($b->bonus_type),
                //         'bonus_type' => $b->where('user_id',$i->user_id )
                //                                 ->where('is_wd', Is_WD::PENDING) 
                //                                 ->where('bonus_type', $b->bonus_type )->get()
                //                                 ->map(function($b){
                //                                     return $b ;
                //                                 }),

                                                                                       
                //         'total_val_bonus_type' => $b->where('user_id',$i->user_id )
                //                                 ->where('bonus_type',$b->bonus_type )
                //                                 // ->where('is_wd', Is_WD::PENDING)
                //                                 ->where('is_wd', Is_WD::PENDING) 

                //                                 ->sum('bonus_val'),

                //         'total_net_bonus_type' => $b->where('user_id',$i->user_id )
                //                                     ->where('bonus_type',$b->bonus_type )
                //                                     // ->where('is_wd', Is_WD::PENDING)
                //                                 ->where('is_wd', Is_WD::PENDING) 

                //                                 ->sum('bonus_net'),
                //         // 'total_val_bonus_type' => $b->where('user_id',$i->user_id )
                //         //                         ->where('bonus_type',$b->bonus_plan )
                //         //                         ->sum('bonus_val'),

                //         // 'total_net_bonus_type' => $b->where('user_id',$i->user_id )
                //         //                         ->where('bonus_type',$b->bonus_plan )
                //         //                         ->sum('bonus_net')
                //     ];
                // })
            ];
        });
        // $perPage = $request->input('per_page', 10);
        $getBonusLogAll = $getBonusLog->where('total_bonus_val', '>=',50000)->paginate(10);
        // $getBonusLogAll->simplePaginate(10);
        // dd($getBonusLogAll);
        return view('adminv2.bonus.transfer.index',compact('getBonusLogAll'));
    }
    public function transferBonus(Request $request){
        try {
            $datartransfer = [];
            // $tfr = DB::transaction(function () use ($request) {
                $ids = $request->ids;
                // \dd($ids);
                $bonuslogs = BonusLog::whereIn('user_id',explode(",",$ids))
                ->where('is_wd',0)
                ->groupBy('user_id')

                ->get();
                // $bLog[] = $ids; 
                //code...
                // $userCollect = [];
                // $bonuslogs = BonusLog::where('user_id',$b)->get();
                    $bonuslogs->transform(function($i){
                        $admincharge = 10000; 
                        $totalbonus = $i->where('is_wd', Is_WD::PENDING)
                        // ->whereDate('created_at', Carbon::now()->subDays(7))
                        ->where('user_id',$i->user_id )->sum('bonus_net');
                        $totaltrnsfer = $totalbonus - $admincharge;
                        return [
                            // 'id' => $i->id,
                            'created_at' => $i->created_at,
                            'user_id' => $i->user_id,
                            'user_name' => $i->user->username,
            
                            'bank_number' => $i->user->bank_number,
                            'bank_name' => $i->user->bank_provider,
                            'bank_provider' => $i->user->bank_provider,
                            'bank_account' => $i->user->bank_account,
            
                            
                            'admin_charge' => $admincharge,
                            'total_transfer' => $totaltrnsfer,  
                            'total_bonus_net' => $i->where('user_id',$i->user_id )
                                                ->where('is_wd', Is_WD::PENDING)
                                                ->sum('bonus_net'), 
                            'total_bonus_val' => $i->where('user_id',$i->user_id )
                                                ->where('is_wd', Is_WD::PENDING)
                                                ->sum('bonus_val'), 
            
                            'is_wd' => $i->is_wd,
             
                        ];
                    });

                    $datartransfer = $bonuslogs->toArray();

                    foreach($datartransfer as $dttf){
                        // $datartransfer = $dttf->user_id;
                        $bonuslogsave = new BonusTransferLog();
                        $bonuslogsave->user_id = $dttf['user_id'];
                        $bonuslogsave->total_bonus_val = $dttf['total_bonus_net'];
                        // $bonuslogsave->total_bonus_net = $value['total_bonus_net'];
                        $bonuslogsave->admin_charge =10000;
                        $bonuslogsave->total_transfer = $dttf['total_transfer'];
                        $bonuslogsave->detail = BonusLog::where('user_id',$dttf['user_id'])->where('is_wd', Is_WD::PENDING)->get()??'';
                        $bonuslogsave->is_tf = 1;
                        $bonuslogsave->save();
                        $manipulatebonus = BonusLog::where('user_id',$dttf['user_id'])->where('is_wd', Is_WD::PENDING)->get();
                        foreach($manipulatebonus as $bonuslog){           
                                if($bonuslog->bonus_type == 1){
                                    $upd= BonusReferral::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 2){
                                    $upd= BonusCouple::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 3){
                                    $upd= BonusExceed::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 4){
                                    $upd= BonusGeneration::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 5){
                                    $upd= BonusCoupleRank::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 6){
                                    $upd= BonusProfitSharing::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 7){
                                    $upd= BonusLeadership::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 8){
                                    $upd= BonusLeadership::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 9){
                                    $upd= BonusRoyaltyRank::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                                if($bonuslog->bonus_type == 10){
                                    $upd= BonusBasicReshopping::
                                    where('user_id',$bonuslog->user_id)
                                    ->where('bonus_val',$bonuslog->bonus_val)
                                    ->where('is_wd',0)
                                    ->first();
                                    $upd = $upd->update(['is_wd'=>1]);
                                }
                            $update =$bonuslog->update(['is_wd'=>1]);
                        }
                    } 
            // });   
            // return response()->json(['success'=> $bLog ]);
            DB::commit();
            return response()->json([
                'code' => 200,
                'message' => "Transfer bonus Success", 
                'msg'=> $bonuslogs
            ]);
            // return $tfr;

        } catch (\Throwable $th) {
            DB::rollback();

            return response()->json([
                'code' => 500,
                'message' => $th->getMessage(),
            ]);
        }
    }
    public function update(){
        $getBonusLog = BonusLog::with('user')
        ->where('is_wd', Is_WD::PENDING)
        // ->whereDate('created_at', Carbon::now()->subDays(7))
        ->groupBy('user_id')
        ->paginate(100); 
        $getBonusLog->getCollection()->transform(function($i){
            $admincharge = 10000;
            $total_bonus_val = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_val');
            $total_bonus_net = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_net');
            $totalbonus = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_net');
            $totaltrnsfer = $totalbonus - $admincharge;
            return [
                'user_id' => $i->user_id,
                'total_bonus_val' => $total_bonus_val,
                'total_bonus_net' => $total_bonus_net,
                'total_bonus' => $totalbonus,
                'detail' => $i->where('user_id',$i->user_id )->get(),
                'admin_charge' => $admincharge,
                'total_transfer' => $totaltrnsfer,
                'username' => $i->user->username,
                'email' => $i->user->email,
                'mobile' => $i->user->mobile,
                'bank_account' => $i->user->bank_account,
                'bank_number' => $i->user->bank_number,
                'bank_provider' => $i->user->bank_provider,
                // 'bank_account' => $i->where('user_id',$i->user_id )->get()
            ];
        });
        $getBonusLogAll = $getBonusLog->where('total_bonus_val', '>=',50000);
        // \dd($getBonusLogAll);

        if (!$getBonusLogAll->isEmpty()) {
            foreach ($getBonusLogAll as $key => $value) {
                foreach ($value['detail'] as $key2 => $data) {
                    $data->update(['is_wd'=> Is_WD::SUCCESS]);
                }
                $bonuslogsave = new BonusTransferLog();
                $bonuslogsave->user_id = $value['user_id'];
                $bonuslogsave->total_bonus_val = $value['total_bonus_val'];
                // $bonuslogsave->total_bonus_net = $value['total_bonus_net'];
                $bonuslogsave->admin_charge = $value['admin_charge'];
                $bonuslogsave->total_transfer = $value['total_transfer'];
                $bonuslogsave->detail = $value['detail'];
                $bonuslogsave->is_tf = 1;
                $bonuslogsave->save();
                // \dd($bonuslogsave);
            }
                  // Retrieve the data from the table(s)
            // $data = DB::table('users')->select('id', 'name', 'email')->get()->toArray();
            
            // Convert the data to CSV format
            $csv = \League\Csv\Writer::createFromString();
            $csv->insertOne(['username','email','phone','total_bonus_val','total_bonus_net','admin_charge','total_transfer', 'bank_account', 'bank_number','bank_provider']);
            foreach ($getBonusLogAll as $row) {
                $csv->insertOne([$row['username'], $row['email'], $row['mobile'], $row['total_bonus_val'],$row['total_bonus_net'], $row['admin_charge'],$row['total_transfer'],$row['bank_account'], $row['bank_number'],$row['bank_provider']]);
            } 
            $csvData = $csv->toString();
             
            $filename="BonusTransferExcel'.now().'.csv";
            Storage::put($filename, $csvData);
            // Set the HTTP headers to indicate a CSV download
            $headers = [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="BonusTransferExcel'.now().'.csv"', 
            ];
            response()->streamDownload(function() use ($csvData) {
                echo $csvData;
            }, $filename, $headers); 
            $notify[] = ['success', 'Berhasil Merubah data transfer, silahkan cek download csv untuk detail transfer'];

            
             
            Session::flash('notify', $notify); 
            return Response::make($csv->toString(), 200, $headers);
        }else{
            $notify[] = ['error', 'Tidak dapat melakukan transfer. Pastikan ada list data'];

        }
        return back()->withNotify($notify);
    }
    public function exportCsv(){
        $getBonusLog = BonusLog::with('user')
        ->where('is_wd', Is_WD::PENDING) 
        ->groupBy('user_id')
        ->get(); 
        $getBonusLog->transform(function($i){
            $admincharge = 10000;
            $total_bonus_val = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_val');
            $total_bonus_net = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_net');
            $totalbonus = $i->where('user_id',$i->user_id )->where('is_wd', Is_WD::PENDING)->sum('bonus_net');
            $totaltrnsfer = $totalbonus - $admincharge;
            return [
                'user_id' => $i->user_id,
                'total_bonus_val' => $total_bonus_val,
                'total_bonus_net' => $total_bonus_net,
                'total_bonus' => $totalbonus,
                'detail' => $i->where('user_id',$i->user_id )->get(),
                'admin_charge' => $admincharge,
                'total_transfer' => $totaltrnsfer,
                'username' => $i->user->username,
                'email' => $i->user->email,
                'mobile' => $i->user->mobile,
                'bank_account' => $i->user->bank_account,
                'bank_number' => $i->user->bank_number,
                'bank_provider' => $i->user->bank_provider,
                // 'bank_account' => $i->where('user_id',$i->user_id )->get()
            ];
        });
        $getBonusLogAll = $getBonusLog->where('total_bonus_val', '>=',50000);
        // \dd($getBonusLogAll);
 
 
        // Convert the data to CSV format
        $csv = \League\Csv\Writer::createFromString();
        $csv->insertOne(['username','email','phone','total_bonus_val','total_bonus_net','admin_charge','total_transfer', 'bank_account', 'bank_number','bank_provider']);
        foreach ($getBonusLogAll as $row) {
            $csv->insertOne([$row['username'], $row['email'], $row['mobile'], $row['total_bonus_val'],$row['total_bonus_net'], $row['admin_charge'],$row['total_transfer'],$row['bank_account'], $row['bank_number'],$row['bank_provider']]);
        } 
        $csvData = $csv->toString();
            
        $filename='BonusTransferExcel'.now().'.csv';
        // Storage::put($filename, $csvData);
        // Set the HTTP headers to indicate a CSV download
        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="BonusTransferExcel'.now().'.csv"', 
        ]; 
        // Session::flash('notify', $notify); 
        $response = Response::make($csv->toString(), 200, $headers);
        $response = response($csv->toString(), 200, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        ]);

        return $response;
 
    }
    public function Excelprint(){
        $filename='BonusTransferExcel'.now().'.xlsx';
        return Excel::download(new BonusExports, $filename);
    }

    
}
