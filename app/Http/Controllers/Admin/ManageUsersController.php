<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AutosaveLog;
use App\Models\Bonus\ReferalBonus;
use App\Models\BonusBasicReshopping;
use App\Models\BonusLog;
use App\Models\BvLog;
use App\Models\BvUserLog;
use App\Models\Cron;
use App\Models\Deposit;
use App\Models\EmailLog;
use App\Models\Gateway;
use App\Models\GeneralSetting;
use App\Models\Pin;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserExtra;
use App\Models\UserPin;
use App\Models\Withdrawal;
use App\Models\WithdrawMethod;
use App\Rules\FileTypeValidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class ManageUsersController extends Controller
{
    public function allUsers(Request $request)
    {
        $pageTitle = 'Manage Users';
        $emptyMessage = 'No user found';
        $users = User::orderBy('id', 'asc')->with('pin','managerial','BvUserLog')
        // Filter
            // ->when(isset($request->search), function($query) use($request){
            //     $query->where('firstname', 'like', '%'.$request->search.'%');
            // })
            ->when(isset($request->search), function($query) use($request){
                $query->where('username', 'like', '%'.$request->search.'%')
                ->orWhere('firstname', 'like', '%'.$request->search.'%');
            })
            // ->orWhen(isset($request->search), function($query) use($request){
            // })
        // Filter
        ->paginate(getPaginate());
        // \dd($users);
        // return response()->json($users);
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function activeUsers()
    {
        $pageTitle = 'Manage Active Users';
        $emptyMessage = 'No active user found';
        $users = User::active()->orderBy('id', 'desc')->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function bannedUsers()
    {
        $pageTitle = 'Banned Users';
        $emptyMessage = 'No banned user found';
        $users = User::banned()->orderBy('id', 'desc')->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function emailUnverifiedUsers()
    {
        $pageTitle = 'Email Unverified Users';
        $emptyMessage = 'No email unverified user found';
        $users = User::emailUnverified()->orderBy('id', 'desc')->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }
    public function emailVerifiedUsers()
    {
        $pageTitle = 'Email Verified Users';
        $emptyMessage = 'No email verified user found';
        $users = User::emailVerified()->orderBy('id', 'desc')->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function smsUnverifiedUsers()
    {
        $pageTitle = 'SMS Unverified Users';
        $emptyMessage = 'No sms unverified user found';
        $users = User::smsUnverified()->orderBy('id', 'desc')->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function smsVerifiedUsers()
    {
        $pageTitle = 'SMS Verified Users';
        $emptyMessage = 'No sms verified user found';
        $users = User::smsVerified()->orderBy('id', 'desc')->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function usersWithBalance()
    {
        $pageTitle = 'Users with balance';
        $emptyMessage = 'No sms verified user found';
        $users = User::where('balance', '!=', 0)->orderBy('id', 'desc')->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function search(Request $request, $scope)
    {
        $search = $request->search;
        $users = User::where(function ($user) use ($search) {
            $user->where('username', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
        });
        $pageTitle = '';
        if ($scope == 'active') {
            $pageTitle = 'Active ';
            $users = $users->where('status', 1);
        } elseif ($scope == 'banned') {
            $pageTitle = 'Banned';
            $users = $users->where('status', 0);
        } elseif ($scope == 'emailUnverified') {
            $pageTitle = 'Email Unverified ';
            $users = $users->where('ev', 0);
        } elseif ($scope == 'smsUnverified') {
            $pageTitle = 'SMS Unverified ';
            $users = $users->where('sv', 0);
        } elseif ($scope == 'withBalance') {
            $pageTitle = 'With Balance ';
            $users = $users->where('balance', '!=', 0);
        }

        $users = $users->paginate(getPaginate());
        $pageTitle .= 'User Search - ' . $search;
        $emptyMessage = 'No search result found';
        return view('adminv2.users.list', compact('pageTitle', 'search', 'scope', 'emptyMessage', 'users'));
    }

    public function detail($id)
    {
        $pageTitle = 'User Detail';
        $user = User::with('children','parent')->findOrFail($id);
        $BonusTotal = BonusLog::where('user_id', $user->id)->sum('bonus_val');
        $totalWithdraw = BonusLog::where('user_id', $user->id)->where('is_wd', 1)->sum('bonus_val');
        $totalAutosave = BonusLog::where('user_id', $user->id)->where('is_wd', 1)->sum('bonus_autosave');
        $totalTransaction = Transaction::where('user_id', $user->id)->count();
        
        $countries = json_decode(file_get_contents(resource_path('views/partials/country.json')));
        // $totalBvCut = BvUserLog::where('user_id', $user->id)->where('trx_type', '-')->sum('amount');
        $ref_id = User::find($user->ref_id);
        // return \response()->json($user->upline());
        return view('adminv2.users.detail', compact('pageTitle', 'user', 'BonusTotal', 'totalWithdraw', 'totalTransaction', 'countries','ref_id'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $countryData = json_decode(file_get_contents(resource_path('views/partials/country.json')));

        $request->validate([
            'username' => 'required|unique:users,username,' .$user->id,
            // 'nik' => 'required|digits:16|integer|unique:users,nik,' .$user->id,
            'firstname' => 'required|max:50',
            // 'lastname' => 'required|max:50',
            // 'email' => 'required|email|max:90|unique:users,email,' . $user->id,
            // 'mobile' => 'required|unique:users,mobile,' . $user->id,
            // 'manager_id' => 'required',
            // 'gender' => 'required',
            // 'country' => 'required',
            'image' => ['image',new FileTypeValidate(['jpg','jpeg','png'])],
            'img_nik' => ['image',new FileTypeValidate(['jpg','jpeg','png'])]
        ]);
        $countryCode = $request->country;
        $user->mobile = $request->mobile;
        $user->country_code = $countryCode;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->nik = $request->nik;
        $user->gender = $request->gender;
        $user->manager_id = $request->manager_id;
        $user->bank_provider = $request->bank_provider;
        $user->bank_account = $request->bank_account;
        $user->bank_number = $request->bank_number;
        $user->address = [
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'country' => @$countryData->$countryCode->country,
        ];

        // Upload Image
        if ($request->hasFile('image')) {
            $location = imagePath()['profile']['user']['path'];
            $size = imagePath()['profile']['user']['size'];
            $filename = uploadImage($request->image, $location, $size, $user->image);
            $user->image = $filename;
        }
        
        if ($request->hasFile('img_nik')) {
            $location = imagePath()['profile']['user']['path'];
            $size = imagePath()['profile']['user']['size'];
            $filename = uploadImage($request->img_nik, $location, $size, $user->img_nik);
            $user->img_nik = $filename;
        }
        // Upload Image

        $user->status = $request->status ? 1 : 0;
        $user->ev = $request->ev ? 1 : 0;
        $user->sv = $request->sv ? 1 : 0;
        $user->ts = $request->ts ? 1 : 0;
        $user->tv = $request->tv ? 1 : 0;
        $user->save();

        $notify[] = ['success', 'User detail has been updated'];
        return redirect()->back()->withNotify($notify);
    }

    public function updatePassword(Request $request, $id)
    {
        $password_validation = Password::min(6);
        $general = GeneralSetting::first();
        if ($general->secure_password) {
            $password_validation = $password_validation->mixedCase()->numbers()->symbols()->uncompromised();
        }

        $this->validate($request, [
            // 'current_password' => 'required',
            'password' => ['required','confirmed',$password_validation]
        ]);


        try {
            $user = User::findOrFail($id);
            $password = Hash::make($request->password);
            $user->password = $password;
            $user->save();
            $notify[] = ['success', 'Password changes successfully.'];
            return back()->withNotify($notify);
            // if (Hash::check($request->current_password, $user->password)) {
            // } else {
            //     $notify[] = ['error', 'The password doesn\'t match!'];
            //     return back()->withNotify($notify);
            // }
        } catch (\PDOException $e) {
            $notify[] = ['error', $e->getMessage()];
            return back()->withNotify($notify);
        }
    }

    public function addSubBalance(Request $request, $id)
    {
        $request->validate(['amount' => 'required|numeric|gt:0']);

        $user = User::findOrFail($id);
        $amount = $request->amount;
        $general = GeneralSetting::first(['cur_text', 'cur_sym']);
        $trx = getTrx();

        if ($request->act) {
            $user->balance += $amount;
            $user->save();
            $notify[] = ['success', $general->cur_sym . $amount . ' has been added to ' . $user->username . '\'s balance'];

            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->amount = $amount;
            $transaction->post_balance = $user->balance;
            $transaction->charge = 0;
            $transaction->trx_type = '+';
            $transaction->details = 'Added Balance Via Admin';
            $transaction->trx = $trx;
            $transaction->save();

            notify($user, 'BAL_ADD', [
                'trx' => $trx,
                'amount' => showAmount($amount),
                'currency' => $general->cur_text,
                'post_balance' => showAmount($user->balance),
            ]);
        } else {
            if ($amount > $user->balance) {
                $notify[] = ['error', $user->username . '\'s has insufficient balance.'];
                return back()->withNotify($notify);
            }
            $user->balance -= $amount;
            $user->save();

            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->amount = $amount;
            $transaction->post_balance = $user->balance;
            $transaction->charge = 0;
            $transaction->trx_type = '-';
            $transaction->details = 'Subtract Balance Via Admin';
            $transaction->trx = $trx;
            $transaction->save();

            notify($user, 'BAL_SUB', [
                'trx' => $trx,
                'amount' => showAmount($amount),
                'currency' => $general->cur_text,
                'post_balance' => showAmount($user->balance),
            ]);
            $notify[] = ['success', $general->cur_sym . $amount . ' has been subtracted from ' . $user->username . '\'s balance'];
        }
        return back()->withNotify($notify);
    }

    public function userLoginHistory($id)
    {
        $user = User::findOrFail($id);
        $pageTitle = 'User Login History - ' . $user->username;
        $emptyMessage = 'No users login found.';
        $login_logs = $user->login_logs()->orderBy('id', 'desc')->with('user')->paginate(getPaginate());
        return view('adminv2.users.logins', compact('pageTitle', 'emptyMessage', 'login_logs'));
    }

    public function showEmailSingleForm($id)
    {
        $user = User::findOrFail($id);
        $pageTitle = 'Send Email To: ' . $user->username;
        return view('adminv2.users.email_single', compact('pageTitle', 'user'));
    }

    public function sendEmailSingle(Request $request, $id)
    {
        $request->validate([
            'message' => 'required|string|max:65000',
            'subject' => 'required|string|max:190',
        ]);

        $user = User::findOrFail($id);
        sendGeneralEmail($user->email, $request->subject, $request->message, $user->username);
        $notify[] = ['success', $user->username . ' will receive an email shortly.'];
        return back()->withNotify($notify);
    }

    public function transactions(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($request->search) {
            $search = $request->search;
            $pageTitle = 'Search User Transactions : ' . $user->username;
            $transactions = $user->transactions()->where('trx', $search)->with('user')->orderBy('id', 'desc')->paginate(getPaginate());
            $emptyMessage = 'No transactions';
            return view('adminv2.reports.transactions', compact('pageTitle', 'search', 'user', 'transactions', 'emptyMessage'));
        }
        $pageTitle = 'User Transactions : ' . $user->username;
        $transactions = $user->transactions()->with('user')->orderBy('id', 'desc')->paginate(getPaginate());
        $emptyMessage = 'No transactions';
        return view('adminv2.reports.transactions', compact('pageTitle', 'user', 'transactions', 'emptyMessage'));
    }

    public function deposits(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $userId = $user->id;
        if ($request->search) {
            $search = $request->search;
            $pageTitle = 'Search User Deposits : ' . $user->username;
            $deposits = $user->deposits()->where('trx', $search)->orderBy('id', 'desc')->paginate(getPaginate());
            $emptyMessage = 'No deposits';
            return view('adminv2.deposit.log', compact('pageTitle', 'search', 'user', 'deposits', 'emptyMessage', 'userId'));
        }

        $pageTitle = 'User Deposit : ' . $user->username;
        $deposits = $user->deposits()->orderBy('id', 'desc')->with(['gateway', 'user'])->paginate(getPaginate());
        $successful = $user->deposits()->orderBy('id', 'desc')->where('status', 1)->sum('amount');
        $pending = $user->deposits()->orderBy('id', 'desc')->where('status', 2)->sum('amount');
        $rejected = $user->deposits()->orderBy('id', 'desc')->where('status', 3)->sum('amount');
        $emptyMessage = 'No deposits';
        $scope = 'all';
        return view('adminv2.deposit.log', compact('pageTitle', 'user', 'deposits', 'emptyMessage', 'userId', 'scope', 'successful', 'pending', 'rejected'));
    }

 

    public function withdrawals(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($request->search) {
            $search = $request->search;
            $pageTitle = 'Search User Withdrawals : ' . $user->username;
            $withdrawals = $user->withdrawals()->where('trx', 'like', "%$search%")->orderBy('id', 'desc')->paginate(getPaginate());
            $emptyMessage = 'No withdrawals';
            return view('adminv2.withdraw.withdrawals', compact('pageTitle', 'user', 'search', 'withdrawals', 'emptyMessage'));
        }
        $pageTitle = 'User Withdrawals : ' . $user->username;
        $withdrawals = $user->withdrawals()->orderBy('id', 'desc')->paginate(getPaginate());
        $emptyMessage = 'No withdrawals';
        $userId = $user->id;
        return view('adminv2.withdraw.withdrawals', compact('pageTitle', 'user', 'withdrawals', 'emptyMessage', 'userId'));
    }

    public function withdrawalsViaMethod($method, $type, $userId)
    {
        $method = WithdrawMethod::findOrFail($method);
        $user = User::findOrFail($userId);
        if ($type == 'approved') {
            $pageTitle = 'Approved Withdrawal of ' . $user->username . ' Via ' . $method->name;
            $withdrawals = Withdrawal::where('status', 1)->where('user_id', $user->id)->with(['user', 'method'])->orderBy('id', 'desc')->paginate(getPaginate());
        } elseif ($type == 'rejected') {
            $pageTitle = 'Rejected Withdrawals of ' . $user->username . ' Via ' . $method->name;
            $withdrawals = Withdrawal::where('status', 3)->where('user_id', $user->id)->with(['user', 'method'])->orderBy('id', 'desc')->paginate(getPaginate());
        } elseif ($type == 'pending') {
            $pageTitle = 'Pending Withdrawals of ' . $user->username . ' Via ' . $method->name;
            $withdrawals = Withdrawal::where('status', 2)->where('user_id', $user->id)->with(['user', 'method'])->orderBy('id', 'desc')->paginate(getPaginate());
        } else {
            $pageTitle = 'Withdrawals of ' . $user->username . ' Via ' . $method->name;
            $withdrawals = Withdrawal::where('status', '!=', 0)->where('user_id', $user->id)->with(['user', 'method'])->orderBy('id', 'desc')->paginate(getPaginate());
        }
        $emptyMessage = 'Withdraw Log Not Found';
        return view('adminv2.withdraw.withdrawals', compact('pageTitle', 'withdrawals', 'emptyMessage', 'method'));
    }

    public function showEmailAllForm()
    {
        $pageTitle = 'Send Email To All Users';
        return view('adminv2.users.email_all', compact('pageTitle'));
    }

    public function sendEmailAll(Request $request)
    {
        $request->validate([
            'message' => 'required|string|max:65000',
            'subject' => 'required|string|max:190',
        ]);

        foreach (User::where('status', 1)->cursor() as $user) {
            sendGeneralEmail($user->email, $request->subject, $request->message, $user->username);
        }

        $notify[] = ['success', 'All users will receive an email shortly.'];
        return back()->withNotify($notify);
    }

    public function login($id)
    {
        $user = User::findOrFail($id);
        Auth::login($user);
        return redirect()->route('member.dashboard');
    }

    public function emailLog($id)
    {
        $user = User::findOrFail($id);
        $pageTitle = 'Email log of ' . $user->username;
        $logs = EmailLog::where('user_id', $id)->with('user')->orderBy('id', 'desc')->paginate(getPaginate());
        $emptyMessage = 'No data found';
        return view('adminv2.users.email_log', compact('pageTitle', 'logs', 'emptyMessage', 'user'));
    }

    public function emailDetails($id)
    {
        $email = EmailLog::findOrFail($id);
        $pageTitle = 'Email details';
        return view('adminv2.users.email_details', compact('pageTitle', 'email'));
    }

    public function userRef($id)
    {
        $emptyMessage = 'No user found';
        $user = User::findOrFail($id);
        $pageTitle = 'Referred By ' . $user->username;
        $users = User::where('ref_id', $id)->latest()->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function tree($username)
    {

        $user = User::where('username', $username)->first();

        if ($user) {
            $tree = showTreePage($user->id);
            $treeNew = showTreePageNew($user->id);
            $pageTitle = "Tree of " . $user->fullname;
            $userRef = ['id' => Crypt::encrypt($user->id), 'name' => $user->fullname, 'countRef' => User::where('ref_id', $user->id)->count()];
            $userPin = UserPin::with('pin')->where('user_id',$user->id)->where('qty','>=',1)->get();
            $area = \Indonesia::allProvinces();
            return view('adminv2.users.tree', compact('tree', 'treeNew', 'pageTitle','userPin', 'userRef', 'area'));
        }

        $notify[] = ['error', 'Tree Not Found!!'];
        return redirect()->route('admin.dashboard')->withNotify($notify);
    }

    public function registeredAsRefferal(Request $req, $idRef)
    {
        try {
            $regUser = DB::transaction(function () use ($req, $idRef) {

                $validate = FacadesValidator::make($req->all(), [
                    'firstname' => 'required|string|max:50',
                    'lastname' => 'required|string|max:50',
                    'email' => 'required|string|email|max:90',
                    'mobile' => 'required|string|max:50',
                    'password' => 'required',
                    'username' => 'required|alpha_num|unique:users|min:6',
                    'gender' => 'required:not_in:0',
                    'nik' => 'required|max:16',
                ]);

                if ($validate->fails()) {
                    $errors = $validate->errors();

                    $mess = [
                        'message' => $errors,
                        'code' => 500,
                    ];
                    return response()->json($mess);
                }

                $getIDUser = User::find(Crypt::decrypt($idRef));
                if (!empty($getIDUser)) {
                    $general = GeneralSetting::first();
                    $getLastPos = User::where('ref_id', $getIDUser->id);
                    $latestPost = $getLastPos->latest()->first()->position ?? 0;
                    $getCountDownline = $getLastPos->count();

                    if ($latestPost + 1 > 6) {
                        $mess = [
                            'message' => [
                                0 => "Kamu tidak diperbolehkan menambahkan atau meregistrasikan user lagi, max 6 User saja!.",
                            ],
                            'code' => 500,
                        ];
                        return response()->json($mess);
                    }
                    
                    $getPin = Pin::where('id',$req->pin_id)->first(); 
                    // return \response()->json($getPin);

                    $userReff = new User;
                    $userReff->ref_id = $getIDUser->id;
                    $userReff->pin_id = $req->pin_id; 
                    $userReff->bv   = $getPin->bv??0;
                    $userReff->pr   = $getPin->pr??0;
                    $userReff->pos_id = $getIDUser->pos_id + 1; //in_array($latestPost, [1, 3, 5]) ? 1 : 2;
                    $userReff->position = $getCountDownline + 1;
                    if($getCountDownline + 1 == 1 || $getCountDownline + 1 == 2){
                        $userReff->leg = 1; 
                    } 
                    if($getCountDownline + 1 == 3 || $getCountDownline + 1 == 4){
                        $userReff->leg = 2; 
                    }
                    if($getCountDownline + 1 == 5 || $getCountDownline + 1 == 6){
                        $userReff->leg = 3; 
                    }
                    $userReff->user_gen = ($getIDUser->id) .','.implode(',',getUplineData($getIDUser->id));
                    $userReff->firstname = isset($req['firstname']) ? $req['firstname'] : null;
                    $userReff->lastname = isset($req['lastname']) ? $req['lastname'] : null;
                    $userReff->email = strtolower(trim($req['email']));
                    $userReff->bank_account = isset($req['bank_account']) ? $req['bank_account'] : "BCA";
                    $userReff->bank_number = isset($req['bank_number']) ? $req['bank_number'] : null;
                    $userReff->bank_provider = isset($req['bank_name']) ? $req['bank_name'] : null;
                    $userReff->password = Hash::make($req['password']);
                    $userReff->username = trim($req['username']);
                    $userReff->country_code = "ID";
                    $userReff->mobile = $req['mobile'];
                    $userReff->identity = json_encode([
                        'nik' => $req->nik,
                        'gender' => $req->gender,
                        'dob' => $req->dob,
                        'pob' => $req->pob,
                    ], true);

                    $userReff->nik=$req['nik'];
                    $userReff->gender=$req['gender'];
                    $userReff->dob=$req['dob'];
                    $userReff->pob=$req['pob'];

                    $userReff->address = json_encode([ 
                        'address' => $req->address,
                        'province_id' => $req->province_id,
                        'city_id' => $req->city_id,
                        'district_id' => $req->district_id,
                        'postal' => $req->zip,
                    ], true); 

                    $userReff->status = 1;
                    $userReff->ev = $general->ev ? 0 : 1;
                    $userReff->sv = $general->sv ? 0 : 1;
                    $userReff->ts = 0;
                    $userReff->tv = 1;
                    $userReff->save();
                    /* remove user pin stock of referral */
                        $Refpin = UserPin::where('user_id',$getIDUser->id)->where('pin_id',$req->pin_id)->first();
                        $Refpin->qty = $Refpin['qty'] - 1;
                        $Refpin->save();  
                    /* remove user pin stock of referral */
                    $getPin = Pin::where('id',$userReff['pin_id'])->first();

                    if($getPin->id != 1){

                        $refBonus = new ReferalBonus();
                        $refBonus->user_id = $getIDUser->id;
                        $refBonus->sender_id = $userReff->id;
                        $refBonus->bonus_val = $getPin->ref_bonus;
                        $refBonus->bonus_net =(80/100) * $getPin->ref_bonus;
                        $refBonus->bonus_autosave =(20/100) * $getPin->ref_bonus;
                        $refBonus->desc = 'Referal Bonus From '.$userReff->username.' to '.$getIDUser->username.' on Leg '.$userReff->leg;
                        $refBonus->save();

                        $bonusLog = new BonusLog(); 
                        $bonusLog->bonus_type =  1;
                        $bonusLog->bonus_plan =  1;
                        $bonusLog->user_id = $getIDUser->id; 
                        $bonusLog->bonus_val = $getPin->ref_bonus;
                        $bonusLog->bonus_net =(80/100) * $getPin->ref_bonus;
                        $bonusLog->bonus_autosave =(20/100) * $getPin->ref_bonus;
                        $bonusLog->desc = 'Referal Bonus From '.$userReff->username.' to '.$getIDUser->username.' on Leg '.$userReff->leg;
                        $bonusLog->is_wd = 0;
                        $bonusLog->save(); 

                        $autosavelog = AutosaveLog::where('user_id',$getIDUser->id)->first();
                        if($autosavelog){
                        $autosavelog->value += $bonusLog->bonus_autosave;
                        $autosavelog->save();
                        }else
                        { 
                            $save = new AutosaveLog();
                            $save->user_id = $getIDUser->id;
                            $save->value =  $bonusLog->bonus_autosave;
                            $save->save();
                        } 
                        if ($getPin->bv != 0) {
                            // $parent = User::find($userReff->id);
                            /* generate omzet by leg */ 
                            updateBvRecursive($getIDUser->id,$userReff->id, $getPin->bv, $userReff->position,$userReff->leg); 
                            /* bonus cron a */
                            \BonusCron($userReff->id,1,$userReff->created_at); 
                            /* bonus cron a */ 
                        }
                        \generateReferralUser($userReff->id); 
                    }
                    if($getPin->id === 1){
                        $user = User::where('id',$userReff->id)->first(); 
                        if($user->ref_id !== 0){
                            $arrs = getUplineData($user->id);
                            // $upline = 
                            Log::info(implode(',',$arrs)); 
                            // \dd(implode(',',$arrs));
                            $data = []; 
                            // $manager = $user->pin_id; 
                            $dm = []; 
                            $total = 10000;
                            $count = 0;
                            foreach( $arrs as $r){ 
                                $ud = User::where('id',$r)->first(); 
                                // if($ud->pin_id === 1 ){  
                                    Log::info('user basic'); 

                                    $data[] = $ud->id;
                                    $dm[] = $ud->manager_id;

                                    // Log::info($ud->managerial->name??'no-label manager'); 
                                    // Log::info($ud->id.'-'.$ud->managerial->name??'no-label manager'); 

                                    $BonusGeneration = new BonusBasicReshopping(); 
                                    $BonusGeneration->user_id =  $ud->id;
                                    $BonusGeneration->bonus_val = $total;
                                    $BonusGeneration->bonus_net = $total;
                                    $BonusGeneration->bonus_autosave = 0; 
                                    $BonusGeneration->desc = 'Bonus Basic Register dari '.$userReff->username;
                                    $BonusGeneration->is_wd = 0; 
                                    $BonusGeneration->save();  

                                    // echo $bonusCouple;
                                    $bonusLog = new BonusLog();
                                    $bonusLog->user_id = $ud->id;
                                    $bonusLog->bonus_type =  10;
                                    $bonusLog->bonus_plan =  1;
                                    $bonusLog->bonus_val = $total;
                                    $bonusLog->bonus_net = $total;
                                    $bonusLog->bonus_autosave = 0;
                                    $bonusLog->desc = 'Bonus Basic Register dari '.$userReff->username;
                                    $bonusLog->is_wd = 0;
                                    $bonusLog->save();  
                        
                                    // $autosavelog = AutosaveLog::where('user_id',$ud->id)->first();
                                    // if($autosavelog){
                                    // $autosavelog->value += $bonusLog->bonus_autosave;
                                    // $autosavelog->save();
                                    // }else
                                    // { 
                                    //     $save = new AutosaveLog();
                                    //     $save->user_id = $ud->id;
                                    //     $save->value =  $bonusLog->bonus_autosave;
                                    //     $save->save();
                                    // }  
                                if($count == 10){
                                    break;
                                }
                                $count++;
                                // } else{
                                //     Log::info('no user basic'); 
                                // }
                            }
                        } 
                    }
                    
        
                }
            });
            
            DB::commit();

            return response()->json([
                'code' => 200,
                'message' => "Inserting data Success",
            ]);

            return $regUser;
        } catch (\Throwable $th) {
            DB::rollback();

            return response()->json([
                'code' => 500,
                'message' => $th->getMessage(),
            ]);
        }
    } 
 
    // private function BonusCron($u,$type)
    // {       
    //         $user = User::where('id',$u)->first();

    //         $cron = new Cron(); 
    //         $cron->user_id = $user->id;
    //         $cron->position = $user->position ; 
    //         $cron->leg = $user->leg ;  
    //         $cron->ref = $user->ref_id ;
    //         $cron->type = $type ;
    //         $cron->status = 'pending' ; 
    //         $cron->save();  
    
    //         if ($user->ref_id != 0) {
    //            return BonusCron($user->ref_id,'a');
    //         }

            
    // }
    private function generateReferralUser($referralUserId) {
    
    
        $user = User::with('members')->find($referralUserId);
    
        // Get the number of members the user has
        $numMembers = $user->members->count();
        // \dd($numMembers);
        // Update the user's status based on the number of members they have
        if ($numMembers == 6) {
            $members = $user->members;
    
            // Check if all members have the same status
            $allSameStatus = $members->every(function ($member) use ($user) {
                return $member->manager_id == $user->manager_id;
            }); 
            if ($allSameStatus) {
                if ($user->manager_id == 0 || $user->manager_id ==null ) {
                    $user->manager_id = 1;
                }
                if ($user->manager_id == 1) {
                    $user->manager_id = 2;
                }
                if ($user->manager_id == 2) {
                    $user->manager_id = 3;
                }
                if ($user->manager_id == 3) {
                    $user->manager_id = 4;
                }
                if ($user->manager_id == 4) {
                    $user->manager_id = 5;
                }
                $user->save();
            // Add more conditions for higher ranks as necessary
            }
        }
    
        // Check if the user has a referrer
        if ($user->ref_id != 0) {
            // Update the referrer's status recursively
            return generateReferralUser($user->parent->id);
        }
    }
    // private function updateBvRecursive($user, $bv, $pos)
    // { 
        
    //     $u = User::where(['id' => $user])->first();   
    //     $bvLog = BvUserLog::where('user_id', $u->id)
    //         ->where('position', $pos)
    //         ->where('leg', $u->leg)
    //         ->first();   

    //     if ($bvLog !== null) {
    //         $bvLog->update(
    //             [
    //                 'bv_log' => $bvLog->bv+$bv  
    //             ]
    //         );
    //     } else {
    //         $bvLog = BvUserLog::create([ 
    //             'user_id' => $u->id ,
    //             'position' => $pos,
    //             'leg' => $u->leg,
    //             'bv' => $bv,
    //             ]
    //         );
    //     }  
    //     if ($u->ref_id != 0) {
    //         $this->updateBvRecursive($u->ref_id, $bv, $u->position); 
    //     }
    // }
    public function detailArea($area, $id)
    {
        try {
            switch ($area) {
                case 'city':
                    try {
                        return response()->json([
                            'code' => 200,
                            'message' => "OK",
                            'data' => \Indonesia::findProvince($id, ['cities']),

                        ])->getContent();
                    } catch (\Throwable $th) {
                        return response()->json([
                            'code' => 500,
                            'message' => $th->getMessage(),
                        ]);
                    }

                    break;

                case 'district':
                    try {
                        return response()->json([
                            'code' => 200,
                            'message' => "OK",
                            'data' => \Indonesia::findCity($id, ['districts']),

                        ])->getContent();
                    } catch (\Throwable $th) {
                        return response()->json([
                            'code' => 500,
                            'message' => $th->getMessage(),
                        ]);
                    }

                    break;

                default:
                    # code...
                    break;
            }
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function otherTree(Request $request, $username = null)
    {
        $search = $request->username;
        if ($request->username) {
            $user = User::where(function ($user) use ($search) {
                $user->where('username', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%");
            })->first();
            // $user = User::where('username', $request->username)->first();
        } else {
            $user = User::where('username', $username)->first();
        }

        if ($user) {
            $tree = showTreePage($user->id);
            $pageTitle = "Tree of " . $user->fullname;

            $treeNew = showTreePageNew($user->id);
            $userRef = ['id' => Crypt::encrypt($user->id), 'name' => $user->fullname, 'countRef' => User::where('ref_id', $user->id)->count()];
            $area = \Indonesia::allProvinces();
            $userPin = UserPin::with('pin')->where('user_id',$user->id)->where('qty','>=',1)->get();
            return view('adminv2.users.tree', compact('tree', 'pageTitle', 'treeNew','userPin', 'userRef', 'area'));
        }

        $notify[] = ['error', 'Tree Not Found!!'];
        return \back()->withNotify($notify);
        // return redirect()->route('admin.dashboard')->withNotify($notify);
    }
}