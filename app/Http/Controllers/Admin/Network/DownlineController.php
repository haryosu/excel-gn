<?php

namespace App\Http\Controllers\Admin\Network;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class DownlineController extends Controller
{
    //
    public function index(){
        $login_user = Auth()->id();
        $downline =  User::where('id',1)->first();
        $downlines = $this->getDownline($downline);
        // \dd($downlines);
        // $getOrderPinHistory = OrderPin::with('user')

        // // Filter
        //     ->when(isset($request->search), function($query) use($request){
        //         $query->where('invoice', 'like', '%'.$request->search.'%');
        //     })
        // // Filter

        // ->where('user_id',$login_user)
        // ->paginate(10);
        return view('member.pin.history.index', compact('getOrderPinHistory'));
    }
    private function getDownline($user)
    {
        $downline = collect([]);

        foreach ($user->children as $child) {
            $downline->push($child);

            if ($child->children->count() > 0) {
                $grandchildren = $this->getDownline($child);
                $downline = $downline->merge($grandchildren);
            }
        }

        return $downline;
    }
}
