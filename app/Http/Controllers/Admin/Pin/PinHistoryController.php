<?php

namespace App\Http\Controllers\Admin\Pin;

use App\Constants\OrderStatus;
use App\Http\Controllers\Controller;
use App\Models\AutosaveLog;
use App\Models\BonusBasicReshopping;
use App\Models\BonusLog;
use App\Models\OrderPin;
use App\Models\OrderPinDetail;
use App\Models\Pin;
use App\Models\User;
use App\Models\UserPin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PinHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $getOrderPinHistory = OrderPin::with('user')->orderBy('id', 'desc')

        // Filter
            ->when(isset($request->search), function($query) use($request){
                $query->where('invoice', 'like', '%'.$request->search.'%');
            })
        // Filter

        // ->where('user_id',$login_user)
        ->paginate(10);
        return view('adminv2.pin.history.index', compact('getOrderPinHistory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $login_user = User::where('id',$id)->first();
        $getOrderPinHistory = OrderPin::with('orderPinDetail.pin')->find($id);

        return view('adminv2.pin.history.show', compact('getOrderPinHistory', 'login_user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approvePin($id){
        $getOrderPinHistory =  OrderPin::where('id', $id)->orderBy('id','desc')->first();
        $PinOrder = $getOrderPinHistory->first();
        $pinUpdate = $getOrderPinHistory->update(['status' => OrderStatus::SUCCESS]); 
        $user = User::where('id',$getOrderPinHistory->user_id)->first();
        if($getOrderPinHistory->is_basic === 1){
            if($user->ref_id !== 0){
                $arrs = getUplineData($user->id);
                // $upline = 
                Log::info(implode(',',$arrs)); 
                // \dd(implode(',',$arrs));
                $data = []; 
                // $manager = $user->pin_id; 
                $dm = []; 
                $total = 10000;
                $count = 0;
                foreach( $arrs as $r){
                    
                    $ud = User::where('id',$r)->first(); 
                    // if($ud->pin_id === 1 ){  
                        Log::info('user basic'); 

                        $data[] = $ud->id;
                        $dm[] = $ud->manager_id;

                        // Log::info($ud->managerial->name??'no-label manager'); 
                        // Log::info($ud->id.'-'.$ud->managerial->name??'no-label manager'); 

                        $BonusGeneration = new BonusBasicReshopping(); 
                        $BonusGeneration->user_id =  $ud->id;
                        $BonusGeneration->bonus_val = $total;
                        $BonusGeneration->bonus_net = $total;
                        $BonusGeneration->bonus_autosave = 0; 
                        $BonusGeneration->desc = 'Bonus Basic Reshopping dari '.$user->username;
                        $BonusGeneration->is_wd = 0; 
                        $BonusGeneration->save();  

                        // echo $bonusCouple;
                        $bonusLog = new BonusLog();
                        $bonusLog->user_id = $ud->id;
                        $bonusLog->bonus_type =  10;
                        $bonusLog->bonus_plan =  1;
                        $bonusLog->bonus_val = $total;
                        $bonusLog->bonus_net = $total;
                        $bonusLog->bonus_autosave = 0;
                        $bonusLog->desc = 'Bonus Basic Reshopping dari '.$user->username;
                        $bonusLog->is_wd = 0;
                        $bonusLog->save();  
            
                        // $autosavelog = AutosaveLog::where('user_id',$ud->id)->first();
                        // if($autosavelog){
                        // $autosavelog->value += $bonusLog->bonus_autosave;
                        // $autosavelog->save();
                        // }else
                        // { 
                        //     $save = new AutosaveLog();
                        //     $save->user_id = $ud->id;
                        //     $save->value =  $bonusLog->bonus_autosave;
                        //     $save->save();
                        // }  
                    if($count == 9){
                        break;
                    }
                    $count++;
                    // } else{
                    //     Log::info('no user basic'); 
                    // }
                }
            }
            $notify[] = ['success', 'Order Aproved. Data Bonus Generated'];
        }
        else{
            $pinDetail = OrderPinDetail::where('order_id',$id)->get();
            foreach($pinDetail as $pd){
                $pins = Pin::where('id',$pd->pin_id)->first();
                if($pins->is_pr==0){
                    $userPin = UserPin::where('user_id',$getOrderPinHistory->user_id)
                    ->where('pin_id',$pd->pin_id)->first();
                    if(!$userPin){ 
                        $x = new UserPin();
                        $x->user_id = $getOrderPinHistory->user_id;
                        $x->pin_id = $pd->pin_id;
                        $x->qty =$pd->qty;
                        $x->save();
                    }else{
                        $userPin->qty = $userPin->qty + $pd->qty;
                        $userPin->save();
                    } 
                }  
            } 
            $notify[] = ['success', 'Order Aproved. '];
        }
        // $message = 'Updated';
        // return view(admin)
        // return response()->json(['message' => 'UserPin updated successfully.']);
        return redirect()->route('admin.pin.history.index')->withNotify($notify);
    }

    public function rejectedPin($id){
        $getOrderPinHistory =  OrderPin::find( $id);
        
        if ($getOrderPinHistory->images_tf) {
            // Delete Image
                $location = 'assets/images/user/order-recipe';
                $old = $getOrderPinHistory->images_tf;
                removeFile($location . '/' . $old);
                removeFile($location . '/thumb_' . $old);
            // Delete Image

            // Update Order History
                $getOrderPinHistory->update(
                    [
                        'images_tf' => null,
                        'status' => OrderStatus::REJECTED
                    ]
                );
            // Update Order History

            $notify[] = ['success', 'Order berhasil ditolak. '];
        }else{
            $notify[] = ['error', 'Member belum mengirim bukti pembayaran'];

        }
        return redirect()->back()->withNotify($notify);
    }

    public function approve(Request $request){
                //
        $getOrderPinHistory = OrderPin::with('user')->orderBy('id', 'desc')->where('status','0')

        // Filter
            ->when(isset($request->search), function($query) use($request){
                $query->where('invoice', 'like', '%'.$request->search.'%');
            })
        // Filter

        // ->where('user_id',$login_user)
        ->paginate(10);
        return view('adminv2.pin.history.index', compact('getOrderPinHistory'));
    }
}
