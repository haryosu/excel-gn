<?php

namespace App\Http\Controllers\Admin\Pin;

use App\Constants\PinStatus;
use App\Constants\OrderStatus;
use App\Constants\TypePins;
use App\Http\Controllers\Controller;
use App\Models\OrderPin;
use App\Models\OrderPinDetail;
use App\Models\Pin;
use App\Models\User;
use App\Models\UserPin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PinTransferController extends Controller
{
    public function index()
    {

        $products = Pin::where('status', PinStatus::ACTIVE)->where('type', TypePins::BV)->get();

        $items = \Cart::session(Auth()->guard('admin')->user()->id)->getContent();
        

        if (\Cart::isEmpty()) {
            $cart_data = [];
        } else {
            foreach ($items as $row) {
                $cart[] = [
                    'rowId' => $row->id,
                    'name' => $row->name,
                    'stock' => $row->quantity,
                    'pricesingle' => $row->price,
                    'price' => $row->getPriceSum(),
                    'created_at' => $row->attributes['created_at'],
                ];
            }
            $cart_data = collect($cart)->sortBy('created_at');
        }
        
        //total
        $total = \Cart::session(Auth()->guard('admin')->user()->id)->getSubTotal();

        $data_total = [
            'total' => $total,
        ];

        return view('adminv2.pin.transfer.index', compact('products', 'cart_data', 'data_total'));

    }

    public function additem(Request $request, $id)
    {
        $getPinProduct = Pin::find($id);

        $cart = \Cart::session(Auth()->guard('admin')->user()->id)->getContent();

        $cek_itemId = $cart->whereIn('id', $id);

        if ($cek_itemId->isNotEmpty()) {
                \Cart::session(Auth()->guard('admin')->user()->id)->remove($id);
                \Cart::session(Auth()->guard('admin')->user()->id)->add(array(
                'id' => $id,
                'name' => $getPinProduct->name,
                'price' => $getPinProduct->price,
                'quantity' => $request->itemQuantity,
                'attributes' => array(
                    'created_at' => date('Y-m-d H:i:s')
                )
            ));
        } else {

            \Cart::session(Auth()->guard('admin')->user()->id)->add(array(
                'id' => $id,
                'name' => $getPinProduct->name,
                'price' => $getPinProduct->price,
                'quantity' => $request->itemQuantity,
                'attributes' => array(
                    'created_at' => date('Y-m-d H:i:s')
                )
            ));

        }

        return redirect()->back();
    }

    public function removeitem($id)
    {
        \Cart::session(Auth()->guard('admin')->user()->id)->remove($id);

        return redirect()->back();
    }

    public function clear()
    {
        \Cart::session(Auth()->guard('admin')->user()->id)->clear();
        return redirect()->back();
    }

    public function transferToMember(Request $request)
    {

        $request->validate([
            'member_id' => 'required|integer',
        ]);
        
        $in['member_id'] = $request->member_id;

        //  Get Total Item Cart
            $cart_total = \Cart::session(Auth()->guard('admin')->user()->id)->getSubTotal();
        //  Get Total Item Cart
        if ($cart_total != 0) {
            DB::beginTransaction();
            try {

                $transferToUserId = $request->member_id;
                
                //  Get Semua Item Cart
                    $all_cart = \Cart::session(Auth()->guard('admin')->user()->id)->getContent();
                    
                    $filterCart = $all_cart->map(function ($item) {
                        return [
                            'id' => $item->id,
                            'price' => $item->price,
                            'quantity' => $item->quantity
                        ];
                    });
                //  Get Semua Item Cart
                
                
                // Creata Order
                    $createOrder = OrderPin::create([
                        'user_id' => $transferToUserId,
                        'total' => $cart_total + rand(100, 999),
                        'status' => OrderStatus::SUCCESS,
                    ]);
                // Creata Order

                // Generate Invoice
                    $date = date("dmy");
                    $counterNumber = $createOrder->id;
                    $invoiceNumber =  'inv'.'-'.$date.'-'. $counterNumber;
                    OrderPin::find($createOrder->id)->update([
                        'invoice' => $invoiceNumber,
                    ]);
                // Generate Invoice

                // Creata Order Detail
                    foreach ($filterCart as $item) {
                        OrderPinDetail::create([
                            'order_id' => $createOrder->id,
                            'pin_id' => $item['id'],
                            'qty' => $item['quantity'],
                            'subtotal' => $item['price']
                        ]);
                    }
                // Creata Order Detail


                // Add To User Pins
                    $pinDetail = OrderPinDetail::where('order_id',$createOrder->id)->get();
                    foreach($pinDetail as $pd){
                        $pins = Pin::where('id',$pd->pin_id)->first();
                        if($pins->is_pr==0){
                            $userPin = UserPin::where('user_id',$transferToUserId)
                            ->where('pin_id',$pd->pin_id)->first();
                            if(!$userPin){ 
                                $x = new UserPin();
                                $x->user_id = $transferToUserId;
                                $x->pin_id = $pd->pin_id;
                                $x->qty =$pd->qty;
                                $x->save();
                            }else{
                                $userPin->qty = $userPin->qty + $pd->qty;
                                $userPin->save();
                            }
                        } else{
                            $userPoint = User::where('id', $transferToUserId)->first(); 
                            $userPoint->pr = $userPoint->pr + $pd->pin->pr;
                            $userPoint->save();
                        }  
                    } 
                // Add To User Pins

                \Cart::session(Auth()->guard('admin')->user()->id)->clear();
                DB::commit();
                $notify[] = ['success', 'Transfer Pin Melalui Admin Berhasil dilakukan'];
                return redirect()->route('admin.pin.history.index')->withNotify($notify);
            } catch (\Exeception $e) {
                DB::rollback();
                $notify[] = ['error', 'Terjadi kesalahan sistem. Silahkan hubungi admin'];
                return redirect()->back()->withNotify($notify);
            }
        } 
        $notify[] = ['error', 'Transfer Pin gagal. pastikan anda sudah memilih item.'];
        return redirect()->back()->withNotify($notify);
    }
}
