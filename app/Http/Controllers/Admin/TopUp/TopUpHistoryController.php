<?php

namespace App\Http\Controllers\Admin\TopUp;

use App\Constants\OrderStatus;
use App\Constants\PinStatus;
use App\Http\Controllers\Controller;
use App\Models\OrderPr;
use App\Models\OrderPrDetail;
use App\Models\Pin;
use App\Models\User;
use App\Rules\FileTypeValidate;
use Illuminate\Http\Request;

class TopUpHistoryController extends Controller
{
    public function index(Request $request)
    {
        $login_user = Auth()->id();
        $getOrderPrHistory = OrderPr::with('user')->orderBy('id', 'desc')

        // Filter
            ->when(isset($request->search), function($query) use($request){
                $query->where('invoice', 'like', '%'.$request->search.'%');
            })
        // Filter

        // ->where('user_id',$login_user)
        ->paginate(10);
        return view('adminv2.top-up.history.index', compact('getOrderPrHistory'));

    }
    public function show($id)
    {
        $getOrderPrHistory = OrderPr::with('orderPrDetail.pin')->find($id);
        $login_user = User::find($getOrderPrHistory->user_id);

        return view('adminv2.top-up.history.show', compact('getOrderPrHistory', 'login_user'));
    }


    public function approve(Request $request){
        $getOrderPrHistory = OrderPr::with('user')->orderBy('id', 'desc')->where('status', OrderStatus::PENDING)

        // Filter
            ->when(isset($request->search), function($query) use($request){
                $query->where('invoice', 'like', '%'.$request->search.'%');
            })
        // Filter

        // ->where('user_id',$login_user)
        ->paginate(10);
        return view('adminv2.top-up.history.index', compact('getOrderPrHistory'));
    }

    public function approvePr($id){
        $getOrderPr =  OrderPr::find($id); 
        $pinUpdate = $getOrderPr->update(['status' => OrderStatus::SUCCESS]); 
        $pinDetail = OrderPrDetail::where('order_id',$id)->get();
        foreach($pinDetail as $pd){
            $userPoint = User::where('id', $getOrderPr->user_id)->first(); 
            $userPoint->pr = $userPoint->pr + $pd->pin->pr;
            $userPoint->save();
        } 
        $notify[] = ['success', 'Order Aproved. '];
        return redirect()->route('admin.top_up.history.index')->withNotify($notify);
    }

    public function rejectedPr($id){
        $getOrderPrHistory =  OrderPr::find( $id);
        
        if ($getOrderPrHistory->images_tf) {
            // Delete Image
                $location = 'assets/images/user/order-pr-recipe';
                $old = $getOrderPrHistory->images_tf;
                removeFile($location . '/' . $old);
                removeFile($location . '/thumb_' . $old);
            // Delete Image

            // Update Order History
                $getOrderPrHistory->update(
                    [
                        'images_tf' => null,
                        'status' => OrderStatus::REJECTED
                    ]
                );
            // Update Order History

            $notify[] = ['success', 'Order berhasil ditolak. '];
        }else{
            $notify[] = ['error', 'Member belum mengirim bukti pembayaran'];

        }
        return redirect()->back()->withNotify($notify);
    }
}
