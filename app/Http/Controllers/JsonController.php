<?php

namespace App\Http\Controllers;

use App\Constants\Is_PR;
use App\Constants\StatusUser;
use App\Http\Controllers\Controller;
use App\Models\Managerial;
use App\Models\Pin;
use App\Models\User;
use App\Models\UserPin;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Case_;

class JsonController extends Controller
{
    public function getJsonUser(Request $request)
    {
        $userAuth = auth()->user(); 

        $datauser = getUplineAndDownlineList($userAuth->id);
        $json_array = json_decode($datauser, true);
        $id_array = array_column($json_array, 'id');
        // \dd($id_array);
        // $dataDownline =  User::whereIn('id',$id_array)->with('pin')->paginate(5); 
        $alluser =  User::select('id')->get();
        if($userAuth->special == 1 ){
            if ($request->has('search_name')) {
                $searchName = $request->search_name;
                $user = User::where('status', StatusUser::ACTIVE)->where('username', 'like', '%'.$searchName.'%')->paginate(10);
            } else {
                $user = User::where('status', StatusUser::ACTIVE)->paginate(5);
            } 
        }else{
            if ($request->has('search_name')) {
                $searchName = $request->search_name;
                $user = User::whereIn('id',$id_array)->where('status', StatusUser::ACTIVE)->where('username', 'like', '%'.$searchName.'%')->paginate(10);
            } else {
                $user = User::whereIn('id',$id_array)->where('status', StatusUser::ACTIVE)->paginate(5);
            }
        }

        
        

        $user->getCollection()->transform(function ($item){
            return [
                'id' => $item->id ,
                'text' => $item->username ?? '-'
            ];
        });

		return response()->json($user, 200);
    }

    public function getJsonUserAll(Request $request)
    {
        $user =  User::with('pin')->paginate(5); 


        if ($request->has('search_name')) {
            $searchName = $request->search_name;
            $user = User::where('status', StatusUser::ACTIVE)->where('username', 'like', '%'.$searchName.'%')->paginate(10);
        } else {
            $user = User::where('status', StatusUser::ACTIVE)->paginate(5);
        }
        
        

        $user->getCollection()->transform(function ($item){
            return [
                'id' => $item->id ,
                'text' => $item->username ?? '-'
            ];
        });

		return response()->json($user, 200);
    }

    public function gelJsonManagerialAll(Request $request)
    {
        $managerial =  Managerial::paginate(10); 


        if ($request->has('search_name')) {
            $searchName = $request->search_name;
            $managerial = Managerial::where('name', 'like', '%'.$searchName.'%')->paginate(10);
        } else {
            $managerial = Managerial::paginate(10);
        }
        
        

        $managerial->getCollection()->transform(function ($item){
            return [
                'id' => $item->id ,
                'text' => $item->name ?? '-'
            ];
        });

		return response()->json($managerial, 200);
    }
}
