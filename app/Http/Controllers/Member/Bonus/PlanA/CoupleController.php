<?php

namespace App\Http\Controllers\Member\Bonus\PlanA;

use App\Http\Controllers\Controller;
use App\Models\BonusCouple;
use Illuminate\Support\Facades\DB;
use App\Constants\Is_WD;

class CoupleController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusCouple::where('user_id',$userAuth->id)->orderBy('id','desc')->paginate(10);

        return view('member.bonus.plan-a.couple.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
