<?php

namespace App\Http\Controllers\Member\Bonus\PlanA;

use App\Http\Controllers\Controller;
use App\Models\BonusCoupleRank;
use Illuminate\Support\Facades\DB;

class CoupleRankController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusCoupleRank::where('user_id',$userAuth->id)->orderBy('id','desc')->paginate(10);

        return view('member.bonus.plan-a.couple-rank.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
