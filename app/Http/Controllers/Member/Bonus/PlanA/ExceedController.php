<?php

namespace App\Http\Controllers\Member\Bonus\PlanA;

use App\Http\Controllers\Controller;
use App\Models\BonusExceed;
use Illuminate\Support\Facades\DB;

class ExceedController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusExceed::where('user_id',$userAuth->id)->orderBy('id','desc')->paginate(10);

        return view('member.bonus.plan-a.exceed.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
