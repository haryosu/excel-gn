<?php

namespace App\Http\Controllers\Member\Bonus\PlanA;

use App\Http\Controllers\Controller;
use App\Models\BonusGeneration;
use Illuminate\Support\Facades\DB;

class GenerationController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusGeneration::where('user_id',$userAuth->id)->orderBy('id','desc')->paginate(10);

        return view('member.bonus.plan-a.generation.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
