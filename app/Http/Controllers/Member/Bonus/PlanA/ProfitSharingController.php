<?php

namespace App\Http\Controllers\Member\Bonus\PlanA;

use App\Http\Controllers\Controller;
use App\Models\BonusProfitSharing;
use Illuminate\Support\Facades\DB;

class ProfitSharingController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusProfitSharing::where('user_id',$userAuth->id)->orderBy('id','desc')->paginate(10);
        
        return view('member.bonus.plan-a.profit-sharing.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
