<?php

namespace App\Http\Controllers\Member\Bonus\PlanA;

use App\Http\Controllers\Controller;
use App\Models\BonusReferral;
use Illuminate\Support\Facades\DB;

class ReferralController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusReferral::where('user_id',$userAuth->id)->orderBy('id','desc')->paginate(10);

        return view('member.bonus.plan-a.referral.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
