<?php

namespace App\Http\Controllers\Member\Bonus\PlanA;

use App\Http\Controllers\Controller;
use App\Models\BonusBasicReshopping;
use Illuminate\Http\Request;

class ReshoppingBasicController extends Controller
{
    //
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusBasicReshopping::where('user_id',$userAuth->id)->orderBy('id','desc')->paginate(10);

        return view('member.bonus.plan-a.re-shopping-basic.index', compact('dummyData'));

    }
}
