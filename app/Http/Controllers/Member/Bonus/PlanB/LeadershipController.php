<?php

namespace App\Http\Controllers\Member\Bonus\PlanB;

use App\Http\Controllers\Controller;
use App\Models\BonusLeadership;
use Illuminate\Support\Facades\DB;

class LeadershipController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusLeadership::where('user_id',$userAuth->id)->paginate(10);

        return view('member.bonus.plan-b.leadership.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
