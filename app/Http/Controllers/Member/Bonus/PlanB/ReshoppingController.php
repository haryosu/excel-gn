<?php

namespace App\Http\Controllers\Member\Bonus\PlanB;

use App\Http\Controllers\Controller;
use App\Models\BonusReshopping;
use Illuminate\Support\Facades\DB;

class ReshoppingController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusReshopping::where('user_id',$userAuth->id)->paginate(10);
        
        return view('member.bonus.plan-b.reshopping.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
