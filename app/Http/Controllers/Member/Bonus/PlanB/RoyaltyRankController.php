<?php

namespace App\Http\Controllers\Member\Bonus\PlanB;

use App\Http\Controllers\Controller;
use App\Models\BonusRoyaltyRank;
use Illuminate\Support\Facades\DB;

class RoyaltyRankController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $dummyData = BonusRoyaltyRank::where('user_id',$userAuth->id)->paginate(10);

        return view('member.bonus.plan-b.royalty-rank.index', compact('dummyData'));

    }
    public function update()
    {
        //
    }
}
