<?php

namespace App\Http\Controllers\Member;

use App\Constants\BonusTypePlan;
use App\Constants\Is_PR;
use App\Constants\TypePins;
use App\Http\Controllers\Controller;
use App\Models\AutosaveLog;
use App\Models\BonusLog;
use App\Models\Pin;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $getAllPins = Pin::with('user', 'userPin')->where('type', TypePins::BV)->get();
        $getAllPins_BV = $getAllPins->map(function($i) use($userAuth){
            return[
                'id' => $i->id,
                'images' => $i->images,
                'name' => $i->name,
                'description' => $i->description,
                'user_pin' => $i->userPin->where('user_id',  $userAuth->id)->where('pin_id',$i->id)->first()->qty ?? '-',
            ];
        });
        $getBonusLog = BonusLog::with('user')->where('user_id',$userAuth->id );
        $tbpa =$getBonusLog->where('bonus_type',1 )
        ->sum('bonus_net');
        $tbpb = $getBonusLog->where('bonus_type',2)
        ->sum('bonus_net');
        $sumbonus = BonusLog::with('user')->where('user_id',$userAuth->id )->sum('bonus_net');
        $sumbonustf = BonusLog::with('user')->where('user_id',$userAuth->id )->where('is_wd',1)->sum('bonus_net'); 
        $sumbonusSaldo = BonusLog::with('user')->where('user_id',$userAuth->id )->where('is_wd',0)->sum('bonus_net'); 
        $sumbonusSaldoAutosave = BonusLog::with('user')->where('user_id',$userAuth->id )->sum('bonus_autosave'); 
        $sumbonusSaldoAutosavein = BonusLog::with('user')->where('user_id',$userAuth->id )->sum('bonus_autosave'); 
        // $sumbonusSaldoAutosave = AutosaveLog::with('user')->where('user_id',$userAuth->id )->select('value')->first()??'0'; 
        // $sumbonusSaldoAutosavein = AutosaveLog::with('user')->where('user_id',$userAuth->id )->select('value')->first()??'0'; 
        // \dd($tbpa);
        $ref = User::where('ref_id',$userAuth->id);
        $ref1 = $ref->where('position',1)->first();
        // \dd('test');
        // return \response()->json($ref);
        // \dd(getDownlineData($ref1->id) .''.$ref1->pr);
        if($ref1){
            $refdata1 = getDownlineData($ref1->id)->sum(function ($product) {
                return intval($product['pr']);
            })+ $ref1->pr; 
        }else{
            $refdata1 = 0; 
        }
        // \dd($refdata1);
        $ref2 = User::with('children')->where('ref_id',$userAuth->id)->where('position',2)->first();
        if($ref2){
            $refdata2 = getDownlineData($ref2->id)->sum(function ($product) {
                return intval($product['pr']);
            })+$ref2->pr;
        }else{
            $refdata2 =0;
        } 
        // \dd(($refdata2));
        // \dd(getDownlineData($ref2->id));
        $ref3 = User::with('children')->where('ref_id',$userAuth->id)->where('position',3)->first();
        if($ref3!=null){
            $refdata3 = getDownlineData($ref3->id)->sum(function ($product) {
                return intval($product['pr']);
            })+$ref3->pr;
        }else{
            $refdata3=0;
        }
        $ref4 = User::with('children')->where('ref_id',$userAuth->id)->where('position',4)->first();
        if($ref4!=null){
            $refdata4 = getDownlineData($ref4->id)->sum(function ($product) {
                return intval($product['pr']);
            })+$ref4->pr;
        }else{
            $refdata4=0;
        }
        $ref5 = User::with('children')->where('ref_id',$userAuth->id)->where('position',5)->first();
        if($ref5){
            $refdata5 = getDownlineData($ref5->id)->sum(function ($product) {
                return intval($product['pr']);
            })+$ref5->pr;
        }else{
            $refdata5=0;
        }
        $ref6 = User::with('children')->where('ref_id',$userAuth->id)->where('position',6)->first();
        if($ref6){
            $refdata6 = getDownlineData($ref6->id)->sum(function ($product) {
                return intval($product['pr']);
            })+$ref6->pr;
        }else{
            $refdata6=0;
        }

        // echo $refdata1.'-'.$refdata2;
        return view('member.dashboard.index', compact('getAllPins_BV', 'userAuth','tbpa','tbpb','sumbonus','sumbonustf','sumbonusSaldo','sumbonusSaldoAutosave','sumbonusSaldoAutosavein','refdata1','refdata2','refdata3','refdata4','refdata5','refdata6'));
    }
}
