<?php

namespace App\Http\Controllers\Member\Network;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;


class DataDownlineController extends Controller
{
    public function index()
    {
        // return view('member.network.data-downline.index'); 
        $userAuth = auth()->user(); 
        $dataDownlines =getDownlineData($userAuth->id);  
        $data = $dataDownlines->map(function ($i){
            return 
            $i->id
            ;
        });
        $json_array = json_decode($dataDownlines, true);
        $id_array = array_column($json_array, 'id');
        // \dd($id_array);
        $dataDownline =  User::whereIn('id',$id_array)->with('pin')->paginate(10); 
        // $dataDownline = $this->paginate(getDownlineData($userAuth->id)) ;
        // var_dump(getDownlineData($userAuth->id));

        // return \response()->json($dataDownlines);
        // foreach
        return view('member.network.data-downline.index',compact('dataDownline'));

    }
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function update()
    {
        //
    }
}
