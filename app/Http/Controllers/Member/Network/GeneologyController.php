<?php

namespace App\Http\Controllers\Member\Network;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserPin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class GeneologyController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        // $user = User::where('username', $u)->first();

        // if ($user) {
            $tree = showTreePage($user->id);
            $treeNew = showTreePageNew($user->id);
            $pageTitle = "Tree of " . $user->fullname;
            $userRef = ['id' => Crypt::encrypt($user->id), 'name' => $user->fullname, 'countRef' => User::where('ref_id', $user->id)->count()];
            $userPin = UserPin::with('pin')->where('user_id',$user->id)->where('qty','>=',1)->get();
            $area = \Indonesia::allProvinces();
            return view('member.network.geneology.index', compact('tree', 'treeNew','userPin',  'pageTitle', 'userRef', 'area'));
        // }

        // $notify[] = ['error', 'Tree Not Found!!'];
        // return view('member.network.geneology.index', compact('notify'));

    }
    
    public function userRef($id)
    {

        $emptyMessage = 'No user found';
        $user = User::findOrFail($id);
        $pageTitle = 'Referred By ' . $user->username;
        $users = User::where('ref_id', $id)->latest()->paginate(getPaginate());
        return view('adminv2.users.list', compact('pageTitle', 'emptyMessage', 'users'));
    }

    public function tree($username)
    {

        $user = User::where('username', $username)->first();

        if ($user) {
            $tree = showTreePage($user->id);
            $treeNew = showTreePageNew($user->id);
            $pageTitle = "Tree of " . $user->fullname;
            $userRef = ['id' => Crypt::encrypt($user->id), 'name' => $user->fullname, 'countRef' => User::where('ref_id', $user->id)->count()];
            $area = \Indonesia::allProvinces();
            return view('member.users.tree', compact('tree', 'treeNew', 'pageTitle', 'userRef', 'area'));
        }

        $notify[] = ['error', 'Tree Not Found!!'];
        return redirect()->route('admin.dashboard')->withNotify($notify);
    }
    
    public function otherTree(Request $request, $username = null)
    {
        if ($request->username) {
            $user = User::where('username', $request->username)->first();
        } else {
            $user = User::where('username', $username)->first();
        }
        if ($user) {
            $tree = showTreePage($user->id);
            $pageTitle = "Tree of " . $user->fullname;

            $treeNew = showTreePageNew($user->id);
            $userRef = ['id' => Crypt::encrypt($user->id), 'name' => $user->fullname, 'countRef' => User::where('ref_id', $user->id)->count()];
            $area = Indonesia::allProvinces();

            return view('adminv2.users.tree', compact('tree', 'pageTitle', 'treeNew', 'userRef', 'area'));
        }

        $notify[] = ['error', 'Tree Not Found!!'];
        return redirect()->route('admin.dashboard')->withNotify($notify);
    }
    public function update()
    {
        //
    }
}
