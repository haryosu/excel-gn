<?php

namespace App\Http\Controllers\Member\Network;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class SponsorController extends Controller
{
    public function index()
    {

        $userAuth = auth()->user(); 
        $dataSponsor =  User::where('ref_id',$userAuth->id)->with('plan')->paginate(10); 
        return view('member.network.sponsor.index',compact('dataSponsor'));

    }
    public function update()
    {
        //
    }
}
