<?php

namespace App\Http\Controllers\Member\Pin;

use App\Constants\PinStatus;
use App\Constants\OrderStatus;
use App\Constants\TypePins;
use App\Http\Controllers\Controller;
use App\Models\OrderPin;
use App\Models\OrderPinDetail;
use App\Models\Pin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PinOrderController extends Controller
{
    public function index()
    {
        $products = Pin::where('status', PinStatus::ACTIVE)->where('type', TypePins::BV)->get();

        $items = \Cart::session(Auth()->id())->getContent();
        

        if (\Cart::isEmpty()) {
            $cart_data = [];
        } else {
            foreach ($items as $row) {
                $cart[] = [
                    'rowId' => $row->id,
                    'name' => $row->name,
                    'stock' => $row->quantity,
                    'pricesingle' => $row->price,
                    'price' => $row->getPriceSum(),
                    'created_at' => $row->attributes['created_at'],
                ];
            }
            $cart_data = collect($cart)->sortBy('created_at');
        }
        
        //total
        $total = \Cart::session(Auth()->id())->getSubTotal();

        $data_total = [
            'total' => $total,
        ];

        return view('member.pin.order.index', compact('products', 'cart_data', 'data_total'));

    }

    public function additem(Request $request, $id)
    {
        $getPinProduct = Pin::find($id);

        $cart = \Cart::session(Auth()->id())->getContent();

        $cek_itemId = $cart->whereIn('id', $id);

        if ($cek_itemId->isNotEmpty()) {
                \Cart::session(Auth()->id())->remove($id);
                \Cart::session(Auth()->id())->add(array(
                'id' => $id,
                'name' => $getPinProduct->name,
                'price' => $getPinProduct->price,
                'quantity' => $request->itemQuantity,
                'attributes' => array(
                    'created_at' => date('Y-m-d H:i:s')
                )
            ));
        } else {

            \Cart::session(Auth()->id())->add(array(
                'id' => $id,
                'name' => $getPinProduct->name,
                'price' => $getPinProduct->price,
                'quantity' => $request->itemQuantity,
                'attributes' => array(
                    'created_at' => date('Y-m-d H:i:s')
                )
            ));

        }

        return redirect()->back();
    }

    public function removeitem($id)
    {
        \Cart::session(Auth()->id())->remove($id);

        return redirect()->back();
    }

    public function clear()
    {
        \Cart::session(Auth()->id())->clear();
        return redirect()->back();
    }

    public function bayar()
    {
        //  Get Total Item Cart
            $cart_total = \Cart::session(Auth()->id())->getSubTotal();
        //  Get Total Item Cart
        if ($cart_total != 0) {
            DB::beginTransaction();
            try {
                
                //  Get Semua Item Cart
                    $all_cart = \Cart::session(Auth()->id())->getContent();
                    
                    $filterCart = $all_cart->map(function ($item) {
                        return [
                            'id' => $item->id,
                            'price' => $item->price,
                            'quantity' => $item->quantity
                        ];
                    });
                //  Get Semua Item Cart
                
                
                // Creata Order
                    $createOrder = OrderPin::create([
                        'user_id' => Auth()->id(),
                        'total' => $cart_total + rand(100, 999),
                        'status' => OrderStatus::PENDING,
                    ]);
                // Creata Order

                // Generate Invoice
                    $date = date("dmy");
                    $counterNumber = $createOrder->id;
                    $invoiceNumber =  'inv'.'-'.$date.'-'. $counterNumber;
                    OrderPin::find($createOrder->id)->update([
                        'invoice' => $invoiceNumber,
                    ]);
                // Generate Invoice

                // Creata Order Detail
                    foreach ($filterCart as $item) {
                        OrderPinDetail::create([
                            'order_id' => $createOrder->id,
                            'pin_id' => $item['id'],
                            'qty' => $item['quantity'],
                            'subtotal' => $item['price']
                        ]);
                    }
                // Creata Order Detail

                \Cart::session(Auth()->id())->clear();
                DB::commit();
                $notify[] = ['success', 'Order Berhasil dilakukan. Silahkan hubungi admin jika sudah melakukan pembayaran. Cek history untuk melihat detail orderan anda'];
                return redirect()->route('member.pin.history.index')->withNotify($notify);
            } catch (\Exeception $e) {
                DB::rollback();
                $notify[] = ['error', 'Terjadi kesalahan sistem. Silahkan hubungi admin'];
                return redirect()->back()->withNotify($notify);
            }
        } 
        $notify[] = ['error', 'Order gagal. pastikan anda sudah memilih item.'];
        return redirect()->back()->withNotify($notify);
    }
}
