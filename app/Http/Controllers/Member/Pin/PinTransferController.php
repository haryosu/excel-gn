<?php

namespace App\Http\Controllers\Member\Pin;

use App\Constants\Is_PR;
use App\Constants\StatusUser;
use App\Constants\TypePins;
use App\Http\Controllers\Controller;
use App\Models\Pin;
use App\Models\User;
use App\Models\UserPin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PinTransferController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $getAllPins = Pin::with('user', 'userPin')->where('type', TypePins::BV)->get();
        $getAllPins_BV = $getAllPins->map(function($i) use($userAuth){
            return[
                'id' => $i->id,
                'images' => $i->images,
                'name' => $i->name,
                'description' => $i->description,
                'user_pin' => $i->userPin->where('user_id',  $userAuth->id)->where('pin_id',$i->id)->first()->qty ?? null,
            ];
        });
        $getUser = User::where('status', StatusUser::ACTIVE)->paginate(10);
        return view('member.pin.transfer.index', compact('getAllPins_BV', 'userAuth', 'getUser'));
    }
    public function transferPinUser(Request $request)
    {
        $userAuth = auth()->user();
        
        $request->validate([
            'pin_id' => 'required|integer',
            'user_id' => 'required|integer',
            'qty' => 'required|integer|min:1',
        ]);
        
        $in['pin_id'] = $request->pin_id;
        $in['user_id'] = $request->user_id;
        $in['qty'] = $request->qty;

        // Validasi jangan sampai transfer ke diri sendiri
            if ($request->user_id == $userAuth->id) {
                $notify[] = ['error', 'Anda Tidak dapat transfer ke diri anda sendiri'];
                return back()->withNotify($notify);
            }else{
                // User Pin Pengirim
                    $getPinFrom = UserPin::where('pin_id', $request->pin_id)->where('user_id', $userAuth->id)->first();
                // User Pin Pengirim

                // User Pin Penerima
                    $getPinTo = UserPin::where('pin_id', $request->pin_id)->where('user_id', $request->user_id)->first();
                // User Pin Penerima

                // Validasi Penerima memiliki User Pin Atau Tidak
                    if ($getPinTo) {

                        // Validasi stock
                            if ($getPinFrom->qty >= $request->qty) {

                                // Update User Pin Penerima
                                    $getPinTo->update([
                                        'qty'=>$getPinTo->qty + $request->qty,
                                    ]);
                                // Update User Pin Penerima
                                
                                // Update User Pin Pengirim
                                    $getPinFrom->update([
                                        'qty'=> $getPinFrom->qty - $request->qty,
                                    ]);
                                // Update User Pin Pengirim

                                $notify[] = ['success', 'Transfer Pin Berhasil.'];
                                return back()->withNotify($notify);
                            } else {
                                $notify[] = ['error', 'Transfer Pin Gagal. Stock pin tidak cukup. Stock pin yang anda miliki adalah '.$getPinFrom->qty];
                                return back()->withNotify($notify);
                            }
                        // Validasi stock

                    }else{
                        
                        // Validasi Pengirim memiliki User Pin Atau Tidak
                            if ($getPinFrom) {
                                // Validasi stock
                                    if ($getPinFrom->qty >= $request->qty) {

                                        // Create User Pin Penerima
                                        UserPin::create([
                                            'user_id'=>$request->user_id,
                                            'pin_id'=>$request->pin_id,
                                            'qty'=>$request->qty,
                                        ]);
                                        // Create User Pin Penerima

                                        // Update User Pin Pengirim
                                        $getPinFrom->update([
                                            'qty'=> $getPinFrom->qty - $request->qty,
                                        ]);
                                        // Update User Pin Pengirim

                                        $notify[] = ['success', 'Transfer Pin Berhasil.'];
                                        return back()->withNotify($notify);
                                    } else {
                                        $notify[] = ['error', 'Transfer Pin Gagal. Stock pin tidak cukup. Stock pin yang anda miliki adalah '.$getPinFrom->qty];
                                        return back()->withNotify($notify);
                                    }
                                // Validasi stock
                            } else {
                                $notify[] = ['error', 'Transfer Pin Gagal. Pin yang anda pilih belum anda beli. '];
                                return back()->withNotify($notify);
                            }
                        // Validasi Pengirim memiliki User Pin Atau Tidak

                    }
                // Validasi Penerima memiliki User Pin Atau Tidak

            }
        // Validasi jangan sampai transfer ke diri sendiri

    }
}
