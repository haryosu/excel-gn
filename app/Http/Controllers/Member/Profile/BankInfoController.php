<?php

namespace App\Http\Controllers\Member\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankInfoController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        return view('member.profile.info-bank', compact('user'));

    }
}
