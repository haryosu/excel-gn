<?php

namespace App\Http\Controllers\Member\ReshoppingBasic;

use App\Http\Controllers\Controller;
use App\Models\OrderPin;
use App\Rules\FileTypeValidate;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    //
    public function index(Request $request)
    {
        $login_user = Auth()->id();
        $getOrderPinHistory = OrderPin::with('user')

        // Filter
            ->when(isset($request->search), function($query) use($request){
                $query->where('invoice', 'like', '%'.$request->search.'%');
            })
        // Filter

        ->where('user_id',$login_user)
        ->where('is_basic',1)
        ->orderBy('id','desc')
        ->paginate(10);
        return view('member.re-shopping-basic.history.index', compact('getOrderPinHistory'));

    }
    public function show($id)
    {
        $login_user = Auth()->user();
        $getOrderPinHistory = OrderPin::with('orderPinDetail.pin')->find($id);

        return view('member.re-shopping-basic.history.show', compact('getOrderPinHistory', 'login_user'));
    }

    public function recipe(Request $request, $id)
    {
        $getOrderPin = OrderPin::with('orderPinDetail.pin')->find($id);

        $request->validate([
            'images_tf' => ['image',new FileTypeValidate(['jpg','jpeg','png'])]
        ]);

        if ($request->hasFile('images_tf')) {
            $location = 'assets/images/user/order-recipe';
            $size = '350x300';
            $filename = uploadImage($request->images_tf, $location, $size, $getOrderPin->images_tf);
            $in['images_tf'] = $filename;
        }

        $getOrderPin->fill($in)->save();
        $notify[] = ['success', 'Profile updated successfully.'];
        return back()->withNotify($notify);
    }
}
