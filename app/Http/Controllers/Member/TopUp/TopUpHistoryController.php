<?php

namespace App\Http\Controllers\Member\TopUp;

use App\Http\Controllers\Controller;
use App\Models\OrderPr;
use App\Rules\FileTypeValidate;
use Illuminate\Http\Request;

class TopUpHistoryController extends Controller
{
    public function index(Request $request)
    {
        $login_user = Auth()->id();
        $getOrderPrHistory = OrderPr::with('user')

        // Filter
            ->when(isset($request->search), function($query) use($request){
                $query->where('invoice', 'like', '%'.$request->search.'%');
            })
        // Filter

        ->where('user_id',$login_user)
        ->paginate(10);
        return view('member.top-up.history.index', compact('getOrderPrHistory'));

    }
    public function show($id)
    {
        $login_user = Auth()->user();
        $getOrderPrHistory = OrderPr::with('orderPrDetail.pin')->find($id);

        return view('member.top-up.history.show', compact('getOrderPrHistory', 'login_user'));
    }

    public function recipe(Request $request, $id)
    {
        $getOrderPin = OrderPr::with('orderPrDetail.pin')->find($id);

        $request->validate([
            'images_tf' => ['image',new FileTypeValidate(['jpg','jpeg','png'])]
        ]);

        if ($request->hasFile('images_tf')) {
            $location = 'assets/images/user/order-pr-recipe';
            $size = '350x300';
            $filename = uploadImage($request->images_tf, $location, $size, $getOrderPin->images_tf);
            $in['images_tf'] = $filename;
        }

        $getOrderPin->fill($in)->save();
        $notify[] = ['success', 'Profile updated successfully.'];
        return back()->withNotify($notify);
    }
}
