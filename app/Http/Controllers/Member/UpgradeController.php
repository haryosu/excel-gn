<?php

namespace App\Http\Controllers\Member;

use App\Constants\TypePins;
use App\Http\Controllers\Controller;
use App\Models\AutosaveLog;
use App\Models\Bonus\ReferalBonus;
use App\Models\BonusLog;
use App\Models\Pin;
use App\Models\UpgradeLog;
use App\Models\User;
use App\Models\UserPin;

class UpgradeController extends Controller
{
    public function index()
    {
        $userAuth = auth()->user();
        $getAllPins = Pin::with('user', 'userPin')->where('type', TypePins::BV)->get();
        $getAllPins_BV = $getAllPins->map(function($i) use($userAuth){
            return[
                'id' => $i->id,
                'images' => $i->images,
                'name' => $i->name,
                'description' => $i->description,
                'user_pin' => $i->userPin->where('user_id',  $userAuth->id)->where('pin_id',$i->id)->first()->qty ?? '-',
            ];
        });
        return view('member.upgrade.index', compact('getAllPins_BV', 'userAuth'));
    }
    public function upgradePinUser($id)
    {
        $userAuth = auth()->user();
        $userPin = UserPin::where('user_id', $userAuth->id)->where('pin_id', $id)->first();
        $getPin = Pin::find($id);

        // Validasi agar tidak bisa update di pin yang sama
            if ($id == $userAuth->pin_id) {
                $notify[] = ['error', 'Tidak dapat melakukan Upgrade ke '.$getPin->name.'. Pin anda saat ini adalah '.$getPin->name];
            }else{
                // Validasi jika pin yang dipilih lebih rendah / Downgrade
                    if ($id > $userAuth->pin_id) {
                        // Validasi jika belum memiliki pin
                            if ($userPin) {
                                // Validasi jika stock kurang
                                    if ($userPin->qty != 0 || $userPin->qty != null) {

                                        // Pengurangan kuantity user pin
                                            $reducedQty = $userPin->qty - 1;
                                
                                            $update = UserPin::find($userPin->id)->update(
                                                [
                                                'qty' => $reducedQty
                                                ]
                                            );
                                        // Pengurangan kuantity user pin

                                        // Perubahan pin id di user
                                            $refBonus = new UpgradeLog();
                                            $refBonus->user_id = $userAuth->id;
                                            $refBonus->from_pin = $userAuth->pin_id;
                                            $refBonus->to_pin = $id; 
                                            $refBonus->save();

                                            if ($getPin->is_pr == 1){
                                                $userpr = $getPin->pr; 
                                            }else{
                                                $userpr = 0; 
                                            }
                                            $update = User::find($userAuth->id)->update(
                                                [
                                                'pin_id' => $id,
                                                'bv' => $userAuth->bv + $getPin->bv,
                                                'pr' => $userAuth->pr + $userpr
                                                ]
                                            );

                                        // Perubahan pin id di user
                                        // bonus referal jika referal non basic
                                            $refPin = User::where('id',$userAuth->ref_id)->first();
                                            if($refPin ){
                                                if($refPin->pin_id == 1 || $refPin->pin_id == 2 || $refPin->pin_id == 3 || $refPin->pin_id == 4 || $refPin->pin_id == 5   )
                                                {
                                                    $refBonus = new ReferalBonus();
                                                    $refBonus->user_id = $userAuth->ref_id;
                                                    $refBonus->sender_id = $userAuth->id;
                                                    $refBonus->bonus_val = $getPin->ref_bonus;
                                                    $refBonus->bonus_net =(80/100) * $getPin->ref_bonus;
                                                    $refBonus->bonus_autosave =(20/100) * $getPin->ref_bonus;
                                                    $refBonus->desc = 'Referal Bonus From '.$userAuth->username.' on Leg '.$userAuth->leg;
                                                    $refBonus->save();
        
                                                    $bonusLog = new BonusLog(); 
                                                    $bonusLog->bonus_type =  1;
                                                    $bonusLog->bonus_plan =  1;
                                                    $bonusLog->user_id = $userAuth->ref_id; 
                                                    $bonusLog->bonus_val = $getPin->ref_bonus;
                                                    $bonusLog->bonus_net =(80/100) * $getPin->ref_bonus;
                                                    $bonusLog->bonus_autosave =(20/100) * $getPin->ref_bonus;
                                                    $bonusLog->desc = 'Referal Bonus From '.$userAuth->username.' on Leg '.$userAuth->leg;
                                                    $bonusLog->is_wd = 0;
                                                    $bonusLog->save(); 
        
                                                    $autosavelog = AutosaveLog::where('user_id',$userAuth->ref_id)->first();
                                                    if($autosavelog){
                                                    $autosavelog->value += $bonusLog->bonus_autosave;
                                                    $autosavelog->save();
                                                    }else
                                                    { 
                                                        $save = new AutosaveLog();
                                                        $save->user_id = $userAuth->ref_id;
                                                        $save->value =  $bonusLog->bonus_autosave;
                                                        $save->save();
                                                    } 
                                                }
                                            }

                                        // bonus referal jika referal non basic
                                        

                                    $notify[] = ['success', 'Berhasil Upgrade menjadi '.$getPin->name];
                                    } else {
                                        $notify[] = ['error', 'Stock pin '.$getPin->name.' anda kosong. Silahkan melakukan pembelian'];
                                    }
                                // Validasi jika stock kurang
                            } else {
                                $notify[] = ['error', 'Anda tidak memliliki stock pin '.$getPin->name.'. Silahkan melakukan pembelian terlebih dahulu'];
                            }
                        // Validasi jika belum memiliki pin

                    } else {
                        $notify[] = ['error', 'Tidak dapat melakukan Downgrade. Pin yang anda pilih lebih rendah dari yang anda miliki sekarang.'];
                    }
                // Validasi jika pin yang dipilih lebih rendah / Downgrade
            }
        // Validasi agar tidak bisa update di pin yang sama

        return back()->withNotify($notify);
    }
}
