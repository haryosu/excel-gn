<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReferalBonus extends Model
{
    use HasFactory;

    protected $table ='bonus_referral';
}
