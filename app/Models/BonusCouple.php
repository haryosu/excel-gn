<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusCouple extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'bonus_couple';
    public $timestamp = false;

    protected $primaryKey = 'id'; // change 'my_id' to the name of your primary key column
    // public $incrementing = false;
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
