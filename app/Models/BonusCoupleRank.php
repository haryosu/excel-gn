<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusCoupleRank extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'bonus_couple_rank';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
