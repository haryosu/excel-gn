<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusLog extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'bonus_log';
    // protected $primaryKey = 'id'; // change 'my_id' to the name of your primary key column
    // public $incrementing = false;
    public $timestamp = false;

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
