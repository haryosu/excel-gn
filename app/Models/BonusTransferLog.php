<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonusTransferLog extends Model
{
    use HasFactory;
    
    protected $guarded = [];
    protected $table = 'bonus_transfer_log';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
