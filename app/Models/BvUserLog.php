<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BvUserLog extends Model
{
    use HasFactory;

    protected $table ='bv_user_log';
    protected $fillable = ['user_id','leg','position','bv_log'	];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
