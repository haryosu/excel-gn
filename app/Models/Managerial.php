<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Managerial extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'managerial';

    public function user(){
        return $this->hasMany(User::class, 'manager_id');
    }
}
