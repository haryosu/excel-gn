<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPin extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'order_pins';

    public function orderPinDetail(){
        return $this->hasMany(OrderPinDetail::class, 'order_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
