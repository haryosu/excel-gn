<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPinDetail extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'order_pin_details';

    public function orderPin(){
        return $this->belongsTo(OrderPin::class, 'order_id');
    }

    public function pin(){
        return $this->belongsTo(Pin::class, 'pin_id');
    }
}
