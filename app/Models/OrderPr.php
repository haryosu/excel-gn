<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPr extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'order_pr';

    public function orderPrDetail(){
        return $this->hasMany(OrderPrDetail::class, 'order_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
