<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPrDetail extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'order_pr_details';

    public function orderPr(){
        return $this->belongsTo(OrderPr::class, 'order_id');
    }

    public function pin(){
        return $this->belongsTo(Pin::class, 'pin_id');
    }
}
