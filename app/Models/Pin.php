<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    use HasFactory;

    public function user(){
        return $this->hasMany(User::class, 'pin_id');
    }
    public function userPin(){
        return $this->hasMany(UserPin::class, 'pin_id');
    }
    
}
