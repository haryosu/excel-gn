<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $guarded = ['id'];
    public function user(){
        return $this->hasMany(User::class, 'pin_id');
    }
    public function userPin(){
        return $this->hasMany(UserPin::class, 'pin_id');
    }
    public function pin(){
        return $this->belongsTo(Pin::class, 'pin_id');
    }
}
