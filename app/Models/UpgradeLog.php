<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpgradeLog extends Model
{
    use HasFactory;
    protected $table = "upgrade_log";

    protected  $guarded = [];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function fromPin()
    {
        return $this->belongsTo(Pin::class,'id','from_pin');
    }
    public function toPin()
    {
        return $this->belongsTo(Pin::class,'id','to_pin');
    }
}
