<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'address' => 'object',
        'ver_code_send_at' => 'datetime',
    ];

    protected $data = [
        'data' => 1,
    ];
    public function bvlog()
    {
        return $this->hasOne(BvUserLog::class, 'user_id');
    }
    public function BvUserLog()
    {
        return $this->hasMany(BvUserLog::class, 'user_id','id');
    }

    public function pin(){
        return $this->belongsTo(Pin::class, 'pin_id');
    }

    public function Userspin(){
        return $this->hasMany(UserPin::class, 'user_id');
    }
    
    public function cron(){
        return $this->hasMany(UserPin::class, 'user_id');
    }

    public function login_logs()
    {
        return $this->hasMany(UserLogin::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class)->orderBy('id', 'desc');
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class)->where('status', '!=', 0);
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class)->where('status', '!=', 0);
    }

    //mlm
    public function userExtra()
    {
        return $this->hasOne(UserExtra::class, 'user_id');
    }

    public function plan()
    {
        return $this->belongsTo(Pin::class, 'pin_id', 'id');
    }

    // SCOPES

    public function getFullnameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function scopeActive()
    {
        return $this->where('status', 1);
    }

    public function scopeBanned()
    {
        return $this->where('status', 0);
    }

    public function scopeEmailUnverified()
    {
        return $this->where('ev', 0);
    }

    public function scopeSmsUnverified()
    {
        return $this->where('sv', 0);
    }
    public function scopeEmailVerified()
    {
        return $this->where('ev', 1);
    }

    public function scopeSmsVerified()
    {
        return $this->where('sv', 1);
    }
    
    public function children()
    {
        return $this->hasMany(User::class, 'ref_id');
    }
    public function downline()
    {
        $downline = [];

        foreach ($this->children as $child) {
            $downline[] = $child;
            $downline = array_merge($downline, $child->downline()->toArray());
        }

        return collect($downline);
    }

    public function parent()
    {
        return $this->belongsTo(User::class, 'ref_id');
    }
    public function upline()
    {
        $upline = [];

        $parent = $this->parent;

        while ($parent) {
            $upline[] = $parent;
            $parent = $parent->parent;
        }

        return collect(($upline));
    }
    public function members()
    {
        return $this->hasMany(User::class, 'ref_id');
    }
    public function BonusLog()
    {
        return $this->hasMany(BonusLog::class, 'user_id');
    }

    public function managerial()
    {
        return $this->belongsTo(Managerial::class, 'manager_id','id');
    }
}
