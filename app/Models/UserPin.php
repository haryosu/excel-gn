<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPin extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'pin_id',
        'qty',
        // other fillable attributes...
    ];
    public function pin(){
        return $this->belongsTo(Pin::class)->where('is_pr', '==', 0);
    }
}
