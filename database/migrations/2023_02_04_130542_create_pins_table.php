<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pins', function (Blueprint $table) {
            $table->bigIncrements();
            $table->string('name', 40);
            $table->text('description')->nullable(); 
            $table->text('images')->nullable();

            $table->string('price', 40);
            $table->string('bv', 40)->default(0);
            $table->string('pr', 40)->default(0);
            $table->boolean('status')->default(1)->comment('1=>active, 2=>inactive,  ');
            
            // $table->unsignedInteger('method_id');
            // $table->unsignedInteger('user_id');
            // $table->decimal('amount', 28, 8)->default(0);
            // $table->decimal('rate', 28, 8)->default(0);
            // $table->decimal('charge', 28, 8)->default(0);
            // $table->string('trx', 40);
            // $table->decimal('final_amount', 28, 8)->default(0);
            // $table->decimal('after_charge', 28, 8)->default(0);
            // $table->text('withdraw_information')->nullable();
            // $table->text('admin_feedback')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pins');
    }
}
