<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPinDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_pin_details', function (Blueprint $table) {
            $table->bigIncrements();
            
            $table->unsignedBigInteger('order_id');
            $table->foreign(['order_id'])->references(['id'])->on('order_pins')->onUpdate('CASCADE')->onDelete('CASCADE');  
            
            $table->unsignedBigInteger('pin_id');
            $table->foreign(['pin_id'])->references(['id'])->on('pins')->onUpdate('CASCADE')->onDelete('CASCADE');  
            
            $table->string('qty', 40)->default(0);
            $table->string('subtotal', 40)->default(0); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_pin_details');
    }
}
