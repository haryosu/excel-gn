<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
                       
            $table->unsignedBigInteger('pin_id')->nullable();
            $table->foreign(['pin_id'])->references(['id'])->on('pins')->onUpdate('CASCADE')->onDelete('CASCADE');  
            $table->date('dob', 40)->nullable();
            $table->string('pob', 40)->nullable();
            $table->string('nik', 40)->nullable(); 
            $table->text('img_nik', 40)->nullable(); 

        });
        // Schema::table('menus', function (Blueprint $table) {
        //     $table->unsignedBigInteger('parent_menu_id')->nullable()->change();
        //     $table->foreign('parent_menu_id')->references('id')->on('menus')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
