<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonusReshoppingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_reshopping', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign(['user_id'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');   
            $table->string('bonus_val', 40)->default('0'); 
            $table->string('bonus_net', 40)->default('0'); 
            $table->string('bonus_autosave', 40)->default('0');  
            $table->string('desc', 40)->default('0');  
            $table->boolean('is_wd')->default(false)->comment('0: pending, 1: success'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_reshopping');
    }
}
