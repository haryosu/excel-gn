<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBvUserLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->integer('bv_log_l1')->nullable();
            $table->integer('bv_log_r1')->nullable();
            $table->integer('bv_log_l2')->nullable();
            $table->integer('bv_log_r2')->nullable();
            $table->integer('bv_log_l3')->nullable();
            $table->integer('bv_log_r3')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
