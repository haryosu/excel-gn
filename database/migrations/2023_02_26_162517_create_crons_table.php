<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCronsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign(['user_id'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');   
            $table->integer('ref');
            $table->boolean('type')->comment('a: plan a, b: plan b');
            $table->boolean('leg')->comment('1: Leg 1, 2: Leg 2, 3: Leg 3');
            $table->boolean('position')->comment('1: Leg 1, 2: Leg 2, 3: Leg 3');
            $table->boolean('status')->default(0)->comment('0: wait, 1: run, 2 : finish'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crons');
    }
}
