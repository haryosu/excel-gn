<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_pr', function (Blueprint $table) {
            $table->bigIncrements();
            
            $table->string('invoice', 40);  
            $table->text('images_tf')->nullable();
       
            $table->unsignedBigInteger('user_id');
            $table->foreign(['user_id'])->references(['id'])->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');  
            
            $table->string('total', 40); 
            $table->boolean('status')->default(1)->comment('1=>success, 2=>failed, 0=>pending  ');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_pr');
    }
}
