<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBankAccountUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) { 
            $table->string('bank_account')->after('email')->nullable();  
            $table->integer('bank_number')->after('bank_account')->nullable();  
            $table->string('bank_provider')->after('bank_number')->nullable();  
            // $table->integer('bank_provider')->after('email');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
