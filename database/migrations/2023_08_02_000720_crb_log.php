<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrbLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('crb_log', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');  

            $table->string('leg', 40)->nullable();

            $table->string('pos_1', 40)->nullable();
            $table->string('pos_2', 40)->nullable();

            $table->string('pos_1_val', 40)->nullable();
            $table->string('pos_2_val', 40)->nullable();

            $table->string('total', 40)->default(0);
            $table->string('status', 40)->default(0);  
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
