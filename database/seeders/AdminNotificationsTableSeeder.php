<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminNotificationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_notifications')->delete();
        
        \DB::table('admin_notifications')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'title' => 'New support ticket has opened',
                'read_status' => 1,
                'click_url' => '/admin/tickets/view/1',
                'created_at' => '2022-12-11 12:53:52',
                'updated_at' => '2022-12-11 12:54:09',
            ),
        ));
        
        
    }
}