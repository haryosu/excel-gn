<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super Admin',
                'email' => 'admin@site.com',
                'username' => 'admin',
                'email_verified_at' => NULL,
                'image' => '5ff1c3531ed3f1609679699.jpg',
                'password' => '$2y$10$2qcOUKrDIUqyyCklvHp7IO8fGNcJ1gAXtxouTn1isZPHu6H8CfHPq',
                'created_at' => NULL,
                'updated_at' => '2021-05-07 14:54:06',
            ),
        ));
        
        
    }
}