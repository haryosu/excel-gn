<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'test',
                'status' => 1,
                'created_at' => '2023-01-25 22:21:14',
                'updated_at' => '2023-01-25 22:21:14',
            ),
        ));
        
        
    }
}