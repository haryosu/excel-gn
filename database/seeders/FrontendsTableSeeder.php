<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FrontendsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('frontends')->delete();
        
        \DB::table('frontends')->insert(array (
            0 => 
            array (
                'id' => 1,
                'data_keys' => 'seo.data',
            'data_values' => '{"seo_image":"1","keywords":["admin","blog","manage","mlm","mlmlab","binary mlm","php mlm"],"description":"MLM lab is a successful multilevel marketing company. Lorem Ipsum is simply dummied text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularly raised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\\r\\n\\r\\nWhy do we use it?\\r\\nIt is a long-established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem Ipsum will uncover many websites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humor and the like).","social_title":"MLM Lab","social_description":"MLM lab is the multilevel marketing platform Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\\r\\n\\r\\nWhy do we use it?\\r\\nIt is a long-established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem Ipsum will uncover many websites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humor and the like).","image":"61af519b979d51638879643.png"}',
                'created_at' => '2020-07-05 06:42:52',
                'updated_at' => '2021-12-07 13:20:43',
            ),
            1 => 
            array (
                'id' => 25,
                'data_keys' => 'blog.content',
                'data_values' => '{"heading":"LATEST NEWS","sub_heading":"Welcome! Please check our latest news and article here"}',
                'created_at' => '2020-10-28 07:51:34',
                'updated_at' => '2021-12-06 15:39:32',
            ),
            2 => 
            array (
                'id' => 27,
                'data_keys' => 'contact_us.content',
                'data_values' => '{"has_image":"1","title":"Contact Us for Asking any Question. We are always ready to assist you.","map_iframe_url":"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m18!1m12!1m3!1d224346.48129412968!2d77.06889969035102!3d28.52728034389636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfd5b347eb62d%3A0x52c2b7494e204dce!2sNew%20Delhi%2C%20Delhi%2C%20India!5e0!3m2!1sen!2sbd!4v1638784996798!5m2!1sen!2sbd","image":"61adde9c0f5f71638784668.png"}',
                'created_at' => '2020-10-28 07:59:19',
                'updated_at' => '2021-12-06 16:33:35',
            ),
            3 => 
            array (
                'id' => 31,
                'data_keys' => 'social_icon.element',
                'data_values' => '{"title":"Facebook","social_icon":"<i class=\\"lab la-facebook\\"><\\/i>","url":"#"}',
                'created_at' => '2020-11-12 11:07:30',
                'updated_at' => '2020-12-05 08:03:28',
            ),
            4 => 
            array (
                'id' => 33,
                'data_keys' => 'service.content',
                'data_values' => '{"heading":"Our Services","sub_heading":"MLM Hero Provides Awesome Services"}',
                'created_at' => '2020-12-01 07:47:44',
                'updated_at' => '2021-10-23 08:06:26',
            ),
            5 => 
            array (
                'id' => 40,
                'data_keys' => 'how_it_works.content',
                'data_values' => '{"heading":"How It\'s Work","sub_heading":"How Can You Earn Profits"}',
                'created_at' => '2020-12-01 07:51:07',
                'updated_at' => '2021-10-23 08:13:58',
            ),
            6 => 
            array (
                'id' => 41,
                'data_keys' => 'how_it_works.element',
                'data_values' => '{"icon":"<i class=\\"fas fa-money-bill-wave\\"><\\/i>","title":"Register Account","description":"At first you have to create an Accont. just go to the register page, fill up the form and get registered."}',
                'created_at' => '2020-12-01 07:52:11',
                'updated_at' => '2021-10-23 08:14:35',
            ),
            7 => 
            array (
                'id' => 42,
                'data_keys' => 'how_it_works.element',
                'data_values' => '{"icon":"<i class=\\"fas fa-users\\"><\\/i>","title":"Invite People","description":"At first you have to create an Accont. just go to the register page, fill up the form and get registered."}',
                'created_at' => '2020-12-01 07:52:26',
                'updated_at' => '2021-10-23 08:14:46',
            ),
            8 => 
            array (
                'id' => 43,
                'data_keys' => 'how_it_works.element',
                'data_values' => '{"icon":"<i class=\\"fas fa-user-edit\\"><\\/i>","title":"Get Commission","description":"At first you have to create an Accont. just go to the register page, fill up the form and get registered."}',
                'created_at' => '2020-12-01 07:52:57',
                'updated_at' => '2021-10-23 08:14:59',
            ),
            9 => 
            array (
                'id' => 44,
                'data_keys' => 'team.content',
                'data_values' => '{"heading":"Team Member","sub_heading":"Our Pasioniate Team Member"}',
                'created_at' => '2020-12-01 07:53:36',
                'updated_at' => '2021-10-23 14:16:48',
            ),
            10 => 
            array (
                'id' => 49,
                'data_keys' => 'mlmPlan.content',
                'data_values' => '{"has_image":"1","heading":"Pricing Plan","sub_heading":"Our pricing plans are very simple and attractive, have a check!","background_image":"608116bd3561b1619072701.jpg"}',
                'created_at' => '2020-12-01 08:04:48',
                'updated_at' => '2021-04-22 07:25:01',
            ),
            11 => 
            array (
                'id' => 50,
                'data_keys' => 'latestTrx.content',
                'data_values' => '{"heading":"Our Latest Transaction","sub_heading":"At this website, making deposits and widthway is simple and straight forward and hardly takes up any of your time. We are constantly striving to offer you, even more, deposit and withdraw options and to make the process even easier."}',
                'created_at' => '2020-12-01 08:05:37',
                'updated_at' => '2020-12-13 19:44:29',
            ),
            12 => 
            array (
                'id' => 56,
                'data_keys' => 'testimonial.content',
                'data_values' => '{"heading":"Client\'s Say","sub_heading":"What Our Client Say About Us"}',
                'created_at' => '2020-12-01 08:11:00',
                'updated_at' => '2021-10-24 06:22:41',
            ),
            13 => 
            array (
                'id' => 68,
                'data_keys' => 'footer_section.content',
                'data_values' => '{"website_footer_address":"#142 Poran Masto Raod 902100 Bangde Britatu Sans Francico","website_footer_phone_number":"+088 2839 98204","website_footer_email":"mstjulika@gmail.com","copyright":"Copyright \\u00a9 2021. All Rights Reserved","description":"In this article, we\\u2019ll share 21 amazing blog design examples to spark your creativity. In this article, we\\u2019ll share 21 amazing blog design examples to spark your creativity."}',
                'created_at' => '2020-12-01 08:23:24',
                'updated_at' => '2021-12-06 14:10:57',
            ),
            14 => 
            array (
                'id' => 73,
                'data_keys' => 'faq.content',
                'data_values' => '{"heading":"A Frequently Asked Questions","sub_heading":"We always care for our clients, we have tried to answer some frequent questions about your business"}',
                'created_at' => '2020-12-01 08:24:57',
                'updated_at' => '2020-12-13 19:12:35',
            ),
            15 => 
            array (
                'id' => 74,
                'data_keys' => 'faq.element',
                'data_values' => '{"question":"How to making a withdrawal?","answer":"You can make a withdrawal from the Withdraw page. Where possible we are required to send funds back to the payment method that was used to deposit the original funds.\\nFor further details relating to processing times and any applicable fees, please refer to the Withdrawals section of our Payments page.\\nYou can make a withdrawal from the Withdraw page. Where possible we are required to send funds back to the payment method that was used to deposit the original funds.\\nFor further details relating to processing times and any applicable fees, please refer to the Withdrawals section of our Payments page."}',
                'created_at' => '2020-12-01 08:25:16',
                'updated_at' => '2020-12-13 19:17:03',
            ),
            16 => 
            array (
                'id' => 75,
                'data_keys' => 'faq.element',
                'data_values' => '{"question":"I have not received my withdrawal","answer":"The processing time for your withdrawal will vary depending on your payment method.\\nYou can view further information on withdrawal clearance times by visiting our Payment Method page. If you are unable to locate your withdrawal after the processing time has passed, please Contact Us."}',
                'created_at' => '2020-12-01 08:25:21',
                'updated_at' => '2020-12-13 19:16:14',
            ),
            17 => 
            array (
                'id' => 76,
                'data_keys' => 'faq.element',
            'data_values' => '{"question":"Advantages\\/Benefits of Binary MLM Plan","answer":"Unlimited depth: Binary plan allows distributors to add members to unlimited levels and earn a high income.\\n\\nGroup work: With left leg or right spilling preferences, the distributors become active as they have to balance the tree for compensations.\\n\\nQuick growth: Binary plan offers quick business growth opportunities.\\n\\nCarry forward: Unpaid sales volume (SV) after present binary payout cycle is carry forward for the next binary payout cycle.\\n\\nSpillover: New members after completing the first level is spilled over to the unlimited downline levels."}',
                'created_at' => '2020-12-01 08:25:27',
                'updated_at' => '2020-12-13 19:14:34',
            ),
            18 => 
            array (
                'id' => 77,
                'data_keys' => 'faq.element',
                'data_values' => '{"question":"How Does the Binary MLM Plan Works?","answer":"Binary MLM plan is a network marketing compensation strategy used by many top-performing MLM companies. The new members sponsored by distributors are added either on the left or right leg. Upon adding two new members on either side of the subtree, a binary tree gets formed.\\n\\nAll the new members referred after forming a binary tree gets spilled to the downlines.\\n\\nNote: Distributors become a part of the binary plan by purchasing an enrollment package. The enrollment package here means either a service or a list of products. The distributor buys the package and becomes a part of the binary MLM company."}',
                'created_at' => '2020-12-01 08:25:33',
                'updated_at' => '2020-12-13 19:13:23',
            ),
            19 => 
            array (
                'id' => 78,
                'data_keys' => 'faq.element',
            'data_values' => '{"question":"What is a Binary MLM Plan?","answer":"The binary MLM plan is defined as a compensation plan that consists of two legs (left &amp; right) or subtrees under every distributor. Upon adding subtrees, a binary tree gets formed. New members joining after them are spilled over to the downlines or next business level."}',
                'created_at' => '2020-12-01 08:25:42',
                'updated_at' => '2020-12-13 19:10:24',
            ),
            20 => 
            array (
                'id' => 79,
                'data_keys' => 'sign_up.content',
                'data_values' => '{"has_image":"1","title":"Create Your Account","short_details":"Haven\'t registered yet! don\'t worry just fillip all the information below and get your account now.","login_section_title":"Well Come To MLM world","login_section_short_details":"Already have an account?","background_image":"606ef3b0397de1617884080.jpg"}',
                'created_at' => '2020-12-01 08:28:59',
                'updated_at' => '2021-04-08 13:18:20',
            ),
            21 => 
            array (
                'id' => 80,
                'data_keys' => 'sign_in.content',
                'data_values' => '{"has_image":"1","title":"Login Account","short_details":"To login into the site please enter your username and password","register_section_title":"Well Come To MLM world","register_section_short_details":"Haven\'t registered yet! don\'t worry just fillip all the information below and get your account now.","background_image":"606ef392d381e1617884050.jpg"}',
                'created_at' => '2020-12-01 08:29:34',
                'updated_at' => '2021-04-08 13:14:10',
            ),
            22 => 
            array (
                'id' => 81,
                'data_keys' => 'terms_conditions.content',
            'data_values' => '{"title":"Below are the Terms &amp; Conditions by which we operate at MLM Ltd.","description":"<h5 class=\\"title\\" style=\\"margin-top:-7px;margin-bottom:8px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(15,25,50);font-family:Poppins, sans-serif;\\">Terms &amp; Conditions<\\/h5><div class=\\"entry-content\\" style=\\"color:rgb(85,85,85);\\"><p style=\\"font-family:Roboto, sans-serif;margin-top:-10px;margin-bottom:32px;\\"><\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);\\"><font size=\\"3\\" face=\\"times new roman\\">GENERAL TERMS &amp; CONDITIONS OF BUSINESS<br \\/><\\/font><font size=\\"3\\" face=\\"times new roman\\">1. Terms and Conditions<br \\/><\\/font><span style=\\"color:rgb(17,17,17);\\"><font size=\\"3\\" face=\\"times new roman\\">1.1 All and any business is undertaken by Projects Limited is transacted subject to the terms and conditions hereinafter set out, each of which will be incorporated or implied in any agreement between Projects and the Client.<br \\/><\\/font><\\/span><font size=\\"3\\" face=\\"times new roman\\">1.2 In the event of a conflict between these terms and conditions and any other terms and conditions, the former shall prevail unless otherwise expressly agreed by Projects in writing.<br \\/><\\/font><font size=\\"3\\" face=\\"times new roman\\">1.3 Projects is acting in the capacity of an employment agency. Any amendments to these terms and conditions must be in writing and signed by an authorized representative of Projects.<br \\/><\\/font><font size=\\"3\\" face=\\"times new roman\\">1.4 The Client is deemed to have accepted these terms and conditions of business upon the arrangement of an interview, or the interview of a Candidate Introduced by Projects whether effected by Projects directly or by the Client.<\\/font><\\/p><h3 style=\\"font-family:\'Open Sans\', Arial, sans-serif;margin-top:15px;margin-bottom:15px;font-weight:700;line-height:1.3;font-size:14px;color:rgb(0,0,0);padding:0px;\\"><\\/h3><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">\\u201cCandidate\\u201d<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">Means any person, firm or body corporate together with any subsidiary, group or associated company (as defined by the Companies Act 1985) Introduced by Projectus to the Client or by the Client to a Qualifying Third Person with a view to the Client or a Qualifying Third Person offering Employment.<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">\\u201cClient\\u201d<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">Means the person, firm or body corporate together with any subsidiary, group or associated company (as defined by the Companies Act 1985) to whom an Introduction is made<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">\\u201cCommencement of Employment\\u201d<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">Means the first day of the Candidate\'s Employment or the first day the Candidate is entitled to any remuneration whichever is the sooner<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">\\u201cEmployment\\u201d<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">Means any employment by the Client or a Qualifying Third Person of the Candidate whether on PAYE, self employed or a contract for services either directly with the Candidate or through any body corporate or third party irrespective of whether the Employment is subject to a trial period and irrespective of whether the Employment is part time or full time. Employs, Employed and Employment shall be construed accordingly.<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">\\u201cEmployee of Projectus\\u201d<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">Means any person, firm or body corporate whom is employed by Projectus whether on PAYE, self employed or a contract for services either directly or through a Qualifying Third Person irrespective of whether the employment is subject to a trial period and irrespective of whether the employment is part time or full time.<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">\\u201cFee\\u201d<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">Shall mean 30% of the Total Salary plus VAT. The minimum Fee shall be \\u00a35,000 plus VAT<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">\\u201cInterest\\u201d<\\/p><p style=\\"margin-bottom:16px;color:rgb(17,17,17);font-family:\'Open Sans\';font-size:16px;\\">Means the rate of 2.5% above the Bank of England plc base rate, to be compounded monthly.<\\/p><\\/div>"}',
                'created_at' => '2020-12-01 08:29:59',
                'updated_at' => '2020-12-14 00:27:36',
            ),
            23 => 
            array (
                'id' => 82,
                'data_keys' => 'slider.element',
                'data_values' => '{"has_image":["1"],"title":"MLMLab","subtitle":"Multilevel Marketing Platform","left_button":"sign in","left_button_link":"login","right_button":"sign up","right_button_link":"register","description":"We are a dedicated Binary Multilevel Marketing company for every MLM plan with custom features.","background_image":"606ef184693bb1617883524.jpg"}',
                'created_at' => '2020-12-01 09:07:41',
                'updated_at' => '2021-04-08 13:07:07',
            ),
            24 => 
            array (
                'id' => 83,
                'data_keys' => 'breadcrumb.content',
                'data_values' => '{"has_image":"1","background_image":"61af2fe9e71f71638871017.jpg"}',
                'created_at' => '2020-12-01 10:22:55',
                'updated_at' => '2021-12-07 16:26:58',
            ),
            25 => 
            array (
                'id' => 84,
                'data_keys' => 'social_icon.element',
                'data_values' => '{"title":"twitter","social_icon":"<i class=\\"lab la-twitter\\"><\\/i>","url":"#"}',
                'created_at' => '2020-12-05 08:00:59',
                'updated_at' => '2020-12-05 08:00:59',
            ),
            26 => 
            array (
                'id' => 85,
                'data_keys' => 'social_icon.element',
                'data_values' => '{"title":"youtube","social_icon":"<i class=\\"fab fa-youtube\\"><\\/i>","url":"#"}',
                'created_at' => '2020-12-05 08:01:07',
                'updated_at' => '2020-12-05 08:01:07',
            ),
            27 => 
            array (
                'id' => 86,
                'data_keys' => 'social_icon.element',
                'data_values' => '{"title":"youtube","social_icon":"<i class=\\"lab la-instagram\\"><\\/i>","url":"#"}',
                'created_at' => '2020-12-05 08:01:15',
                'updated_at' => '2020-12-05 08:01:15',
            ),
            28 => 
            array (
                'id' => 87,
                'data_keys' => 'social_icon.element',
                'data_values' => '{"title":"telegram","social_icon":"<i class=\\"lab la-telegram-plane\\"><\\/i>","url":"#"}',
                'created_at' => '2020-12-05 08:02:49',
                'updated_at' => '2020-12-05 08:02:49',
            ),
            29 => 
            array (
                'id' => 89,
                'data_keys' => 'feature.content',
                'data_values' => '{"heading":"Feature Overview","sub_heading":"We support the most secure services and features. This secured website supports a user-friendly interface and various attractive features that ready to use."}',
                'created_at' => '2020-12-13 19:19:28',
                'updated_at' => '2020-12-17 21:31:51',
            ),
            30 => 
            array (
                'id' => 90,
                'data_keys' => 'feature.element',
                'data_values' => '{"title":"Full Control","description":"You will have full control over the system. This includes everything from investment plans to payment processor configuration.","feature_icon":"<i class=\\"las la-file-contract\\"><\\/i>"}',
                'created_at' => '2020-12-13 19:20:16',
                'updated_at' => '2020-12-13 19:20:16',
            ),
            31 => 
            array (
                'id' => 93,
                'data_keys' => 'slider.element',
                'data_values' => '{"has_image":["1"],"title":"MlmLab","subtitle":"Multilevel Marketing Platform","left_button":"sign in","left_button_link":"login","right_button":"sign up","right_button_link":"register","description":"We are a dedicated Binary Multilevel Marketing company for every MLM plan with custom features.","background_image":"60714f72554721618038642.jpg"}',
                'created_at' => '2021-04-10 08:08:45',
                'updated_at' => '2021-04-10 08:10:50',
            ),
            32 => 
            array (
                'id' => 94,
                'data_keys' => 'banner.content',
                'data_values' => '{"has_image":"1","heading":"Binaryecom Is Just What Your Business Needs","sub_heading":"Behind the word mountains, far from the countries Vokalia and Conson antia, there seedBehind the word mountains, far from the countries Vokalia and Conson antia, there seed","left_button":"Get started","left_button_link":"register","right_button":"Products","right_button_link":"products","image":"61af265b85b201638868571.jpg"}',
                'created_at' => '2021-10-18 10:57:10',
                'updated_at' => '2021-12-07 15:50:07',
            ),
            33 => 
            array (
                'id' => 95,
                'data_keys' => 'service.element',
                'data_values' => '{"icon":"<i class=\\"las la-graduation-cap\\"><\\/i>","title":"Marketing Strategy","description":"orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an u"}',
                'created_at' => '2021-10-23 07:55:14',
                'updated_at' => '2021-10-23 07:55:14',
            ),
            34 => 
            array (
                'id' => 96,
                'data_keys' => 'service.element',
                'data_values' => '{"icon":"<i class=\\"las la-clipboard-list\\"><\\/i>","title":"Get Commission","description":"Harness the value of your data by keeping it all in one place with data visualization If you are going to"}',
                'created_at' => '2021-10-23 07:55:48',
                'updated_at' => '2021-10-23 07:55:48',
            ),
            35 => 
            array (
                'id' => 97,
                'data_keys' => 'service.element',
                'data_values' => '{"icon":"<i class=\\"las la-project-diagram\\"><\\/i>","title":"Profit Gain","description":"Harness the value of your data by keeping it all in one place with data visualization If you are going to"}',
                'created_at' => '2021-10-23 07:56:44',
                'updated_at' => '2021-10-23 07:56:44',
            ),
            36 => 
            array (
                'id' => 98,
                'data_keys' => 'service.element',
                'data_values' => '{"icon":"<i class=\\"lab la-bandcamp\\"><\\/i>","title":"Marketing Strategy","description":"Harness the value of your data by keeping it all in one place with data visualization If you are going to"}',
                'created_at' => '2021-10-23 07:57:11',
                'updated_at' => '2021-10-23 07:57:11',
            ),
            37 => 
            array (
                'id' => 99,
                'data_keys' => 'service.element',
                'data_values' => '{"icon":"<i class=\\"las la-money-bill-alt\\"><\\/i>","title":"Marketing Strategy","description":"Harness the value of your data by keeping it all in one place with data visualization If you are going to"}',
                'created_at' => '2021-10-23 07:57:46',
                'updated_at' => '2021-10-23 07:57:46',
            ),
            38 => 
            array (
                'id' => 100,
                'data_keys' => 'service.element',
                'data_values' => '{"icon":"<i class=\\"las la-hand-paper\\"><\\/i>","title":"Profit Gain","description":"Harness the value of your data by keeping it all in one place with data visualization If you are going to"}',
                'created_at' => '2021-10-23 07:58:10',
                'updated_at' => '2021-10-23 07:58:10',
            ),
            39 => 
            array (
                'id' => 101,
                'data_keys' => 'plan.content',
                'data_values' => '{"has_image":"1","heading":"Our Pricing Plan","sub_heading":"Choose a Plan What Suits to You"}',
                'created_at' => '2021-10-23 08:29:18',
                'updated_at' => '2021-10-23 08:29:18',
            ),
            40 => 
            array (
                'id' => 102,
                'data_keys' => 'refer.content',
                'data_values' => '{"heading":"Refer a Friend &amp; Get Commission on every Transection","description":"Refer a frined and earn money from online. voluptatum saepe distinctio, cumque aut consequuntur enim consectetur non dicta esse a blanditiis corrupti tempora magnam odit maiores, quasi natus?","button_text":"Get Statrted","button_url":"#","has_image":"1","background_image":"617406882b7241634993800.jpg","refer_image":"6173be1dedf681634975261.png"}',
                'created_at' => '2021-10-23 08:47:41',
                'updated_at' => '2021-10-23 13:56:40',
            ),
            41 => 
            array (
                'id' => 103,
                'data_keys' => 'transaction.content',
                'data_values' => '{"heading":"Our Transection Table","sub_heading":"Our Transection Table"}',
                'created_at' => '2021-10-23 14:00:17',
                'updated_at' => '2021-10-23 14:00:17',
            ),
            42 => 
            array (
                'id' => 104,
                'data_keys' => 'team.element',
                'data_values' => '{"has_image":["1"],"name":"Robinson Datag","designation":"Web Developer","facebook_link":"#","twitter_link":"#","instagram_link":"#","vimeo_link":"#","image":"61740b7599fab1634995061.jpg"}',
                'created_at' => '2021-10-23 14:17:41',
                'updated_at' => '2021-12-07 13:57:15',
            ),
            43 => 
            array (
                'id' => 105,
                'data_keys' => 'team.element',
                'data_values' => '{"has_image":["1"],"name":"Robinson Datag","designation":"Web Developer","facebook_link":"#","twitter_link":"#","instarag_link":"#","vimeo_link":"#","image":"61740bc99d1211634995145.jpg"}',
                'created_at' => '2021-10-23 14:17:55',
                'updated_at' => '2021-10-23 14:19:05',
            ),
            44 => 
            array (
                'id' => 106,
                'data_keys' => 'team.element',
                'data_values' => '{"has_image":["1"],"name":"Robinson Datag","designation":"Laravel Developer","facebook_link":"3","twitter_link":"#","instarag_link":"#","vimeo_link":"#","image":"61740bb6a99b61634995126.jpg"}',
                'created_at' => '2021-10-23 14:18:46',
                'updated_at' => '2021-10-23 14:18:46',
            ),
            45 => 
            array (
                'id' => 107,
                'data_keys' => 'product.content',
                'data_values' => '{"heading":"Featured Products","sub_heading":"Trending This Week."}',
                'created_at' => '2021-10-23 14:47:50',
                'updated_at' => '2021-10-23 14:47:50',
            ),
            46 => 
            array (
                'id' => 108,
                'data_keys' => 'login.content',
                'data_values' => '{"title":"Welcome to","heading":"Multi Level Marketing Rock","sub_heading":"Login to Access Account, changes to your Acconnt as per your Need.","has_image":"1","login_image":"61756759612201635084121.jpg"}',
                'created_at' => '2021-10-24 15:02:01',
                'updated_at' => '2021-10-24 15:02:01',
            ),
            47 => 
            array (
                'id' => 109,
                'data_keys' => 'register.content',
                'data_values' => '{"title":"WELCOME TO","heading":"Multi Level Marketing Rock","sub_heading":"Create an Account to get extra facility on MLM Hero and Access all Plans.","has_image":"1","register_image":"61766d7fe740c1635151231.jpg"}',
                'created_at' => '2021-10-25 09:40:31',
                'updated_at' => '2021-10-25 09:40:32',
            ),
            48 => 
            array (
                'id' => 110,
                'data_keys' => 'policy_pages.element',
            'data_values' => '{"title":"Privacy and Policies","details":"<div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">What information do we collect?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We gather data from you when you register on our site, submit a request, buy any services, react to an overview, or round out a structure. At the point when requesting any assistance or enrolling on our site, as suitable, you might be approached to enter your: name, email address, or telephone number. You may, nonetheless, visit our site anonymously.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">How do we protect your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">All provided delicate\\/credit data is sent through Stripe.<br \\/>After an exchange, your private data (credit cards, social security numbers, financials, and so on) won\'t be put away on our workers.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">Do we disclose any information to outside parties?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We don\'t sell, exchange, or in any case move to outside gatherings by and by recognizable data. This does exclude confided in outsiders who help us in working our site, leading our business, or adjusting you, since those gatherings consent to keep this data private. We may likewise deliver your data when we accept discharge is suitable to follow the law, implement our site strategies, or ensure our own or others\' rights, property, or wellbeing.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">Children\'s Online Privacy Protection Act Compliance<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We are consistent with the prerequisites of COPPA (Children\'s Online Privacy Protection Act), we don\'t gather any data from anybody under 13 years old. Our site, items, and administrations are completely coordinated to individuals who are in any event 13 years of age or more established.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">Changes to our Privacy Policy<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">If we decide to change our privacy policy, we will post those changes on this page.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">How long we retain your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">At the point when you register for our site, we cycle and keep your information we have about you however long you don\'t erase the record or withdraw yourself (subject to laws and guidelines).<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">What we don\\u2019t do with your data<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We don\'t and will never share, unveil, sell, or in any case give your information to different organizations for the promoting of their items or administrations.<\\/p><\\/div>"}',
                'created_at' => '2021-10-25 11:17:39',
                'updated_at' => '2021-12-06 15:19:33',
            ),
            49 => 
            array (
                'id' => 111,
                'data_keys' => 'policy_pages.element',
            'data_values' => '{"title":"Terms and Condition","details":"<div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">What information do we collect?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We gather data from you when you register on our site, submit a request, buy any services, react to an overview, or round out a structure. At the point when requesting any assistance or enrolling on our site, as suitable, you might be approached to enter your: name, email address, or telephone number. You may, nonetheless, visit our site anonymously.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">How do we protect your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">All provided delicate\\/credit data is sent through Stripe.<br \\/>After an exchange, your private data (credit cards, social security numbers, financials, and so on) won\'t be put away on our workers.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">Do we disclose any information to outside parties?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We don\'t sell, exchange, or in any case move to outside gatherings by and by recognizable data. This does exclude confided in outsiders who help us in working our site, leading our business, or adjusting you, since those gatherings consent to keep this data private. We may likewise deliver your data when we accept discharge is suitable to follow the law, implement our site strategies, or ensure our own or others\' rights, property, or wellbeing.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">Children\'s Online Privacy Protection Act Compliance<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We are consistent with the prerequisites of COPPA (Children\'s Online Privacy Protection Act), we don\'t gather any data from anybody under 13 years old. Our site, items, and administrations are completely coordinated to individuals who are in any event 13 years of age or more established.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">Changes to our Privacy Policy<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">If we decide to change our privacy policy, we will post those changes on this page.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">How long we retain your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">At the point when you register for our site, we cycle and keep your information we have about you however long you don\'t erase the record or withdraw yourself (subject to laws and guidelines).<\\/p><\\/div><div class=\\"mb-5\\" style=\\"color:rgb(111,111,111);font-family:Nunito, sans-serif;margin-bottom:3rem;\\"><h3 class=\\"mb-3\\" style=\\"font-weight:600;line-height:1.3;font-size:24px;font-family:Exo, sans-serif;color:rgb(54,54,54);\\">What we don\\u2019t do with your data<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;font-size:18px;\\">We don\'t and will never share, unveil, sell, or in any case give your information to different organizations for the promoting of their items or administrations.<\\/p><\\/div>"}',
                'created_at' => '2021-10-25 11:18:03',
                'updated_at' => '2021-12-06 15:19:07',
            ),
            50 => 
            array (
                'id' => 112,
                'data_keys' => 'cookie.data',
                'data_values' => '{"link":"#","description":"At first you have to create an Accont. just go to the register page, fill up the form and get registered.","status":1}',
                'created_at' => NULL,
                'updated_at' => '2021-12-07 13:39:09',
            ),
            51 => 
            array (
                'id' => 113,
                'data_keys' => 'testimonial.element',
                'data_values' => '{"has_image":"1","author":"Farhan Ahmed","designation":"CEO at Google","quote":"Customer testimonials are a beneficial type of social proof: they tell potential new customers about the successes and triumphs others have experienced. And because these are real people.","image":"61acaaa8bafba1638705832.jpg"}',
                'created_at' => '2021-12-05 18:33:52',
                'updated_at' => '2021-12-05 18:33:52',
            ),
            52 => 
            array (
                'id' => 114,
                'data_keys' => 'testimonial.element',
                'data_values' => '{"has_image":"1","author":"Farhan Ahmed","designation":"CEO at Google","quote":"Customer testimonials are a beneficial type of social proof: they tell potential new customers about the successes and triumphs others have experienced. And because these are real people.","image":"61acaac1510b51638705857.jpg"}',
                'created_at' => '2021-12-05 18:34:17',
                'updated_at' => '2021-12-05 18:34:17',
            ),
            53 => 
            array (
                'id' => 115,
                'data_keys' => 'testimonial.element',
                'data_values' => '{"has_image":"1","author":"Farhan Ahmed","designation":"CEO at Google","quote":"Customer testimonials are a beneficial type of social proof: they tell potential new customers about the successes and triumphs others have experienced. And because these are real people.","image":"61acaad39182b1638705875.jpg"}',
                'created_at' => '2021-12-05 18:34:35',
                'updated_at' => '2021-12-05 18:34:35',
            ),
            54 => 
            array (
                'id' => 116,
                'data_keys' => 'testimonial.element',
                'data_values' => '{"has_image":"1","author":"Farhan Ahmed","designation":"CEO at Google","quote":"Customer testimonials are a beneficial type of social proof: they tell potential new customers about the successes and triumphs others have experienced. And because these are real people.","image":"61acaae97f4651638705897.jpg"}',
                'created_at' => '2021-12-05 18:34:57',
                'updated_at' => '2021-12-05 18:34:57',
            ),
            55 => 
            array (
                'id' => 117,
                'data_keys' => 'testimonial.element',
                'data_values' => '{"has_image":"1","author":"Farhan Ahmed","designation":"CEO at Google","quote":"Customer testimonials are a beneficial type of social proof: they tell potential new customers about the successes and triumphs others have experienced. And because these are real people.","image":"61adba0eeecac1638775310.jpg"}',
                'created_at' => '2021-12-06 13:51:50',
                'updated_at' => '2021-12-06 13:51:51',
            ),
            56 => 
            array (
                'id' => 118,
                'data_keys' => 'blog.element',
            'data_values' => '{"has_image":["1"],"title":"What to do When You are About to Meet an Accident","description":"<p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results The FreeSpirit FAQ page combines useful information navigational features with interactive content to empower the user to progress through the site and making buying decisions faster. pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective. enable a more effective mobile-first experience and to support quick top-level access to information without excessive scrolling.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">It also includes a useful prompt to ask if the information was useful and to gather user feedback for improving the resource. These predominantly rely on the pre-results It is text heavy, blocked into key topic areas, and has extensive access to all the key support areas you could ever need.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">WorldFirst\\u2019sFAQs hub provides single-click content segmentation, plus view all capabilities which place functionality and usability of the resource first \\u2013 an important part of an effective frequently asked questions resource..Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results (Google Answers and Featured Snippets) and can be targeted specifically with FAQ pages.An effective FAQ pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p>","blog_image":"61add3e6e3f331638781926.jpg"}',
                'created_at' => '2021-12-06 15:42:06',
                'updated_at' => '2021-12-06 15:42:07',
            ),
            57 => 
            array (
                'id' => 119,
                'data_keys' => 'blog.element',
            'data_values' => '{"has_image":["1"],"title":"What to do When You are About to Meet an Accident","description":"<p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results The FreeSpirit FAQ page combines useful information navigational features with interactive content to empower the user to progress through the site and making buying decisions faster. pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective. enable a more effective mobile-first experience and to support quick top-level access to information without excessive scrolling.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">It also includes a useful prompt to ask if the information was useful and to gather user feedback for improving the resource. These predominantly rely on the pre-results It is text heavy, blocked into key topic areas, and has extensive access to all the key support areas you could ever need.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">WorldFirst\\u2019sFAQs hub provides single-click content segmentation, plus view all capabilities which place functionality and usability of the resource first \\u2013 an important part of an effective frequently asked questions resource..Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results (Google Answers and Featured Snippets) and can be targeted specifically with FAQ pages.An effective FAQ pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p>","blog_image":"61add3f4736a51638781940.jpg"}',
                'created_at' => '2021-12-06 15:42:20',
                'updated_at' => '2021-12-06 15:42:20',
            ),
            58 => 
            array (
                'id' => 120,
                'data_keys' => 'blog.element',
            'data_values' => '{"has_image":["1"],"title":"What to do When You are About to Meet an Accident","description":"<p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results The FreeSpirit FAQ page combines useful information navigational features with interactive content to empower the user to progress through the site and making buying decisions faster. pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective. enable a more effective mobile-first experience and to support quick top-level access to information without excessive scrolling.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">It also includes a useful prompt to ask if the information was useful and to gather user feedback for improving the resource. These predominantly rely on the pre-results It is text heavy, blocked into key topic areas, and has extensive access to all the key support areas you could ever need.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">WorldFirst\\u2019sFAQs hub provides single-click content segmentation, plus view all capabilities which place functionality and usability of the resource first \\u2013 an important part of an effective frequently asked questions resource..Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results (Google Answers and Featured Snippets) and can be targeted specifically with FAQ pages.An effective FAQ pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p>","blog_image":"61add401c6e991638781953.jpg"}',
                'created_at' => '2021-12-06 15:42:33',
                'updated_at' => '2021-12-06 15:42:34',
            ),
            59 => 
            array (
                'id' => 121,
                'data_keys' => 'blog.element',
            'data_values' => '{"has_image":["1"],"title":"What to do When You are About to Meet an Accident","description":"<p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results The FreeSpirit FAQ page combines useful information navigational features with interactive content to empower the user to progress through the site and making buying decisions faster. pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective. enable a more effective mobile-first experience and to support quick top-level access to information without excessive scrolling.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">It also includes a useful prompt to ask if the information was useful and to gather user feedback for improving the resource. These predominantly rely on the pre-results It is text heavy, blocked into key topic areas, and has extensive access to all the key support areas you could ever need.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">WorldFirst\\u2019sFAQs hub provides single-click content segmentation, plus view all capabilities which place functionality and usability of the resource first \\u2013 an important part of an effective frequently asked questions resource..Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results (Google Answers and Featured Snippets) and can be targeted specifically with FAQ pages.An effective FAQ pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p>","blog_image":"61add40acd25f1638781962.jpg"}',
                'created_at' => '2021-12-06 15:42:42',
                'updated_at' => '2021-12-06 15:42:43',
            ),
            60 => 
            array (
                'id' => 122,
                'data_keys' => 'blog.element',
            'data_values' => '{"has_image":["1"],"title":"What to do When You are About to Meet an Accident","description":"<p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results The FreeSpirit FAQ page combines useful information navigational features with interactive content to empower the user to progress through the site and making buying decisions faster. pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective. enable a more effective mobile-first experience and to support quick top-level access to information without excessive scrolling.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">It also includes a useful prompt to ask if the information was useful and to gather user feedback for improving the resource. These predominantly rely on the pre-results It is text heavy, blocked into key topic areas, and has extensive access to all the key support areas you could ever need.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">WorldFirst\\u2019sFAQs hub provides single-click content segmentation, plus view all capabilities which place functionality and usability of the resource first \\u2013 an important part of an effective frequently asked questions resource..Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results (Google Answers and Featured Snippets) and can be targeted specifically with FAQ pages.An effective FAQ pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p>","blog_image":"61add417d659e1638781975.jpg"}',
                'created_at' => '2021-12-06 15:42:55',
                'updated_at' => '2021-12-06 15:42:56',
            ),
            61 => 
            array (
                'id' => 123,
                'data_keys' => 'blog.element',
            'data_values' => '{"has_image":["1"],"title":"What to do When You are About to Meet an Accident","description":"<p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results The FreeSpirit FAQ page combines useful information navigational features with interactive content to empower the user to progress through the site and making buying decisions faster. pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective. enable a more effective mobile-first experience and to support quick top-level access to information without excessive scrolling.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">It also includes a useful prompt to ask if the information was useful and to gather user feedback for improving the resource. These predominantly rely on the pre-results It is text heavy, blocked into key topic areas, and has extensive access to all the key support areas you could ever need.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">WorldFirst\\u2019sFAQs hub provides single-click content segmentation, plus view all capabilities which place functionality and usability of the resource first \\u2013 an important part of an effective frequently asked questions resource..Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p><p style=\\"margin-right:0px;margin-bottom:15px;margin-left:0px;padding:0px;color:rgb(119,119,119);font-family:\'Open Sans\', sans-serif;font-size:16px;\\">These predominantly rely on the pre-results (Google Answers and Featured Snippets) and can be targeted specifically with FAQ pages.An effective FAQ pageReflects your audience\\u2019s needs.Covers a broad range of intent (transactional, informational, etc.).Frequently gets updated based on new data insights.Lands new users to the website by solving problems.Drives internal pageviews to other important pages. Fuels blog (and deeper content) creation.Showcases expertise, trust, and authority within your niche.Now let\\u2019s look at 25 great examples of FAQ page\\/resources, as well as why they\\u2019re so effective.<\\/p>","blog_image":"61add427420001638781991.jpg"}',
                'created_at' => '2021-12-06 15:43:11',
                'updated_at' => '2021-12-06 15:43:11',
            ),
            62 => 
            array (
                'id' => 124,
                'data_keys' => 'contact_us.element',
                'data_values' => '{"has_image":"1","title":"Company Location","content":"23\\/A Hamham Tower Purba, Delhi, India","image":"61ade081dce571638785153.png"}',
                'created_at' => '2021-12-06 16:35:53',
                'updated_at' => '2021-12-06 16:35:53',
            ),
            63 => 
            array (
                'id' => 125,
                'data_keys' => 'contact_us.element',
                'data_values' => '{"has_image":"1","title":"Phone Number","content":"+01234 5678 9000","image":"61ade0923dcb31638785170.png"}',
                'created_at' => '2021-12-06 16:36:10',
                'updated_at' => '2021-12-06 16:36:10',
            ),
            64 => 
            array (
                'id' => 126,
                'data_keys' => 'contact_us.element',
                'data_values' => '{"has_image":"1","title":"Email Address","content":"demo@example.com","image":"61ade0aa711791638785194.png"}',
                'created_at' => '2021-12-06 16:36:34',
                'updated_at' => '2021-12-06 16:36:34',
            ),
            65 => 
            array (
                'id' => 127,
                'data_keys' => 'policy_pages.element',
            'data_values' => '{"title":"Refund Policy","details":"<div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">What information do we collect?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We gather data from you when you register on our site, submit a request, buy any services, react to an overview, or round out a structure. At the point when requesting any assistance or enrolling on our site, as suitable, you might be approached to enter your: name, email address, or telephone number. You may, nonetheless, visit our site anonymously.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">How do we protect your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">All provided delicate\\/credit data is sent through Stripe.<br \\/>After an exchange, your private data (credit cards, social security numbers, financials, and so on) won\'t be put away on our workers.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">Do we disclose any information to outside parties?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We don\'t sell, exchange, or in any case move to outside gatherings by and by recognizable data. This does exclude confided in outsiders who help us in working our site, leading our business, or adjusting you, since those gatherings consent to keep this data private. We may likewise deliver your data when we accept discharge is suitable to follow the law, implement our site strategies, or ensure our own or others\' rights, property, or wellbeing.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">Children\'s Online Privacy Protection Act Compliance<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We are consistent with the prerequisites of COPPA (Children\'s Online Privacy Protection Act), we don\'t gather any data from anybody under 13 years old. Our site, items, and administrations are completely coordinated to individuals who are in any event 13 years of age or more established.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">Changes to our Privacy Policy<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">If we decide to change our privacy policy, we will post those changes on this page.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">How long we retain your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">At the point when you register for our site, we cycle and keep your information we have about you however long you don\'t erase the record or withdraw yourself (subject to laws and guidelines).<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">What we don\\u2019t do with your data<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We don\'t and will never share, unveil, sell, or in any case give your information to different organizations for the promoting of their items or administrations.<\\/p><\\/div>"}',
                'created_at' => '2021-12-07 14:22:50',
                'updated_at' => '2021-12-07 14:22:50',
            ),
            66 => 
            array (
                'id' => 128,
                'data_keys' => 'policy_pages.element',
            'data_values' => '{"title":"Commission Policy","details":"<div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">What information do we collect?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We gather data from you when you register on our site, submit a request, buy any services, react to an overview, or round out a structure. At the point when requesting any assistance or enrolling on our site, as suitable, you might be approached to enter your: name, email address, or telephone number. You may, nonetheless, visit our site anonymously.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">How do we protect your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">All provided delicate\\/credit data is sent through Stripe.<br \\/>After an exchange, your private data (credit cards, social security numbers, financials, and so on) won\'t be put away on our workers.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">Do we disclose any information to outside parties?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We don\'t sell, exchange, or in any case move to outside gatherings by and by recognizable data. This does exclude confided in outsiders who help us in working our site, leading our business, or adjusting you, since those gatherings consent to keep this data private. We may likewise deliver your data when we accept discharge is suitable to follow the law, implement our site strategies, or ensure our own or others\' rights, property, or wellbeing.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">Children\'s Online Privacy Protection Act Compliance<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We are consistent with the prerequisites of COPPA (Children\'s Online Privacy Protection Act), we don\'t gather any data from anybody under 13 years old. Our site, items, and administrations are completely coordinated to individuals who are in any event 13 years of age or more established.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">Changes to our Privacy Policy<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">If we decide to change our privacy policy, we will post those changes on this page.<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">How long we retain your information?<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">At the point when you register for our site, we cycle and keep your information we have about you however long you don\'t erase the record or withdraw yourself (subject to laws and guidelines).<\\/p><\\/div><div class=\\"mb-5\\" style=\\"margin-bottom:3rem;color:rgb(111,111,111);font-family:Nunito, sans-serif;\\"><h3 class=\\"mb-3\\" style=\\"margin-top:-5px;font-weight:600;line-height:1.3;font-size:24px;color:rgb(54,54,54);font-family:Exo, sans-serif;\\">What we don\\u2019t do with your data<\\/h3><p class=\\"font-18\\" style=\\"margin-right:0px;margin-left:0px;padding:0px;font-size:18px;\\">We don\'t and will never share, unveil, sell, or in any case give your information to different organizations for the promoting of their items or administrations.<\\/p><\\/div>"}',
                'created_at' => '2021-12-07 14:23:20',
                'updated_at' => '2021-12-07 14:23:20',
            ),
            67 => 
            array (
                'id' => 129,
                'data_keys' => 'about.content',
                'data_values' => '{"has_image":"1","heading":"About Us","sub_heading":"Binaryecom is the best Multi Level Marketing Web Platform","description":"orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an u orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an u","button_text":"Learn more","button_url":"blog","about_image":"61af59c313a061638881731.png"}',
                'created_at' => '2021-12-07 13:54:20',
                'updated_at' => '2021-12-07 13:55:31',
            ),
        ));
        
        
    }
}