<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 4,
                'name' => 'Blog',
                'slug' => 'blog',
                'tempname' => 'templates.basic.',
                'secs' => NULL,
                'is_default' => 1,
                'created_at' => '2020-10-22 02:14:43',
                'updated_at' => '2020-12-01 04:40:45',
            ),
            1 => 
            array (
                'id' => 5,
                'name' => 'Contact',
                'slug' => 'contact',
                'tempname' => 'templates.basic.',
                'secs' => NULL,
                'is_default' => 1,
                'created_at' => '2020-10-22 02:14:53',
                'updated_at' => '2020-10-22 02:14:53',
            ),
            2 => 
            array (
                'id' => 6,
                'name' => 'Faq',
                'slug' => 'faq',
                'tempname' => 'templates.basic.',
                'secs' => '["faq"]',
                'is_default' => 0,
                'created_at' => '2020-12-01 04:27:45',
                'updated_at' => '2020-12-01 04:45:40',
            ),
            3 => 
            array (
                'id' => 7,
                'name' => 'Home',
                'slug' => 'home',
                'tempname' => 'templates.basic.',
                'secs' => '["about","service","how_it_works","plan","refer","transaction","team","product","testimonial","blog"]',
                'is_default' => 1,
                'created_at' => '2021-10-18 03:51:47',
                'updated_at' => '2021-12-07 13:58:45',
            ),
            4 => 
            array (
                'id' => 8,
                'name' => 'Product',
                'slug' => 'product',
                'tempname' => 'templates.basic.',
                'secs' => NULL,
                'is_default' => 1,
                'created_at' => '2021-12-06 09:22:31',
                'updated_at' => '2021-12-06 09:35:11',
            ),
        ));
        
        
    }
}