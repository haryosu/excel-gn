<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('plans')->delete();
        
        \DB::table('plans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'gold',
                'price' => '1000000.00000000',
                'bv' => 1,
                'ref_com' => '100000.00000000',
                'tree_com' => '12.00000000',
                'status' => 1,
                'updated_at' => '2022-12-15 19:14:59',
                'created_at' => '2022-12-11 03:38:20',
            ),
        ));
        
        
    }
}