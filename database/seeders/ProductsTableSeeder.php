<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'test',
                'category_id' => 1,
                'sku' => '',
                'price' => '23234234234234.00000000',
                'quantity' => 12323,
                'description' => '123212 fasdf werqwe asdf <br>',
                'specifications' => NULL,
                'thumbnail' => '63d1491145e7d1674660113.jpg',
                'images' => NULL,
                'meta_title' => '234234',
                'meta_keyword' => '["asdasd asdasd","asdads"]',
                'meta_description' => '234234234',
                'bv' => 12,
                'status' => 1,
                'is_featured' => 1,
                'created_at' => '2023-01-25 22:21:53',
                'updated_at' => '2023-01-25 22:24:26',
            ),
        ));
        
        
    }
}