<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SupportMessagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('support_messages')->delete();
        
        \DB::table('support_messages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'supportticket_id' => 1,
                'admin_id' => 0,
                'message' => 'mas randedet',
                'created_at' => '2022-12-11 12:53:52',
                'updated_at' => '2022-12-11 12:53:52',
            ),
            1 => 
            array (
                'id' => 2,
                'supportticket_id' => 1,
                'admin_id' => 1,
                'message' => 'kerjo',
                'created_at' => '2022-12-11 12:54:20',
                'updated_at' => '2022-12-11 12:54:20',
            ),
        ));
        
        
    }
}