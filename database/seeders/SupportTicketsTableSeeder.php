<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SupportTicketsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('support_tickets')->delete();
        
        \DB::table('support_tickets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'name' => 'user user',
                'email' => 'user@user.com',
                'ticket' => '332450',
                'subject' => 'test',
                'status' => 1,
                'priority' => 3,
                'last_reply' => '2022-12-11 12:54:20',
                'created_at' => '2022-12-11 12:53:52',
                'updated_at' => '2022-12-11 12:54:20',
            ),
        ));
        
        
    }
}