<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserExtrasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_extras')->delete();
        
        \DB::table('user_extras')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'paid_left' => 0,
                'paid_right' => 0,
                'free_left' => 0,
                'free_right' => 0,
                'bv_left' => '0.00000000',
                'bv_right' => '0.00000000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}