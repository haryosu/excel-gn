<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserLoginsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_logins')->delete();
        
        \DB::table('user_logins')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'user_ip' => '127.0.0.1',
                'city' => '',
                'country' => '',
                'country_code' => '',
                'longitude' => '',
                'latitude' => '',
                'browser' => 'Firefox',
                'os' => 'Windows 10',
                'created_at' => '2022-12-11 03:29:16',
                'updated_at' => '2022-12-11 03:29:16',
            ),
        ));
        
        
    }
}