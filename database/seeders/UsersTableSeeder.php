<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'ref_id' => 0,
                'pos_id' => 0,
                'position' => 0,
                'plan_id' => 1,
                'total_invest' => '0.00000000',
                'firstname' => 'user',
                'lastname' => 'user',
                'username' => 'user',
                'email' => 'user@user.com',
                'country_code' => 'BH',
                'mobile' => '5454545454',
                'balance' => '0.00000000',
                'password' => '$2y$10$QvD49iOEHmGWKDmImszCZu69q1RRKA.3YoAVWCMvOVrmvQkSvFedO',
                'image' => NULL,
                'address' => '{"address":"Address a","city":"City","state":"State","zip":"1208","country":"Bahrain"}',
                'status' => 1,
                'ev' => 1,
                'sv' => 1,
                'ver_code' => '123asd',
                'ver_code_send_at' => NULL,
                'ts' => 1,
                'tv' => 1,
                'tsc' => NULL,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => '2023-01-25 22:25:33',
            ),
        ));
        
        
    }
}