@extends('admin.layouts.app')

@push('style')
    <link href="{{ asset('assets/admin/css/tree.css') }}" rel="stylesheet">
@endpush

@section('panel') 

    <div class="card mt-3">
        <div class="card-body">
            @php
                echo view('admin.users.tree_new_form_register', ['ref' => $userRef, 'province' => $area]);
                echo view('admin.users.tree_new', ['treeNew' => $treeNew, 'ref' => $userRef]);
            @endphp
        </div>
    </div>
 
@endsection
@push('script')
    <script>
        'use strict';
        // (function($) {
        //     $('.showDetails').on('click', function() {
        //         var modal = $('#exampleModalCenter');
        //         $('.tree_name').text($(this).data('name'));
        //         $('.tree_url').attr({
        //             "href": $(this).data('treeurl')
        //         });
        //         $('.tree_status').text($(this).data('status'));
        //         $('.tree_plan').text($(this).data('plan'));
        //         $('.tree_image').attr({
        //             "src": $(this).data('image')
        //         });
        //         $('.user-details-header').removeClass('Paid');
        //         $('.user-details-header').removeClass('Free');
        //         $('.user-details-header').addClass($(this).data('status'));
        //         $('.tree_ref').text($(this).data('refby'));
        //         $('.lbv').text($(this).data('lbv'));
        //         $('.rbv').text($(this).data('rbv'));
        //         $('.lpaid').text($(this).data('lpaid'));
        //         $('.rpaid').text($(this).data('rpaid'));
        //         $('.lfree').text($(this).data('lfree'));
        //         $('.rfree').text($(this).data('rfree'));
        //         $('#exampleModalCenter').modal('show');
        //     });

        //     $(document).ready(function() {
        //         $("#form-registers-act").submit(function(e) {
        //             e.preventDefault();

        //             $.ajax({
        //                 url: $(this).attr('action'),
        //                 type: 'POST',
        //                 data: $(this).serialize(),
        //                 dataType: "JSON",
        //                 success: function(data) {
        //                     if (data.code == 200) {
        //                         $("#form-registers-act")[0].reset()
        //                         $('#formModelRegister').modal('hide')
        //                         iziToast.success({
        //                             message: data.message,
        //                             position: "topRight",
        //                             onClosed: function() {
        //                                 window.location.reload()
        //                             }
        //                         });
        //                     } else {
        //                         if (data.message) {
        //                             $.each(data.message, function(e, f) {
        //                                 iziToast.warning({
        //                                     message: f,
        //                                     position: "topRight"
        //                                 });
        //                             })
        //                         }

        //                     }
        //                 },
        //                 error: function(data) {
        //                     console.log(data)
        //                 }
        //             });

        //         });
        //     })
        // })(jQuery)
    </script>
@endpush
@push('breadcrumb-plugins')
    <form action="{{ route('admin.users.other.tree.search') }}" method="GET" class="form-inline float-right bg--white">
        <div class="input-group has_append">
            <input type="text" name="username" class="form-control" placeholder="@lang('Search by username')">
            <div class="input-group-append">
                <button class="btn btn--success" type="submit"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
@endpush
