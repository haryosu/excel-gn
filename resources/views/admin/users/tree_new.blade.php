<link rel="stylesheet" href="{{ asset('assets/global/css/tree_new.css') }}" />

<div class="row">
    <div class="col-lg-12">
        <h4>New Tree</h4>
        @if ($ref['countRef'] < 6) <button class="openFormRegister btn btn-success mt-3">
            <em class="fa fa-user"></em> Form Register
            </button>
            @else
            <button class="btn btn-secondary mt-3">
                <em class="fa fa-user"></em> Form Register
            </button>
            @endif
    </div>
</div>

<div class="body user-body user-scroll text-center">
    <span>loading...</span>
    <div class="user-tree text-center">
        {!! $treeNew !!}
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
$(function() {
    $(document).ready(function() {
        $(".user-scroll").find('span').remove()
        $(".main-ul").removeClass('hidden-tree')

        $('.openFormRegister').on('click', function() {
            $("#form-registers-act")[0].reset()
            var modal = $('#formModelRegister')

            modal.modal('show')
        });

        $(".selecting-area").find("select[name=province_id]").on('change', function() {
            var id = $(this).val()
            if (id)
                $("select[name=city_id]").html("<option>loading...</option>")
            return option("city", id)
        });

        $(".selecting-area").find("select[name=city_id]").on('change', function() {
            var id = $(this).val()
            if (id)
                $("select[name=district_id]").html("<option>loading...</option>")
            return option("district", id)
        });

        function option(area, id) {
            if (area == 'city') {
                var htmlCity = ''
                $.ajax({
                    url: '{{ url("user/detail-area/city") }}/' + id,
                    type: 'get',
                    dataType: "JSON",
                    success: function(data) {
                        setTimeout(function() {
                            if (data.code == 200) {
                                htmlCity += '<option value="">Pilih...</option>'
                                $.each(data.data.cities, function(e, f) {
                                    htmlCity += '<option value="' + f
                                        .id +
                                        '">' + f
                                        .name + '</option>'
                                })
                            }

                            return $("select[name=city_id]").html(htmlCity)
                        }, 1000)
                    }
                });
            }

            if (area == 'district') {
                var htmlDistrict = ''
                $.ajax({
                    url: '{{ url("user/detail-area/district") }}/' + id,
                    type: 'get',
                    dataType: "JSON",
                    success: function(data) {
                        setTimeout(function() {
                            if (data.code == 200) {
                                htmlDistrict +=
                                    '<option value="">Pilih...</option>'
                                $.each(data.data.districts, function(e, f) {
                                    htmlDistrict += '<option value="' +
                                        f
                                        .id + '">' + f
                                        .name + '</option>'
                                })
                            }

                            return $("select[name=district_id]").html(
                                htmlDistrict)
                        }, 1000)
                    }
                });
            }
        }
    });

    $('.user-tree ul').hide();
    $('.user-tree>ul').show();
    $('.user-tree ul.active').show();
    $('.user-tree li.way').on('click', function(e) {
        var children = $(this).find('> ul');
        if (children.is(":visible")) {
            children.hide('fast').removeClass('active');
        } else {

            var thisData = $(this).find('.showHover')

            if (thisData.length > 0) {
                // var modal = $('#exampleModalCenter');
                $('.tree_name').text(thisData.data('name'));
                $('.tree_url').attr({
                    "href": thisData.data('treeurl')
                });
                // $('.tree_status').text(thisData.data('status'));
                // $('.tree_plan').text(thisData.data('plan'));
                // $('.tree_image').attr({
                //     "src": thisData.data('image')
                // });
                // $('.user-details-header').removeClass('Paid');
                // $('.user-details-header').removeClass('Free');
                // $('.user-details-header').addClass(thisData.data('status'));
                // $('.tree_ref').text(thisData.data('refby'));
                // $('.lbv').text(thisData.data('lbv'));
                // $('.rbv').text(thisData.data('rbv'));
                // $('.lpaid').text(thisData.data('lpaid'));
                // $('.rpaid').text(thisData.data('rpaid'));
                // $('.lfree').text(thisData.data('lfree'));
                // $('.rfree').text(thisData.data('rfree'));
                // $('#exampleModalCenter').modal('show'); 
            }

            children.show('fast').addClass('active');
        }
        e.stopPropagation();
    });
});
</script>