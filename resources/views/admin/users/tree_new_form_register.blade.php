<div class="modal fade form-register-modal-area" id="formModelRegister" tabindex="-1" role="dialog"
    aria-labelledby="formModelRegisterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModelRegisterTitle">@lang('Form Register by Refferal '){{$ref['name']}}
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-register-body">
                    <div class="card mt-0">
                        <div class="card-body">

                            <form id="form-registers-act"
                                action="{{route('admin.users.registered-from-refferal',[$ref['id']])}}" method="POST"
                                enctype="multipart/form-data">
                                @csrf

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('First Name')<span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="firstname"
                                                form="form-registers-act">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="   font-weight-bold">@lang('Last Name') <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="lastname"
                                                form="form-registers-act">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('Email') <span
                                                    class="text-danger">*</span></label>
                                            <input autocomplete="off" class="form-control" type="email" name="email"
                                                form="form-registers-act">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="   font-weight-bold">@lang('Mobile Number') <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="mobile" maxlength="20"
                                                form="form-registers-act">
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="  font-weight-bold">@lang('Username') <span
                                                    class="text-danger">*</span></label>
                                            <input autocomplete="off" class="form-control" type="text" name="username"
                                                form="form-registers-act">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="  font-weight-bold">@lang('Password') <span
                                                    class="text-danger">*</span></label>
                                            <input autocomplete="off" class="form-control" type="password"
                                                name="password" form="form-registers-act">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('Gender') <span
                                                    class="text-danger">*</span></label>
                                            <select name="gender" class="form-control">
                                                <option value="">Pilih...</option>
                                                <option value="1">Laki-Laki</option>
                                                <option value="2">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('NIK') <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="number" maxlength="16" name="nik"
                                                form="form-registers-act">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('Day Of Birth') </label>
                                            <input class="form-control" type="date" name="dob"
                                                form="form-registers-act">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('Place Of Birth') </label>
                                            <input class="form-control" type="text" name="pob"
                                                form="form-registers-act">
                                        </div>
                                    </div>
                                </div>


                                <div class="row mt-2 selecting-area">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="  font-weight-bold">@lang('Province') </label>
                                            <select name="province_id" class="form-control">
                                                <option value="">Pilih...</option>
                                                @if (!empty($province))
                                                @foreach ($province as $ar)
                                                <option value="{{ $ar->id}}">{{ $ar->name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="  font-weight-bold">@lang('City') </label>
                                            <select name="city_id" class="form-control">
                                                <option value="">Pilih...</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="  font-weight-bold">@lang('Distinct') </label>
                                            <select name="district_id" class="form-control">
                                                <option value="">Pilih...</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xl-3 col-md-6">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('Zip/Postal') </label>
                                            <input class="form-control" type="text" maxlength="5" name="zip"
                                                form="form-registers-act">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label class="  font-weight-bold">@lang('Address') </label>
                                            <textarea class="form-control" name="address"
                                                form="form-registers-act"></textarea>
                                        </div>
                                    </div>



                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" form="form-registers-act"
                                                class="btn btn--primary btn-block btn-lg">@lang('Save')
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>