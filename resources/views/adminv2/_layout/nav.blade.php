<div class="nav-content d-flex">
    <!-- Logo Start -->
    <div class="logo position-relative">
        {{-- <a href="{{route('admin.dashboard')}}"> 
            <img src="{{asset('assets/images/logoIcon/logo.png')}}" alt="logo" />  
        </a> --}}
    {{-- </div>
    <div class="sidebar__logo"> --}}
        <a href="{{route('admin.dashboard')}}" class="sidebar__main-logo"><img
                src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="@lang('image')"></a>
        {{-- <a href="{{route('admin.dashboard')}}" class="sidebar__logo-shape"><img
                src="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}" alt="@lang('image')"></a> --}}
        {{-- <button type="button" class="navbar__expand"></button> --}}
    </div>
    <!-- Logo End -->

    <!-- Language Switch Start -->
    {{-- <div class="language-switch-container">
        <button class="btn btn-empty language-button dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EN</button>
        <div class="dropdown-menu">
            <a href="#" class="dropdown-item">DE</a>
            <a href="#" class="dropdown-item active">EN</a>
            <a href="#" class="dropdown-item">ES</a>
        </div>
    </div> --}}
    <!-- Language Switch End -->

    <!-- User Menu Start -->
    
    <div class="user-container d-flex">
        <a href="#" class="d-flex user position-relative" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{-- <img class="profile" alt="profile" src="{{ getImage('assets/admin/images/profile/'. auth()->guard('admin')->user()->image) }}" /> --}}
          <div class="name">{{auth()->guard('admin')->user()->username??''}}</div>
        </a>
        <div class="dropdown-menu dropdown-menu-end user-menu wide">
          <div class="row mb-3 ms-0 me-0">
            <div class="col-12 ps-1 mb-2">
              <div class="text-extra-small text-primary">ACCOUNT</div>
            </div>
            <div class="col-6 ps-1 pe-1">
              <ul class="list-unstyled">
                <li>
                  <a href="#">User Info</a>
                </li>
                <li>
                  <a href="#">Preferences</a>
                </li>
                <li>
                  <a href="#">Calendar</a>
                </li>
              </ul>
            </div>
            <div class="col-6 pe-1 ps-1">
              <ul class="list-unstyled">
                <li>
                  <a href="#">Security</a>
                </li>
                <li>
                  <a href="#">Billing</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="row mb-1 ms-0 me-0">
            <div class="col-12 p-1 mb-2 pt-2">
              <div class="text-extra-small text-primary">APPLICATION</div>
            </div>
            <div class="col-6 ps-1 pe-1">
              <ul class="list-unstyled">
                <li>
                  <a href="#">Themes</a>
                </li>
                <li>
                  <a href="#">Language</a>
                </li>
              </ul>
            </div>
            <div class="col-6 pe-1 ps-1">
              <ul class="list-unstyled">
                <li>
                  <a href="#">Devices</a>
                </li>
                <li>
                  <a href="#">Storage</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="row mb-1 ms-0 me-0">
            <div class="col-12 p-1 mb-3 pt-3">
              <div class="separator-light"></div>
            </div>
            <div class="col-6 ps-1 pe-1">
              <ul class="list-unstyled">
                <li>
                  <a href="#">
                    <i data-acorn-icon="help" class="me-2" data-acorn-size="17"></i>
                    <span class="align-middle">Help</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i data-acorn-icon="file-text" class="me-2" data-acorn-size="17"></i>
                    <span class="align-middle">Docs</span>
                  </a>
                </li>
              </ul>
            </div>
            <div class="col-6 pe-1 ps-1">
              <ul class="list-unstyled">
                <li>
                  <a href="#">
                    <i data-acorn-icon="gear" class="me-2" data-acorn-size="17"></i>
                    <span class="align-middle">Settings</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i data-acorn-icon="logout" class="me-2" data-acorn-size="17"></i>
                    <span class="align-middle">Logout</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    <!-- User Menu End -->

    <!-- Icons Menu Start -->
    <ul class="list-unstyled list-inline text-center menu-icons">
        <li class="list-inline-item"> 
            <a href="{{ route('admin.logout') }}"
                class="dropdown-menu__item d-flex align-items-center px-3 py-2">
                <i data-acorn-icon="logout" data-acorn-size="18"></i>
                <span class="dropdown-menu__caption"> </span>
            </a>
            {{-- <form class="navbar-search" onsubmit="return false;">
                <button type="submit" class="navbar-search__btn">
                    <i class="las la-search"></i>
                </button>
                <input type="search" name="navbar-search__field" id="navbar-search__field"
                       placeholder="Search...">
                <button type="button" class="navbar-search__close"><i class="las la-times"></i></button>
        
                <div id="navbar_search_result_area">
                    <ul class="navbar_search_result"></ul>
                </div>
            </form> --}}
        </li>
        <li class="list-inline-item"> 
            <a href="{{route('admin.password')}}"
                class="dropdown-menu__item d-flex align-items-center px-3 py-2">
                <i data-acorn-icon="lock-on" data-acorn-size="18"></i>
                <span class="dropdown-menu__caption"></span>
            </a>
        </li>
        <li class="list-inline-item">
            <a href="#" id="colorButton">
                <i data-acorn-icon="light-on" class="light" data-acorn-size="18"></i>
                <i data-acorn-icon="light-off" class="dark" data-acorn-size="18"></i>
            </a>
        </li>
        <li class="list-inline-item">
            <a href="#" data-bs-toggle="dropdown" data-bs-target="#notifications" aria-haspopup="true" aria-expanded="false" class="notification-button">
                <div class="position-relative d-inline-flex">
                    <i data-acorn-icon="bell" data-acorn-size="18"></i>
                    @if($adminNotifications->count() > 0)
                        {{-- <span class="pulse--primary"></span> --}}
                        <span class="position-absolute notification-dot rounded-xl"></span>
                    @endif
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-end wide notification-dropdown scroll-out" id="notifications">
                {{-- <div class="dropdown-menu__header"> --}}
                    <span class="caption">@lang('Notification')</span>
                    @if($adminNotifications->count() > 0)
                        <p>@lang('You have') {{ $adminNotifications->count() }} @lang('unread notification')</p>
                    @else
                        <p>@lang('No unread notification found')</p>
                    @endif
                {{-- </div> --}}
                <div class="dropdown-menu__body">
                    <div class="scroll">
                        <ul class="list-unstyled border-last-none">
                            @foreach($adminNotifications as $notification)
                                <li class="mb-3 pb-3 border-bottom border-separator-light d-flex">
                                    <img src="{{ getImage(imagePath()['profile']['user']['path'].'/'.@$notification->user->image,imagePath()['profile']['user']['size'])}}" alt="@lang('Profile Image')"  class="me-3 sw-4 sh-4 rounded-xl align-self-center">
                                    <div class="align-self-center">
                                        
                                        <a href="{{ route('admin.notification.read',$notification->id) }}" >
                                            <div class="navbar-notifi">
                                                <div class="navbar-notifi__left bg--green b-radius--rounded">
                                                </div>
                                                <div class="navbar-notifi__right">
                                                    <h6 class="notifi__title">{{ __($notification->title) }}</h6>
                                                    <span class="time"><i class="far fa-clock"></i> {{ $notification->created_at->diffForHumans() }}</span>
                                                </div>
                                            </div><!-- navbar-notifi end -->
                                        </a>
                                    </div>
                                </li>
                                @endforeach 
                        </ul>
                    </div>
                    <div class="dropdown-menu__footer">
                    <a href="{{ route('admin.notifications') }}" class="view-all-message">@lang('View all notification')</a>
                    </div>
                </div> 
            </div>
        </li>
    </ul>
    <!-- Icons Menu End -->

    <!-- Menu Start -->
    <div class="menu-container flex-grow-1">
        <ul id="menu" class="menu">
            <li>
                <a href="{{route('admin.dashboard')}}">
                    <i data-acorn-icon="home" class="icon" data-acorn-size="18"></i>
                    <span class="label">Dashboards</span>
                </a> 
            </li>
            {{-- <li>
                <a href="#courses">
                    <i data-acorn-icon="online-class" class="icon" data-acorn-size="18"></i>
                    <span class="label">Master</span>
                </a>
                <ul id="courses">
                    <li>
                        <a href="#">
                            <span class="label">Data Group Admin</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label">Data Administrator</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label">Data Menu Admin</span>
                        </a>
                    </li>
                </ul>
            </li> --}}
            <li>
                <a href="#quiz">
                    <i data-acorn-icon="quiz" class="icon" data-acorn-size="18"></i>
                    <span class="label">Network</span>
                </a>
                <ul id="quiz">
                    <li>
                        <a href="#">
                            <span class="label">Genealogy</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label">Data Sponsorisasi</span>
                        </a>
                    </li>
                    <li>
                        <a href="/Quiz/Result">
                            <span class="label">Data Downline</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#paths" class=" {{menuActive('admin.users*')}}">
                    <i data-acorn-icon="user" class="icon" data-acorn-size="18"></i>
                    <span class="label">Member</span>
                </a>
                <ul id="paths"> 
                    <li  >
                        <a href="{{route('admin.users.all')}}" class=" {{menuActive('admin.users.all')}}"> 
                            <span class="label">@lang('All Member')</span>
                        </a>
                    </li> 
                    <li >
                        <a href="{{route('admin.users.active')}}" class=" {{menuActive('admin.users.active')}}"> 
                            <span class="label">@lang('Active Users')</span>
                        </a>
                    </li>
                    <li >
                        <a href="{{route('admin.users.banned')}}" class=" {{menuActive('admin.users.banned')}}"> 
                            <span class="label">@lang('Banned Users')</span>
                            @if($banned_users_count)
                                <span class="menu-badge pill bg--primary ml-auto">{{$banned_users_count}}</span>
                            @endif
                        </a>
                    </li> 
                    {{-- <li >
                        <a href="{{route('admin.users.email.unverified')}}" class=" {{menuActive('admin.users.email.unverified')}}"> 
                            <span class="label">@lang('Email Unverified')</span>

                            @if($email_unverified_users_count)
                                <span
                                    class="menu-badge pill bg--primary ml-auto">{{$email_unverified_users_count}}</span>
                            @endif
                        </a>
                    </li> 
                    <li >
                        <a href="{{route('admin.users.email.all')}}" class=" {{menuActive('admin.users.email.all')}}"> 
                            <span class="label">@lang('Email to All')</span>
                        </a>
                    </li> --}}
                </ul>
            </li>
            <li>
                <a href="#pin">
                    <i data-acorn-icon="lecture" class="icon" data-acorn-size="18"></i>
                    <span class="label">Pin</span>
                </a>
                <ul id="pin">
                    <li>
                        <a href="{{route('admin.pin.approve.index')}}">
                            <span class="label">Approval</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.pin.transfer_to_member.index')}}">
                            <span class="label">Pin Transfer</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.pin.history.index')}}">
                            <span class="label">Pin Log</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#bonus">
                    <i data-acorn-icon="money" class="icon" data-acorn-size="18"></i>
                    <span class="label">Bonus</span>
                </a>
                <ul id="bonus">
                    <li>
                        <a href="{{route('admin.bonus.summary.index')}}">
                            <span class="label">Bonus Summary</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.bonus.transfer.index')}}">
                            <span class="label">Transfer Bonus</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.bonus.log.index')}}">
                            <span class="label">Bonus Log</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#autosave">
                    <i data-acorn-icon="coin" class="icon" data-acorn-size="18"></i>
                    <span class="label">Auto Save</span>
                </a>
                <ul id="autosave">
                    <li>
                        <a href="#">
                            <span class="label">AutoSave Member Saldo</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label">Total AutoSave</span>
                        </a>
                    </li> 
                </ul>
            </li>
            <li>
                <a href="#reward">
                    <i data-acorn-icon="money-bag" class="icon" data-acorn-size="18"></i>
                    <span class="label">Reward</span>
                </a>
                <ul id="reward">
                    <li>
                        <a href="#">
                            <span class="label">Reward List</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label">Qualified Member</span>
                        </a>
                    </li> 
                </ul>
            </li>
            <li>
                <a href="#top-up">
                    <i data-acorn-icon="cart" class="icon" data-acorn-size="18"></i>
                    <span class="label">Top-Up</span>
                </a>
                <ul id="top-up">
                    <li>
                        <a href="{{route('admin.top_up.approve.index')}}">
                            <span class="label">Top-Up Approval</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.top_up.history.index')}}">
                            <span class="label">Top-Up Log</span>
                        </a>
                    </li> 
                </ul>
            </li>
            <li>
                <a href="#ticket" class=" {{menuActive('admin.ticket*')}} ">
                    <i data-acorn-icon="message" class="icon" data-acorn-size="18"></i> 
                    <span class="label">@lang('Support Ticket') </span>
                    @if(0 < $pending_ticket_count)
                        <span class="menu-badge pill bg--primary ml-auto">
                            <i class="fa fa-evenlope"></i>
                        </span>
                    @endif
                </a>
                <ul id="ticket"> 
                    <li class="sidebar-menu-item {{menuActive('admin.ticket')}} ">
                        <a href="{{route('admin.ticket')}}" class="nav-link"> 
                            <span class="label">@lang('All Ticket')</span>
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{menuActive('admin.ticket.pending')}} ">
                        <a href="{{route('admin.ticket.pending')}}" class="nav-link">
                            <i class="menu-icon las la-dot-circle"></i>
                            <span class="label">@lang('Pending Ticket')</span>
                            @if($pending_ticket_count)
                                <span
                                    class="menu-badge pill bg--primary ml-auto">{{$pending_ticket_count}}</span>
                            @endif
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{menuActive('admin.ticket.closed')}} ">
                        <a href="{{route('admin.ticket.closed')}}" class="nav-link">
                            <i class="menu-icon las la-dot-circle"></i>
                            <span class="label">@lang('Closed Ticket')</span>
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{menuActive('admin.ticket.answered')}} ">
                        <a href="{{route('admin.ticket.answered')}}" class="nav-link">
                            <i class="menu-icon las la-dot-circle"></i>
                            <span class="label">@lang('Answered Ticket')</span>
                        </a>
                    </li>
                </ul>
            </li>
{{--              
            <li class="sidebar-menu-item {{menuActive('admin.setting.index')}}">
                <a href="{{route('admin.setting.index')}}" class="nav-link">
                    <i class="menu-icon las la-life-ring"></i>
                    <span class="label">@lang('General Setting')</span>
                </a>
            </li> --}}
{{-- 
            <li class="sidebar-menu-item {{menuActive('admin.setting.notice')}}">
                <a href="{{route('admin.setting.notice')}}" class="nav-link">
                    <i class="menu-icon las la-exclamation-triangle"></i>
                    <span class="label">@lang('Notice')</span>
                </a>
            </li> --}}

            {{-- <li class="sidebar-menu-item {{menuActive('admin.setting.logo.icon')}}">
                <a href="{{route('admin.setting.logo.icon')}}" class="nav-link">
                    <i class="menu-icon las la-images"></i>
                    <span class="label">@lang('Logo & Favicon')</span>
                </a>
            </li>
 
            <li class="sidebar-menu-item  {{menuActive(['admin.language.manage','admin.language.key'])}}">
                <a href="{{route('admin.language.manage')}}" class="nav-link"
                    data-default-url="{{ route('admin.language.manage') }}">
                    <i class="menu-icon las la-language"></i>
                    <span class="label">@lang('Language') </span>
                </a>
            </li> --}}

            {{-- <li class="sidebar-menu-item {{menuActive('admin.seo')}}">
                <a href="{{route('admin.seo')}}" class="nav-link">
                    <i class="menu-icon las la-globe"></i>
                    <span class="label">@lang('SEO Manager')</span>
                </a>
            </li> --}}

            {{-- <li class=" ">
                <a href="#email" class="{{menuActive('admin.email.template*')}}"> 
                    <i data-acorn-icon="messages" class="icon" data-acorn-size="18"></i> 
                    <span class="label">@lang('Email Manager')</span>
                </a>
                <ul id="email"> 
                    <li class="sidebar-menu-item {{menuActive('admin.email.template.global')}} ">
                        <a href="{{route('admin.email.template.global')}}" class="nav-link">
                            <i class="menu-icon las la-dot-circle"></i>
                            <span class="label">@lang('Global Template')</span>
                        </a>
                    </li>
                    <li class="sidebar-menu-item {{menuActive(['admin.email.template.index','admin.email.template.edit'])}} ">
                        <a href="{{ route('admin.email.template.index') }}" class="nav-link">
                            <i class="menu-icon las la-dot-circle"></i>
                            <span class="label">@lang('Email Templates')</span>
                        </a>
                    </li>
      
                    <li class="sidebar-menu-item {{menuActive('admin.email.template.setting')}} ">
                        <a href="{{route('admin.email.template.setting')}}" class="nav-link">
                            <i class="menu-icon las la-dot-circle"></i>
                            <span class="label">@lang('Email Configure')</span>
                        </a>
                    </li>
                    <li {{menuActive('admin.setting.optimize')}}">
                        <a href="{{route('admin.setting.optimize')}}" class="nav-link"> 
                            <span class="label">@lang('Clear Cache')</span>
                        </a>
                    </li> 
                </ul> 
            </li> --}}
 

        </ul>
    </div>
    <!-- Menu End -->

    <!-- Mobile Buttons Start -->
    <div class="mobile-buttons-container">
        <!-- Scrollspy Mobile Button Start -->
        <a href="#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
            <i data-acorn-icon="menu-dropdown"></i>
        </a>
        <!-- Scrollspy Mobile Button End -->

        <!-- Scrollspy Mobile Dropdown Start -->
        <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
        <!-- Scrollspy Mobile Dropdown End -->

        <!-- Menu Button Start -->
        <a href="#" id="mobileMenuButton" class="menu-button">
            <i data-acorn-icon="menu"></i>
        </a>
        <!-- Menu Button End -->
    </div>
    <!-- Mobile Buttons End -->
</div>
<div class="nav-shadow"></div>
