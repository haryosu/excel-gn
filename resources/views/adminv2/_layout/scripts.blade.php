

<!-- Vendor Scripts Start -->
<script src="{{asset('assets/adminv2/js/vendor/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('assets/adminv2/js/vendor/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/adminv2/js/vendor/OverlayScrollbars.min.js')}}"></script>
<script src="{{asset('assets/adminv2/js/vendor/autoComplete.min.js')}}"></script>
<script src="{{asset('assets/adminv2/js/vendor/clamp.min.js')}}"></script>
<script src="{{asset('assets/adminv2/icon/acorn-icons.js')}}"></script>
<script src="{{asset('assets/adminv2/icon/acorn-icons-interface.js')}}"></script>
<script src="{{asset('assets/adminv2/icon/acorn-icons-learning.js')}}"></script>
<script src="{{asset('assets/adminv2/icon/acorn-icons-commerce.js')}}"></script>


<script src="{{asset('assets/adminv2/js/vendor/glide.min.js')}}"></script>

<script src="{{asset('assets/adminv2/js/vendor/Chart.bundle.min.js')}}"></script>

<script src="{{asset('assets/adminv2/js/vendor/jquery.barrating.min.js')}}"></script>
<!-- Vendor Scripts End -->

<!-- Template Base Scripts Start -->
<script src="{{asset('assets/adminv2/js/base/helpers.js')}}"></script>
<script src="{{asset('assets/adminv2/js/base/globals.js')}}"></script>
<script src="{{asset('assets/adminv2/js/base/nav.js')}}"></script>
<script src="{{asset('assets/adminv2/js/base/settings.js')}}"></script>
{{-- <script src="{{asset('assets/adminv2/js/base/search.js')}}"></script>  --}}
<!-- Template Base Scripts End -->
<!-- Page Specific Scripts Start -->

<script src="{{asset('assets/adminv2/js/common.js')}}"></script>
<script src="{{asset('assets/member/js/scripts.js')}}"></script>
<!-- Page Specific Scripts End -->
