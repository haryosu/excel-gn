@extends('adminv2.layouts.master')
@section('content')
    <div class="page-wrapper default-version">
        <div class="form-area bg_img" data-background="{{asset('assets/admin/images/1.jpg')}}">
            <div class="form-wrapper">
                <h4 class="logo-text mb-15">@lang('Welcome to') <strong>{{__($general->sitename)}}</strong></h4>
                <p>{{__($pageTitle)}} @lang('to')  {{__($general->sitename)}} @lang('dashboard')</p>
                <form action="{{ route('admin.login') }}" method="POST" class="cmn-form mt-30">
                    @csrf
                    <div class="form-group">
                        <label for="email">@lang('Username')</label>
                        <input type="text" name="username" class="form-control b-radius--capsule" id="username" value="{{ old('admin') }}" placeholder="@lang('Enter your username')">
                        <i class="las la-user input-icon"></i>
                    </div>
                    <div class="form-group">
                        <label for="pass">@lang('Password')</label>
                        <input type="password" name="password" class="form-control b-radius--capsule" id="pass" placeholder="@lang('Enter your password')">
                        <i class="las la-lock input-icon"></i>
                    </div>
                    <div class="form-group d-flex justify-content-between align-items-center">
                        <a href="{{ route('admin.password.reset') }}" class="text-muted text--small"><i class="las la-lock"></i>@lang('Forgot password?')</a>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="submit-btn mt-25 b-radius--capsule">@lang('Login') <i class="las la-sign-in-alt"></i></button>
                    </div>
                </form>
            </div>
        </div><!-- login-area end -->
    </div>
@endsection
@section('content_left')
<div class="min-h-100 d-flex align-items-center">
    <div class="w-100 w-lg-75 w-xxl-50">
        <div>
            <div class="mb-5">
                <h1 class="display-3 cta-1 mb-0  text-white  ">@lang('Welcome to') {{__($general->sitename)}} </h1>
                {{-- <h1 class="display-3 cta-1 mb-0  text-white  ">Ready for Your Project</h1> --}}
            </div>
            <p class="h6  text-white    lh-1-5 mb-5">
                Perusahaan dinamis yang bergerak dalam mengembangkan teknologi tertinggi. Objektif, akurat dan bersinergi.

                {{-- Dynamically target high-payoff intellectual capital for customized technologies. Objectively integrate
                emerging core competencies before
                process-centric communities... --}}
            </p>
            <div class="mb-5">
                <a class="btn btn-lg btn-outline-white" href="/">Pelajari Lebih Lanjut</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content_right')
<div class="sw-lg-70 min-h-100 bg-foreground d-flex justify-content-center align-items-center shadow-deep py-5 full-page-content-right-border">
    {{-- <div class="sw-lg-50 px-5">
        <div class="" style="background-image:{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}">
         </div>
    </div> --}}
    <div class="sw-lg-50 px-5">
        <div class="logo"><a href="{{ route('user.login') }}"><img src="{{getImage('assets/images/logoIcon/logo.png')}}" width="100" alt="logo"></a></div>
                 
        {{-- <div class="sh-11">
        <a href="index.html">
            
        </a>
    </div> --}}
        <div class="mb-5">
        <h2 class="cta-1 mb-0 text-primary">Admin Excel</h2>
        <h2 class="cta-1 text-primary">let's get started!</h2>
        </div>
        <div class="mb-5">
        <p class="h6">Please use your credentials to login.</p> 
        </div>
        <div>
        {{-- <form id="loginForm" class="tooltip-end-bottom" novalidate>
            <div class="mb-3 filled form-group tooltip-end-top">
            <i data-acorn-icon="email"></i>
            <input class="form-control" placeholder="Email" name="email" />
            </div>
            <div class="mb-3 filled form-group tooltip-end-top">
            <i data-acorn-icon="lock-off"></i>
            <input class="form-control pe-7" name="password" type="password" placeholder="Password" />
            <a class="text-small position-absolute t-3 e-3" href="Pages.Authentication.ForgotPassword.html">Forgot?</a>
            </div>
            <button type="submit" class="btn btn-lg btn-primary">Login</button>
        </form> --}}
        <form action="{{ route('admin.login') }}" method="POST" class="tooltip-end-bottom" >
            @csrf
            <div class="mb-3 filled form-group tooltip-end-top">
                {{-- <label for="email">@lang('Username')</label> --}}
                <i data-acorn-icon="email"></i>
                <input type="text" name="username" class="form-control  pe-7" id="username" value="{{ old('admin') }}" placeholder="@lang('Enter your username')">
                {{-- <i class="las la-user input-icon"></i> --}}

            </div>
            <div class="mb-3 filled form-group tooltip-end-top">
                {{-- <label for="pass">@lang('Password')</label> --}}
                <i data-acorn-icon="lock-off"></i>
                <input type="password" name="password" class="form-control  pe-7" id="pass" placeholder="@lang('Enter your password')">
                {{-- <i class="las la-lock input-icon"></i> --}}

                <a href="{{ route('admin.password.reset') }}" class="text-small position-absolute t-3 e-3"><i class="las la-lock"></i>@lang('Forgot password?')</a>
            </div>
            {{-- <div class="form-group d-flex justify-content-between align-items-center"> --}}
            {{-- </div> --}}
            <button type="submit" class="btn btn-lg btn-primary">@lang('Login') <i class="las la-sign-in-alt"></i></button>

            {{-- <div class="form-group">
                <button type="submit" class="submit-btn mt-25 b-radius--capsule">@lang('Login') <i class="las la-sign-in-alt"></i></button>
            </div> --}}
        </form>
        </div>
    </div>
</div>
@endsection

