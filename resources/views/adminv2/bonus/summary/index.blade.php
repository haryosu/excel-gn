@extends('adminv2.layouts.app')
@section('title' , 'Admin - Bonus Summary')

@php
    use App\Constants\Is_WD;
    use App\Constants\BonusType;
@endphp
@section('panel')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Bonus Summary</h1>
            </div>
        </div>
    </div>

    <!-- Filter -->
    <div class="row mb-2">
        <form action="">
            <!-- Search Start -->
            <div class="col-sm-12 col-md-5 col-lg-3 col-xxl-2 mb-1">
                <div
                    class="d-inline-block float-md-start me-1 mb-1 search-input-container w-100 shadow bg-foreground">
                    <input class="form-control" placeholder="Search Firstname" name='search'
                        value="{{ Request::get('search') }}" />
                    <span class="search-magnifier-icon">
                        <a onclick="this.closest('form').submit();return false;"><i
                                data-acorn-icon="search"></i></a>
                    </span>
                    <span class="search-delete-icon d-none">
                        <i data-acorn-icon="close"></i>
                    </span>
                </div>
            </div>
            <!-- Search End -->
        </form>
    </div>
    <!-- Filter -->

    <!-- Order List -->
        <div class="row">
            <section class="scroll-section" id="breakpointSpecificResponsive" id="stripedRows">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive-sm">
                            <table class="table table-bordered" style="margin: 0px">
                                <tbody>
                                    <tr class="table-active">
                                        <th scope="row" rowspan="2" class="align-middle"> Date</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Nama Member</th>
                                        <th scope="row" colspan="2" class="text-center"> Bonus Plan</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Bonus Value</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Bonus Net</th>
                                        <th scope="row" rowspan="2" class="text-center align-middle"> Status</th>
                                        <th scope="row" rowspan="2" class="text-center align-middle"> Bank Account</th>
                                        <th scope="row" rowspan="2" class="text-center align-middle"> Action</th>
                                    </tr>
                                    <tr class="table-active">
                                        <th scope="row"> Bonus Plan A</th>
                                        <th scope="row"> Bonus Plan B</th>
                                    </tr>
                                </tbody>
                                <tbody>
                                    @forelse ($getBonusLogAll as $key => $item)
                                    <tr>
                                        <td>{{$item['created_at']}}</td>
                                        <td>{{$item['user_name']}}</td>
                                        <td>Rp. {{number_format($item['total_bonus_val_plan_a']) ?? '-'}}</td>
                                        <td>Rp. {{number_format($item['total_bonus_val_plan_b']) ?? '-'}}</td>
                                        <td>Rp. {{number_format($item['total_bonus_val']) ?? '-'}}</td>
                                        <td>Rp. {{number_format($item['total_bonus_net']) ?? '-'}}</td>

                                        <td class="text-center align-middle">
                                            {!!Is_WD::labelHTML($item['is_wd'])!!}
                                        </td>
                                        <td class="text-center align-middle">
                                            {{$item['bank_number']}} -- {{$item['bank_account']}} -- {{$item['bank_provider']}}
                                        </td>
                                        <td class="text-center align-middle">
                                            <button type="button"
                                                class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1 modalUser"
                                                data-bs-toggle="modal" data-id="{{$item['user_id']}}"
                                                data-bs-target="#verticallyCenteredExample-{{$item['user_id']}}">
                                                <i data-acorn-icon="eye"></i>
                                                <span class="d-xxl-inline-block">Detail</span>
                                            </button>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="7" class="text-center">Data Kosong</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <!-- Order List -->

    {{-- Modal Detail --}}
        @foreach ($getBonusLogAll as $key => $item)

                <div class="modal fade modal-close-out" id="verticallyCenteredExample-{{$item['user_id']}}"
                role="dialog" aria-labelledby="verticallyCenteredLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="verticallyCenteredLabel">
                                Detail Bonus Berdasarkan Type</h5>
                            <button type="button" class="btn-close"
                                data-bs-dismiss="modal"><i
                                    data-acorn-icon="close"></i></button>
                        </div>
                            <form
                                action="{{ route('member.pin.transfer.transfer_pin_user',) }}"
                                method="POST" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="modal-body">
                                    <section class="scroll-section"
                                        id="breakpointSpecificResponsive" id="stripedRows">
                                        @forelse ($item['detail'] as $detail)
                                        <div class="card">
                                            <div class="card-body">
                                                <h2 class="small-title text-center">{{$detail['bonus_type_name']}}</h2>
                                                <div class="table-responsive-sm">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr class="table-active">
                                                                {{-- <th scope="row">
                                                                    Bonus Id</th> --}}
                                                                <th scope="row">
                                                                    Bonus Value</th>
                                                                <th scope="row">
                                                                    Bonus Net</th>
                                                                <th scope="row">
                                                                    Bonus Auto Save</th>
                                                                <th scope="row">
                                                                    Deskripsi</th>
                                                            </tr>
                                                        </tbody>
                                                        <tbody>
                                                                @forelse ($detail['bonus_type'] as $bonusType)
                                                                    <tr>
                                                                        {{-- <td>
                                                                            {{$bonusType->bonus_id}}
                                                                            @if ($bonusType->bonus_id)
                                                                                {{BonusType::label($bonusType->bonus_id)}}
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td> --}}
                                                                        <td>
                                                                            Rp. {{$bonusType->bonus_val}}
                                                                        </td>
                                                                        <td>
                                                                            Rp. {{$bonusType->bonus_net}}
                                                                        </td>
                                                                        <td>
                                                                            Rp. {{$bonusType->bonus_autosave}}
                                                                        </td>
                                                                        <td>
                                                                            {{$bonusType->desc}}
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="5" class="text-center">
                                                                            Data kosong
                                                                        </td>
                                                                    </tr>
                                                                @endforelse
                                                                <tr class="table-active">
                                                                    <td>Total Bonus Value {{$detail['bonus_type_name']}}</td>
                                                                    <td colspan="4">Rp. {{$detail['total_val_bonus_type']}}</td>
                                                                </tr>
                                                                <tr class="table-active">
                                                                    <td>Total Bonus Net {{$detail['bonus_type_name']}}</td>
                                                                    <td colspan="4">Rp. {{$detail['total_net_bonus_type']}}</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        @empty
                                        <div class="card">
                                                <div class="card-body">
                                                    <h2 class="small-title text-center">Data kosong </h2>
                                                </div>
                                            </div>
                                        @endforelse
                                    </section>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        @endforeach
    {{-- Modal Detail --}}

    <!-- Pagination -->
        {{ $getBonusLogAll->appends(request()->query())->links('adminv2.layouts.partials.pagination') }}
    <!-- Pagination -->
@endsection