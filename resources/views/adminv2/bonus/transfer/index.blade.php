@extends('adminv2.layouts.app')
@section('title' , 'Admin - Transfer Bonus')

@php
    use App\Constants\Is_WD;
    use App\Constants\BonusType;
@endphp

@section('style')
    <link rel="stylesheet" href="{{asset('assets/adminv2/css/vendor/datatables.min.css')}}"/>
@endsection
 
@section('panel')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Transfer Bonus</h1>
            </div>
        </div>
    </div>

    <!-- Filter -->
        <div class="row mb-2">
            <!-- Search Start -->


            <div class="col-sm-6 col-md-7 col-lg-9 col-xxl-10 text-end mb-1">

            </div>
            <div class="data-table-boxed">
                <!-- Controls Start -->
                <div class="row mb-2">
                    <!-- Search Start -->
                    <div class="col-sm-12 col-md-5 col-lg-3 col-xxl-2 mb-1">
                        <form action="">
                            <div class="d-inline-block float-md-start me-1 mb-1 search-input-container w-100 shadow bg-foreground">
                                <input class="form-control" placeholder="Search Firstname" name='search'
                                    value="{{ Request::get('search') }}" />
                                <span class="search-magnifier-icon">
                                    <a onclick="this.closest('form').submit();return false;"><i
                                            data-acorn-icon="search"></i></a>
                                </span>
                                <span class="search-delete-icon d-none">
                                    <i data-acorn-icon="close"></i>
                                </span>
                            </div>
                        </form>
                    </div>
                    <!-- Search End -->

                    <div class="col-sm-12 col-md-7 col-lg-9 col-xxl-10 text-end mb-1">
                        {{-- <div class="d-inline-block me-0 me-sm-3 float-start float-md-none">
                             
                                <form  action="{{route('admin.bonus.transfer.update')}}">
                                    <button id="export-csv-btn" onclick="return confirm('Apakah anda yakin ?');" type="submit" class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1">
                                        <i data-acorn-icon="send" data-acorn-size="15"></i>
                                        <span class="d-xxl-inline-block">Transfer All Bonus</span>
                                    </button>
                                </form> 
                        </div> --}}
                        <div class="d-inline-block me-0 me-sm-3 float-start float-md-none">
              
                            <button  class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1 transfer" data-url="{{ route('admin.bonus.transfer.bonus') }}">
                                <i data-acorn-icon="send" data-acorn-size="15"></i>
                                <span class="d-xxl-inline-block">Transfer Selected</span>
                            </button>
                        </div> 
                        {{-- <div class="d-inline-block">
        
                        </div> --}}
                        <div class="d-inline-block">
                            <!-- Print Button Start -->
                            <button
                                    class="btn btn-icon btn-icon-only btn-foreground-alternate shadow datatable-print"
                                    data-bs-delay="0"
                                    data-datatable="#datatableBoxed"
                                    data-bs-toggle="tooltip"
                                    data-bs-placement="top"
                                    title="Print"
                                    type="button"
                                    onclick="printAllData()"
                            >
                                <i data-acorn-icon="print"></i>
                            </button>
                            <!-- Print Button End -->

                            <!-- Export Dropdown Start -->
                            <div class="d-inline-block datatable-export" data-datatable="#datatableBoxed">
                                <button class="btn p-0" data-bs-toggle="dropdown" type="button" data-bs-offset="0,3">
                            <span
                                    class="btn btn-icon btn-icon-only btn-foreground-alternate shadow dropdown"
                                    data-bs-delay="0"
                                    data-bs-placement="top"
                                    data-bs-toggle="tooltip"
                                    title="Export"
                            >
                                <i data-acorn-icon="download"></i>
                            </span>
                                </button>
                                <div class="dropdown-menu shadow dropdown-menu-end">
                                    {{-- <button class="dropdown-item export-copy" type="button">Copy</button> --}}
                                    <a class="dropdown-item export-excel" href="{{ route("admin.bonus.transfer.Excelprint") }}"  id="export-btn-excl">Export xlsx</a>
                                    <a class="dropdown-item export-cvs" href="{{ route("admin.bonus.transfer.exportCsv") }}" id="export-csv">Export CSV</a> 
                                    {{-- <button class="dropdown-item export-excel" type="button">Excel</button>
                                    <button class="dropdown-item export-cvs" type="button">Cvs</button> --}}
                                </div>
                            </div>
                            <!-- Export Dropdown End --> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Filter -->

    <!-- Order List -->
        <div class="row">
            <section class="scroll-section" id="breakpointSpecificResponsive" id="stripedRows">
                <div class="card">
                    <div class="card-body"> 
                        <div class="table-responsive-sm">
                            <table id="datatableBoxed" class="table  table-bordered" style="margin: 0px">
                                <tbody>
                                    <tr class="table-active">
                                        <th scope="row" rowspan="2" width="50px"><input type="checkbox" id="master"></th>
                                        <th scope="row" rowspan="2" class="align-middle">Date</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Nama Member</th>
                                        <th scope="row" colspan="2" class="text-center"> Bonus Plan</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Bonus Value</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Bonus Net</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Admin Fee</th>
                                        <th scope="row" rowspan="2" class="align-middle"> Total Transfer</th>
                                        <th scope="row" rowspan="2" class="text-center align-middle"> Status</th>
                                        <th scope="row" rowspan="2" class="text-center align-middle"> Bank Account</th>
                                        {{-- <th scope="row" rowspan="2" class="text-center align-middle"> Action</th> --}}
                                    </tr>
                                    <tr class="table-active">
                                        <th scope="row"> Bonus Plan A</th>
                                        <th scope="row"> Bonus Plan B</th>
                                    </tr>
                                </tbody>
                                <tbody>
                                    @forelse ($getBonusLogAll as $key => $item)
                                    {{-- @if ($item['total_bonus_val'] >= 50000)     --}}
                                        <tr id="tr_{{$item['user_id']}}">
                                            <td><input type="checkbox" class="sub_chk" data-id="{{$item['user_id']}}"> {{$item['user_id']}}</td>
                                            <td>{{$item['created_at']}}</td>
                                            <td>{{$item['user_name']}}</td>
                                            <td>Rp. {{number_format($item['total_bonus_val_plan_a']) ?? '-'}}</td>
                                            <td>Rp. {{number_format($item['total_bonus_val_plan_b']) ?? '-'}}</td>
                                            <td>Rp. {{number_format($item['total_bonus_val']) ?? '-'}}</td>
                                            <td>Rp. {{number_format($item['total_bonus_net']) ?? '-'}}</td>
                                            <td>Rp. {{number_format($item['admin_charge']) ?? '-'}}</td>
                                            <td>Rp. {{number_format($item['total_transfer']) ?? '-'}}</td>
                                            <td class="text-center align-middle">
                                                {!!Is_WD::labelHTML($item['is_wd'])!!}
                                            </td>
                                            <td class="text-center align-middle">
                                                {{$item['bank_number']}} -- {{$item['bank_account']}} -- {{$item['bank_provider']}}

                                            </td>
                                            {{-- <td class="text-center align-middle">
                                                <button type="button"
                                                    class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1 modalUser"
                                                    data-bs-toggle="modal" data-id="{{$item['user_id']}}"
                                                    data-bs-target="#verticallyCenteredExample-{{$item['user_id']}}">
                                                    <i data-acorn-icon="eye"></i>
                                                    <span class="d-xxl-inline-block">Detail</span>
                                                </button>
                                            </td> --}}
                                        </tr> 
                                    @empty
                                    <tr>
                                        <td colspan="7" class="text-center">Data Kosong</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <!-- Order List -->

    {{-- Modal Detail --}}
        {{-- @foreach ($getBonusLogAll as $key => $item)

                <div class="modal fade modal-close-out" id="verticallyCenteredExample-{{$item['user_id']}}"
                role="dialog" aria-labelledby="verticallyCenteredLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="verticallyCenteredLabel">
                                Detail Bonus Berdasarkan Plan</h5>
                            <button type="button" class="btn-close"
                                data-bs-dismiss="modal"><i
                                    data-acorn-icon="close"></i></button>
                        </div>
                            <form
                                action="{{ route('member.pin.transfer.transfer_pin_user',) }}"
                                method="POST" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="modal-body">
                                    <section class="scroll-section"
                                        id="breakpointSpecificResponsive" id="stripedRows">
                                        @forelse ($item['detail'] as $detail)
                                        <div class="card">
                                            <div class="card-body">
                                                <h2 class="small-title text-center">{{$detail['bonus_type_name']}}</h2>
                                                <div class="table-responsive-sm">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr class="table-active">
                                                                <th scope="row">
                                                                    Date</th>
                                                                <th scope="row">
                                                                    Bonus Value</th>
                                                                <th scope="row">
                                                                    Bonus Net</th>
                                                                <th scope="row">
                                                                    Bonus Auto Save</th>
                                                                <th scope="row">
                                                                    Deskripsi</th>
                                                            </tr>
                                                        </tbody>
                                                        <tbody>
                                                                @forelse ($detail['bonus_type'] as $bonusType)
                                                                    <tr>
                                                                        <td>
                                                                            {{$bonusType->created_at}} 
                                                                        </td>
                                                                        <td>
                                                                            Rp. {{$bonusType->bonus_val}}
                                                                        </td>
                                                                        <td>
                                                                            Rp. {{$bonusType->bonus_net}}
                                                                        </td>
                                                                        <td>
                                                                            Rp. {{$bonusType->bonus_autosave}}
                                                                        </td>
                                                                        <td>
                                                                            {{$bonusType->desc}}
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="5" class="text-center">
                                                                            Data kosong
                                                                        </td>
                                                                    </tr>
                                                                @endforelse
                                                                <tr class="table-active">
                                                                    <td>Total Bonus Value {{$detail['bonus_type_name']}}</td>
                                                                    <td colspan="4">Rp. {{$detail['total_val_bonus_type']}}</td>
                                                                </tr>
                                                                <tr class="table-active">
                                                                    <td>Total Bonus Net {{$detail['bonus_type_name']}}</td>
                                                                    <td colspan="4">Rp. {{$detail['total_net_bonus_type']}}</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        @empty
                                        <div class="card">
                                                <div class="card-body">
                                                    <h2 class="small-title text-center">Data kosong </h2>
                                                </div>
                                            </div>
                                        @endforelse
                                    </section>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        @endforeach --}}
    {{-- Modal Detail --}}

    <!-- Pagination -->
        {{ $getBonusLogAll->appends(request()->query())->links('adminv2.layouts.partials.pagination') }}
    
        {{-- {{ $getBonusLogAll->appends(request()->query())->links('adminv2.layouts.partials.pagination') }} --}}
    <!-- Pagination -->
@endsection

@push('js') 
    <script src="{{asset('assets/adminv2/js/vendor/bootstrap-submenu.js')}}"></script>
    <script src="{{asset('assets/adminv2/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/adminv2/js/vendor/mousetrap.min.js')}}"></script>
    {{-- @endsection
        
        @section('js_page') --}}
@endpush
@push('js-add') 
    <script src="{{asset('assets/adminv2/js/cs/datatable.extend.js')}}"></script>
    <script src="{{asset('assets/adminv2/js/plugins/datatable.editableboxed.js')}}"></script>
    <script> 
        $(document).ready(function() {
            $('#export-csv').click(function() {
                console.log('jos');
                const exp = '{{ route("admin.bonus.transfer.exportCsv") }}';
                console.log(exp);
                $.ajax({
                    url:exp,
                    method: 'GET',
                    dataType: 'text',
                    success: function(response) {
                        // Handle success
                    console.log('jos-');
                    console.log(response);

                    },
                    error: function(xhr, status, error) {
                        // Handle error
                        alert(data.responseText);
                    }
                });
            });
        });
 
        $(document).ready(function() {
            $('#export-csv-btn').click(function(e) {
                console.log('run');
                setTimeout(function() {
                    location.reload();
                }, 5000);
            });
        });
        function printAllData() {
            var printContents = document.documentElement.innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
        $(document).ready(function () {


        $('#master').on('click', function(e) {
        if($(this).is(':checked',true))  
        {
            $(".sub_chk").prop('checked', true);  
        } else {  
            $(".sub_chk").prop('checked',false);  
        }  
        });

        $('.transfer').on('click', function(e) {
            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  

            if(allVals.length <=0)  
            {  
                alert("Please select row.");  
            }  else {  
                var check = confirm("Are you sure you want to execute this row?");  
                if(check == true){  
                    var parentCard = document.getElementById('datatableBoxed');
                    parentCard.classList.add('overlay-spinner');
                    var join_selected_values = allVals.join(","); 
                    console.log(join_selected_values);
                    console.log($(this).data('url'));
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'POST',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data.code == 200) {
                                console.log(data);
                                iziToast.success({
                                        message: data.message,
                                        position: "topRight",
                                        onClosed: function() {
                                            location.reload()
                                        }
                                    });
                                parentCard.classList.remove('overlay-spinner');
                                setTimeout(function() {
                                }, 500);
                                $(".sub_chk").prop('checked',false); 
                                location.reload();
                            } else {
                                console.log(data)
                                parentCard.classList.remove('overlay-spinner');
                                    if (data.message) {
                                        // $.each(JSON.parse(data.message), function(e, f) {
                                            iziToast.warning({
                                                message: data.message,
                                                position: "topRight"
                                            });
                                        // })
                                    }

                            }
                        },

                        error: function (data) {
                            alert(data.responseText);
                        }
                    });

                $.each(allVals, function( index, value ) {
                    $('table tr').filter("[data-row-id='" + value + "']").remove();
                });

                }  

            }  

        });


        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                element.trigger('confirm');
            }

        });


        $(document).on('confirm', function (e) {
            var ele = e.target;
            e.preventDefault();
            $.ajax({
                url: ele.href,
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    if (data['success']) {
                        $("#" + data['tr']).slideUp("slow");
                        alert(data['success']);
                    } else if (data['error']) {
                        alert(data['error']);
                    } else {
                        alert('Whoops Something went wrong!!');
                    }
                },
                error: function (data) {
                    alert(data.responseText);
                }
            });
            return false;
        });
        });
    </script>
@endpush