@extends('adminv2.layouts.app')

@section('panel')
        <div class="row">
            <div class="col-12">
                <div class="page-title-container">
                    <h1 class="mb-0 pb-0 display-4" id="title">Selamat Datang Admin</h1>
                </div>
            </div>
        </div>

        <div class="row g-2">
            <div class="col-12">
                {{-- Total Member --}}
                    <div class="row mb-2">
                        <section class="scroll-section" id="breakpointSpecificResponsive">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="small-title">Total Member</h2>
                                    <div class="table-responsive-sm mb-5">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    @foreach ($countUser as $item)
                                                        <th scope="col" style="text-transform: uppercase">{{$item['pin_name']}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    @foreach ($countUser as $item)
                                                    <td>
                                                        {{$item['count_user'] ?? '-'}}
                                                    </td>
                                                    @endforeach
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                {{-- Total Member --}}
    
                {{-- OMSET (BV) --}}
                    <div class="row mb-2">
                        <section class="scroll-section" id="breakpointSpecificResponsive">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="small-title">OMSET (BV)</h2>
                                    <div class="table-responsive-sm mb-5">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    @foreach ($omsetBv as $item)
                                                        <th scope="col" style="text-transform: uppercase">{{$item['pin_name']}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    @foreach ($omsetBv as $item)
                                                    <td>
                                                        Rp. {{number_format($item['omset']) ?? '-'}}
                                                    </td>
                                                    @endforeach
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                {{-- OMSET (BV) --}}
                
                {{-- Auto Save --}}
                    <div class="row mb-2">
                        <section class="scroll-section" id="breakpointSpecificResponsive">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="small-title">Auto Save</h2>
                                    <div class="table-responsive-sm">
                                        {{-- Rp 1000 --}}
                                        Rp. {{number_format($autosave) ?? '-'}} 
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                {{-- Auto Save --}}
            </div>
        </div>
@endsection