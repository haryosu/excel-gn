@extends('adminv2.layouts.app')

@section('panel')
<div class="row mb-none-30">
    <div class="col-xl-3 col-lg-5 col-md-5 mb-30">
        <div class="card mb-4">
            <div class="card-body">
                <div class="d-flex align-items-center flex-column mb-4">
                    <div class="d-flex align-items-center flex-column">
                      <div class="sw-13 position-relative mb-3">
                        <img class="img-fluid rounded-xl" src="{{ getImage(imagePath()['profile']['admin']['path'].'/'. $admin->image,imagePath()['profile']['admin']['size'])}}"
                        alt="@lang('Profile Image')">
                      </div>
                      <div class="h5 mb-0">{{$admin->name}}</div>
                      <div class="text-muted">@lang('Joined At')</div>
                      <div class="text-muted">
                        <span class="align-middle"><strong>{{showDateTime($admin->created_at,'d M, Y h:i A')}}</strong></span>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-20 text-muted">@lang('User information')</h5>
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        @lang('Name')
                        <span class="font-weight-bold">{{ __($admin->name) }}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        @lang('Username')
                        <span  class="font-weight-bold">{{ __($admin->username) }}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        @lang('Email')
                        <span  class="font-weight-bold">{{ $admin->email }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xl-9 col-lg-7 col-md-7 ">
        <div class="card">
            <form action="{{ route('admin.password.update') }}" method="POST" enctype="multipart/form-data">
                <div class="card-header">
                    <h5 class="card-title">@lang('Change Password')</h5>
                </div>
                <div class="card-body">

                    @csrf

                    <div class="form-group row mb-2">
                        <label class="col-lg-3 col-form-label ">@lang('Password')</label>
                        <div class="col-lg-9">
                            <input class="form-control" type="password" placeholder="@lang('Password')"
                                name="old_password">
                        </div>
                    </div>

                    <div class="form-group row mb-2">
                        <label class="col-lg-3 col-form-label  ">@lang('New Password')</label>
                        <div class="col-lg-9">
                            <input class="form-control" type="password" placeholder="@lang('New Password')"
                                name="password">
                        </div>
                    </div>

                    <div class="form-group row mb-2">
                        <label class="col-lg-3 col-form-label  ">@lang('Confirm Password')</label>
                        <div class="col-lg-9">
                            <input class="form-control" type="password" placeholder="@lang('Confirm Password')"
                                name="password_confirmation">
                        </div>
                    </div>

                </div>
                <div class="card-footer align-items-center">
                    <div>
                        <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                            <span>@lang('Changes Password')</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('breadcrumb-plugins')
    <a href="{{route('admin.profile')}}" class="btn btn-sm btn--primary box--shadow1 text--small" ><i class="fa fa-user"></i>@lang('Profile Setting')</a>
@endpush
