@extends('adminv2.layouts.app')
@section('title' , 'Admin - Detail History')

@php
use App\Constants\OrderStatus;
@endphp
@section('panel')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Detail History</h1>
            </div>
        </div>
    </div>

    <div class="row gx-4 gy-5">
        <div class="col-12 col-xl-8 col-xxl-9 mb-n5">
            <!-- Status Start -->
                <h2 class="small-title">Status</h2>
                <div class="mb-5">
                    <div class="row g-2">
                        <div class="col-12 col-sm-6 col-lg-6">
                            <div class="card sh-13 sh-lg-15 sh-xl-14">
                                <div class="h-100 row g-0 card-body align-items-center py-3">
                                    <div class="col-auto pe-3">
                                        <div
                                            class="border border-primary sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center">
                                            <i data-acorn-icon="tag" class="text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="d-flex align-items-center lh-1-25">Order Id</div>
                                        <div class="text-primary">{{$getOrderPinHistory->invoice}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-6">
                            <div class="card sh-13 sh-lg-15 sh-xl-14">
                                <div class="h-100 row g-0 card-body align-items-center py-3">
                                    <div class="col-auto pe-3">
                                        <div
                                            class="border border-primary sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center">
                                            <i data-acorn-icon="clipboard" class="text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="d-flex align-items-center lh-1-25">Order Status</div>
                                        <div class="text-primary">{{OrderStatus::label($getOrderPinHistory->status)}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-12">
                            <div class="card sh-13 sh-lg-15 sh-xl-14">
                                <div class="h-100 row g-0 card-body align-items-center py-3">
                                    <div class="col-auto pe-3">
                                        <div
                                            class="border border-primary sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center">
                                            <i data-acorn-icon="calendar" class="text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="d-flex align-items-center lh-1-25">Order Date</div>
                                        <div class="text-primary">{{$getOrderPinHistory->created_at->format('d M Y') ?? '-'}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Status End -->

            <!-- Cart Start -->
            @if ($getOrderPinHistory->orderPinDetail)
                <h2 class="small-title">Cart</h2>
                <div class="card mb-5">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-5">
                                    @foreach ($getOrderPinHistory->orderPinDetail as $orderPinDetail)
                                    <div class="row g-0 sh-9 mb-3">
                                        <div class="col-auto">
                                            <img src="{{asset('assets/member/img/product/small/product-1.webp')}}"
                                                class="card-img rounded-md h-100 sw-13" alt="thumb" />
                                        </div>
                                        <div class="col">
                                            <div class="ps-4 pt-0 pb-0 pe-0 h-100">
                                                <div class="row g-0 h-100 align-items-start align-content-center">
                                                    <div class="col-12 d-flex flex-column mb-2">
                                                        <div>{{$orderPinDetail->pin->name ??'-'}}</div>
                                                        <div class="text-muted text-small">
                                                            {{$orderPinDetail->pin->description ??'-'}}</div>
                                                    </div>
                                                    <div class="col-12 d-flex flex-column mb-md-0 pt-1">
                                                        <div class="row g-0">
                                                            <div
                                                                class="col-6 d-flex flex-row pe-2 align-items-end text-alternate">
                                                                <span>{{$orderPinDetail->qty ??'-'}}</span>
                                                                <span class="text-muted ms-1 me-1">x</span>
                                                                <span>
                                                                    <span class="text-small">Rp</span>
                                                                    {{number_format($orderPinDetail->pin->price) ?? '-'}}
                                                                </span>
                                                            </div>
                                                            <div
                                                                class="col-6 d-flex flex-row align-items-end justify-content-end text-alternate">
                                                                <span>
                                                                    <span class="text-small">Rp</span>
                                                                    {{number_format($orderPinDetail->subtotal*$orderPinDetail->qty) ?? '-'}}
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div>
                                    <div class="row g-0 mb-2">
                                        <div class="col-auto ms-auto ps-3 text-muted">Total</div>
                                        <div class="col-auto sw-13 text-end">
                                            <span>
                                                <span class="text-small text-muted">Rp</span>
                                                {{number_format($getOrderPinHistory->total) ?? '-'}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <!-- Cart End -->
        </div>

        <!-- Address Start -->
            <div class="col-12 col-xl-4 col-xxl-3">
                <h2 class="small-title">Profile Customer</h2>

                <div class="card mb-5">
                    <div class="card-body mb-n5">
                        <div class="mb-5">
                            <p class="text-small text-muted mb-2">CUSTOMER</p>
                            <div class="row g-0 mb-2">
                                <div class="col-auto">
                                    <div class="sw-3 me-1">
                                        <i data-acorn-icon="user" class="text-primary" data-acorn-size="17"></i>
                                    </div>
                                </div>
                                <div class="col text-alternate align-middle">{{$getOrderPinHistory->user->firstname}}</div>
                            </div>
                            <div class="row g-0 mb-2">
                                <div class="col-auto">
                                    <div class="sw-3 me-1">
                                        <i data-acorn-icon="email" class="text-primary" data-acorn-size="17"></i>
                                    </div>
                                </div>
                                <div class="col text-alternate">{{$getOrderPinHistory->user->email}}</div>
                            </div>
                        </div>

                        <div class="mb-5">
                            <div>
                                <p class="text-small text-muted mb-2">PAYMENT (CREDIT CARD)</p>
                                <div class="row g-0 mb-2">
                                    <div class="col-auto">
                                        <div class="sw-3 me-1">
                                            <i data-acorn-icon="credit-card" class="text-primary" data-acorn-size="17"></i>
                                        </div>
                                    </div>
                                    <div class="col text-alternate">3452 42** **** 4251</div>
                                </div>
                            </div>
                        </div>

                        <div class="mb-5">
                            <div>
                                <p class="text-small text-muted mb-2">APPROVE ORDER</p>
                                <div class="row g-0 mt-2">
                                    <div class="col-auto">
                                        @if ($getOrderPinHistory->status != OrderStatus::SUCCESS && $getOrderPinHistory->images_tf)
                                            <div class="mb-3">
                                                <a href="{{ getImage('assets/images/user/order-recipe'.'/'. $getOrderPinHistory->images_tf,'350x300') }}"
                                                    class="lightbox">
                                                    <img alt="lightbox"
                                                        src="{{ getImage('assets/images/user/order-recipe'.'/'. $getOrderPinHistory->images_tf,'350x300') }}"
                                                        class="card-img" />
                                                </a>
                                            </div>
                                            <div class="w-100">
                                                <form  action="{{route('admin.pin.approve', ['id' =>$getOrderPinHistory->id])}}" method="POST">
                                                    @csrf
                                                    <button onclick="return confirm('Apakah anda yakin menyetujui orderan ini ?');" type="submit" class="btn btn-success w-100  m-1">Approve</button>
                                                </form>
                                                <form  action="{{route('admin.pin.rejected', ['id' =>$getOrderPinHistory->id])}}" method="POST">
                                                    @csrf
                                                    <button onclick="return confirm('Apakah anda yakin menolak orderan ini ?');" type="submit" class="btn btn-danger w-100  m-1">Rejected</button>
                                                </form>
                                                <p class="text-small text-muted mb-0 text-danger">
                                                    Customer telah mengirim bukti transfer. Silahkan lakukan Approve jika data sudah sesuai.
                                                </p>
                                            </div>
                                        @else
                                            @if ($getOrderPinHistory->status == OrderStatus::SUCCESS)
                                                <div class="mb-3">
                                                    <a href="{{ getImage('assets/images/user/order-recipe'.'/'. $getOrderPinHistory->images_tf,'350x300') }}"
                                                        class="lightbox">
                                                        <img alt="lightbox"
                                                            src="{{ getImage('assets/images/user/order-recipe'.'/'. $getOrderPinHistory->images_tf,'350x300') }}"
                                                            class="card-img" />
                                                    </a>
                                                </div>
                                                <div class="alert alert-success" role="alert">Order berhasil di Approve</div>
                                            @elseif ($getOrderPinHistory->status == OrderStatus::REJECTED)
                                                <div class="w-100">
                                                    <button type="submit" class="btn btn-danger w-100  m-1" disabled>Order Rejected</button>
                                                    <p class="text-small text-muted mb-0 text-danger w-100">
                                                        Order ini ditolak
                                                    </p>
                                                </div>
                                            @else
                                                <div class="w-100">
                                                    <button type="submit" class="btn btn-success w-100  m-1" disabled>Approve</button>
                                                    <p class="text-small text-muted mb-0 text-danger">
                                                        Belum dapat melakukan Approve. Tunggu sampai customer mengirim bukti transfer.
                                                    </p>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <!-- Address End -->
    </div>
@endsection