@extends('adminv2.layouts.app')
@section('title' , 'Admin Pin - Transfer To Member')

@php
    use App\Constants\OrderStatus;
@endphp

@push('style')
    <link rel="stylesheet" href="{{asset('assets/member/css/vendor/baguetteBox.min.css')}}" />
    {{-- SELECT2 --}}
        <link rel="stylesheet" href="{{asset('assets/member/css/vendor/select2.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/member/css/vendor/select2-bootstrap4.min.css')}}" />
    {{-- SELECT2 --}}
@endpush

@section('panel')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Transfer To Member</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Items Start -->
            <div class="col-lg-5 col-lg order-1 order-lg-0">
                <h2 class="small-title">Items</h2>
                @foreach ($products as $item)
                    <div class="mb-2">
                        <div class="card mb-2">
                            <div class="row g-0 sh-18 sh-md-14">
                                <div class="col-auto">
                                    <img src="{{ getImage('assets/images/products/pins'.'/'. $item->images,'350x300') }}"
                                        class="card-img card-img-horizontal h-100 sw-9 sw-sm-13 sw-md-13" alt="thumb" />
                                </div>
                                
                                <div class="col position-relative h-100">
                                    <form action="{{route('admin.pin.transfer_to_member.additem', $item->id)}}" method="POST">
                                        @csrf
                                        <div class="card-body">
                                            <div class="row h-100">
                                                <div class="col-12 col-md-6 mb-2 mb-md-0 d-flex align-items-center">
                                                    <div class="pt-0 pb-0 pe-2">
                                                        <div class="h6 mb-0 clamp-line" data-line="1">{{$item->name ??'-'}}</div>
                                                        <div class="text-muted text-small">{{$item->description ??'-'}}</div>
                                                        <div class="mb-0 sw-19">Rp. {{ number_format($item->price) ??'-'}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-6 col-md-3 pe-0 d-flex align-items-center">
                                                    <div class="input-group spinner sw-11" data-trigger="spinner">
                                                        <input type="text" class="form-control text-center px-0" name="itemQuantity"
                                                            data-rule="quantity" required />
                                                    </div>
                                                </div>
                                                <div
                                                    class="col-6 col-md-3 d-flex justify-content-end justify-content-md-start align-items-center">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i data-acorn-icon="plus"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        <!-- Items End -->

        <!-- Summary Start -->
            <div class="col-lg-7 col-lg-auto order-0 order-lg-1">
                <h2 class="small-title">Summary</h2>
                <div class="card mb-5">
                    <div class="card-body">
                        <div>
                            <div class="row mb-4 d-none d-sm-flex">
                                <div class="col-2">
                                    <p class="mb-0 text-small text-muted"></p>
                                </div>
                                <div class="col-4 p-0">
                                    <p class="mb-0 text-small text-muted">ITEM NAME</p>
                                </div>
                                <div class="col-2">
                                    <p class="mb-0 text-small text-muted">COUNT</p>
                                </div>
                                <div class="col-4 text-end p-0">
                                    <p class="mb-0 text-small text-muted">PRICE</p>
                                </div>
                            </div>

                            
                            @php
                            $no=1
                            @endphp
                            @forelse($cart_data as $index=>$item)
                                <div class="row mb-4 mb-sm-2">
                                    <div class="col-2">
                                        <form action="{{route('admin.pin.transfer_to_member.removeitem',$item['rowId'])}}" method="POST">
                                            @csrf
                                            <a onclick="this.closest('form').submit();return false;"><i data-acorn-icon="multiply"></i></a>
                                        </form>
                                    </div>
                                    <div class="col-4 p-0 ">
                                        <h6 class="mb-0">{{Str::words($item['name'],3)}} </h6>
                                        <h6 class="mb-0">Rp. {{ number_format($item['pricesingle']) ?? ''}}</h6>
                                    </div>
                                    <div class="col-2">
                                        <p class="mb-0 text-alternate">{{$item['stock'] ?? ''}}</p>
                                    </div>
                                    <div class="col-4 text-sm-end p-0">
                                        <p class="mb-0 text-alternate">Rp. {{ number_format($item['price']) ?? ''}}</p>
                                    </div>
                                </div>
                            @empty
                                <div class="row mb-5 mb-sm-2">
                                    <div class="col-12">
                                    </div>
                                </div>
                            @endforelse
                            <div class="separator separator-light mt-5 mb-5"></div>

                            <div class="row mb-5">
                                <div class="col text-sm-end text-muted">
                                <div>Total :</div>
                                </div>
                                <div class="col-auto text-end">
                                <div>Rp. {{ number_format($data_total['total']) ?? ''}}</div>
                                </div>
                            </div>
                        </div>
                        <form  action="{{route('admin.pin.transfer_to_member.bayar')}}" method="POST">
                        <div class="w-100 mb-5">
                            <label class="fw-bold form-label">Transfer ke member</label>
                            <select id="select2Basic" name="member_id" class="member form-control">
                            </select>
                          </div>
                        
                        <div class="w-100">
                                @csrf
                                <button onclick="return confirm('Apakah anda yakin ?');" type="submit" class="btn btn-success w-100  m-1">Transfer</button>
                            </div>
                        </form>
                        <div class="w-100">
                            <form  action="{{route('admin.pin.transfer_to_member.clear')}}" method="POST">
                                @csrf
                                <button onclick="return confirm('Apakah anda yakin ingin menghapus keranjang ?');" type="submit" class="btn btn-outline-danger w-100  m-1">Clear Transfer</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Summary End -->
    </div>

@endsection

@push('js')
    <script src="{{asset('assets/member/js/vendor/input-spinner.min.js')}}"></script>
    <script src="{{asset('assets/member/js/forms/controls.spinner.js')}}"></script>
    {{-- SELECT2 --}}
        <script src="{{asset('assets/member/js/vendor/select2.full.min.js')}}"></script>
    {{-- SELECT2 --}}

    <script>
        // $('.member').select2({
        //     placeholder: 'Pilih member',
        //     ajax: {
        //         url: "{{ route('admin.json.get_json_user_all') }}",
        //         dataType: 'json',
        //         data: function (params) {
        //             var query = {
        //                 search_name: params.term,
        //                 page: params.page || 1
        //             }
        //             return query
        //         },
        //         delay: 250,
        //         processResults: function (data) {
        //             return {
        //                 results: data.data,
        //                 pagination: {
        //                     more: (data.current_page == data.last_page) ? null : data
        //                         .current_page + 1
        //                 }
        //             }
        //         },
        //         cache: true
        //     }
        // });
          
        class Select2Controls {
                constructor() {
                    $('.member').select2({
                        placeholder: 'Pilih user',
                        ajax: {
                            url: "{{ route('admin.json.get_json_user_all') }}",
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search_name: params.term,
                                    page: params.page || 1
                                }
                                return query
                            },
                            delay: 250,
                            processResults: function (data) {
                                return {
                                    results: data.data,
                                    pagination: {
                                        more: (data.current_page == data.last_page) ? null : data
                                            .current_page + 1
                                    }
                                }
                            },
                            cache: true
                        }
                    });
                }
            }
        // SELECT2 USER
    </script>
@endpush