@extends('adminv2.layouts.app')


@php
    use App\Constants\StatusUser;
@endphp

@push('style')
    {{-- SELECT2 --}}
        <link rel="stylesheet" href="{{asset('assets/member/css/vendor/select2.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/member/css/vendor/select2-bootstrap4.min.css')}}" />
    {{-- SELECT2 --}}
@endpush

@section('panel')
    <div class="row mb-none-30">
        <div class="col-xl-3 col-lg-5 col-md-5 mb-30">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="d-flex align-items-center flex-column mb-4">
                        <div class="d-flex align-items-center flex-column">
                          <div class="sw-13 position-relative mb-3">
                            <img class="img-fluid rounded-xl" src="{{ getImage(imagePath()['profile']['user']['path'].'/'.$user->image,imagePath()['profile']['user']['size'])}}"
                            alt="@lang('Profile Image')">
                          </div>
                          <div class="h5 mb-0">{{$user->fullname}}</div>
                          <div class="text-muted">@lang('Joined At')</div>
                          <div class="text-muted">
                            <span class="align-middle"><strong>{{showDateTime($user->created_at,'d M, Y h:i A')}}</strong></span>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-body">
                    <div class="h-100 d-flex flex-column justify-content-between card-body align-items-center">
                        <div
                            class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center mb-2">
                            <i data-acorn-icon="user" class="text-white"></i>
                        </div>
                        <div class="heading text-center mb-0 d-flex align-items-center lh-1">Type Member</div>
                        <div class=" text-primary" style="text-transform: uppercase">{{$user->pin->name}}</div>
                    </div>
                    <div class="h-100 d-flex flex-column justify-content-between card-body align-items-center">
                        <div
                            class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center mb-2">
                            {{-- <i data-acorn-icon="star" class="text-white"></i> --}}
                        </div>
                        <div class="heading text-center mb-0 d-flex align-items-center lh-1">Peringkat</div>
                        <div class=" text-primary" style="text-transform: uppercase">{{$user->managerial->name?? '-'}}</div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="mb-20 text-muted">@lang('User information')</h5>
                    <ul class="list-group">

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            @lang('Username')
                            <span class="font-weight-bold">{{$user->username}}</span>
                        </li>
 

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            @lang('Status')
                            @if($user->status == StatusUser::ACTIVE)
                                {!!StatusUser::labelHTML($user->status)!!}
                            @elseif($user->status == StatusUser::BANNED)
                                {!!StatusUser::labelHTML($user->status)!!}
                            @endif
                        </li>

                        {{-- <li class="list-group-item d-flex justify-content-between align-items-center">
                            @lang('Balance')
                            <span class="font-weight-bold">{{showAmount($user->balance)}}  {{__($general->cur_text)}}</span>
                        </li> --}}
                    </ul>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="mb-20 text-muted">@lang('User action')</h5>
                    <a data-toggle="modal" href="#addSubModal" class="btn btn--success btn--shadow btn-block btn-lg">
                        @lang('Add/Subtract Balance')
                    </a>
                    <div class="d-grid gap-2 mb-1">
                        <a href="{{ route('admin.users.login.history.single', $user->id) }}"
                            class="btn btn-lg btn-icon btn-icon-start btn-primary mb-1"
                            {{-- class="btn btn--primary btn--shadow btn-block btn-lg" --}}
                            >
                            @lang('Login Logs')
                        </a>
                        
                    </div>
                    <div class="d-grid gap-2 mb-1">
                        <a href="{{route('admin.users.email.single',$user->id)}}"
                            class="btn btn-lg btn-icon btn-icon-start btn-danger mb-1">
                            {{-- class="btn btn--info btn--shadow btn-block btn-lg"> --}}
                            @lang('Send Email')
                        </a>
                    </div>
                    <div class="d-grid gap-2 mb-1">
                        <a href="{{route('admin.users.login',$user->id)}}" target="_blank"
                            {{-- class="btn btn--dark btn--shadow btn-block btn-lg"> --}}
                            class="btn btn-lg btn-icon btn-icon-start btn-success mb-1">
                            @lang('Login as User')
                        </a>
                    </div>
                    <div class="d-grid gap-2 mb-1">
                        <a href="{{route('admin.users.ref',$user->id)}}"
                            {{-- class="btn btn--info btn--shadow btn-block btn-lg"> --}}
                            class="btn btn-lg btn-icon btn-icon-start btn-dark mb-1">
                            @lang('User Referrals')
                        </a>
                    </div>
                    <div class="d-grid gap-2 mb-1">
                        <a href="{{route('admin.users.email.log',$user->id)}}" 
                            {{-- class="btn btn--warning btn--shadow btn-block btn-lg"> --}}
                            class="btn btn-lg btn-icon btn-icon-start btn-tertiary mb-1">
                            @lang('Email Log')
                        </a>
                    </div>
                    <div class="d-grid gap-2 mb-1">
                        <a href="{{route('admin.users.single.tree',$user->username)}}"
                            {{-- class="btn btn--primary btn--shadow btn-block btn-lg"> --}}
                            class="btn btn-lg btn-icon btn-icon-start btn-warning    mb-1">
                            @lang('User Tree')
                         </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9 col-lg-7 col-md-7 ">

            <div class="row g-2">
                <div class="col-12 col-sm-6 col-lg-4">
                  <div class="card hover-border-primary">
                    <div class="card-body">
                      <div class="heading mb-0 d-flex justify-content-between lh-1-25 mb-3"> 
                        <span>@lang('Total Bonus')</span> 
                      </div>
                      <div class="cta-1 text-primary">
                            <span class="amount">{{getAmount($BonusTotal)}}</span>
                            <span class="currency-sign">{{$general->cur_text}}</span>
                        </div>
                    </div>
                  </div>
                </div>

                <div class="col-12 col-sm-6 col-lg-4">
                  <div class="card hover-border-primary">
                    <div class="card-body">
                      <div class="heading mb-0 d-flex justify-content-between lh-1-25 mb-3"> 
                        <span>@lang('Total Withdraw')</span> 
                      </div>
                      <div class="cta-1 text-primary">
                            <span class="amount">{{getAmount($totalWithdraw)}}</span>
                            <span class="currency-sign">{{$general->cur_text}}</span>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4">
                  <div class="card hover-border-primary">
                    <div class="card-body">
                      <div class="heading mb-0 d-flex justify-content-between lh-1-25 mb-3"> 
                        <span>@lang('Total Autosave')</span> 
                      </div>
                      <div class="cta-1 text-primary">
                            <span class="amount">{{getAmount($totalWithdraw)}}</span>
                            <span class="currency-sign">{{$general->cur_text}}</span>
                        </div>
                    </div>
                  </div>
                </div>
{{-- 
                <div class="col-12 col-sm-6 col-lg-4">
                  <div class="card hover-border-primary">
                    <div class="card-body">
                      <div class="heading mb-0 d-flex justify-content-between lh-1-25 mb-3"> 
                        <span>@lang('Total Transaction')</span> 
                      </div>
                      <div class="cta-1 text-primary">
                            <span class="amount">{{$totalTransaction}}</span>
                        </div>
                    </div>
                  </div>
                </div> --}}

                {{-- <div class="col-12 col-sm-6 col-lg-4">
                  <div class="card hover-border-primary">
                    <div class="card-body">
                      <div class="heading mb-0 d-flex justify-content-between lh-1-25 mb-3">
                        <span>@lang('Total Invest')</span>
                        <i data-acorn-icon="money" class="text-primary"></i>
                      </div>
                      <div class="cta-1 text-primary">
                        <span class="amount">{{getAmount($user->total_invest)}}</span>
                        <span class="currency-sign">{{$general->cur_text}}</span>
                        </div>
                    </div>
                  </div>
                </div> --}}
{{-- 
                <div class="col-12 col-sm-6 col-lg-4">
                  <div class="card hover-border-primary">
                    <div class="card-body">
                      <div class="heading mb-0 d-flex justify-content-between lh-1-25 mb-3"> 
                        <span>@lang('Total Referral Commission')</span>
                        <i data-acorn-icon="user" class="text-primary"></i>
                      </div>
                      <div class="cta-1 text-primary">
                        <span class="amount">{{getAmount($user->total_ref_com)}}</span>
                                <span class="currency-sign">{{$general->cur_text}}</span>
                        </div>
                    </div>
                  </div>
                </div> --}}
{{-- 
                <div class="col-12 col-sm-6 col-lg-4">
                  <div class="card hover-border-primary">
                    <div class="card-body">
                      <div class="heading mb-0 d-flex justify-content-between lh-1-25 mb-3"> 
                        <span>@lang('Total Binary Commission')</span>
                        <i data-acorn-icon="diagram-1" class="text-primary"></i>
                      </div>
                      <div class="cta-1 text-primary">
                        <span class="amount">{{getAmount($user->total_binary_com)}}</span>
                                <span class="currency-sign">{{$general->cur_text}}</span>
                        </div>
                    </div>
                  </div>
                </div> --}}
            </div>

            <div class="row mt-2">

                <form action="{{route('admin.users.update',[$user->id])}}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="card mt-50">
                        <div class="card-body">
                            <h5 class="card-title border-bottom pb-2 mb-4">@lang('Update Information of') {{$user->fullname}}</h5>
                        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class=" font-weight-bold">@lang('Username')<span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="username" value="{{$user->username}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="   font-weight-bold">@lang('NIK') <span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="nik" value="{{$user->nik}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class=" font-weight-bold">@lang('First Name')<span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="firstname" value="{{$user->firstname}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="   font-weight-bold">@lang('Last Name') <span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="lastname" value="{{$user->lastname}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="  font-weight-bold">@lang('Email') <span class="text-danger">*</span></label>
                                        <input class="form-control" type="email" name="email" value="{{$user->email}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="   font-weight-bold">@lang('Mobile Number') <span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="mobile" value="{{$user->mobile}}">
                                    </div>
                                </div>
                                {{-- bank --}}
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="   font-weight-bold">@lang('Bank Provider') <span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="bank_provider" value="{{$user->bank_provider}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="   font-weight-bold">@lang('Bank Account') <span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="bank_account" value="{{$user->bank_account}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="font-weight-bold">@lang('Bank Number') <span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="bank_number" value="{{$user->bank_number}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <select id="select2Basic" name="member_id" class="member form-control">
                                    </select> --}}
                                    <div class="w-100 mb-3">
                                        <label class=" font-weight-bold">@lang('Pringkat Managrial') <span class="text-danger">*</span></label>
                                        <select id="select2Basic" name="manager_id" class="manager_id form-control">
                                            <option value="{{$user->manager_id}}" selected>{{$user->managerial->name?? "-"}}</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12">
                                    <div>
                                        {{-- <p class="mb-3 text-alternate">No Line</p> --}}
                                        <label class="mb-3 font-weight-bold">@lang('Jenis Kelamin') <span class="text-danger">*</span></label>
                                        <div class="mb-2">
                                            <label class="form-check custom-icon mb-0">
                                                <input type="radio" value="Laki-laki" class="form-check-input" @if($user->gender == 'Laki-laki') checked @endif
                                                    name="gender">
                                                <span class="form-check-label align-middle">
                                                    <span>Laki-laki</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div>
                                            <label class="form-check custom-icon mb-0">
                                                <input type="radio" value="Perempuan" class="form-check-input"
                                                    name="gender" @if($user->gender == 'Perempuan') checked @endif>
                                                <span class="form-check-label align-middle">
                                                        <span>Perempuan</span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="  font-weight-bold">@lang('Address') </label>
                                        <input class="form-control" type="text" name="address" value="{{@$user->address->address}}">
                                    </div>
                                </div>

                                <div class="col-xl-3 col-md-6">
                                    <div class="mb-3">
                                        <label class="  font-weight-bold">@lang('City') </label>
                                        <input class="form-control" type="text" name="city" value="{{@$user->address->city}}">
                                    </div>
                                </div>

                                <div class="col-xl-3 col-md-6">
                                    <div class="mb-3">
                                        <label class="  font-weight-bold">@lang('State') </label>
                                        <input class="form-control" type="text" name="state" value="{{@$user->address->state}}">
                                    </div>
                                </div>

                                <div class="col-xl-3 col-md-6">
                                    <div class="mb-3">
                                        <label class="  font-weight-bold">@lang('Zip/Postal') </label>
                                        <input class="form-control" type="text" name="zip" value="{{@$user->address->zip}}">
                                    </div>
                                </div>

                                <div class="col-xl-3 col-md-6">
                                    <div class="mb-3">
                                        <label class="  font-weight-bold">@lang('Country') </label>
                                        <select name="country" class="form-control">
                                            @foreach($countries as $key => $country)
                                                <option value="{{ $key }}" @if($country->country == @$user->address->country ) selected @endif>{{ __($country->country) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                {{-- Foto Profile --}}
                                    <div class="col-md-6">
                                        <div class="text-center">
                                        <label class="font-weight-bold">@lang('Profile User')</label>
                                        </div>
                                        <div class="text-center">
                                            <img class="img-fluid rounded mb-3"
                                                src="{{ getImage(imagePath()['profile']['user']['path'].'/'. $user->image,imagePath()['profile']['user']['size']) }}"
                                                alt="@lang('Image')">
                                        </div>
                                        <div class="input-group mb-3">
                                            <input type="file" class="form-control" id="inputGroupFile02" name="image" />
                                            <label class="input-group-text" for="inputGroupFile02">@lang('Image')</label>
                                        </div>
                                        <code>@lang('Image size') {{imagePath()['profile']['user']['size']}}</code>
                                    </div>
                                {{-- Foto Profile --}}
            
                                {{-- Foto KTP --}}
                                    <div class="col-md-6">
                                        <div class="text-center">
                                        <label class="font-weight-bold">@lang('Foto KTP')</label>
                                        </div>
                                        <div class="text-center">
                                            <img class="img-fluid rounded mb-3"
                                                src="{{ getImage(imagePath()['profile']['user']['path'].'/'. $user->img_nik,imagePath()['profile']['user']['size']) }}"
                                                alt="@lang('Image KTP')">
                                        </div>
                                        <div class="input-group mb-3">
                                            <input type="file" class="form-control" id="inputGroupFile02" name="img_nik" />
                                            <label class="input-group-text" for="inputGroupFile02">@lang('Image KTP')</label>
                                        </div>
                                        <code>@lang('Image size') {{imagePath()['profile']['user']['size']}}</code>
                                    </div>
                                {{-- Foto KTP --}}
                            </div>

                            <div class="row mt-4">
                                <div class="col-6">
                                    <div class="mb-2">
                                        <label class="form-check custom-icon mb-0">
                                            <input class="form-check-input" type="checkbox" data-onstyle="-success"
                                                data-offstyle="-danger" data-toggle="toggle" data-on="@lang('Active')"
                                                data-off="@lang('Banned')" data-width="100%" name="status"
                                                @if($user->status) checked @endif>
                                            <span class="form-check-label align-middle">
                                                <span class="">@lang('Status')</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="mb-2">
                                        <label class="form-check custom-icon mb-0">
                                            <input class="form-check-input" type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                            data-toggle="toggle" data-on="@lang('Verified')" data-off="@lang('Unverified')" name="ev"
                                            @if($user->ev) checked @endif>
                                            <span class="form-check-label align-middle">
                                                <span class="">@lang('Email Verification')</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="mb-2">
                                        <label class="form-check custom-icon mb-0">
                                            <input class="form-check-input" type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                            data-toggle="toggle" data-on="@lang('Verified')" data-off="@lang('Unverified')" name="sv"
                                            @if($user->sv) checked @endif>
                                            <span class="form-check-label align-middle">
                                                <span class="">@lang('SMS Verification')</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-6">
                                    <div class="mb-2">
                                        <label class="form-check custom-icon mb-0">
                                            <input class="form-check-input" type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                            data-toggle="toggle" data-on="@lang('Active')" data-off="@lang('Deactive')" name="ts"
                                            @if($user->ts) checked @endif>
                                            <span class="form-check-label align-middle">
                                                <span class="">@lang('2FA Status')</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-6">
                                    <div class="mb-2">
                                        <label class="form-check custom-icon mb-0">
                                            <input class="form-check-input" type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                            data-toggle="toggle" data-on="@lang('Verified')" data-off="@lang('Unverified')" name="tv"
                                            @if($user->tv) checked @endif>
                                            <span class="form-check-label align-middle">
                                                <span class="">@lang('2FA Verification')</span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-icon btn-icon-end btn-primary">@lang('Save Changes')
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="row mt-2">
                <form action="{{route('admin.users.update_password',[$user->id])}}"
                    id="personalForm" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="card mt-50">
                        <div class="card-body">
                            {{-- <div class="mb-3 filled">
                                <i data-acorn-icon="lock-off"></i>
                                <input class="form-control form--control form-control form--control-lg" type="password"
                                    name="current_password" placeholder="Password Lama" required minlength="5">
                            </div> --}}
                            <div class="mb-3 filled">
                                <i data-acorn-icon="lock-off"></i>
                                <input class="form-control form--control form-control form--control-lg" type="password"
                                    name="password" placeholder="Password Baru" required minlength="5">
                                @if($general->secure_password)
                                <div class="input-popup">
                                    <p class="error lower">@lang('1 small letter minimum')</p>
                                    <p class="error capital">@lang('1 capital letter minimum')</p>
                                    <p class="error number">@lang('1 number minimum')</p>
                                    <p class="error special">@lang('1 special character minimum')</p>
                                    <p class="error minimum">@lang('6 character password')</p>
                                </div>
                                @endif
                            </div>
                            <div class="mb-3 filled">
                                <i data-acorn-icon="lock-off"></i>
                                <input class="form-control form--control form-control form--control-lg"
                                    placeholder="Konfirmasi Password Baru" type="password" name="password_confirmation"
                                    required minlength="5">
                            </div>
                        </div>
                        <div class="card-footer align-items-center">
                            <div>
                                <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                                    <span>@lang('Changes Password')</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>



    {{-- Add Sub Balance MODAL --}}
    <div id="addSubModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Add / Subtract Balance')</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.users.add.sub.balance', $user->id)}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger" data-toggle="toggle" data-on="@lang('Add Balance')" data-off="@lang('Subtract Balance')" name="act" checked>
                            </div>


                            <div class="form-group col-md-12">
                                <label>@lang('Amount')<span class="text-danger">*</span></label>
                                <div class="input-group has_append">
                                    <input type="text" name="amount" class="form-control" placeholder="@lang('Please provide positive amount')">
                                    <div class="input-group-append">
                                        <div class="input-group-text">{{ __($general->cur_sym) }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-bs-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--success">@lang('Submit')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('js')
    {{-- SELECT2 --}}
        <script src="{{asset('assets/member/js/vendor/select2.full.min.js')}}"></script>
    {{-- SELECT2 --}}

    <script>
        class Select2Controls {
                constructor() {
                    $('.manager_id').select2({
                        placeholder: 'Pilih pringkat managerial',
                        ajax: {
                            url: "{{ route('admin.json.gel_json_managerial_all') }}",
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search_name: params.term,
                                    page: params.page || 1
                                }
                                return query
                            },
                            delay: 250,
                            processResults: function (data) {
                                return {
                                    results: data.data,
                                    pagination: {
                                        more: (data.current_page == data.last_page) ? null : data
                                            .current_page + 1
                                    }
                                }
                            },
                            cache: true
                        }
                    });
                }
            }
        // SELECT2 USER
    </script>
@endpush
