@extends('adminv2.layouts.app')
@php
    $html_tag_data = ["scrollspy"=>"true"];
    $title = 'Stats';
    $description = 'Numeral value containers for different stats with icons and various layouts.';
    $breadcrumbs = ["/"=>"Home","/Blocks"=>"Blocks"]
@endphp
{{-- @extends('breadcrumb') --}}

@section('panel')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">All Member</h1>
            </div>
        </div>
    </div>

    <!-- Filter -->
        <div class="row mb-2">
        <form action="">
            <!-- Search Start -->
            <div class="col-sm-12 col-md-5 col-lg-3 col-xxl-2 mb-1">
                <div
                    class="d-inline-block float-md-start me-1 mb-1 search-input-container w-100 shadow bg-foreground">
                    <input class="form-control" placeholder="Search User Firstname" name='search'
                        value="{{ Request::get('search') }}" />
                    <span class="search-magnifier-icon">
                        <a onclick="this.closest('form').submit();return false;"><i
                                data-acorn-icon="search"></i></a>
                    </span>
                    <span class="search-delete-icon d-none">
                        <i data-acorn-icon="close"></i>
                    </span>
                </div>
            </div>
            <!-- Search End -->
        </form>
        </div>
    <!-- Filter -->

    {{-- Table --}}
        <section class="scroll-section" id="breakpointSpecificResponsive">
            <div class="card mb-5">
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-bordered" style="margin: 0px">
                            <thead>
                            <tr class="table-active">
                                <th>@lang('User')</th>
                                <th>@lang('Email-Phone')</th>
                                <th>@lang('Package')</th>
                                <th>@lang('BV')</th>
                                <th>@lang('PR')</th>
                                <th>@lang('Managerial')</th>
                                {{-- <th>@lang('Country')</th> --}}
                                <th>@lang('Joined At')</th>
                                <th>@lang('Geneology')</th>
                                <th>@lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse ($users as $user)
                                <tr>
                                    <td data-label="@lang('User')">
                                        <span class="font-weight-bold">{{$user->fullname}}</span>
                                        <br>
                                        <span class="small">
                                        <a href="{{ route('admin.users.detail', $user->id) }}"><span>@</span>{{ $user->username }}</a>
                                        </span>
                                    </td>


                                    <td data-label="@lang('Email-Phone')">
                                        {{ $user->email }}<br>{{ $user->mobile }}
                                    </td> 
                                    <td data-label="@lang('Package')">
                                        {{ $user->pin->name }}
                                    </td> 
                                    <td data-label="@lang('BV')">
                                         {{ $user->bv }}
                                    </td> 
                                    <td data-label="@lang('PR')">
                                         {{ $user->pr }}
                                    </td> 
                                    <td data-label="@lang('Managerial')">
                                         {{ $user->managerial->name ??'-'}}
                                    </td> 



                                    <td data-label="@lang('Joined At')">
                                        {{ showDateTime($user->created_at) }} <br> {{ diffForHumans($user->created_at) }}
                                    </td>


                                    <td data-label="@lang('Geneology')">
                                        <span class="font-weight-bold">  
                                            <a href="{{route('admin.users.single.tree',$user->username)}}"
                                                {{-- class="btn btn--primary btn--shadow btn-block btn-lg"> --}}
                                                class="btn btn-lg btn-icon btn-icon-start btn-warning    mb-1">
                                                @lang('User Tree')
                                             </a> 
                                        </span>
                                    </td>



                                    <td data-label="@lang('Action')">
                                        <a href="{{ route('admin.users.detail', $user->id) }}" class="icon-btn" data-toggle="tooltip" title="" data-original-title="@lang('Details')">
                                            <i class="las la-desktop text--shadow"></i>
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                    <tr>
                                        <td colspan="6" class="text-center">Data Kosong</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    {{-- Table --}}

    <!-- Pagination -->
        {{ $users->appends(request()->query())->links('member.layouts.partials.pagination-semibordered') }}
    <!-- Pagination -->
@endsection



@push('breadcrumb-plugins')
    <form action="{{ route('admin.users.search', $scope ?? str_replace('admin.users.', '', request()->route()->getName())) }}" method="GET" class="form-inline float-sm-right bg--white">
        <div class="input-group has_append">
            <input type="text" name="search" class="form-control" placeholder="@lang('Username or email')" value="{{ $search ?? '' }}">
            <div class="input-group-append">
                <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
@endpush
