
{{-- @push('style')  --}}
    {{-- <style> .user{ height: 100px; width: 100px; margin-left: auto; margin-right: auto; line-height: 120px; position: relative; border-radius: 50%; /* background-color: #ffffff; */ border: none; -webkit-appearance: initial!important; } .user img { /* width: 100px; height: 100px; border-radius: 50%; object-fit: cover; position: relative; z-index: 99; */ } .line { width: 50%; margin-left: auto; margin-right: auto; height: 80px; display: inherit; border: 2px dotted #bbb; border-bottom: none; position: relative; } .line::before, .line::after { position: absolute; content: "\f107"; font-family: "Line Awesome Free"; font-weight: 600; font-size: 24px; color: #bbb; bottom: 0; width: 30px; text-align: center; background: #fff; z-index: 1; line-height: 20px; height: 20px; } .line::before { left: -15px; } .line::after { right: -15px; } .w-8 .line::before, .w-8 .line::after { display: none; } .llll:last-child  .line { border: none; } .w-1 { width: 100%; } .w-2 { width: 50%; } .w-4 { width: 25%; } .w-8 { width: 12.5%; } .user .user-name { line-height: 1.5; font-size: 16px; font-weight: 500; color: #777777; } @media (max-width: 767px) { /* .user { line-height: 0; position: relative; z-index: 2; } */ .w-4 .line::before, .w-4 .line::after { display: none; } .user img { border-width: 3px; } .line { transform: translateY(-10px); } .w-1 .user { width: 80px; height: 80px; } .w-1 .user img { width: 80px; height: 80px; } .user::before { display: none; } .w-1 .line { height: 50px; } .w-2 .user { width: 70px; height: 70px; } .w-2 .user img { width: 70px; height: 70px; } .w-2 .line { height: 50px; } .w-4 .user { width: 60px; height: 60px; } .w-4 .user img { width: 60px; height: 60px; } .w-4 .line { height: 40px; margin-top: 17px; width: 60%; transform: translate(-8px,-20px); z-index: 0; } .w-8 { transform: translateY(-30px); } .w-8 .user { width: 50px; height: 50px; } .w-8 .user img { width: 50px; height: 50px; } .w-8 .line { height: 0; } .user .user-name { display: none; } } @media (max-width: 575px) { /* .user { line-height: 0; position: relative; z-index: 2; } */ .line { transform: translateY(-10px); } .w-1 .user { width: 70px; height: 70px; } .w-1 .user img { width: 70px; height: 70px; } .w-1 .line { height: 50px; } .w-2 .user { width: 60px; height: 60px; } .w-2 .user img { width: 60px; height: 60px; } .w-2 .line { height: 50px; } .w-4 .user { width: 55px; height: 55px; } .w-4 .user img { width: 55px; height: 55px; } .w-4 .line { height: 40px; margin-top: 18px; width: 70%; transform: translate(-1px,-20px); z-index: 0; } .w-8 { transform: translateY(-30px); } .w-8 .user { width: 45px; height: 45px; } .w-8 .user img { width: 45px; height: 45px; } .w-8 .line { height: 0; } .user img { border-width: 2px; } } @media (max-width: 400px) { /* .user { line-height: 0; position: relative; z-index: 2; } */ .line { transform: translateY(-10px); } .w-1 .user { width: 70px; height: 70px; } .w-1 .user img { width: 70px; height: 70px; } .w-1 .line { height: 50px; } .w-2 .user { width: 60px; height: 60px; } .w-2 .user img { width: 60px; height: 60px; } .w-2 .line { height: 50px; } .w-4 .user { width: 50px; height: 50px; } .w-4 .user img { width: 50px; height: 50px; } .w-4 .line { height: 50px; margin-top: 17px; width: 49%; transform: translate(-2px,-19px); z-index: 0; } .w-8 { transform: translateY(-30px); } .w-8 .user { width: 35px; height: 35px; } .w-8 .user img { width: 35px; height: 35px; } .w-8 .line { height: 0; } .card { padding: 30px; margin: 0 -30px; } } .paid-user { border: 5px solid #2ecc71; } .free-user { border: 5px solid #101536; } .no-user { border: 5px solid #ddd; } .user-details-modal-area .modal-body { padding: 0; } .user-details-header { /*background-color: rgba(6, 243, 183, 0.22);*/ display: flex; align-items: center; padding: 25px 30px; } .Paid { background-color: rgba(6, 243, 183, 0.22); } .Free { background-color: rgba(241, 196, 15, 0.22); } .user-details-header .thumb { width: 80px; height: 80px; overflow: hidden; border-radius: 50%; } .user-details-header .content { width: calc(100% - 80px); padding-left: 30px; } .user-details-header .content .user-name { display: block; font-size: 22px; font-weight: 500; text-transform: capitalize; } .user-details-header .content .user-status { font-weight: 500; } .user-details-body { padding: 20px 30px; } .user-details-body h4 { margin-bottom: 20px; } .user-details-body p { margin-bottom: 0; color: #777777; } .user-details-body p+p { margin-top: 10px; } img { max-width: 100%; } </style> --}}
    {{-- @endpush --}}
    
    <link rel="stylesheet" href="{{ asset('assets/global/css/tree_new.css') }}" />

<div class="row">
    <div class="col-lg-12">
        <h4>New Tree</h4>
        @if ($ref['countRef'] < 6) <button class="openFormRegister btn btn-success mt-3">
            <em class="fa fa-user"></em> Form Register
            </button>
        @else
        <button class="btn btn-secondary mt-3">
            <em class="fa fa-user"></em> Form Register
        </button>
        @endif
    </div>
</div>

<div class="body user-body user-scroll text-center">
    <span>loading...</span>
    <div class="user-tree text-center">
        {!! $treeNew !!}
    </div> 
</div>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script>
$(function() {
    $(document).ready(function() {
        $(".user-scroll").find('span').remove()
        $(".main-ul").removeClass('hidden-tree')

        $('.openFormRegister').on('click', function() {
            $("#form-registers-act")[0].reset()
            var modal = $('#formModelRegister')

            modal.modal('show')
        });

        $(".selecting-area").find("select[name=province_id]").on('change', function() {
            var id = $(this).val()
            if (id)
                $("select[name=city_id]").html("<option>loading...</option>")
            return option("city", id)
        });

        $(".selecting-area").find("select[name=city_id]").on('change', function() {
            var id = $(this).val()
            if (id)
                $("select[name=district_id]").html("<option>loading...</option>")
            return option("district", id)
        });

        function option(area, id) {
            if (area == 'city') {
                var htmlCity = ''
                $.ajax({
                    url: '{{ url("user/detail-area/city") }}/' + id,
                    type: 'get',
                    dataType: "JSON",
                    success: function(data) {
                        setTimeout(function() {
                            if (data.code == 200) {
                                htmlCity += '<option value="">Pilih...</option>'
                                $.each(data.data.cities, function(e, f) {
                                    htmlCity += '<option value="' + f .id + '">' + f .name + '</option>'
                                })
                            }

                            return $("select[name=city_id]").html(htmlCity)
                        }, 1000)
                    }
                });
            }

            if (area == 'district') {
                var htmlDistrict = ''
                $.ajax({
                    url: '{{ url("user/detail-area/district") }}/' + id,
                    type: 'get',
                    dataType: "JSON",
                    success: function(data) {
                        setTimeout(function() {
                            if (data.code == 200) {
                                htmlDistrict +=
                                    '<option value="">Pilih...</option>'
                                $.each(data.data.districts, function(e, f) {
                                    htmlDistrict += '<option value="' +
                                        f
                                        .id + '">' + f
                                        .name + '</option>'
                                })
                            }

                            return $("select[name=district_id]").html(
                                htmlDistrict)
                        }, 1000)
                    }
                });
            }
        }
    });

    $('.user-tree ul').hide();
    $('.user-tree>ul').show();
    $('.user-tree ul.active').show();
    $('.user-tree li.way').on('click', function(e) {
        var children = $(this).find('> ul');
        if (children.is(":visible")) {
            children.hide('fast').removeClass('active');
        } else {

            var thisData = $(this).find('.showHover')

            if (thisData.length > 0) {
                var modal = $('#exampleModalCenter');
                $('.tree_name').text(thisData.data('name'));
                $('.tree_url').attr({
                    "href": thisData.data('treeurl')
                });
                $('.tree_status').text(thisData.data('status'));
                $('.tree_plan').text(thisData.data('plan'));
                $('.tree_image').attr({
                    "src": thisData.data('image')
                });
                $('.user-details-header').removeClass('Paid');
                $('.user-details-header').removeClass('Free');
                $('.user-details-header').addClass(thisData.data('status'));
                $('.tree_ref').text(thisData.data('refby'));
                $('.lbv').text(thisData.data('lbv'));
                $('.rbv').text(thisData.data('rbv'));
                $('.lpaid').text(thisData.data('lpaid'));
                $('.rpaid').text(thisData.data('rpaid'));
                $('.lfree').text(thisData.data('lfree'));
                $('.rfree').text(thisData.data('rfree'));
                // $('#exampleModalCenter').modal('show'); 
            }

            children.show('fast').addClass('active');
        }
        e.stopPropagation();
    });
});
</script>