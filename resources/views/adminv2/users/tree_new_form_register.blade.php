
<div class="modal fade form-register-modal-area" id="formModelRegister" tabindex="-1" role="dialog"
    aria-labelledby="formModelRegisterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModelRegisterTitle">@lang('Form Register by Refferal '){{$ref['name']}}
                </h5> 
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
 
                </button>
            </div>
            <div class="modal-body">
                <div class="form-register-body"> 
                    <form id="form-registers-act"
                        action="{{route('member.users.registered-from-refferal',[$ref['id']])}}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row"> 
                            <div class="col-md-8">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang(' Select Pin') <span
                                            class="text-danger">*</span></label>
                                    <select name="pin_id" class="form-control">
                                        <option value="">Select pin ..</option>
                                        @foreach ($userPin as $p)
                                        <option  value="{{$p->pin->id}}"> {{$p->pin->name}} - {{$p->qty}} </option>  
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="font-weight-bold">@lang('Referal Name')<span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="firstname" disabled value="{{$ref['name']}}"
                                        form="form-registers-act">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('First Name')<span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="firstname"
                                        form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="   font-weight-bold">@lang('Last Name') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="lastname"
                                        form="form-registers-act">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('Email') <span
                                            class="text-danger">*</span></label>
                                    <input autocomplete="off" class="form-control" type="email" name="email"
                                        form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="   font-weight-bold">@lang('Mobile Number') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="mobile" maxlength="20"
                                        form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="   font-weight-bold">@lang('Bank Account') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="bank_account" maxlength="90"
                                        form="form-registers-act">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="   font-weight-bold">@lang('Bank Account Number') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="bank_number" maxlength="20"
                                        form="form-registers-act">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="   font-weight-bold">@lang('Bank Account Name') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="bank_name" maxlength="30"
                                        form="form-registers-act">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="  font-weight-bold">@lang('Username') <span
                                            class="text-danger">*</span></label>
                                    <input autocomplete="off" class="form-control" type="text" name="username"
                                        form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="  font-weight-bold">@lang('Password') <span
                                            class="text-danger">*</span></label>
                                    <input autocomplete="off" class="form-control" type="password"
                                        name="password" form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('Gender') <span
                                            class="text-danger">*</span></label>
                                    <select name="gender" class="form-control">
                                        <option value="">Pilih...</option>
                                        <option value="1">Laki-Laki</option>
                                        <option value="2">Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('NIK') <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="number" maxlength="16" name="nik"
                                        form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('Day Of Birth') </label>
                                    <input class="form-control" type="date" name="dob"
                                        form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('Place Of Birth') </label>
                                    <input class="form-control" type="text" name="pob"
                                        form="form-registers-act">
                                </div>
                            </div>
                        </div>


                        <div class="row mt-2 selecting-area">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="  font-weight-bold">@lang('Province') </label>
                                    <select name="province_id" class="form-control">
                                        <option value="">Pilih...</option>
                                        @if (!empty($province))
                                        @foreach ($province as $ar)
                                        <option value="{{ $ar->id}}">{{ $ar->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="  font-weight-bold">@lang('City') </label>
                                    <select name="city_id" class="form-control">
                                        <option value="">Pilih...</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="  font-weight-bold">@lang('Distinct') </label>
                                    <select name="district_id" class="form-control">
                                        <option value="">Pilih...</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('Zip/Postal') </label>
                                    <input class="form-control" type="text" maxlength="5" name="zip"
                                        form="form-registers-act">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="  font-weight-bold">@lang('Address') </label>
                                    <textarea class="form-control" name="address"
                                        form="form-registers-act"></textarea>
                                </div>
                            </div>



                        </div>

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{-- <button type="submit" form="form-registers-act"
                                        class="btn btn--primary btn-block btn-lg">@lang('Save')
                                    </button> --}}
                                    <button form="form-registers-act" class="btn btn-icon btn-icon-end btn-outline-primary mb-1" type="submit">
                                        <span>@lang('Submit')</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="acorn-icons acorn-icons-arrow-bottom-right undefined"><path d="M13 8L17.6464 12.6464C17.8417 12.8417 17.8417 13.1583 17.6464 13.3536L13 18"></path><path d="M2 2V9C2 10.0609 2.42143 11.0783 3.17157 11.8284C3.92172 12.5786 4.93913 13 6 13H17.5"></path></svg>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>