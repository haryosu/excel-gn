 
<!DOCTYPE html>
<!-- Metreex - SEO & Digital Marketing Agency Landing Page Template design by Jthemes -->
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">




<head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="PT. EXCEL GLOBAL NETWORK" />
    <meta name="description" content="Minuman serbuk untuk kesehatan tubuh Collskin, dan Excel C dari PT. EXCEL GLOBAL NETWORK." />
    <meta name="keywords"
        content="Minuman serbuk untuk kesehatan tubuh Collskin, dan Excel C dari PT. EXCEL GLOBAL NETWORK.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- SITE TITLE -->
    <title>PT. EXCEL GLOBAL NETWORK</title>

    <!-- FAVICON AND TOUCH ICONS  -->
    <link rel="shortcut icon" href="{{asset('front/assets/img/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('front/assets/img/favicon.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="icon" href="{{asset('front/assets/img/favicon.png')}}" type="image/x-icon">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- BOOTSTRAP CSS -->
    <link href="{{asset('front/assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- FONT ICONS -->
    <link href="https://use.fontawesome.com/releases/v5.11.0/css/all.css" rel="stylesheet" crossorigin="anonymous">
    <link href="{{asset('front/assets/css/flaticon.css')}}" rel="stylesheet">

    <!-- PLUGINS STYLESHEET -->
    <link href="{{asset('front/assets/css/menu.css')}}" rel="stylesheet">
    <link id="effect" href="{{asset('front/assets/css/dropdown-effects/fade-down.css')}}" media="all" rel="stylesheet">
    <link href="{{asset('front/assets/css/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/flexslider.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/owl.theme.default.min.css')}}" rel="stylesheet">

    <!-- ON SCROLL ANIMATION -->
    <link href="{{asset('front/assets/css/animate.css" rel="stylesheet')}}">

    <!-- TEMPLATE CSS -->
    <link href="{{asset('front/assets/css/red.css')}}" rel="stylesheet">

    <!-- STYLE SWITCHER CSS -->
    <link href="{{asset('front/assets/css/carrot.css')}}" rel="alternate stylesheet" title="carrot-theme">
    <link href="{{asset('front/assets/css/dodgerblue.css')}}" rel="alternate stylesheet" title="dodgerblue-theme">
    <link href="{{asset('front/assets/css/green.css')}}" rel="alternate stylesheet" title="green-theme">
    <link href="{{asset('front/assets/css/magneta.css')}}" rel="alternate stylesheet" title="magneta-theme">
    <link href="{{asset('front/assets/css/olive.css')}}" rel="alternate stylesheet" title="olive-theme">
    <link href="{{asset('front/assets/css/orange.css')}}" rel="alternate stylesheet" title="orange-theme">
    <link href="{{asset('front/assets/css/purple.css')}}" rel="alternate stylesheet" title="purple-theme">
    <link href="{{asset('front/assets/css/skyblue.css')}}" rel="alternate stylesheet" title="skyblue-theme">
    <link href="{{asset('front/assets/css/teal.css')}}" rel="alternate stylesheet" title="teal-theme">

    <!-- RESPONSIVE CSS -->
    <link href="{{asset('front/assets/css/responsive.css')}}" rel="stylesheet"> 
</head>



<body>
 

    <!-- PAGE CONTENT
		============================================= -->
    {{-- <div id="page" class="page">  --}}
        <!-- HEADER
			============================================= -->
        <section id="hero-1" class="bg-scroll hero-section division">
            <div class="container">
                <!-- <div class="row d-flex align-items-center"> -->
                <div class="row">
                    <div class="hero-txt text-center white-color">

                        <!-- Title -->
                        <h3>  Excel Global Network <br><span>mengucapkan</span></h3>
                        {{-- <span>mengucapkan</span> --}}

                        <!-- Text -->
                        <p> 
                            Selamat Hari Raya Idul Fitri 1444 H. Minal aidzin wal faidzin, mohon maaf lahir dan batin. Semoga silaturahmi kita semua terus terjaga.</p>

                        <!-- Button -->
                        <!-- <a href="#excelc" class="btn btn-md btn-primary tra-white-hover">Cek Produk Kami</a> -->

                    </div>
                    <div class="col-xl-10  text-center  offset-xl-1">
                        <!-- HERO TEXT -->
                        <!-- <div class="col-md-6 col-xl-5"> -->
                        <!-- </div> END HERO TEXT -->

                    </div>    
                </div>    


                    <!-- HERO IMAGE -->
                    <!-- <div class="col-md-6 col-xl-7">
                        <div class="hero-9-img text-center">
                            <img class="img-fluid" src="front/assets/img/challenge-bali-excel.jpg')}}" alt="challenge-bali-excel">
                        </div>
                    </div> -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hero-12-img text-center">
                        <img class="img-fluid" src="{{asset('front/assets/img/eid.jpg')}}" alt="ramadhan-excel">
                        </div>
                    </div>
                </div>


                <!-- </div> End row -->
            </div> <!-- End container -->
            <div class="bg-fixed white-overlay-top"></div>
        </section> 
        <section  class=" wide-60 about-section division mt-80">
                <div class="container">
                    <div class="row d-flex align-items-center">
    

                        <!-- ABOUT TEXT -->
                        <div class="col-md-12 col-lg-12 align-content-center">
                            <div class="txt-block  text-center  pc-25 mb-40 wow fadeInRight" data-wow-delay="0.4s">

    
                                <h4 class="h4-xl bluestone-color">PT. EXCEL GLOBAL NETWORK
                                </h4>
                                <p>
                                <strong>PT. EXCEL GLOBAL NETWORK</strong> di moment ini kami sedang melakukan perbaikan system aplikasi.
                                </p>
                                <p>
                                    Untuk itu, sementara waktu kami maintenance mode supaya tidak terjadi penumpukan data.
                                </p> 
                                <div class="img-block pe-25 mb-40 wow fadeInLeft" data-wow-delay="0.6s">
                                    <img class="img-fluid" width="80" src="{{asset('front/assets/img/owner.png')}}" alt="about-image">
                                    <img class="img-fluid" width="120" src="{{asset('front/assets/img/logo.png')}}" alt="about-image">
                                </div>
                            </div>
                        </div> <!-- END ABOUT TEXT -->


                    </div> <!-- End row -->
                </div> <!-- End container -->
        </section> 
        <!-- END HEADER -->



        <!-- ABOUT-1
			============================================= -->
        <!-- End ABOUT-1 -->
 

    {{-- </div>    --}}
    <script src="{{asset('front/assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('front/assets/js/bootstrap.min.js')}}"></script>
    {{-- <script src="{{asset('front/assets/js/modernizr.custom.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.easing.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.appear.js')}}"></script>
    <script src="{{asset('front/assets/js/menu.js')}}"></script>
    <script src="{{asset('front/assets/js/materialize.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.scrollto.js')}}"></script>
    <script src="{{asset('front/assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('front/assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.flexslider.js')}}"></script>
    <script src="{{asset('front/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('front/assets/js/seo-form.js')}}"></script>
    <script src="{{asset('front/assets/js/contact-form.js')}}"></script>
    <script src="{{asset('front/assets/js/comment-form.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('front/assets/js/wow.js')}}"></script> --}}

    <!-- Custom Script -->
    <script src="{{asset('front/assets/js/custom.js')}}"></script>

  
    <script src="{{asset('front/assets/js/changer.js')}}"></script>
    <script defer src="{{asset('front/assets/js/styleswitch.js')}}"></script> 
</body> 
</html>