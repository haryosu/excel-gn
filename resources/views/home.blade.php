 
<!DOCTYPE html>
<!-- Metreex - SEO & Digital Marketing Agency Landing Page Template design by Jthemes -->
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">




<head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="PT. EXCEL GLOBAL NETWORK" />
    <meta name="description" content="Minuman serbuk untuk kesehatan tubuh Collskin, dan Excel C dari PT. EXCEL GLOBAL NETWORK." />
    <meta name="keywords"
        content="Minuman serbuk untuk kesehatan tubuh Collskin, dan Excel C dari PT. EXCEL GLOBAL NETWORK.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- SITE TITLE -->
    <title>PT. EXCEL GLOBAL NETWORK</title>

    <!-- FAVICON AND TOUCH ICONS  -->
    <link rel="shortcut icon" href="{{asset('front/assets/img/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('front/assets/img/favicon.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('front/assets/img/favicon.png')}}">
    <link rel="icon" href="{{asset('front/assets/img/favicon.png')}}" type="image/x-icon">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- BOOTSTRAP CSS -->
    <link href="{{asset('front/assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- FONT ICONS -->
    <link href="https://use.fontawesome.com/releases/v5.11.0/css/all.css" rel="stylesheet" crossorigin="anonymous">
    <link href="{{asset('front/assets/css/flaticon.css')}}" rel="stylesheet">

    <!-- PLUGINS STYLESHEET -->
    <link href="{{asset('front/assets/css/menu.css')}}" rel="stylesheet">
    <link id="effect" href="{{asset('front/assets/css/dropdown-effects/fade-down.css')}}" media="all" rel="stylesheet">
    <link href="{{asset('front/assets/css/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/flexslider.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/owl.theme.default.min.css')}}" rel="stylesheet">

    <!-- ON SCROLL ANIMATION -->
    <link href="{{asset('front/assets/css/animate.css" rel="stylesheet')}}">

    <!-- TEMPLATE CSS -->
    <link href="{{asset('front/assets/css/red.css')}}" rel="stylesheet">

    <!-- STYLE SWITCHER CSS -->
    <link href="{{asset('front/assets/css/carrot.css')}}" rel="alternate stylesheet" title="carrot-theme">
    <link href="{{asset('front/assets/css/dodgerblue.css')}}" rel="alternate stylesheet" title="dodgerblue-theme">
    <link href="{{asset('front/assets/css/green.css')}}" rel="alternate stylesheet" title="green-theme">
    <link href="{{asset('front/assets/css/magneta.css')}}" rel="alternate stylesheet" title="magneta-theme">
    <link href="{{asset('front/assets/css/olive.css')}}" rel="alternate stylesheet" title="olive-theme">
    <link href="{{asset('front/assets/css/orange.css')}}" rel="alternate stylesheet" title="orange-theme">
    <link href="{{asset('front/assets/css/purple.css')}}" rel="alternate stylesheet" title="purple-theme">
    <link href="{{asset('front/assets/css/skyblue.css')}}" rel="alternate stylesheet" title="skyblue-theme">
    <link href="{{asset('front/assets/css/teal.css')}}" rel="alternate stylesheet" title="teal-theme">

    <!-- RESPONSIVE CSS -->
    <link href="{{asset('front/assets/css/responsive.css')}}" rel="stylesheet"> 
</head>



<body>
 

    <!-- PAGE CONTENT
		============================================= -->
    <div id="page" class="page">

 
        <!-- HEADER
			============================================= -->
        <header id="header" class="header tra-menu navbar-light">
            <div class="header-wrapper"> 
                <!-- MOBILE HEADER -->
                <div class="wsmobileheader clearfix" style="transition: top 0.2s ease-in-out;">
                    <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
                    <span class="smllogo smllogo-black"><img src="{{asset('front/assets/img/logo.png')}}" width="162" height="40"
                            alt="mobile-logo" /></span>
                    <span class="smllogo smllogo-white"><img src="{{asset('front/assets/img/logo-white.png')}}" width="162" height="40"
                            alt="mobile-logo" /></span>
                    <a class="callusbtn" role="button" href="front/assets/pdf/MP_Excel_Cetak.pdf" download="proposed_file_name"> <i class="fas fa-download"></i> </a>
                    <!-- <a href="tel:123456789" class=""></a> -->
                </div> 
                <!-- NAVIGATION MENU -->
                <div class="wsmainfull menu clearfix">
                    <div class="wsmainwp clearfix">


                        <!-- LOGO IMAGE -->
                        <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 334 x 80 pixels) -->
                        <div class="desktoplogo"><a href="#hero-9" class="logo-black"><img src="{{asset('front/assets/img/logo.png')}}"
                                    width="150" height="40" alt="header-logo"></a></div>
                        <div class="desktoplogo"><a href="#hero-9" class="logo-white"><img src="{{asset('front/assets/img/logo-white.png')}}"
                                    width="150" height="40" alt="header-logo"></a></div>


                        <!-- MAIN MENU -->
                        <nav class="wsmenu clearfix blue-header">
                            <ul class="wsmenu-list">


                                <!-- SIMPLE NAVIGATION LINK -->
                                <li class="nl-simple" aria-haspopup="true"><a href="#about-1">About</a></li> 
                                <li class="nl-simple" aria-haspopup="true"><a href="#about-1">Vision</a></li>
                                <li class="nl-simple" aria-haspopup="true"><a href="#excelc">Our Product</a></li>
                                <li class="nl-simple" aria-haspopup="true"><a href="front/assets/pdf/KODE-ETIK-EXCEL-6-DES-22.pdf">Kode Etik</a></li> 
                                <!-- <li class="nl-simple" aria-haspopup="true"><a class="btn btn-tra-white primary-hover last-link" role="button" href="front/assets/pdf/KODE-ETIK-EXCEL-6-DES-22.pdf" download="proposed_file_name"> <i class="fas fa-download"></i> Marketing Plan</a></li>  -->
                                <li class="nl-simple" aria-haspopup="true"><a class="btn btn-tra-white primary-hover last-link" role="button" href="front/assets/pdf/MP_Excel_Cetak.pdf" download="proposed_file_name"> <i class="fas fa-download"></i> Marketing Plan</a></li> 

 
                                <!-- HEADER BUTTON -->
                                <li class="nl-simple" aria-haspopup="true">
                                    <a href="{{URL('login')}}" class="btn btn-tra-white primary-hover last-link">
                                    <i class="fa fa-user"></i>    
                                    Login</a>
                                </li>


                            </ul>
                        </nav> <!-- END MAIN MENU -->

                    </div>
                </div> <!-- END NAVIGATION MENU -->


            </div> <!-- End header-wrapper -->
        </header> 
        <!-- END HEADER -->




        <!-- HEADER
			============================================= -->
        <section id="hero-1" class="bg-scroll hero-section division">
            <div class="container">
                <!-- <div class="row d-flex align-items-center"> -->
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <!-- HERO TEXT -->
                        <!-- <div class="col-md-6 col-xl-5"> -->
                            <div class="hero-txt text-center white-color">
        
                                <!-- Title -->
                                <h3>  Excel Global Network <br><span>CHALLENGE BALI </span></h3>
        
                                <!-- Text -->
                                <p> 
                                Ayo ikut berbahagia bersama keluarga besar EXCEL di Bali bersama orang-orang yang Anda cintai, Mumpung tiketnya dihitung kelipatan guys.!
                                 <!-- Perkenalkan <strong>Collskin</strong>, dan <strong>Excel-C</strong>  dari <strong>PT. EXCEL GLOBAL NETWORK</strong>.
                                    Minuman Serbuk kesehatan yang kaya manfaat untuk meningkatkan imunitas tubuh serta banyak manfaat lainnya.  -->
                                </p>
        
                                <!-- Button -->
                                <!-- <a href="#excelc" class="btn btn-md btn-primary tra-white-hover">Cek Produk Kami</a> -->
        
                            </div>
                        <!-- </div> END HERO TEXT -->

                    </div>    
                </div>    


                    <!-- HERO IMAGE -->
                    <!-- <div class="col-md-6 col-xl-7">
                        <div class="hero-9-img text-center">
                            <img class="img-fluid" src="front/assets/img/challenge-bali-excel.jpg')}}" alt="challenge-bali-excel">
                        </div>
                    </div> -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hero-12-img text-center">
                        <img class="img-fluid" src="{{asset('front/assets/img/challenge-bali-excel.jpg')}}" alt="challenge-bali-excel">
                        </div>
                    </div>
                </div>


                <!-- </div> End row -->
            </div> <!-- End container -->
            <div class="bg-fixed white-overlay-top"></div>
        </section> 
        <!-- END HEADER -->



        <!-- ABOUT-1
			============================================= -->
            <section id="about-1" class="bg-lightgrey wide-60 about-section division">
            <div class="container">
                <div class="row d-flex align-items-center">


                    <!-- ABOUT IMAGE -->
                    <div class="col-md-5 col-lg-6">
                        <div class="img-block pe-25 mb-40 wow fadeInLeft" data-wow-delay="0.6s">
                            <img class="img-fluid" src="{{asset('front/assets/img/owner.png')}}" alt="about-image">
                        </div>
                    </div>


                    <!-- ABOUT TEXT -->
                    <div class="col-md-7 col-lg-6">
                        <div class="txt-block pc-25 mb-40 wow fadeInRight" data-wow-delay="0.4s">

                            <!-- Title -->
                            <h3 class="h4-xl bluestone-color">Company Profile
                            </h3>
                            <h4 class="h4-xl bluestone-color">PT. EXCEL GLOBAL NETWORK
                            </h4>
                            <p>
                            <strong>PT. EXCEL GLOBAL NETWORK</strong> adalah perusahaan Penjualan Langsung
                            (Direct Selling) yang telah terdaftar di Asosiasi Perusahaan Penjualan Langsung
                            Indonesia <strong>AP2LI</strong> .
                            </p>
                            <p>
                            Didirikan pada Tahun 2022 berdasarkan Akta Notaris No. 7 Tanggal 17 Oktober
                            2022 oleh dan dihadapan Notaris Tn. DEDY ARDIANSYAH SYAMRUDDIN 
                            SH.MKn. Sebagai perusahaan Penjualan Langsung yang berpusat di Kota 
                            Makassar, Sulawesi Selatan.
                            </p> 
 
                            <div class="quote quote-primary mb-10">

                                <!-- Quote Text -->
                                <p>"PT. EXCEL GLOBAL NETWORK paham bahwa kesehatan dan keuangan sangatlah penting dan menjadi yang No. 1 dalam kehidupan manusia. Maka dari itu, PT. EXCEL GLOBAL NETWORK mempersembahkan produk-produk kesehatan terbaik dan berkualitas tinggi untuk masyarakat."
                                </p>

                                <!-- Quote Avatar -->
                                <div class="quote-avatar">
                                    <img src="{{asset('front/assets/img/favicon.png')}}" alt="quote-avatar">
                                </div>

                                <!-- Quote Author -->
                                <div class="quote-author">
                                    <h5 class="h5-xs bluestone-color">Mr. HERMAN SH.MH. </h5>
                                    <span class="grey-color">Presiden Direktur</span>
                                </div>

                            </div>

                            <p>
                            Produk telah terdaftar di Kementerian Kesehatan RI dan sudah mendapatkan No Registrasi dari BPOM RI serta sertifikat HALAL dari MUI.
                            </p>
                            <p>
                            PT. Excel Global Network memberikan peluang usaha bagi seluruh Mitranya untuk mendapatkan penghasilan dengan sistem bagi hasil yang menguntungkan Mitra Usaha.
                            </p>
                        </div>
                    </div> <!-- END ABOUT TEXT -->


                </div> <!-- End row -->
            </div> <!-- End container -->
        </section> 
        <!-- End ABOUT-1 -->



        <!-- VISION ============================================= -->
        <section id="vision" class="wide-60 services-section division">
            <div class="container">


                <!-- SECTION TITLE -->
                <div class="row">
                    <div class="col-lg-10 offset-lg-1 section-title wow fadeInUp" data-wow-delay="0.2s">

                        <!-- Title 	-->
                        <h3 class="h3-sm bluestone-color">VISI DAN MISI PT. EXCEL GLOBAL NETWORK </h3>

                        <!-- Text -->
                        <p class="p-lg">
                        PT. Excel Global Network memberikan peluang usaha bagi seluruh Mitranya untuk mendapatkan penghasilan dengan sistem bagi hasil yang menguntungkan Mitra Usaha.
                <!--     
                        Sebagai seorang yang produktif, kesehatan merupakan suatu aset yang berharga bagi dirinya.
                                Untuk itu sangat penting bagi kita untuk menjaganya agar tetap fit dan terhindar dari berbagai penyakit. -->
                        </p>

                    </div>
                </div>  
                
                <div class="row">
                    <div class="col-lg-10 offset-lg-1 section-title wow fadeInUp" data-wow-delay="0.2s">

                        <!-- Title 	-->
                        <h4 class="h3-sm bluestone-color">VISI PT. EXCEL GLOBAL NETWORK</h4>
                        <p class="p-lg">
                            <strong>
                                Menjadi Perusahaan Penjualan Langsung Terdepan dan Terpercaya
                            </strong> 
                        </p>
                    </div>
                </div>  

                <div class="row">
                    <div class="col-lg-10 offset-lg-1 section-title wow fadeInUp" data-wow-delay="0.2s">

                        <!-- Title 	-->
                        <h4 class="h3-sm bluestone-color">MISI PT. EXCEL GLOBAL NETWORK</h4>
 
                    </div>
                </div>  

                <div class="row">  
                    <div class="col-md-4">
                        <div class="sbox-3 icon-xl wow fadeInUp" data-wow-delay="0.4s">

                            <!-- Icon  -->
                            <img class="img-100 mb-25" src="{{asset('front/assets/images/icons/analytics-2.png')}}" alt="feature-icon" />
                            <br>
                            <!-- Text -->
                            <p class="grey-color">Mengembangkan jaringan bisnis dengan mengedepankan Kepercayaan, Profesionalitas dan Penuh Tanggung Jawab.
                            </p>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="sbox-3 icon-lg wow fadeInUp" data-wow-delay="0.8s">

                            <!-- Icon  -->
                            <img class="img-100 mb-25" src="{{asset('front/assets/images/icons/online-shop-1.png')}}" alt="feature-icon" />
 
                            <!-- Text -->
                            <p class="grey-color">Menyedikan Produk-produk terbaik dan Marketing Plan yang menguntungkan seluruh Mitra Bisnis.
                            </p>

                        </div>
                    </div>
 
                    <div class="col-md-4">
                        <div class="sbox-3 icon-lg wow fadeInUp" data-wow-delay="0.6s">

                            <!-- Icon  -->
                            <img class="img-100 mb-25" src="{{asset('front/assets/images/icons/investment-1.png')}}" alt="feature-icon" />
 
                            <!-- Text -->
                            <p class="grey-color">Menyediakan pendidikan bisnis untuk Mitra. Dengan metode Edukasi, Duplikasi, dan Leadership.
                            </p>

                        </div>
                    </div>
 
                

                </div> <!-- END SERVICE BOXES -->


            </div> <!-- End container -->
        </section> 
        <!-- VISION -->




        <!-- PRODUCT EXCEL C
        ============================================= -->	
        <section id="project-details-page" class="page-hero-section division" style="background: url({{asset('front/assets/img/all-product.png')}}) ;background-blend-mode: multiply;background-size: cover; box-shadow: inset 0 0 0 2000px rgba(0, 0, 0, 0.6)">
            <div class="container">	
                <div class="row">	


                    <!-- PAGE HERO TEXT -->
                    <div class="col-md-10 offset-md-1">
                        <div class="hero-txt text-center white-color">

                            <!-- Title -->
                            <h3 id="excelc" class="h3-xl">EXCEL - C SUPLEMENT ANTI OKSIDAN</h3>

                            <!-- Text -->
                            <p>Suplemen Antioksidan yang baik untuk kesehatan dalam kemasan serbuk untuk mencegah masalah yang berkaitan dengan imunitas tubuh</p>

                        </div>
                    </div>	<!-- END PAGE HERO TEXT -->


                </div>    <!-- End row --> 
            </div>	   <!-- End container --> 
        </section>	 
   
        <section id="sp-1" class="single-project-section sp-page sp-left division">				
            <div class="container">
                <div class="sp-wrapper bg-white">
                    <div class="row d-flex align-items-center">


                        <!-- PROJECT PREVIEW IMAGE -->
                        <div class="col-md-6">
                            <div class="sp-preview text-center">
                                <img class="img-fluid" src="{{asset('front/assets/img/excel-c.png')}}" alt="project-preview" />
                            </div>
                        </div>


                        <!-- PROJECT TEXT -->
                        <div class="col-md-6">
                            <div class="sp-txt mt-40">

                                <!-- Brand Logo -->
                                <!-- <div class="sp-logo"><img class="img-fluid" src="front/assets/img/logo.png')}}" alt="brand-logo" /></div> -->
                                
                                <!-- Text -->
                                <p>Minuman serbuk rasa Lemon banyak manfaat untuk kesehatan organ dalam tubuh serta menghindarkan dari virus yang berbahaya bagi tubuh.
                                </p>

                                <!-- Project Data -->
                                <div class="sp-data">

                                    <div class="row">
                                        <div class="col-md-12"><p>Komposisi :</p></div>
                                    </div>
                                    
                                    <div class="row d-flex align-items-left clearfix">
                                        Ekstrak Lemon, Ekstrak Madu, Ekstrak Mint, Ekstrak Jeruk Nipis, Ekstrak Stroberi, Maltodekstrin, Inulin, Isomalto Oligosakarida, Antioksidan Asam, Askorbat L-Sistein, Pemanis Alami Glikosida Steviol, Premiks Vitamin dan Mineral </div> 
                                </div> 
                                <div class="sp-data">

                                    <div class="row">
                                        <div class="col-md-12"><p>Manfaat :</p></div>
                                    </div>
                                    <div class="row d-flex align-items-left clearfix">
                                        Meningkatkan Imunitas Tubuh
                                        • Menjaga Kulit Tetap Kencang
                                        • Menurunkan Berat Badan
                                        • Mengontrol Tekanan Darah
                                        • Berfungsi Sebagai Anti Virus
                                        • Mencegah Penyakit Asam Urat
                                        • Mencegah Penyakit Kanker
                                        • Mencegah Penyakit Diabetes
                                        • Mencegah Penyakit Jantung
                                    </div> 
                                </div> 
 

                            </div>
                        </div>	<!-- END PROJECT TEXT -->


                    </div>    <!-- End row -->
                </div>
            </div>	   <!-- End container -->
        </section>	 
 
        <section id="content-10" class="wide-60 content-section division">
            <div class="container">
                <div class="row">	


                    <!-- TEXT BLOCK -->	
                    <div class="col-md-6">
                        <div class="txt-block pc-25 mb-40 wow fadeInLeft mx-20" data-wow-delay="0.4s">

                            <!-- Title -->	
                            <h4 class="h4-xs">Latar Belakang:</h4>

                            <!-- Text -->
                            <p>Radikal Bebas dapat menyerang Sel, DNA, Protein, dan Jaringan Tubuh.
                            </p> 

                            <!-- Text -->
                            <p>Jika Tubuh Kekurangan Antioksidan hingga konsentrasi Radikal Bebas lebih tinggi, dapat mengakibatkan Stress Oksidatif yang kemudian memicu penyakit, antara lain :
                            <ul class="txt-list mb-15">

                            <li> Mudah terserang Bakteri dan Virus </li> 
                            <li> Mudah Lelah dan Letih </li> 
                            <li> Katarak atau Penglihatan Buram </li> 
                            <li> Penyakit Parkinson dan Alzheimer </li> 
                            <li> Artritis atau Peradangan Sendi </li> 
                            <li> Penyakit Jantung Koroner </li> 
                            <li> Penyakit Kronis </li> 
                            <li> Penyakit Kanker </li> 
                            <li> Penyakit Diabetes </li> 
                            <li> Tekanan Darah Tinggi </li>
                            </ul>
                            </p> 
  
                        </div>
                    </div>	<!-- END TEXT BLOCK -->	


                    <!-- TEXT BLOCK -->	
                    <div class="col-md-6">
                        <div class="txt-block pc-25 mb-40 wow fadeInRight" data-wow-delay="0.4s">

                            <!-- Title -->	
                            <h4 class="h4-xs">Solusi:</h4>

                            <!-- Text -->
                            <p>
                                Penanganan radikal bebas yang paling penting adalah antioksidan.
                                Antioksidan merupakan zat yang mampu melindungi sel-sel tubuh Anda dari
                                efek buruk radikal bebas. 
                                <ul class="txt-list mb-15">
                                <li><strong>Lemon</strong>. memiliki kandungan antioksidan tinggi berupa vitamin C dan flavonoid yang mana meminum air lemon tersebut, berguna untuk memperkuat sistem daya tahan tubuh, mengatasi kerusakan sel tubuh, serta memaksimalkan penyerapan zat besi dalam tubuh.
                                </li>
                                <li><strong>Madu</strong>. Kandungan senyawa asam fenolat pada madu kerap berfungsi sebagai antioksidan alami bagi tubuh yang mampu menghambat aktivitas radikal bebas di dalam tubuh. </li>
                                <li><strong>Mint</strong>. Ekstrak Mint memberikan kulit dengan sejumlah nutrisi seperti Vitamin A dan C, kedua antioksidan kuat yang melawan radikal bebas dan peradangan. </li>
                                <li><strong>Jeruk Nipis</strong>. Kandungan vitamin C yang terdapat dalam jeruk nipis berkhasiat untuk menangkal radikal bebas. Selain itu vitamin C berkhasiat membentuk sel darah putih yang dapat membantu melindungi tubuh dari serangan penyakit dan infeksi. </li>
                                <li><strong>Stroberi</strong>. Kandungan vitamin C yang juga merupakan antioksidan dalam stroberi dapat melindungi sel-sel tubuh dari kerusakan akibat radikal bebas. </li>
                                </ul>
                            </p> 
 
                        </div>
                    </div>	<!-- END TEXT BLOCK -->	

                    
                </div>    <!-- End row -->
            </div>     <!-- End container -->
        </section>	 
        <!--END PRODUCT EXCEL C
        ============================================= -->	


        <!-- PRODUCT COLLSKIN
        ============================================= -->	
        <section id="project-details-page" class="page-hero-section division" style="background: url({{asset('front/assets/img/all-product.png')}}) ;background-blend-mode: multiply;background-size: cover; box-shadow: inset 0 0 0 2000px rgba(0, 0, 0, 0.6)">
            <div class="container">	
                <div class="row">	


                    <!-- PAGE HERO TEXT -->
                    <div class="col-md-10 offset-md-1">
                        <div class="hero-txt text-center white-color">

                            <!-- Title -->
                            <h3 id="collskin" class="h3-xl">COLLSKIN FISH COLLAGEN </h3>
                            <!-- <h3 class="h3-xl">COLLSKIN FISH COLLAGEN & FRUITS EXTRACT</h3> -->

                            <!-- Text -->
                            <p>Suplemen dengan Kolagen Rasa Stroberi yang baik untuk kesehatan kulit dan tulang dalam kemasan serbuk.</p> 

                        </div>
                    </div>	<!-- END PAGE HERO TEXT -->


                </div>    <!-- End row --> 
            </div>	   <!-- End container --> 
        </section>	 
   
        <section id="sp-1" class="single-project-section sp-page sp-left division">				
            <div class="container">
                <div class="sp-wrapper bg-white">
                    <div class="row d-flex align-items-center">


                        <!-- PROJECT PREVIEW IMAGE -->
                        <div class="col-md-6">
                            <div class="sp-preview text-center">
                                <img class="img-fluid" src="{{asset('front/assets/img/collskin.png')}}" alt="project-preview" />
                            </div>
                        </div>


                        <!-- PROJECT TEXT -->
                        <div class="col-md-6">
                            <div class="sp-txt mt-40">

                                <!-- Brand Logo -->
                                <!-- <div class="sp-logo"><img class="img-fluid" src="front/assets/img/logo.png')}}" alt="brand-logo" /></div> -->
                                
                                <!-- Text -->
                                <p >Minuman serbuk rasa Stroberi dengan kandungan kolagen yang bermanfaat untuk kesehatan kulit dan tulang Anda.
                                </p>

                                <!-- Project Data -->
                                <div class="sp-data">

                                    <div class="row">
                                        <div class="col-md-12"><p>Komposisi :</p></div>
                                    </div>
                                    
                                    <div class="row d-flex align-items-left clearfix">
                                    Ekstrak Stroberi (27,4%), Kolagen Ikan (22%),
                                    Ekstrak Ceri, Bubuk Bit Merah, Ekstrak Lemon,
                                    Ekstrak Anggur, Ekstrak Acaiberry, Ekstrak
                                    Lidah Buaya, Dekstrosa Monohidrat, L-Glutation,
                                    Ganggang Laut (Ganggang Hijau Biru),
                                    L-Sistein, Pemanis Alami Glikosida Steviol,
                                    Premiks Vitamin & Mineral</div> 
                                </div> 
                                <div class="sp-data">

                                    <div class="row">
                                        <div class="col-md-12"><p>Manfaat :</p></div>
                                    </div>
                                    <div class="row d-flex align-items-left clearfix">
                                        Meregenerasi Sel Dalam Tubuh
                                        Mengencangkan Kulit
                                        Mencegah Penuaan (Anti Aging)
                                        Menurunkan Berat Badan
                                        Mendukung Kesehatan Jantung
                                        Meningkatkan Massa Otot
                                        Mengurangi Selulit
                                        Meredakan Nyeri Sendi
                                        Memperbaiki Pencernaan
                                        Mencegah Rambut Rontok
                                    </div> 
                                </div> 
 

                            </div>
                        </div>	<!-- END PROJECT TEXT -->


                    </div>    <!-- End row -->
                </div>
            </div>	   <!-- End container -->
        </section>	 
 
        <section id="content-10" class="wide-60 content-section division">
            <div class="container">
                <div class="row">	


                    <!-- TEXT BLOCK -->	
                    <div class="col-md-6">
                        <div class="txt-block pc-25 mb-40 wow fadeInLeft" data-wow-delay="0.4s">

                            <!-- Title -->	
                            <h4 class="h4-xs">Latar Belakang:</h4>

                            <!-- Text -->
                            <p>Didalam tubuh kita, terdapat 60% - 70% kolagen yang tersebar di berbagai organ tubuh seperti kulit, otot, sendi, dan lainnya.
                            </p> 
                            <p>Kolagen sendiri adalah protein alami yang jumlahnya melimpah dalam tubuh serta berfungsi menjaga kesehatan berbagai organ tubuh kita.
                            </p> 

                            <!-- Text -->
                            <p>Sebaliknya, kekurangan kolagen juga dapat menyebabkan gangguan kesehatan tertentu., antara lain :
                            <ul class="txt-list mb-15">

                            <li> Rambut Rontok</li> 
                            <li> Nyeri Sendi</li> 
                            <li> Muncul Kerutan Pada Kulit</li> 
                            <li> Meningkatnya Sensitivitas Gigi</li> 
                            <li> Pencernaan Terganggu</li>  
                            </ul>
                            </p> 
  
                        </div>
                    </div>	<!-- END TEXT BLOCK -->	


                    <!-- TEXT BLOCK -->	
                    <div class="col-md-6">
                        <div class="txt-block pc-25 mb-40 wow fadeInRight" data-wow-delay="0.4s">

                            <!-- Title -->	
                            <h4 class="h4-xs">Solusi:</h4>

                            <!-- Text -->
                            <strong>RAHASIA KULIT TAMPAK LEBIH MUDA</strong>
                            <p> 
                                Fish Collagen & Fruit Extracts merupakan protein yang diekstrak dari ikan dan ekstrak buah.  
                            </p> 
                            <p>
                            Fish Kolagen banyak digunakan sebagai bahan suplemen. Jenis Kolagen ini merupakan tipe 1 yang memang terdapat dalam banyak kulit ikan. 
                            </p> 
                            <p>
                            Kolagen tipe 1 adalah jenis kolagen terbanyak dalam tubuh manusia dan 80% diantaranya terdapat dikulit.
                            </p>  
                            <p>
                            Jenis Kolagen dari ikan memiliki daya serap lebih bagus karena ukuran partikelnya lebih kecil dibandingkan kolagen dari hewan lain karena dapat diserap tubuh lebih efisien dan masuk ke peredaran darah lebih cepat, kolagen ikan adalah sumber kolagen lebih bagus.  
                            </p> 
 
                        </div>
                    </div>	<!-- END TEXT BLOCK -->	

                    
                </div>    <!-- End row -->
            </div>     <!-- End container -->
        </section>	 


        <!-- PRODUCT COLLSKIN

 


        <!-- CONTACTS-2
			============================================= -->
        <section id="contacts-2" class="bg-blue bg-map contacts-section division">
            <div class="container white-color">
                <div class="row center-item">
                    <div class="col-md-6  ">
                        <div class="contact-box icon-sm clearfix">
                            <img class="img-50" src="{{asset('front/assets/images/icons/placeholder-4.png')}}" alt="clock-icon" />
                            <div class="cbox-2-txt"> 
                                <h5 class="h5-lg">Lokasi :</h5>
                                <p>Jl. muhajirin 1 no 27 Komplek PU</p>
                                <p>Makassar sulawesi Selatan</p>  
                            </div>

                        </div>
                    </div>  
                    <div class="col-md-6  ">
                        <div class="contact-box icon-sm clearfix">
                            <img class="img-50" src="{{asset('front/assets/images/icons/link-1.png')}}" alt="clock-icon" />
                            <div class="cbox-2-txt"> 
                                <h5 class="h5-lg">Legalitas :</h5>
                                <p><strong>SK KEMENKUMHAM</strong> &emsp;: NOMOR AHU-0072088.AH.01.01.TAHUN 2022 </p>
                                <p><strong>NOMOR INDUK BERUSAHA (NIB)</strong> &emsp; : 2810220021147</p>
                                <p><strong>NPWP</strong> &emsp; : 61.353.210.0-804.000</p>
                                <p><strong>AP2LI</strong> &emsp; : 246/AP2LI/DN/XI/2022</p>  
                            </div>

                        </div>
                    </div>  
                </div>
            </div>
        </section>



 
        <footer id="footer-2" class="pt-100 footer division">
            <div class="container">
 
                <div class="row">
 
                    <div class="col-md-10 col-lg-4">
                        <div class="footer-info mb-40">

                            <img src="{{asset('front/assets/img/logo.png')}}" width="182" height="45" alt="footer-logo">
   
                            <p><strong>SK KEMENKUMHAM</strong> &emsp;: NOMOR AHU-0072088.AH.01.01.TAHUN 2022 <br>
                            <strong>NOMOR INDUK BERUSAHA (NIB)</strong> &emsp; : 2810220021147 <br>
                            <strong>NPWP</strong> &emsp; : 61.353.210.0-804.000 <br>
                            <strong>AP2LI</strong> &emsp; : 246/AP2LI/DN/XI/2022</p>
                        </div>
                    </div>

<!--  
                    <div class="col-md-3 col-lg-2 col-xl-2">
                        <div class="footer-links mb-40">
 
                            <h5 class="h5-sm bluestone-color">Quick Links</h5>
 
                            <ul class="foo-links clearfix">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Case Studies</a></li>
                                <li><a href="#">Lawyer SEO</a></li>
                                <li><a href="#">Testimonials</a></li>
                                <li><a href="#">From the Blog</a></li>
                            </ul>

                        </div>
                    </div>

 
                    <div class="col-md-4 col-lg-3 col-xl-3">
                        <div class="footer-links mb-40">
 
                            <h5 class="h5-sm bluestone-color">Featuprimary Services</h5>
 
                            <ul class="clearfix">
                                <li><a href="#">Local SEO</a></li>
                                <li><a href="#">Social Media Marketing</a></li>
                                <li><a href="#">Pay Per Click Management</a></li>
                                <li><a href="#">Search Engine Optimization</a></li>
                                <li><a href="#">Free SEO Analysis</a></li>
                            </ul>

                        </div>
                    </div>

 
                    <div class="col-md-5 col-lg-3 col-xl-3">
                        <div class="footer-form mb-20">
 
                            <h5 class="h5-sm bluestone-color">Follow the Best</h5>
 
                            <form class="newsletter-form">

                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="Email Address" required
                                        id="s-email">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn">
                                            <i class="far fa-arrow-alt-circle-right"></i>
                                        </button>
                                    </span>
                                </div>
 
                                <label for="s-email" class="form-notification"></label>

                            </form>

                        </div>
                    </div>   -->


                </div> 

 
                <div class="bottom-footer">
                    <div class="row">

 
                        <div class="col-lg-8">
                            <ul class="bottom-footer-list">
                                <li>
                                    <p>&copy; Copyright Excel 2022</p>
                                </li>
                                <!-- <li>
                                    <p><a href="tel:123456789">508.746.9892</a></p>
                                </li> -->
                                <li>
                                    <p class="last-li"><a href="mailto:admin@www.excel_gn.com">admin@excel_gn.com</a></p>
                                </li>
                            </ul>
                        </div>

 
                        <div class="col-lg-4 text-right">
                            <ul class="foo-socials text-center clearfix">

                                <li><a href="#" class="ico-facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="ico-twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="ico-google-plus"><i class="fab fa-google-plus-g"></i></a></li>
                                <!-- <li><a href="#" class="ico-tumblr"><i class="fab fa-tumblr"></i></a></li> -->
 

                            </ul>
                        </div>


                    </div>
                </div>  


            </div>  
        </footer>  




    </div>   
    <script src="{{asset('front/assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('front/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('front/assets/js/modernizr.custom.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.easing.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.appear.js')}}"></script>
    <script src="{{asset('front/assets/js/menu.js')}}"></script>
    <script src="{{asset('front/assets/js/materialize.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.scrollto.js')}}"></script>
    <script src="{{asset('front/assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('front/assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.flexslider.js')}}"></script>
    <script src="{{asset('front/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('front/assets/js/seo-form.js')}}"></script>
    <script src="{{asset('front/assets/js/contact-form.js')}}"></script>
    <script src="{{asset('front/assets/js/comment-form.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('front/assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('front/assets/js/wow.js')}}"></script>

    <!-- Custom Script -->
    <script src="{{asset('front/assets/js/custom.js')}}"></script>

    <script>
        // new WOW().init();
        var prevScrollpos = window.pageYOffset;
            window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("navbar").style.top = "0";
            } else {
                document.getElementById("navbar").style.top = "-50px";
            }
            prevScrollpos = currentScrollPos;
            } 
    </script> 
    <script src="{{asset('front/assets/js/changer.js')}}"></script>
    <script defer src="{{asset('front/assets/js/styleswitch.js')}}"></script> 
</body> 
</html>