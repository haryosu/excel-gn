@extends('member.layouts.app')
@section('title' , 'Member - Bonus Referral')

@php
    use App\Constants\Is_WD;
    use App\Constants\BonusType;
@endphp
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Bonus Generasi Basic</h1>
            </div>
        </div>
    </div>

    {{-- Table --}}
        <section class="scroll-section" id="breakpointSpecificResponsive">
            <div class="card mb-5">
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-bordered" style="margin: 0px">
                            <thead>
                            <tr class="table-active">
                                <th scope="col">TANGGAL</th>
                                <th scope="col">BONUS DIPEROLEH</th>
                                <th scope="col">BONUS NET</th>
                                <th scope="col">AUTOSAVE</th>
                                <th scope="col">STATUS</th>
                                <th scope="col">KETERANGAN</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($dummyData as $key => $item)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($item->created_at)->format('d M Y')}}</td>
                                    <td>Rp. {{number_format($item->bonus_val) }}</td>
                                    <td>Rp. {{number_format($item->bonus_net) }}</td>
                                    <td>Rp. {{number_format($item->bonus_autosave) }}</td>
                                    <td class="text-center align-middle">
                                        {!!Is_WD::labelHTML($item->is_wd)!!}
                                    </td>
                                    <td>{{$item->desc}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">Data Kosong</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    {{-- Table --}}

    <!-- Pagination -->
        {{ $dummyData->appends(request()->query())->links('member.layouts.partials.pagination-semibordered') }}
    <!-- Pagination -->
@endsection