@extends('member.layouts.app')
@section('title' , 'Member Dashboard')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Selamat Datang User</h1>
            </div>
        </div>
    </div>
    
    <div class="row g-2">
        <div class="col-12 col-sm-6 col-lg-6">
            <div class="row mb-2">
                <div class="col-12 col-lg-6">
                    <div class="card hover-scale-up cursor-pointer sh-19">
                        <div class="h-100 d-flex flex-column justify-content-between card-body align-items-center">
                            <div
                                class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center mb-2">
                                <i data-acorn-icon="user" class="text-white"></i>
                            </div>
                            <div class="heading text-center mb-0 d-flex align-items-center lh-1">Type Member</div>
                            <div class=" text-primary" style="text-transform: uppercase">{{$userAuth->pin->name}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="card hover-scale-up cursor-pointer sh-19">
                        <div class="h-100 d-flex flex-column justify-content-between card-body align-items-center">
                            <div
                                class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center mb-2">
                                <i data-acorn-icon="star" class="text-white"></i>
                            </div>
                            <div class="heading text-center mb-0 d-flex align-items-center lh-1">Peringkat</div>
                            <div class=" text-primary" style="text-transform: uppercase">{{$userAuth->managerial->name??'-'}}</div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Info Bonus --}}
                <div class="row mb-2">
                    <section class="scroll-section" id="breakpointSpecificResponsive">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="small-title">Info Bonus</h2>
                                <div class="table-responsive-sm mb-5">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">PLAN A</th>
                                                <th scope="col">PLAN B</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$tbpa}}</td>
                                                <td>{{$tbpb}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            {{-- Info Bonus --}}

            {{-- Summary Bonus --}}
                <div class="row mb-2">
                    <section class="scroll-section" id="breakpointSpecificResponsive">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="small-title">Summary Bonus</h2>
                                <div class="table-responsive-sm mb-5">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">DIPEROLEH</th>
                                                <th scope="col">DITRANSFER</th>
                                                <th scope="col">SALDO BONUS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$sumbonus}}</td>
                                                <td>{{$sumbonustf}}</td>
                                                <td>{{$sumbonusSaldo}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            {{-- Summary Bonus --}}

            {{-- Stok Pin --}}
                <div class="row mb-2">
                    <section class="scroll-section" id="breakpointSpecificResponsive">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="small-title">Stok Pin</h2>
                                <div class="table-responsive-sm mb-5">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                @foreach ($getAllPins_BV as $item)
                                                    <th scope="col" style="text-transform: uppercase">{{$item['name']}}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                @foreach ($getAllPins_BV as $item)
                                                <td>
                                                    {{$item['user_pin']}}
                                                </td>
                                                @endforeach
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            {{-- Stok Pin --}}
        </div>
        <div class="col-12 col-sm-6 col-lg-6">
            <div class="row mb-2">
                <section class="scroll-section" id="breakpointSpecificResponsive">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="small-title">Auto Save</h2>
                            <div class="table-responsive-sm mb-5">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">In</th>
                                            <th scope="col">Poin Reward</th>
                                            <th scope="col">Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$sumbonusSaldoAutosave??0}}</td>
                                            <td>{{$userAuth->pr}}</td>
                                            <td>{{$sumbonusSaldoAutosavein??0}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="row mb-2">
                <section class="scroll-section" id="breakpointSpecificResponsive">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="small-title">Poin Reward</h2>
                            <div class="table-responsive-sm mb-5">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">LEG 1</th>
                                            <th scope="col">LEG 2</th>
                                            <th scope="col">LEG 3</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$refdata1}}:{{$refdata2}}</td>
                                            <td>{{$refdata3}}:{{$refdata4}}</td>
                                            <td>{{$refdata5}}:{{$refdata6}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection     