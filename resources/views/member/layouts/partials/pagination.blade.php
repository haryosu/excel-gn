<div class="d-flex align-items-center bd-highlight">
    <div class="p-2 flex-grow-1 bd-highlight">
        <div class="table-numb">
            Menampilkan {{ $paginator->count() ?? 0 }} data
        </div>
    </div>
    <div class="p-2 bd-highlight">
        <ul class="pagination">
            {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <li class="page-item disabled" aria-disabled="true">
                        <a class="page-link shadow" aria-hidden="true"><i data-acorn-icon="chevron-left"></i></a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i data-acorn-icon="chevron-left"></i></a>
                    </li>
                @endif
            {{-- Previous Page Link --}}


            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><a class="page-link shadow">{{ $element }}</a></li>
                @endif
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><a class="page-link shadow">{{ $page }}</a></li>
                        @else
                            <li class="page-item"><a class="page-link shadow" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next"><i data-acorn-icon="chevron-right"></i></a>
                    </li>
                @else
                    <li class="page-item disabled" aria-disabled="true">
                        <a class="page-link" aria-hidden="true"><i data-acorn-icon="chevron-right"></i></a>
                    </li>
                @endif
            {{-- Next Page Link --}}
        </ul>
    </div>
  </div>