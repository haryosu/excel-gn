<div class="col-auto d-none d-lg-flex">
  <ul class="sw-25 side-menu mb-0 primary" id="menuSide">
    <li>
      <a href="{{route('member.dashboard')}}">
        <i data-acorn-icon="dashboard-1" class="icon" data-acorn-size="18"></i>
        <span class="label">Dashboard</span>
      </a>
    </li>
    
    {{-- Profile --}}
      <li>
        <a href="#" data-bs-target="#profile">
          <i data-acorn-icon="user" class="icon" data-acorn-size="18"></i>
          <span class="label">Profile</span>
        </a>
        <ul>
          <li>
            <a href="{{route('member.profile.change_profile.edit')}}">
              <i data-acorn-icon="user" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Edit Profile</span>
            </a>
          </li>
          <li>
            <a href="{{route('member.profile.change_password.edit')}}">
              <i data-acorn-icon="key" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Ubah Password</span>
            </a>
          </li>
          <li>
            <a href="{{route('member.profile.bank_info.index')}}">
              <i data-acorn-icon="info-circle" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Bank Info</span>
            </a>
          </li>
        </ul>
      </li>
    {{-- Profile --}}

    {{-- Network --}}
      <li>
        <a href="#" data-bs-target="#network">
          <i data-acorn-icon="diagram-1" class="icon" data-acorn-size="18"></i>
          <span class="label">Network</span>
        </a>
        <ul>
          <li>
            <a href="{{route('member.network.geneology.index')}}">
              <i data-acorn-icon="share" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Geneology</span>
            </a>
          </li>
          <li>
            <a href="{{route('member.network.sponsor.index')}}">
              <i data-acorn-icon="money-bag" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Data Sponsorisasi</span>
            </a>
          </li>
          <li>
            <a href="{{route('member.network.data_downline.index')}}">
              <i data-acorn-icon="diagram-1" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Data Downline</span>
            </a>
          </li>
        </ul>
      </li>
    {{-- Network --}}

    {{-- Bonus --}}
      <li>
        <a href="#" data-bs-target="#bonus">
          <i data-acorn-icon="dollar" class="icon" data-acorn-size="18"></i>
          <span class="label">Bonus</span>
        </a>
        <ul>
          {{-- Plan A --}}
            <li>
              <a href="#" data-bs-target="#plan-a" >
                <i data-acorn-icon="minus" class="icon" data-acorn-size="18"></i>
                <span class="label">Plan A</span>
              </a>
              <ul>
                <li>
                  <a href="{{route('member.bonus.plan_a.referral.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Referral</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_a.couple.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Couple</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_a.exceed.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Exceed</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_a.generation.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Generasi</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_a.couple_rank.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Couple Ranking</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_a.profit_sharing.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Profit Sharing</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_a.re-shopping-basic.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Generasi Basic</span>
                  </a>
                </li>
              </ul>
            </li>
          {{-- Plan A --}}
          
          {{-- Plan B --}}
            <li>
              <a href="#" data-bs-target="#plan-b">
                <i data-acorn-icon="minus" class="icon" data-acorn-size="18"></i>
                <span class="label">Plan B</span>
              </a>
              <ul>
                <li>
                  <a href="{{route('member.bonus.plan_b.reshopping.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Reshopping</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_b.leadership.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Leadership</span>
                  </a>
                </li>
                <li>
                  <a href="{{route('member.bonus.plan_b.royalty_rank.index')}}">
                    <i data-acorn-icon="minus" class="icon d-none" data-acorn-size="18"></i>
                    <span class="label">Bonus Royalty Ranking</span>
                  </a>
                </li>
              </ul>
            </li>
          {{-- Plan B --}}
        </ul>
      </li>
    {{-- Bonus --}}
    
    {{-- Pin --}}
      <li>
        <a href="#" data-bs-target="#pin">
          <i data-acorn-icon="pin" class="icon" data-acorn-size="18"></i>
          <span class="label">Pin</span>
        </a>
        <ul>
          <li>
            <a href="{{route('member.pin.order.index')}}">
              <i data-acorn-icon="cart" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Order Pin</span>
            </a>
          </li>
          <li>
            <a href="{{route('member.pin.transfer.index')}}">
              <i data-acorn-icon="send" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Transfer Pin</span>
            </a>
          </li>
          <li>
            <a href="{{route('member.pin.history.index')}}">
              <i data-acorn-icon="bookmark" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">History</span>
            </a>
          </li>
        </ul>
      </li>
    {{-- Pin --}}
    @if (Auth::user()->pin_id == 1)

    {{-- Reshopping Basic --}}
    <li>
      <a href="#" data-bs-target="#reshopping-basic">
        <i data-acorn-icon="arrow-top" class="icon" data-acorn-size="18"></i>
        <span class="label">Reshopping Basic</span>
      </a>
      <ul>
   
        <li>
          <a href="{{route('member.re-shopping-basic.order.index')}}">
            <i data-acorn-icon="startup" class="icon" data-acorn-size="18"></i>
            <span class="label">Re-shopping Basic</span>
          </a>
        </li> 
        <li>
          <a href="{{route('member.re-shopping-basic.history.index')}}">
            <i data-acorn-icon="bookmark" class="icon d-none" data-acorn-size="18"></i>
            <span class="label">History</span>
          </a>
        </li>
      </ul>
    </li>
    {{-- Reshopping Basic --}} 
    {{-- @else --}}
        
    @endif
    
    {{-- Top Up --}}
      <li>
        <a href="#" data-bs-target="#top_up">
          <i data-acorn-icon="arrow-top" class="icon" data-acorn-size="18"></i>
          <span class="label">Top Up</span>
        </a>
        <ul>
          <li>
            <a href="{{route('member.top_up.order.index')}}">
              <i data-acorn-icon="cart" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">Top Up</span>
            </a>
          </li>
          <li>
            <a href="{{route('member.top_up.history.index')}}">
              <i data-acorn-icon="bookmark" class="icon d-none" data-acorn-size="18"></i>
              <span class="label">History Top Up</span>
            </a>
          </li>
        </ul>
      </li>
    {{-- Top Up --}}
 
    <li>
      <a href="{{route('member.upgrade.index')}}">
        <i data-acorn-icon="startup" class="icon" data-acorn-size="18"></i>
        <span class="label">Upgrade</span>
      </a>
    </li>
    {{-- Logout --}}
      <li>
        <a href="{{ route('user.logout') }}">
          <i data-acorn-icon="logout" class="icon" data-acorn-size="18"></i>
          <span class="label">Logout</span>
        </a>
      </li>
    {{-- Logout --}}

  </ul>
</div>
