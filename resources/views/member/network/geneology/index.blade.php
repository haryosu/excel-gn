@extends('member.layouts.app')
@section('title' , 'Member - Geneology')

 
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Geneology</h1>
            </div>
        </div>
    </div>

    <div class="row">   
        <div class="card mt-3">
            <div class="card-body">
                @php
                    echo view('adminv2.users.tree_new_form_register', ['userPin'=>$userPin,'ref' => $userRef, 'province' => $area]);
                    echo view('adminv2.users.tree_new', ['treeNew' => $treeNew, 'ref' => $userRef]);
                @endphp
            </div>
        </div>

        <div class="modal fade  user-details-modal-area" id="exampleModalCenter" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog  modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">@lang('User Details')</h5>
                        {{-- <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> --}}
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                    </div>
                    <div class="modal-body">
                        <div class="user-details-modal">
                            <div class="user-details-header ">
                                <div class="thumb"><img class="w-h-100-p tree_image" src="#" alt="*"></div>
                                <div class="content">
                                    <a class="user-name tree_url tree_name" href=""></a>
                                    <span class="user-status tree_status"></span>
                                    <span class="user-status tree_plan"></span>
                                </div>
                            </div>
                            <div class="user-details-body text-center">
                                <h6 class="my-3">@lang('Referred By'): <span class="tree_ref"></span></h6>
                                <table class="table border-none">
                                    <tr>
                                        {{-- <th>&nbsp;</th> --}}
                                        <th colspan="2">@lang('Leg 1')</th>
                                        <th colspan="2">@lang('Leg 2')</th>
                                        <th colspan="2">@lang('Leg 3')</th>
                                    </tr>
                                    <tr>
                                        {{-- <th>&nbsp;</th> --}}
                                        <th>@lang('LEFT')</th>
                                        <th>@lang('RIGHT')</th>
                                        <th>@lang('LEFT')</th>
                                        <th>@lang('RIGHT')</th>
                                        <th>@lang('LEFT')</th>
                                        <th>@lang('RIGHT')</th>
                                    </tr>

                                    <tr>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>

                                    {{-- <tr>
                                    <td>@lang('Current BV')</td>
                                    <td><span class="lbv"></span></td>
                                    <td><span class="rbv"></span></td>
                                </tr>
                                    <tr>
                                    <td>@lang('Free Member')</td>
                                    <td><span class="lfree"></span></td>
                                    <td><span class="rfree"></span></td>
                                </tr>
                                    <tr>
                                    <td>@lang('Point Reward')</td>
                                    <td><span class="lpaid"></span></td>
                                    <td><span class="rpaid"></span></td>
                                </tr> --}}
                                </table>

                                <br>

                                <table class="table col-lg-6">
                                    <thead>
                                        <th>
                                            <strong>Kiri</strong>
                                        </th>
                                        <th>
                                            <strong>Kanan</strong>
                                        </th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <strong>Total Anggota</strong>
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Anggota Hari ini</strong>
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Total Belanja</strong>
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Repeat Order Hari Ini</strong>
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <strong>Point Reward</strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                - BRONZE
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                - SILVER
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                - GOLD
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                - DIAMOND
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <strong>Total Klain Reward</strong>
                                            </td>
                                            <td>
                                                0
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

<script src="{{asset('assets/member/js/cs/scrollspy.js')}}"></script>
<script src="{{asset('assets/member/js/vendor/select2.full.min.js')}}"></script>
<script src="{{asset('assets/member/js/forms/controls.select2.js')}}"></script>

<script>
    'use strict';
    (function($) {
        $('.showDetails').on('click', function() {
            var modal = $('#exampleModalCenter');
            $('.tree_name').text($(this).data('name'));
            $('.tree_url').attr({
                "href": $(this).data('treeurl')
            });
            $('.tree_status').text($(this).data('status'));
            $('.tree_plan').text($(this).data('plan'));
            $('.tree_image').attr({
                "src": $(this).data('image')
            });
            $('.user-details-header').removeClass('Paid');
            $('.user-details-header').removeClass('Free');
            $('.user-details-header').addClass($(this).data('status'));
            $('.tree_ref').text($(this).data('refby'));
            $('.lbv').text($(this).data('lbv'));
            $('.rbv').text($(this).data('rbv'));
            $('.lpaid').text($(this).data('lpaid'));
            $('.rpaid').text($(this).data('rpaid'));
            $('.lfree').text($(this).data('lfree'));
            $('.rfree').text($(this).data('rfree'));
            $('#exampleModalCenter').modal('show');
        });

        $(document).ready(function() {
            $("#form-registers-act").submit(function(e) {
                e.preventDefault();

                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: $(this).serialize(),
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data)

                        if (data.code == 200) {
                            $("#form-registers-act")[0].reset()
                            $('#formModelRegister').modal('hide')
                            iziToast.success({
                                message: data.message,
                                position: "topRight",
                                onClosed: function() {
                                    window.location.reload()
                                }
                            });
                        } else {
                            console.log(data)

                            if (data.message) { 
                                $.each(data.message, function(e, f) {
                                    iziToast.warning({
                                        message: f,
                                        position: "topRight"
                                    });
                                })
                            }

                        }
                    },
                    error: function(data) {
                        console.log(data)
                        // console.error();
                    }
                    // console.error();
                });

            });
        })
    })(jQuery)
</script>
@endpush
@push('breadcrumb-plugins')
    <form action="{{ route('admin.users.other.tree.search') }}" method="GET" class="form-inline float-right bg--white">
        <div class="input-group has_append">
            <input type="text" name="username" class="form-control" placeholder="@lang('Search by username')">
            <div class="input-group-append">
                <button class="btn btn--success" type="submit"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
@endpush
