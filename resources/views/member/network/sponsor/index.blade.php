@extends('member.layouts.app')
@section('title' , 'Member - Sponsor')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-container">
            <h1 class="mb-0 pb-0 display-4" id="title">Data Sponsor</h1>
        </div>
    </div>
</div>

{{-- Table --}}
    <section class="scroll-section" id="breakpointSpecificResponsive">
        <div class="card mb-5">
            <div class="card-body">
                <div class="table-responsive-sm">
                    <table class="table table-bordered" style="margin: 0px">
                        <thead>
                        <tr class="table-active">
                            <th scope="col">Username</th>
                            <th scope="col">Plan</th>
                            <th scope="col">PR</th>
                            {{-- <th scope="col"></th>
                            <th scope="col">AUTOSAVE</th> --}}
                            <th scope="col">Join At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($dataSponsor as $key => $item)
                            <tr>
                                <td>{{($item->firstname) }} {{($item->lastname) }} - {{($item->username) }}</td>
                                <td>{{($item->plan->name) }}</td>
                                <td>{{($item->pr)??0 }}</td>
                                <td>{{\Carbon\Carbon::parse($item->created_at)->format('d M Y')}}</td>
                                {{-- <td>{{\Carbon\Carbon::parse($item->created_at)->format('d M Y')}}</td>
                                <td>Rp. {{number_format($item->bonus_net) }}</td>
                                <td>Rp. {{number_format($item->bonus_autosave) }}</td>
                                <td>{{$item->desc}}</td> --}}
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">Data Kosong</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
{{-- Table --}}

<!-- Pagination -->
    {{ $dataSponsor->appends(request()->query())->links('member.layouts.partials.pagination-semibordered') }}
<!-- Pagination -->
 
@endsection