@extends('member.layouts.app')
@section('title' , 'Member - Transfer Pin')

@push('style')
    <link rel="stylesheet" href="{{asset('assets/member/css/vendor/baguetteBox.min.css')}}" />
    {{-- SELECT2 --}}
        <link rel="stylesheet" href="{{asset('assets/member/css/vendor/select2.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/member/css/vendor/select2-bootstrap4.min.css')}}" />
    {{-- SELECT2 --}}
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Transfer Pin</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-6 mb-5 order-1 order-lg-0">
            @forelse ($getAllPins_BV as $item)
            <div class="row g-0">
                <div class="col mb-2">
                    <div class="row">
                        <!-- Buttons Start -->
                        <div class="col">
                            <div class="card mb-2">
                                <div class="row g-0 sh-14 sh-md-10">
                                    
                                    <div class="col-auto position-relative h-100">
                                        @if ($item['user_pin'] != null)
                                        <span class="badge rounded-pill bg-success me-1 position-absolute e-n3 t-2">
                                            {{$item['user_pin']}} Stock Pin
                                        </span>
                                        @else
                                        <span class="badge rounded-pill bg-danger me-1 position-absolute e-n3 t-2">
                                            Belum dibeli
                                        </span>
                                        @endif
                                        <img src="{{ getImage('assets/images/products/pins'.'/'. $item['images'],'350x300') }}" alt="alternate text"
                                            class="card-img card-img-horizontal sw-11" />
                                    </div>
                                    <div class="col">
                                        <div class="card-body pt-0 pb-0 h-100">
                                            <div class="row g-0 h-100 align-content-center">
                                                <div class="col-12 col-md-7 d-flex flex-column mb-2 mb-md-0">
                                                    <div>{{$item['name']}}</div>
                                                    <div class="text-small text-muted text-truncate">{{$item['description']}}</div>
                                                </div>
                                                <div class="col-12 col-md-5 d-flex align-items-center justify-content-md-end">
                                                        <button type="button" class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1 modalUser"
                                                            data-bs-toggle="modal" data-id="{{$item['id']}}" data-bs-target="#verticallyCenteredExample">
                                                            <i data-acorn-icon="send" data-acorn-size="15"></i>
                                                            <span class="d-xxl-inline-block">Transfer Pin {{$item['name']}} ke ..</span>
                                                        </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Buttons End -->
                    </div>
                </div>
            </div>
            @empty

            @endforelse
        </div>
        <div class="col-12 col-lg-6 mb-5 order-0 order-lg-1">
            <div class="row">
                <div class="card mb-3">
                    <div class="card-body mb-n5">
                      <div class="d-flex align-items-center flex-column">
                        <div class="mb-5 d-flex align-items-center flex-column">
                          <div class="sw-13 position-relative mb-3">
                            <img src="{{ getImage('assets/images/products/pins'.'/'. $userAuth->pin->images,'350x300') }}" class="img-fluid rounded-xl" alt="thumb" />
                          </div>
                          <div class="h5 mb-0"><strong>{{$userAuth->pin->name}}</strong></div>
                          <div class="text-muted">{{$userAuth->username}}</div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="row">
                <div class="card mb-3">
                    <div class="card-body mb-n5">
                      <div class="row mb-5">
                          <div class=" mb-1 text-center justify-content-center">
                              <h5>Stock Pin</h5>
                          </div>
      
                          @foreach ($getAllPins_BV as $item)
                              <div class="col-6 col-sm-3 text-center d-flex justify-content-center align-items-center flex-column mt-2 mb-4 mb-sm-0">
                                  <div class="bg-gradient-light sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center mb-3">
                                      <img src="{{ getImage('assets/images/products/pins'.'/'. $item['images'],'350x300') }}" class="card-img rounded-xl sh-6 sw-6" alt="thumb">
                                  </div>
                                  <div class="text-muted mb-1">Stock {{$item['name']}}</div>
                                  <div class="cta-2 text-primary">
                                    @if ($item['user_pin'] != null)
                                        {{$item['user_pin']}}
                                    @else
                                        -
                                    @endif
                                  </div>
                              </div>
                          @endforeach
                      </div>
                    </div>
                  </div>
            </div>
          </div>
    </div>

    
    {{-- Modal Upload Bukti Transfer --}}
        <div
        class="modal fade modal-close-out"
        id="verticallyCenteredExample"
        role="dialog"
        aria-labelledby="verticallyCenteredLabel"
        aria-hidden="true"
        >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="verticallyCenteredLabel">Transfer Pin</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('member.pin.transfer.transfer_pin_user',) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
            <div class="modal-body">
                        <input type="hidden" class="pin" name="pin_id">
                        <div class="col-12">
                            <label class="form-label">Jumlah</label>
                           <input type="text" class="form-control" id="maskNumber" name="qty">
                        </div>
                        <div class="col-12">
                            <label class="form-label">User</label>
                            <select id="select2Basic" name="user_id" data-placeholder="Pilih User">
                                <option value=""></option>
                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button onclick="return confirm('Apakah anda yakin untuk melakukan transfer ke user tujuan yang anda pilih ?. Aktifitas ini akan mengurangi jumlah stock pin anda, sesuai dengan pin yang anda pilih');" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            </div>
        </div>
        </div>
    {{-- Modal Upload Bukti Transfer --}}
@endsection

@push('script')
    <script src="{{asset('assets/member/js/plugins/lightbox.js')}}"></script>
    <script src="{{asset('assets/member/js/vendor/baguetteBox.min.js')}}"></script>

    {{-- SELECT2 --}}
        <script src="{{asset('assets/member/js/vendor/select2.full.min.js')}}"></script>
    {{-- SELECT2 --}}

    <script>
        // GET ID PIN
            $(document).on("click", ".modalUser", function () {
                var pinId = $(this).data('id');
                $(".pin").val( pinId );
            });
        // GET ID PIN

        // SELECT2 USER
            class Select2Controls {
                constructor() {
                    $('#select2Basic').select2({
                        placeholder: 'Pilih user',
                        ajax: {
                            url: "{{ route('member.json.get_json_user') }}",
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search_name: params.term,
                                    page: params.page || 1
                                }
                                return query
                            },
                            delay: 250,
                            processResults: function (data) {
                                return {
                                    results: data.data,
                                    pagination: {
                                        more: (data.current_page == data.last_page) ? null : data
                                            .current_page + 1
                                    }
                                }
                            },
                            cache: true
                        }
                    });
                }
            }
        // SELECT2 USER
    </script>
@endpush