@extends('member.layouts.app')
@section('title' , 'Member - Edit Profile')

@push('style')
<style>
    .disableForm {
        background: none !important;
    }
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-container">
            <h1 class="mb-0 pb-0 display-4" id="title">Edit Profile</h1>
        </div>
    </div>
</div>

<div class="row g-2">
    <form class="tooltip-end-top" action="{{route('member.profile.change_profile.update')}}" id="personalForm"
        method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="card mb-5">
            <div class="card-body">
                <div class="row g-3">
                    <div class="col-md-6">
                        <label class=" mb-3 top-label">
                            <label for="lastname" class="col-form-label disableForm">@lang('Username'):</label>
                            <input type="text" class="form-control form--control" 
                                value="{{$user->username}}" readonly>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label class=" mb-3 top-label">
                            <label for="lastname" class="col-form-label">@lang('NIK'):</label>
                            <input type="text" class="form-control form--control" name="nik"
                                value="{{$user->nik}}">
                        </label>
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-md-6">
                        <label class=" top-label">
                            <label for="InputFirstname" class="col-form-label">@lang('First Name'):</label>
                            <input type="text" class="form-control form--control" id="InputFirstname" name="firstname"
                                 value="{{$user->firstname}}" minlength="3">
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-3 top-label">
                            <label for="lastname" class="col-form-label">@lang('Last Name'):</label>
                            <input type="text" class="form-control form--control" id="lastname" name="lastname"
                                 value="{{$user->lastname}}" required>
                        </label>
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-md-6">
                        <label class=" top-label">
                            <label for="phone" class="col-form-label disableForm">@lang('Mobile Number')</label>
                            <input class="form-control form--control" id="phone" value="{{$user->mobile}}" readonly>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-3 top-label">
                            <label for="email" class="col-form-label disableForm">@lang('E-mail Address'):</label>
                            <input class="form-control form--control" id="email" 
                                value="{{$user->email}}" readonly>
                        </label>
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-md-6">
                        <label class=" top-label">
                            <label for="address" class="col-form-label">@lang('Address'):</label>
                            <input type="text" class="form-control form--control" id="address" name="address"
                                 value="{{@$user->address->address}}" required="">
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-3 top-label">
                            <label for="state" class="col-form-label">@lang('State'):</label>
                            <input type="text" class="form-control form--control" id="state" name="state"
                                 value="{{@$user->address->state}}" required="">
                        </label>
                    </div>
                </div>

                <div class="row g-3">
                    <div class="col-md-4">
                        <label class=" top-label">
                            <label for="zip" class="col-form-label">@lang('Zip Code'):</label>
                            <input type="text" class="form-control form--control" id="zip" name="zip"
                                 value="{{@$user->address->zip}}" required="">
                        </label>
                    </div>
                    <div class="col-md-4">
                        <label class="mb-3 top-label">
                            <label for="city" class="col-form-label">@lang('City'):</label>
                            <input type="text" class="form-control form--control" id="city" name="city"
                                 value="{{@$user->address->city}}" required="">
                        </label>
                    </div>
                    <div class="col-md-4">
                        <label class="mb-3 top-label">
                            <label class="col-form-label disableForm">@lang('Country'):</label>
                            <input class="form-control form--control" value="{{@$user->address->country}}" disabled>
                        </label>
                    </div>
                </div>

                <div class="row g-3">
                    {{-- Foto Profile --}}
                    <div class="col-md-6">
                        <div class="text-center">
                          <label class="col-form-label disableForm">@lang('Profile User')</label>
                        </div>
                        <div class="text-center">
                            <img class="img-fluid rounded mb-3"
                                src="{{ getImage(imagePath()['profile']['user']['path'].'/'. $user->image,imagePath()['profile']['user']['size']) }}"
                                alt="@lang('Image')">
                        </div>
                        <div class="input-group mb-3">
                            <input type="file" class="form-control" id="inputGroupFile02" name="image" />
                            <label class="input-group-text" for="inputGroupFile02">@lang('Image')</label>
                        </div>
                        <code>@lang('Image size') {{imagePath()['profile']['user']['size']}}</code>
                    </div>
                    {{-- Foto Profile --}}

                    {{-- Foto KTP --}}
                    <div class="col-md-6">
                        <div class="text-center">
                          <label class="col-form-label disableForm">@lang('Foto KTP')</label>
                        </div>
                        <div class="text-center">
                            <img class="img-fluid rounded mb-3"
                                src="{{ getImage(imagePath()['profile']['user']['path'].'/'. $user->img_nik,imagePath()['profile']['user']['size']) }}"
                                alt="@lang('Image KTP')">
                        </div>
                        <div class="input-group mb-3">
                            <input type="file" class="form-control" id="inputGroupFile02" name="img_nik" />
                            <label class="input-group-text" for="inputGroupFile02">@lang('Image KTP')</label>
                        </div>
                        <code>@lang('Image size') {{imagePath()['profile']['user']['size']}}</code>
                    </div>
                    {{-- Foto KTP --}}
                </div>

                <div class="card-footer border-0 pt-0 d-flex justify-content-end align-items-center">
                    <div>
                        <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                            <span>@lang('Save')</span>
                            <i data-acorn-icon="chevron-right"></i>
                        </button>
                    </div>
                </div>
            </div>
    </form>
</div>
@endsection