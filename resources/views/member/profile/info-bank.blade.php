@extends('member.layouts.app')
@section('title' , 'Member - Informasi Data Bank')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Informasi Data Bank</h1>
            </div>
        </div>
    </div>

    <div class="row g-2">
        <div class="col-12 col-lg-4">
            <div class="card hover-scale-up cursor-pointer sh-19">
                <div class="h-100 d-flex flex-column justify-content-between card-body align-items-center">
                    <div
                        class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center mb-2">
                        <i data-acorn-icon="credit-card" class="text-white"></i>
                    </div>
                    <div class="heading text-center mb-0 d-flex align-items-center lh-1">Bank</div>
                    <div class=" text-primary">{{$user->bank_provider??"--"}}</div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card hover-scale-up cursor-pointer sh-19">
                <div class="h-100 d-flex flex-column justify-content-between card-body align-items-center">
                    <div
                        class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center mb-2">
                        <i data-acorn-icon="user" class="text-white"></i>
                    </div>
                    <div class="heading text-center mb-0 d-flex align-items-center lh-1">Nama Pemilik</div>
                    <div class=" text-primary">{{$user->bank_account??"--"}}</div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card hover-scale-up cursor-pointer sh-19">
                <div class="h-100 d-flex flex-column justify-content-between card-body align-items-center">
                    <div class="bg-gradient-light sh-5 sw-5 rounded-xl d-flex justify-content-center align-items-center mb-2">
                        <span class="text-white">123</span>
                    </div>
                    <div class="heading text-center mb-0 d-flex align-items-center lh-1">Nomer Rekening</div>
                    <div class=" text-primary">{{$user->bank_number??"--"}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection