@extends('member.layouts.app')
@section('title' , 'Member - Ubah Password')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Ubah Password</h1>
            </div>
        </div>
    </div>

    <div class="row g-2">
            <form class="card mb-5 tooltip-end-top" action="{{route('member.profile.change_password.update')}}" id="personalForm" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="card-body">
                <div class="mb-3 filled">
                    <i data-acorn-icon="lock-off"></i>
                    <input class="form-control form--control form-control form--control-lg" type="password" name="current_password" placeholder="Password Lama" required minlength="5">
                </div>
                <div class="mb-3 filled">
                    <i data-acorn-icon="lock-off"></i>
                    <input class="form-control form--control form-control form--control-lg" type="password" name="password" placeholder="Password Baru" required minlength="5">
                @if($general->secure_password)
                    <div class="input-popup">
                        <p class="error lower">@lang('1 small letter minimum')</p>
                        <p class="error capital">@lang('1 capital letter minimum')</p>
                        <p class="error number">@lang('1 number minimum')</p>
                        <p class="error special">@lang('1 special character minimum')</p>
                        <p class="error minimum">@lang('6 character password')</p>
                    </div>
                @endif
                </div>
                <div class="mb-3 filled">
                    <i data-acorn-icon="lock-off"></i>
                    <input class="form-control form--control form-control form--control-lg" placeholder="Konfirmasi Password Baru" type="password" name="password_confirmation" required minlength="5">
                </div>
            </div>
            <div class="card-footer border-0 pt-0 d-flex justify-content-end align-items-center">
              <div>
                <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                  <span>@lang('Save')</span>
                  <i data-acorn-icon="chevron-right"></i>
                </button>
              </div>
            </div>
          </form>
    </div>
@endsection

@push('script-lib')
<script src="{{ asset('assets/global/js/secure_password.js') }}"></script>
@endpush

@push('script')
    <script>
        (function ($) {
            "use strict";
            @if($general->secure_password)
                $('input[name=password]').on('input',function(){
                    secure_password($(this));
                });
            @endif
        })(jQuery);
    </script>
@endpush