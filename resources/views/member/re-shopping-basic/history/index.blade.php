@extends('member.layouts.app')
@section('title' , 'Member - History')

@php
    use App\Constants\OrderStatus;
@endphp
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">History</h1>
            </div>
        </div>
    </div>

    <!-- Filter -->
        <div class="row mb-2">
            <form action="">
                <!-- Search Start -->
                <div class="col-sm-12 col-md-5 col-lg-3 col-xxl-2 mb-1">
                    <div
                        class="d-inline-block float-md-start me-1 mb-1 search-input-container w-100 shadow bg-foreground">
                        <input class="form-control" placeholder="Search Invoice Number" name='search'
                            value="{{ Request::get('search') }}" />
                        <span class="search-magnifier-icon">
                            <a onclick="this.closest('form').submit();return false;"><i
                                    data-acorn-icon="search"></i></a>
                        </span>
                        <span class="search-delete-icon d-none">
                            <i data-acorn-icon="close"></i>
                        </span>
                    </div>
                </div>
                <!-- Search End -->
            </form>
        </div>
    <!-- Filter -->

    <!-- Order List -->
        <div class="row">
            <div class="col-12 mb-5">
                <div class="card mb-2 bg-transparent no-shadow d-none d-md-block">
                    <div class="card-body pt-0 pb-0 sh-3">
                        <div class="row g-0 h-100 align-content-center">
                            <div class="col-md-2 d-flex align-items-center mb-2 mb-md-0 text-muted text-small">Invoice Number</div>
                            <div class="col-md-3 d-flex align-items-center text-muted text-small">NAME</div>
                            <div class="col-md-2 d-flex align-items-center text-muted text-small">PRICE</div>
                            <div class="col-md-2 d-flex align-items-center text-muted text-small">DATE</div>
                            <div class="col-md-2 d-flex align-items-center text-muted text-small">STATUS</div>
                        </div>
                    </div>
                </div>
                @forelse ($getOrderPinHistory as $item)
                    <div id="checkboxTable">
                        <div class="card mb-2">
                            <div class="card-body pt-0 pb-0 sh-21 sh-md-8">
                                <div class="row g-0 h-100 align-content-center">
                                    <div
                                        class="col-6 col-md-2 d-flex flex-column justify-content-center mb-2 mb-md-0 order-1 order-md-1 h-md-100 position-relative">
                                        <div class="text-muted text-small d-md-none">Invoice Number</div>
                                        <a href="Orders.Detail.html" class="text-truncate h-100 d-flex align-items-center">{{$item->invoice ?? '-'}}</a>
                                    </div>
                                    <div
                                        class="col-6 col-md-3 d-flex flex-column justify-content-center mb-2 mb-md-0 order-3 order-md-2">
                                        <div class="text-muted text-small d-md-none">Name</div>
                                        <div class="text-alternate">{{$item->user->firstname ?? '-'}}</div>
                                    </div>
                                    <div
                                        class="col-6 col-md-2 d-flex flex-column justify-content-center mb-2 mb-md-0 order-4 order-md-3">
                                        <div class="text-muted text-small d-md-none">Price</div>
                                        <div class="text-alternate">
                                            <span>
                                                <span class="text-small">Rp</span>
                                                {{number_format($item->total) ?? '-'}}
                                            </span>
                                        </div>
                                    </div>
                                    <div
                                        class="col-6 col-md-2 d-flex flex-column justify-content-center mb-2 mb-md-0 order-5 order-md-4">
                                        <div class="text-muted text-small d-md-none">Date</div>
                                        <div class="text-alternate">{{$item->created_at->format('d M Y') ?? '-'}}</div>
                                    </div>
                                    <div
                                        class="col-6 col-md-2 d-flex flex-column justify-content-center mb-2 mb-md-0 order-last order-md-5">
                                        <div class="text-muted text-small d-md-none">Status</div>
                                        <div class="text-alternate">
                                            {!!OrderStatus::labelHTML($item->status)!!}
                                        </div>
                                    </div>
                                    <div
                                        class="col-6 col-md-1 d-flex flex-column justify-content-center align-items-md-end mb-2 mb-md-0 order-2 text-end order-md-last">
                                            <a href="{{route('member.re-shopping-basic.history.show', $item->id)}}"><i data-acorn-icon="eye"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="card">
                        <div class="card-body text-center">
                                Data kosong
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
    <!-- Order List -->

    <!-- Pagination -->
        {{ $getOrderPinHistory->appends(request()->query())->links('member.layouts.partials.pagination') }}
    <!-- Pagination -->

@endsection