@extends('member.layouts.app')
@section('title' , 'Member - Detail History')

@php
    use App\Constants\OrderStatus;
@endphp

@push('style')
    <link rel="stylesheet" href="{{asset('assets/member/css/vendor/baguetteBox.min.css')}}" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Detail History</h1>
            </div>
        </div>
    </div>

    <div class="row gx-4 gy-5">
        <div class="col-12 col-xl-8 col-xxl-9 mb-n5">
            <!-- Status Start -->
                <h2 class="small-title">Status</h2>
                <div class="mb-5">
                    <div class="row g-2">
                        <div class="col-12 col-sm-6 col-lg-6">
                            <div class="card sh-13 sh-lg-15 sh-xl-14">
                                <div class="h-100 row g-0 card-body align-items-center py-3">
                                    <div class="col-auto pe-3">
                                        <div
                                            class="border border-primary sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center">
                                            <i data-acorn-icon="tag" class="text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="d-flex align-items-center lh-1-25">Order Id</div>
                                        <div class="text-primary">{{$getOrderPinHistory->invoice}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-6">
                            <div class="card sh-13 sh-lg-15 sh-xl-14">
                                <div class="h-100 row g-0 card-body align-items-center py-3">
                                    <div class="col-auto pe-3">
                                        <div
                                            class="border border-primary sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center">
                                            <i data-acorn-icon="clipboard" class="text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="d-flex align-items-center lh-1-25">Order Status</div>
                                        <div class="text-primary">{{OrderStatus::label($getOrderPinHistory->status)}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-12">
                            <div class="card sh-13 sh-lg-15 sh-xl-14">
                                <div class="h-100 row g-0 card-body align-items-center py-3">
                                    <div class="col-auto pe-3">
                                        <div
                                            class="border border-primary sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center">
                                            <i data-acorn-icon="calendar" class="text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="d-flex align-items-center lh-1-25">Order Date</div>
                                        <div class="text-primary">{{$getOrderPinHistory->created_at->format('d M Y') ?? '-'}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Status End -->

            <!-- Cart Start -->
                @if ($getOrderPinHistory->orderPinDetail)
                    <h2 class="small-title">Cart</h2>
                    <div class="card mb-5">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-5">
                                        @foreach ($getOrderPinHistory->orderPinDetail as $orderPinDetail)
                                        <div class="row g-0 sh-9 mb-3">
                                            <div class="col-auto">
                                                <img src="{{ getImage('assets/images/products/pins'.'/'. $orderPinDetail->pin->images,'350x300') }}"
                                                    class="card-img rounded-md h-100 sw-13" alt="thumb" />
                                            </div>
                                            <div class="col">
                                                <div class="ps-4 pt-0 pb-0 pe-0 h-100">
                                                    <div class="row g-0 h-100 align-items-start align-content-center">
                                                        <div class="col-12 d-flex flex-column mb-2">
                                                            <div>{{$orderPinDetail->pin->name ??'-'}}</div>
                                                            <div class="text-muted text-small">
                                                                {{$orderPinDetail->pin->description ??'-'}}</div>
                                                        </div>
                                                        <div class="col-12 d-flex flex-column mb-md-0 pt-1">
                                                            <div class="row g-0">
                                                                <div
                                                                    class="col-6 d-flex flex-row pe-2 align-items-end text-alternate">
                                                                    <span>{{$orderPinDetail->qty ??'-'}}</span>
                                                                    <span class="text-muted ms-1 me-1">x</span>
                                                                    <span>
                                                                        <span class="text-small">Rp</span>
                                                                        {{number_format($orderPinDetail->pin->price) ?? '-'}}
                                                                    </span>
                                                                </div>
                                                                <div
                                                                    class="col-6 d-flex flex-row align-items-end justify-content-end text-alternate">
                                                                    <span>
                                                                        <span class="text-small">Rp</span>
                                                                        {{number_format($orderPinDetail->subtotal*$orderPinDetail->qty) ?? '-'}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div>
                                        <div class="row g-0 mb-2">
                                            <div class="col-auto ms-auto ps-3 text-muted">Total</div>
                                            <div class="col-auto sw-13 text-end">
                                                <span>
                                                    <span class="text-small text-muted">Rp</span>
                                                    {{number_format($getOrderPinHistory->total) ?? '-'}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            <!-- Cart End -->
        </div>

        <!-- Address Start -->
            <div class="col-12 col-xl-4 col-xxl-3">
                <h2 class="small-title">Detail</h2>

                <div class="card mb-5">
                    <div class="card-body mb-n5">
                        <div class="mb-5">
                            <p class="text-small text-muted mb-2">CUSTOMER</p>
                            <div class="row g-0 mb-2">
                                <div class="col-auto">
                                    <div class="sw-3 me-1">
                                        <i data-acorn-icon="user" class="text-primary" data-acorn-size="17"></i>
                                    </div>
                                </div>
                                <div class="col text-alternate align-middle">{{$login_user->firstname}}</div>
                            </div>
                            <div class="row g-0 mb-2">
                                <div class="col-auto">
                                    <div class="sw-3 me-1">
                                        <i data-acorn-icon="email" class="text-primary" data-acorn-size="17"></i>
                                    </div>
                                </div>
                                <div class="col text-alternate">{{$login_user->email}}</div>
                            </div>
                        </div>

                        <div class="mb-5">
                            <div>
                                <p class="text-small text-muted mb-2">PAYMENT</p>
                                <div class="row g-0 mb-2">
                                    <div class="col-auto">
                                        <div class="sw-3 me-1">
                                            <i data-acorn-icon="credit-card" class="text-primary" data-acorn-size="17"></i>
                                        </div>
                                    </div>
                                    <div class="col text-alternate">7991387777 bca PT excel global network</div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-5">
                            <div>
                                <p class="text-small text-muted mb-2">BUKTI TRASFER</p>
                                <div class="row g-0 mt-2">
                                    <div class="col-auto">
                                        @if ($getOrderPinHistory->images_tf)
                                            @if ($getOrderPinHistory->status === OrderStatus::SUCCESS)
                                            <div class="mb-3">
                                                <a href="{{ getImage('assets/images/user/order-recipe'.'/'. $getOrderPinHistory->images_tf,'350x300') }}"
                                                    class="lightbox">
                                                    <img alt="lightbox"
                                                        src="{{ getImage('assets/images/user/order-recipe'.'/'. $getOrderPinHistory->images_tf,'350x300') }}"
                                                        class="card-img" />
                                                </a>
                                            </div>
                                            <div class="alert alert-success" role="alert">Order success</div>
                                            @else
                                            <div class="alert alert-success" role="alert">Anda sudah mengirim bukti transfer. Tunggu sampai <strong>Order Status</strong> menjadi success</div>
                                            @endif
                                        @else
                                            <button type="button" class="btn btn-outline-secondary mb-1"
                                                data-bs-toggle="modal" data-bs-target="#verticallyCenteredExample">
                                                Upload bukti transfer
                                            </button>
                                            <p class="text-small text-muted mb-0 text-danger">
                                                Bukti transfer digunakan untuk konfirmasi atas pemesanan pin yang anda
                                                pesan.
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <!-- Address End -->

        {{-- Modal Upload Bukti Transfer --}}
            <div
            class="modal fade modal-close-out"
            id="verticallyCenteredExample"
            tabindex="-1"
            role="dialog"
            aria-labelledby="verticallyCenteredLabel"
            aria-hidden="true"
            >
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="verticallyCenteredLabel">Upload Bukti Transfer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('member.pin.history.recipe', $getOrderPinHistory->id) }}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                <div class="modal-body">
                        {{-- Foto KTP --}}
                            <div class="col-12">
                                <div class="text-center">
                                    <img class="img-fluid rounded mb-3"
                                        src="{{ getImage('assets/images/user/order-recipe'.'/'. $getOrderPinHistory->images_tf,'350x300') }}"
                                        alt="@lang('Image KTP')">
                                </div>
                                <div class="input-group mb-3">
                                    <input type="file" class="form-control" id="inputGroupFile02" name="images_tf" required/>
                                    <label class="input-group-text" for="inputGroupFile02">@lang('Image KTP')</label>
                                </div>
                                <code>@lang('Image size') {{'350x300'}}</code>
                            </div>
                        {{-- Foto KTP --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        {{-- Modal Upload Bukti Transfer --}}
    </div>
@endsection

@push('script')
    <script src="{{asset('assets/member/js/plugins/lightbox.js')}}"></script>
    <script src="{{asset('assets/member/js/vendor/baguetteBox.min.js')}}"></script>
@endpush