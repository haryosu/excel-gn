@extends('member.layouts.app')
@section('title' , 'Member - Order Pin')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-container">
            <h1 class="mb-0 pb-0 display-4" id="title">Order Reshopping Basic</h1>
        </div>
    </div>
</div>

<div class="row">
    <!-- Items Start -->
        {{-- <div class="col-lg-5 col-lg order-1 order-lg-0">
            <h2 class="small-title">Items</h2>
            @foreach ($products as $item)
                <div class="mb-2">
                    <div class="card mb-2">
                        <div class="row g-0 sh-18 sh-md-14">
                            <div class="col-auto">
                                <img src="{{ getImage('assets/images/products/pins'.'/'. $item->images,'350x300') }}"
                                    class="card-img card-img-horizontal h-100 sw-9 sw-sm-13 sw-md-13" alt="thumb" />
                            </div>
                            
                            <div class="col position-relative h-100">
                                <form action="{{route('member.pin.order.additem', $item->id)}}" method="POST">
                                    @csrf
                                    <div class="card-body">
                                        <div class="row h-100">
                                            <div class="col-12 col-md-6 mb-2 mb-md-0 d-flex align-items-center">
                                                <div class="pt-0 pb-0 pe-2">
                                                    <div class="h6 mb-0 clamp-line" data-line="1">{{$item->name ??'-'}}</div>
                                                    <div class="text-muted text-small">{{$item->description ??'-'}}</div>
                                                    <div class="mb-0 sw-19">Rp. {{ number_format($item->price) ??'-'}}</div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-md-3 pe-0 d-flex align-items-center">
                                                <div class="input-group spinner sw-11" data-trigger="spinner">
                                                    <input type="text" class="form-control text-center px-0" name="itemQuantity"
                                                        data-rule="quantity" required />
                                                </div>
                                            </div>
                                            <div
                                                class="col-6 col-md-3 d-flex justify-content-end justify-content-md-start align-items-center">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i data-acorn-icon="plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        </div> --}}
    <!-- Items End -->

    <!-- Summary Start -->
        <div class="col-lg-7 col-lg-auto order-0 order-lg-1">
            <h2 class="small-title">Summary</h2>
            <div class="card mb-5">
                <div class="card-body">
                    <div>
                        <div class="row mb-4 d-none d-sm-flex">
                            {{-- <div class="col-2">
                                <p class="mb-0 text-small text-muted"></p>
                            </div> --}}
                            <div class="col-4 p-0">
                                <p class="mb-0 text-small text-muted">ITEM NAME</p>
                            </div>
                            <div class="col-4">
                                <p class="mb-0 text-small text-muted">COUNT</p>
                            </div>
                            <div class="col-4 text-end p-0">
                                <p class="mb-0 text-small text-muted">PRICE</p>
                            </div>
                        </div>

                        
                        @php
                        $no=1
                        @endphp
                            <div class="row mb-4 mb-sm-2"> 
                                <div class="col-4 p-0 ">
                                    <h6 class="mb-0">{{Str::words('Basic Reshopping',3)}} </h6>
                                    <h6 class="mb-0">Rp. {{ number_format(250000) ?? ''}}</h6>
                                </div>
                                <div class="col-4">
                                    <p class="mb-0 text-alternate">1</p>
                                </div>
                                <div class="col-4 text-sm-end p-0">
                                    <p class="mb-0 text-alternate">Rp. {{ number_format(250000) ?? ''}}</p>
                                </div>
                            </div> 
                        <div class="separator separator-light mt-5 mb-5"></div>

                        <div class="row mb-5">
                            <div class="col text-sm-end text-muted">
                            <div>Total :</div>
                            </div>
                            <div class="col-auto text-end">
                            <div>Rp. {{ number_format(250000) ?? ''}}</div>
                            </div>
                        </div>
                    </div> 
                    <div class="w-100">
                        <form  action="{{route('member.re-shopping-basic.order.bayar')}}" method="POST">
                            @csrf
                            <button onclick="return confirm('Apakah anda yakin ?');" type="submit" class="btn btn-success w-100  m-1">Order</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- Summary End -->
</div>
@endsection

@push('script')
<script src="{{asset('assets/member/js/vendor/input-spinner.min.js')}}"></script>
<script src="{{asset('assets/member/js/forms/controls.spinner.js')}}"></script>
@endpush