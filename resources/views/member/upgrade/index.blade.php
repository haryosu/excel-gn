@extends('member.layouts.app')
@section('title' , 'Member - Upgrade')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-container">
                <h1 class="mb-0 pb-0 display-4" id="title">Upgrade</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-6 mb-5 order-1 order-lg-0">
            @forelse ($getAllPins_BV as $item)
            <div class="row g-0">
                    @if ($loop->first || $loop->last)
                    <div class="col-auto sw-2 d-flex flex-column justify-content-center align-items-center position-relative me-4">
                    @else
                    <div class="col-auto sw-2 d-flex flex-column justify-content-center align-items-center me-4">
                    @endif

                        @if ($loop->first)
                        <div class="w-100 d-flex sh-6"></div>
                        @else
                        <div class="w-100 d-flex sh-6 justify-content-center position-relative">
                            <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                        </div>
                        @endif

                        @if ($loop->first)
                        <div
                            class="bg-foreground sw-2 sh-2 rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center">
                            <div class=" @if($item['id'] == $userAuth->pin_id) bg-success @else bg-gradient-light @endif sw-1 sh-1 rounded-xl position-relative"></div>
                        </div>
                        @else
                        <div class="bg-foreground sw-2 sh-2 rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center position-relative">
                            <div class=" @if($item['id'] == $userAuth->pin_id) bg-success @else bg-gradient-light @endif sw-1 sh-1 rounded-xl position-relative"></div>
                        </div>
                        @endif

                        @if ($loop->last)
                        <div class="w-100 d-flex h-100"></div>
                        @else
                        <div class="w-100 d-flex h-100 justify-content-center position-relative">
                            <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                        </div>
                        @endif
                    </div>

                <div class="col mb-2">
                    <div class="row">
                        <!-- Buttons Start -->
                        <div class="col">
                            <div class="card mb-2 @if($item['id'] == $userAuth->pin_id) border-success @endif">
                                <div class="row g-0 sh-14 sh-md-10">
                                    
                                    <div class="col-auto position-relative h-100">
                                        @if ($item['id'] == $userAuth->pin_id)
                                        <span class="badge rounded-pill bg-success me-1 position-absolute e-n3 t-2">Level Anda</span>
                                        @endif
                                        <img src="{{ getImage('assets/images/products/pins'.'/'. $item['images'],'350x300') }}" alt="alternate text"
                                            class="card-img card-img-horizontal sw-11" />
                                    </div>
                                    <div class="col">
                                        <div class="card-body pt-0 pb-0 h-100">
                                            <div class="row g-0 h-100 align-content-center">
                                                <div class="col-12 col-md-7 d-flex flex-column mb-2 mb-md-0">
                                                    <div>{{$item['name']}}</div>
                                                    <div class="text-small text-muted text-truncate">{{$item['description']}}</div>
                                                </div>
                                                <div class="col-12 col-md-5 d-flex align-items-center justify-content-md-end">
                                                    @if ($item['id'] != $userAuth->pin_id && $item['id'] >= $userAuth->pin_id)
                                                        <form  action="{{route('member.upgrade.upgrade_pin_user', $item['id'])}}" method="POST">
                                                            @method('PUT')
                                                            @csrf
                                                            <button onclick="return confirm('Apakah anda untuk melakukan update pin?');" type="submit" class="btn btn-sm btn-icon btn-icon-start btn-outline-primary ms-1">
                                                                <i data-acorn-icon="startup" data-acorn-size="15"></i>
                                                                <span class="d-xxl-inline-block">Upgrade to {{$item['name']}}</span>
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Buttons End -->
                    </div>
                </div>

            </div>
            @empty

            @endforelse
        </div>
        <div class="col-12 col-lg-6 mb-5 order-0 order-lg-1">
            <div class="row">
                <div class="card mb-3">
                    <div class="card-body mb-n5">
                      <div class="d-flex align-items-center flex-column">
                        <div class="mb-5 d-flex align-items-center flex-column">
                          <div class="sw-13 position-relative mb-3">
                            <img src="{{ getImage('assets/images/products/pins'.'/'. $userAuth->pin->images,'350x300') }}" class="img-fluid rounded-xl" alt="thumb" />
                          </div>
                          <div class="h5 mb-0"><strong>{{$userAuth->pin->name}}</strong></div>
                          <div class="text-muted">{{$userAuth->username}}</div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            <div class="row">
                <div class="card mb-3">
                    <div class="card-body mb-n5">
                      <div class="row mb-5">
                          <div class=" mb-1 text-center justify-content-center">
                              <h5>Stock Pin</h5>
                          </div>  
      
                          @foreach ($getAllPins_BV as $item)
                              <div class="col-6 col-sm-3 text-center d-flex justify-content-center align-items-center flex-column mt-2 mb-4 mb-sm-0">
                                  <div class="bg-gradient-light sw-6 sh-6 rounded-xl d-flex justify-content-center align-items-center mb-3">
                                      <img src="{{ getImage('assets/images/products/pins'.'/'. $item['images'],'350x300') }}" class="card-img rounded-xl sh-6 sw-6" alt="thumb">
                                  </div>
                                  <div class="text-muted mb-1">Stock {{$item['name']}}</div>
                                  <div class="cta-2 text-primary">
                                      {{$item['user_pin']}}
                                  </div>
                              </div>
                          @endforeach
                      </div>
                    </div>
                  </div>
            </div>
          </div>
    </div>
@endsection