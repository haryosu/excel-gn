<!-- ============Footer Section Starts Here============ -->
@stack('modal')
@php
    $socials = getContent('social_icon.element');
    $footer = getContent('footer_section.content',true);
    $policies = getContent('policy_pages.element',false,null,true);
    $cookie = App\Models\Frontend::where('data_keys','cookie.data')->first();
@endphp

@if(@$cookie->data_values->status && !session('cookie_accepted'))
<section class="cookie-policy">
    <div class="container">
        <div class="cookie-wrapper">
            <div class="cookie-cont">
                <span>
                    @php echo @$cookie->data_values->description @endphp
                </span>
                <a href="{{ @$cookie->data_values->link }}" class="text--base">@lang('Read more about cookies')</a>
            </div>
            <button class="cmn--btn btn--sm cookie-close policy" style="padding: 9px 17px !important;">@lang('Accept Policy')</button>
        </div>
    </div>
</section>
@endif

<!-- Footer Section Starts Here -->
<footer class="footer-section">
    <div class="footer-top"> 
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-wrapper">
                <p class="copy-text">{{ @$footer->data_values->copyright }}</p>
                <ul class="social-icons">
                    @foreach($socials as $social)
                    <li>
                        <a href="{{@$social->data_values->url}}" target="_blank" title="{{@$social->data_values->title}}">
                            @php echo @$social->data_values->social_icon; @endphp
                        </a>
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section Ends Here -->
@push('script')
<script>
    (function($){
        "use strict"
        $('.cookie-close').on('click', function(){
            $('.cookie-policy').slideUp(300)
        })

        $('.policy').on('click',function(){
            $.get('{{route('cookie.accept')}}', function(response){
                $('.cookie-policy').addClass('d-none');
            });
        });

    })(jQuery);
</script>
@endpush
