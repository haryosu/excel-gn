<?php

use App\Http\Controllers\Admin\ManageUsersController;
use App\Http\Controllers\JsonController;
use App\Http\Controllers\Member\Bonus\PlanA\CoupleController;
use App\Http\Controllers\Member\Bonus\PlanA\CoupleRankController;
use App\Http\Controllers\Member\Bonus\PlanA\ExceedController;
use App\Http\Controllers\Member\Bonus\PlanA\GenerationController;
use App\Http\Controllers\Member\Bonus\PlanA\ProfitSharingController;
use App\Http\Controllers\Member\Bonus\PlanA\ReferralController;
use App\Http\Controllers\Member\Bonus\PlanA\ReshoppingBasicController as PlanAReshoppingBasicController;
use App\Http\Controllers\Member\Bonus\PlanB\LeadershipController;
use App\Http\Controllers\Member\Bonus\PlanB\ReshoppingController;
use App\Http\Controllers\Member\Bonus\PlanB\RoyaltyRankController;
use App\Http\Controllers\Member\DashboardController;
use App\Http\Controllers\Member\Network\DataDownlineController;
use App\Http\Controllers\Member\Network\GeneologyController;
use App\Http\Controllers\Member\Network\SponsorController;
use App\Http\Controllers\Member\Pin\PinHistoryController;
use App\Http\Controllers\Member\Pin\PinOrderController;
use App\Http\Controllers\Member\Pin\PinTransferController;
use App\Http\Controllers\Member\Profile\BankInfoController;
use App\Http\Controllers\Member\Profile\ChangePasswordController;
use App\Http\Controllers\Member\Profile\ProfileController;
use App\Http\Controllers\Member\ReshoppingBasic\HistoryController;
use App\Http\Controllers\Member\ReshoppingBasic\ReShoppingBasicController;
use App\Http\Controllers\Member\TopUp\TopUpController;
use App\Http\Controllers\Member\TopUp\TopUpHistoryController;
use App\Http\Controllers\Member\UpgradeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Start User Area
|--------------------------------------------------------------------------
 */

Route::name('member.')->prefix('member')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::post('user/registered-from-refferal/{id}', [ManageUsersController::class,'registeredAsRefferal'])->name('users.registered-from-refferal');

            // Json
                Route::name('json.')->prefix('json')->group(function () {
                    Route::get('user', [JsonController::class, 'getJsonUser'])->name('get_json_user');
                });
            // Json

            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

            // Profile User
                Route::name('profile.')->prefix('profile')->group(function () {
                    Route::get('change-profile', [ProfileController::class, 'edit'])->name('change_profile.edit');
                    Route::put('change-profile', [ProfileController::class, 'update'])->name('change_profile.update');

                    Route::get('change-password', [ChangePasswordController::class, 'edit'])->name('change_password.edit');
                    Route::put('change-password', [ChangePasswordController::class, 'update'])->name('change_password.update');

                    Route::get('bank-info', [BankInfoController::class, 'index'])->name('bank_info.index');
                });
            // Profile User

            // Network
                Route::name('network.')->prefix('network')->group(function () {
                    Route::get('geneology', [GeneologyController::class, 'index'])->name('geneology.index');
                    Route::put('geneology', [GeneologyController::class, 'update'])->name('geneology.update');

                    Route::get('sponsor', [SponsorController::class, 'index'])->name('sponsor.index');
                    Route::put('sponsor', [SponsorController::class, 'update'])->name('sponsor.update');

                    Route::get('data-downline', [DataDownlineController::class, 'index'])->name('data_downline.index');
                    Route::put('data-downline', [DataDownlineController::class, 'update'])->name('data_downline.update');
                });
            // Network

            // Bonus
                Route::name('bonus.')->prefix('bonus')->group(function () {
                    // Plan A
                        Route::name('plan_a.')->prefix('plan-a')->group(function () {
                            Route::get('referral', [ReferralController::class, 'index'])->name('referral.index');
                            Route::put('referral', [ReferralController::class, 'update'])->name('referral.update');
        
                            Route::get('couple', [CoupleController::class, 'index'])->name('couple.index');
                            Route::put('couple', [CoupleController::class, 'update'])->name('couple.update');
        
                            Route::get('exceed', [ExceedController::class, 'index'])->name('exceed.index');
                            Route::put('exceed', [ExceedController::class, 'update'])->name('exceed.update');
                            
                            Route::get('generation', [GenerationController::class, 'index'])->name('generation.index');
                            Route::put('generation', [GenerationController::class, 'update'])->name('generation.update');

                            Route::get('couple-rank', [CoupleRankController::class, 'index'])->name('couple_rank.index');
                            Route::put('couple-rank', [CoupleRankController::class, 'update'])->name('couple_rank.update');
                            
                            Route::get('profit-sharing', [ProfitSharingController::class, 'index'])->name('profit_sharing.index');
                            Route::put('profit-sharing', [ProfitSharingController::class, 'update'])->name('profit_sharing.update');
                            
                            Route::get('re-shopping-basic', [PlanAReshoppingBasicController::class, 'index'])->name('re-shopping-basic.index');
                            // Route::put('re-shopping-basic', [ProfitSharingController::class, 'update'])->name('profit_sharing.update');
                        });  
                    // Plan A

                    // Plan B
                        Route::name('plan_b.')->prefix('plan-b')->group(function () {
                            Route::get('reshopping', [ReshoppingController::class, 'index'])->name('reshopping.index');
                            Route::put('reshopping', [ReshoppingController::class, 'update'])->name('reshopping.update');
        
                            Route::get('leadership', [LeadershipController::class, 'index'])->name('leadership.index');
                            Route::put('leadership', [LeadershipController::class, 'update'])->name('leadership.update');
        
                            Route::get('royalty-rank', [RoyaltyRankController::class, 'index'])->name('royalty_rank.index');
                            Route::put('royalty-rank', [RoyaltyRankController::class, 'update'])->name('royalty_rank.update');
                        });  
                    // Plan B
                });  
            // Bonus

            // Reshopping-Pin
                Route::name('re-shopping-basic.')->prefix('re-shopping-basic')->group(function () {
                    Route::get('order', [ReShoppingBasicController::class, 'index'])->name('order.index');
                    // Route::post('/order/additem/{id}', [ReShoppingBasicController::class, 'additem'])->name('order.additem');
                    // Route::post('/order/removeitem/{id}', [ReShoppingBasicController::class, 'removeitem'])->name('order.removeitem');
                    // Route::post('/order/clear', [ReShoppingBasicController::class, 'clear'])->name('order.clear');
                    Route::post('/order/bayar', [ReShoppingBasicController::class, 'bayar'])->name('order.bayar');

                    // Route::get('transfer', [PinTransferController::class, 'index'])->name('transfer.index');
                    // Route::put('transfer-pin-user', [PinTransferController::class, 'transferPinUser'])->name('transfer.transfer_pin_user');

                    Route::get('history', [HistoryController::class, 'index'])->name('history.index');
                    Route::get('history/show/{id}', [HistoryController::class, 'show'])->name('history.show');

                    Route::put('history/upload-recipe/{id}', [HistoryController::class, 'recipe'])->name('history.recipe');
                });
            // Pin
            // Pin
                Route::name('pin.')->prefix('pin')->group(function () {
                    Route::get('order', [PinOrderController::class, 'index'])->name('order.index');
                    Route::post('/order/additem/{id}', [PinOrderController::class, 'additem'])->name('order.additem');
                    Route::post('/order/removeitem/{id}', [PinOrderController::class, 'removeitem'])->name('order.removeitem');
                    Route::post('/order/clear', [PinOrderController::class, 'clear'])->name('order.clear');
                    Route::post('/order/bayar', [PinOrderController::class, 'bayar'])->name('order.bayar');

                    Route::get('transfer', [PinTransferController::class, 'index'])->name('transfer.index');
                    Route::put('transfer-pin-user', [PinTransferController::class, 'transferPinUser'])->name('transfer.transfer_pin_user');

                    Route::get('history', [PinHistoryController::class, 'index'])->name('history.index');
                    Route::get('history/show/{id}', [PinHistoryController::class, 'show'])->name('history.show');

                    Route::put('history/upload-recipe/{id}', [PinHistoryController::class, 'recipe'])->name('history.recipe');
                });
            // Pin

            // Top Up
                Route::name('top_up.')->prefix('top-up')->group(function () {
                    Route::get('order', [TopUpController::class, 'index'])->name('order.index');
                    Route::post('/order/additem/{id}', [TopUpController::class, 'additem'])->name('order.additem');
                    Route::post('/order/removeitem/{id}', [TopUpController::class, 'removeitem'])->name('order.removeitem');
                    Route::post('/order/clear', [TopUpController::class, 'clear'])->name('order.clear');
                    Route::post('/order/bayar', [TopUpController::class, 'bayar'])->name('order.bayar');

                    Route::get('history', [TopUpHistoryController::class, 'index'])->name('history.index');
                    Route::get('history/show/{id}', [TopUpHistoryController::class, 'show'])->name('history.show');

                    Route::put('history/upload-recipe/{id}', [TopUpHistoryController::class, 'recipe'])->name('history.recipe');
                });
            // Top Up

            // Upgrade
                Route::name('upgrade.')->prefix('upgrade')->group(function () {
                    Route::get('', [UpgradeController::class, 'index'])->name('index');
                    Route::put('upgrade-pin-user/{id}', [UpgradeController::class, 'upgradePinUser'])->name('upgrade_pin_user');
                });
            // Upgrade
    });
});
 